import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Address, UserAddressService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { CountryEnum, CountryUtils } from '@qaroni-app/core/types';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-address-form-dialog',
  templateUrl: './address-form-dialog.component.html',
  styleUrls: ['./address-form-dialog.component.scss'],
})
export class AddressFormDialogComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;
  public InputConfig = InputConfig;
  public CountryUtils = CountryUtils;

  public addressForm: FormGroup;
  private addressSkeleton = {
    alias: [
      '',
      Validators.compose([
        Validators.maxLength(InputConfig.address.alias.maxLength),
      ]),
    ],
    default: [false],
    line1: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    line2: [
      '',
      Validators.compose([
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    postalCode: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.postalCode.maxLength),
      ]),
    ],
    city: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.city.maxLength),
      ]),
    ],
    stateProvince: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.stateProvince.maxLength),
      ]),
    ],
    country: [
      { value: CountryEnum.DEFAULT, disabled: true },
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.country.maxLength),
      ]),
    ],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    public dialogRef: MatDialogRef<AddressFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public address: Address,
    private allApp: AllAppService,
    private fb: FormBuilder,
    private userAddressService: UserAddressService
  ) {
    this.createAddressForm();
  }

  ngOnInit(): void {
    this.subs.add(
      this.userAddressService.getUserAddress$().subscribe(this.getUserAddress)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createAddressForm(): void {
    this.addressForm = this.fb.group(this.addressSkeleton);
    this.populateAddressForm(this.address);
  }

  private populateAddressForm(address: Address): void {
    if (address) {
      this.addressForm.patchValue(address);
      if (address.default === true) {
        this.default.setValidators(Validators.requiredTrue);
        this.default.disable();
      }
    }
  }

  get alias(): AbstractControl {
    return this.addressForm.get('alias');
  }

  get default(): AbstractControl {
    return this.addressForm.get('default');
  }

  get line1(): AbstractControl {
    return this.addressForm.get('line1');
  }

  get line2(): AbstractControl {
    return this.addressForm.get('line2');
  }

  get postalCode(): AbstractControl {
    return this.addressForm.get('postalCode');
  }

  get city(): AbstractControl {
    return this.addressForm.get('city');
  }

  get stateProvince(): AbstractControl {
    return this.addressForm.get('stateProvince');
  }

  get country(): AbstractControl {
    return this.addressForm.get('country');
  }

  get validatedForm(): boolean {
    return this.addressForm.valid && !this.submitting;
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      if (this.address === null) {
        this.userAddressService.createUserAddress(
          this.addressForm.getRawValue()
        );
      } else {
        this.userAddressService.updateUserAddress(
          this.address.addressId,
          this.addressForm.getRawValue()
        );
      }
    }
  }

  private getUserAddress = (address: Address): void => {
    this.disableLoading();
    this.dialogRef.close(address);
  }
}
