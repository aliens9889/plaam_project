import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  OAuth,
  OAuthService,
  OrderCreatedService,
  OtpJson,
  UserData,
  UserDataService,
  UserSnackbars
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ValidateComponent implements OnInit, OnDestroy {
  public initLoaded = false;
  public validated = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private oAuthService: OAuthService,
    private userDataService: UserDataService,
    private orderCreatedService: OrderCreatedService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    if (
      this.route.snapshot.paramMap.has('userID') &&
      this.route.snapshot.paramMap.has('otp')
    ) {
      this.subs.add(this.oAuthService.getOAuth$().subscribe(this.getOAuth));
      const otpJSON: OtpJson = {
        otp: this.route.snapshot.paramMap.get('otp'),
      };
      this.oAuthService.validateUser(
        this.route.snapshot.paramMap.get('userID'),
        otpJSON
      );
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(UserSnackbars.failureValidate.message),
        this.allApp.translate.instant(UserSnackbars.failureValidate.closeBtn),
        UserSnackbars.failureValidate.config
      );
      this.allApp.router.navigate(['/']);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getOAuth = (oAuth: OAuth): void => {
    if (oAuth) {
      this.subs.add(
        this.userDataService.getUserData$().subscribe(this.successGetUserData)
      );
      this.userDataService.getUserData();
    } else {
      this.disableLoading();
    }
  }

  private successGetUserData = (userData: UserData): void => {
    this.allApp.language.setLanguage(userData.language);
    this.orderCreatedService.getOrderCreated();
    this.allApp.router.navigate(['/']);
  }
}
