import { animate, animation, style } from '@angular/animations';

export const qaroniEnterFadeIn = animation([
  style({ opacity: 0 }),
  animate(200, style({ opacity: 1 })),
]);
