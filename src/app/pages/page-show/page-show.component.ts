import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Page, PageService } from '@qaroni-app/core/entities/page';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-page-show',
  templateUrl: './page-show.component.html',
  styleUrls: ['./page-show.component.scss'],
})
export class PageShowComponent implements OnInit, OnDestroy {
  public page$: Observable<Page> = this.pageService
    .getPage$()
    .pipe(shareReplay(1));

  public pageTitle = this.allApp.translate.instant('Pages');

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    public pageService: PageService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.route.params.subscribe(this.getQueryParams));

    this.subs.add(this.page$.subscribe(this.getPage));

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getQueryParams = (queryParams: Params) => {
    if (queryParams.hasOwnProperty('pageID')) {
      this.pagesCalls(queryParams);
    } else {
      this.allApp.router.navigate(['/']);
    }
  }

  private pagesCalls(queryParams: Params): void {
    this.enableLoading();
    this.pageService.getPage(queryParams.pageID);
  }

  private getPage = (page: Page) => {
    if (page) {
      this.pageTitle = page.title;
    }
  }

  private languageChanged = (): void => {
    const params: Params = this.route.snapshot.params;
    this.pagesCalls(params);
  }
}
