export enum NewsAuthorStatusEnum {
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
  INACTIVE = 'INACTIVE',
}

export type NewsAuthorStatusType =
  | NewsAuthorStatusEnum.ACTIVE
  | NewsAuthorStatusEnum.CREATED
  | NewsAuthorStatusEnum.INACTIVE;

export const NewsAuthorStatusArray = [
  NewsAuthorStatusEnum.ACTIVE,
  NewsAuthorStatusEnum.CREATED,
  NewsAuthorStatusEnum.INACTIVE,
];

export const NewsAuthorStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    created: {
      name: 'Creado',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
