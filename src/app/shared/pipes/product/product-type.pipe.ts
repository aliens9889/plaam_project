import { Pipe, PipeTransform } from '@angular/core';
import { ProductTypeEnum, ProductTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'productType',
})
export class ProductTypePipe implements PipeTransform {
  transform(value: ProductTypeEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of ProductTypeInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
