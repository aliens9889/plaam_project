import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleFourComponent } from './footer-style-four.component';

describe('FooterStyleFourComponent', () => {
  let component: FooterStyleFourComponent;
  let fixture: ComponentFixture<FooterStyleFourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleFourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
