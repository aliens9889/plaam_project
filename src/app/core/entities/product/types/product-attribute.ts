export interface ProductAttribute {
  attributeId: number;
  creationDate: string;
  description: string;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  selection: ProductAttributeSelection;
  status: ProductAttributeStatus;
  values: string[];
}

export enum ProductAttributeSelection {
  MULTIPLE = 'MULTIPLE',
  SIMPLE = 'SIMPLE',
}

export enum ProductAttributeStatus {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}
