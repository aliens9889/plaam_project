import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {
  AssociationService,
  Group,
  GroupStatusEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-all-groups',
  templateUrl: './all-groups.component.html',
  styleUrls: ['./all-groups.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class AllGroupsComponent implements OnInit, OnDestroy {
  private hasPartnerID: boolean = this.route.snapshot.paramMap.has('partnerID');
  public partnerID: string = this.route.snapshot.paramMap.get('partnerID');

  public groups$: Observable<
    Group[]
  > = this.associationService.getGroups$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  public initLoaded = false;

  constructor(
    private allApp: AllAppService,
    private associationService: AssociationService,
    private route: ActivatedRoute
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.subs.add(this.groups$.subscribe(this.getGroups));
    this.groupsCalls();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  get title(): string {
    return this.associationService.strings.getTitleComponentString();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private groupsCalls(): void {
    this.enableLoading();
    const queryParams: Params = {
      status: GroupStatusEnum.ACTIVE,
    };
    if (this.hasPartnerID) {
      queryParams.hasPartner = true;
    }
    this.associationService.getGroups(queryParams);
  }

  private getGroups = (groups: Group[]): void => {
    this.disableLoading();
    this.initLoaded = true;
  }

  private languageChanged = (): void => {
    this.groupsCalls();
  }
}
