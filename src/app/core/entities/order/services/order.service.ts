import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { DeliveriesJson } from '../../shipping';
import { OrderSnackbars } from '../snackbars/order-snackbar.config';
import { Item } from '../types/item';
import { Order } from '../types/order';
import { UserOrdersHttpService } from './user-orders-http.service';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private ordersSubject = new Subject<Order[]>();
  private orderSubject = new Subject<Order>();
  private deletedOrderID$ = new Subject<number>();

  constructor(
    private allApp: AllAppService,
    private ordersHttp: UserOrdersHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getItemImageUrl(item: Item): string {
    if (item.imagesURL && item.imagesURL.length && item.imagesURL[0]) {
      return item.imagesURL[0];
    } else {
      return null;
    }
  }

  public hasItemDiscount(item: Item): boolean {
    if (item && item.discount && parseFloat(item.discount) !== 0) {
      return true;
    }
    return false;
  }

  public getUrlItemName(name: string): string {
    if (name) {
      const urlName = name
        .replace(/  +/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase();
      return urlName;
    }
    return '';
  }

  // ==========================================================================================
  // Get Orders
  // ==========================================================================================

  public getOrders$(): Observable<Order[]> {
    return this.ordersSubject.asObservable();
  }

  public getOrders(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .getOrders$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetOrders, this.failureGetOrders);
    }
  }

  private successGetOrders = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const orders: Order[] = data.body.result;
      this.ordersSubject.next(orders);
    } else {
      this.ordersSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetOrders = (error: HttpErrorResponse): void => {
    this.ordersSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Order
  // ==========================================================================================

  public getOrder$(): Observable<Order> {
    return this.orderSubject.asObservable();
  }

  public getOrder(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .getOrder$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetOrder, this.failureGetOrder);
    }
  }

  private successGetOrder = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderSubject.next(order);
    } else {
      this.orderSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetOrder = (error: HttpErrorResponse): void => {
    this.orderSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Delete Order
  // ==========================================================================================

  public getDeletedOrderID$(): Observable<number> {
    return this.deletedOrderID$.asObservable();
  }

  public deleteOrder(order: Order): void {
    this.enableLoading();
    this.ordersHttp
      .deleteOrder$(this.oAuthService.getUserID, order.orderId)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successDeleteOrder, this.failureDeleteOrder);
  }

  private successDeleteOrder = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus204(data)) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.successDeleteOrder.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.successDeleteOrder.closeBtn
        ),
        OrderSnackbars.successDeleteOrder.config
      );
      this.deletedOrderID$.next(parseInt(data.url.split('/').pop(), 10));
    } else {
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureDeleteOrder = (error: HttpErrorResponse): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(OrderSnackbars.failureDeleteOrder.message),
      this.allApp.translate.instant(OrderSnackbars.failureDeleteOrder.closeBtn),
      OrderSnackbars.failureDeleteOrder.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Set Order Deliveries
  // ==========================================================================================
  public updateOrderDeliveries(
    orderID: number | string,
    deliveriesJSON: DeliveriesJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .updateOrderDeliveries$(
          this.oAuthService.getUserID,
          orderID,
          deliveriesJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateOrderDeliveries,
          this.failureUpdateOrderDeliveries
        );
    }
  }

  private successUpdateOrderDeliveries = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderSubject.next(order);
    } else {
      this.orderSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateOrderDeliveries = (error: HttpErrorResponse): void => {
    this.orderSubject.next(null);
    if (
      error.status === 400 &&
      error.error &&
      error.error.errors &&
      error.error.errors.length &&
      error.error.errors[0] &&
      error.error.errors[0].code &&
      error.error.errors[0].code === 'E0015'
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.failureDeliveriesE0015.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.failureDeliveriesE0015.closeBtn
        ),
        OrderSnackbars.failureDeliveriesE0015.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Associate Order To Client
  // ==========================================================================================
  public associateOrderToClient(
    orderID: number | string,
    clientID: number | string
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .associateOrderToClient$(this.oAuthService.getUserID, orderID, clientID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successAssociateOrderToClient,
          this.failureAssociateOrderToClient
        );
    }
  }

  private successAssociateOrderToClient = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderSubject.next(order);
    } else {
      this.orderSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureAssociateOrderToClient = (error: HttpErrorResponse): void => {
    this.orderSubject.next(null);
    if (
      error.status === 400 &&
      error.error &&
      error.error.errors &&
      error.error.errors.length &&
      error.error.errors[0] &&
      error.error.errors[0].code &&
      error.error.errors[0].code === 'E0015'
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.failureDeliveriesE0015.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.failureDeliveriesE0015.closeBtn
        ),
        OrderSnackbars.failureDeliveriesE0015.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }
}
