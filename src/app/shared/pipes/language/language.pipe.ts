import { Pipe, PipeTransform } from '@angular/core';
import { LanguageInfo } from '@qaroni-app/core/types';

@Pipe({
  name: 'language'
})
export class LanguagePipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      value = value.trim().toLowerCase();
      for (const iterator of LanguageInfo) {
        if (Object.keys(iterator)[0] === value) {
          return iterator[value].name;
        }
      }
    }
    return null;
  }
}
