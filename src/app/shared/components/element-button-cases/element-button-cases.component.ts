import { Component, Input, OnInit } from '@angular/core';
import { Element } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-element-button-cases',
  templateUrl: './element-button-cases.component.html',
  styleUrls: ['./element-button-cases.component.scss'],
})
export class ElementButtonCasesComponent implements OnInit {
  @Input() element: Element;

  constructor() {}

  ngOnInit(): void {}
}
