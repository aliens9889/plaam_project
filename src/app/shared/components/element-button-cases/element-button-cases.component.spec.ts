import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementButtonCasesComponent } from './element-button-cases.component';

describe('ElementButtonCasesComponent', () => {
  let component: ElementButtonCasesComponent;
  let fixture: ComponentFixture<ElementButtonCasesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementButtonCasesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementButtonCasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
