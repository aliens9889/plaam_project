import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '192',
};

export const appEnvironment = {
  title: 'Decoranja',
  basePath: 'https://decoranja.com/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  boxes: AppLandingPageBoxesEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  brands: false,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 12,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '5:4',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 1,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  attributes: true,
  search: true,
  prices: true,
  noveltyOutlet: false,
  collections: false,
  brands: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: false,
  myStreamings: false,
  myTickets: false,
  mySubscriptions: false,
  myMultimedia: false,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: 'UA-171114580-1',
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/decoranja/logos/decoranja-logo.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/decoranja/logos/decoranja-logo.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/decoranja/logos/decoranja-logo.png',
};

export const urlsEnvironment = {
  website: 'https://decoranja.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@decoranja.com',
  // facebook: 'https://www.facebook.com/',
  // twitter: 'https://www.twitter.com/',
  // instagram: 'https://www.instagram.com/',
  privacy: 'https://decoranja.com/#/pages/1009/política-de-privacidad/',
  cookies: 'https://decoranja.com/#/pages/1011/política-de-cookies/',
  purchase: 'https://decoranja.com/#/pages/1013/condiciones-de-compra/',
  shippingDelivery: 'https://decoranja.com/#/pages/1010/envío-y-entrega/',
  returnsRefunds:
    'https://decoranja.com/#/pages/1012/política-de-devoluciones-y-reembolso/',
};
