// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '122',
};

export const appEnvironment = {
  title: 'Federación Galega de Surf e Bodyboard',
  basePath: 'https://fgsurf.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  groups: false,
  brands: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.FEDERATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/fgsurf/logos/logo-fgsurf.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/fgsurf/logos/logo-fgsurf.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/fgsurf/logos/logo-fgsurf.png',
};

export const urlsEnvironment = {
  website: 'https://fgsurf.staging.plaam.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@fgsurf.org',
  phone: '+34 986 239 095',
  facebook:
    'https://www.facebook.com/pages/FGSurfcom-Federaci%C3%B3n-Galega/189745261046177',
  twitter: 'https://twitter.com/FedeGalegaSurf',
  instagram: 'https://instagram.com/fgsurf',
  vimeo: 'https://vimeo.com/fgsurf',
  youtube: 'https://www.youtube.com/user/FederacionGalegaSurf?feature=watch',
  privacy: 'https://fgsurf.staging.plaam.com/#/',
  cookies: 'https://fgsurf.staging.plaam.com/#/',
  tsAndCs: 'https://fgsurf.staging.plaam.com/#/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
