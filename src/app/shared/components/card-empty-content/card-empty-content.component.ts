import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-card-empty-content',
  templateUrl: './card-empty-content.component.html',
  styleUrls: ['./card-empty-content.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class CardEmptyContentComponent implements OnInit {
  @Input() loaded = false;

  @Input() faIcon: string;
  @Input() imageSrc: string;

  @Input() title: string;
  @Input() subtitle: string;

  @Input() actionMatIcon: string;
  @Input() actionFaIcon: string;
  @Input() actionText: string;

  @Output() action: EventEmitter<boolean> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  public onAction($event): void {
    this.action.emit(true);
  }
}
