import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerMembersTableComponent } from './partner-members-table.component';

describe('PartnerMembersTableComponent', () => {
  let component: PartnerMembersTableComponent;
  let fixture: ComponentFixture<PartnerMembersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerMembersTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerMembersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
