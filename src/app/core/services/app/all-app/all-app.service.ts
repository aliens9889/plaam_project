import { BreakpointObserver } from '@angular/cdk/layout';
import { Location, ViewportScroller } from '@angular/common';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GoogleReCaptchaEnv } from '@qaroni-app/core/config';
import { BuildHttpData, FilesValidations } from '@qaroni-app/core/utils';
import { ProgressBarService, ToolbarService } from '../../layouts';
import { AppStorageService } from '../app-storage/app-storage.service';
import { LanguageService } from '../language/language.service';
import { CookiesService } from '../cookies/cookies.service';

@Injectable({
  providedIn: 'root',
})
export class AllAppService {
  public FilesValidations = FilesValidations;
  public BuildHttpData = BuildHttpData;

  constructor(
    public progressBar: ProgressBarService,
    public toolbar: ToolbarService,
    public appStorage: AppStorageService,
    public cookies: CookiesService,
    public language: LanguageService,
    public snackbar: MatSnackBar,
    public router: Router,
    public viewportScroller: ViewportScroller,
    public location: Location,
    public dialog: MatDialog,
    public translate: TranslateService,
    public breakpointObserver: BreakpointObserver
  ) {}

  get hasReCaptcha(): boolean {
    if (GoogleReCaptchaEnv.sitekey) {
      return true;
    }
    return false;
  }
}
