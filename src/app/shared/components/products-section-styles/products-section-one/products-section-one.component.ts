import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductsEnv } from '@qaroni-app/core/config';
import { Product, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-products-section-one',
  templateUrl: './products-section-one.component.html',
  styleUrls: ['./products-section-one.component.scss'],
})
export class ProductsSectionOneComponent implements OnInit, OnDestroy {
  public products$: Observable<
    Product[]
  > = this.productService.getProducts$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private productService: ProductService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );

    this.productsCalls();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private productsCalls(): void {
    this.productService.getProducts({
      last: ProductsEnv.featuredCount,
      novelty: 1,
    });
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.productsCalls();
  }
}
