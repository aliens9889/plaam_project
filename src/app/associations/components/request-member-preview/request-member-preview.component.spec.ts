import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestMemberPreviewComponent } from './request-member-preview.component';

describe('RequestMemberPreviewComponent', () => {
  let component: RequestMemberPreviewComponent;
  let fixture: ComponentFixture<RequestMemberPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestMemberPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestMemberPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
