import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerCompanyInscriptionsListComponent } from './partner-company-inscriptions-list.component';

describe('PartnerCompanyInscriptionsListComponent', () => {
  let component: PartnerCompanyInscriptionsListComponent;
  let fixture: ComponentFixture<PartnerCompanyInscriptionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerCompanyInscriptionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerCompanyInscriptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
