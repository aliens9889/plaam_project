import { Location } from '@qaroni-app/core/types';
import { Address } from '../../address';
import { Product } from '../../product';
import { EventStateEnum } from './event-state.enum';
import { EventTypeEnum } from './event-type.enum';
import { Zone } from './zone';

export interface Event {
  eventId: number;
  templateUUID: string;
  merchantId: number;
  categoryId: number;
  novelty: number;
  code: number;
  state: EventStateEnum;
  startDate: string;
  finishDate: string;
  language: string;
  imagesURL: string[];
  title: string;
  shortDescription: string;
  largeDescription: string;
  type: EventTypeEnum;
  place: string;
  capacity: number;
  isPaid: number;
  creationDate: string;
  lastUpdateDate: string;
  address: Address;
  location: Location;
  zones: Zone[];
  products: Product[];
}
