import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderPaymentService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-payment-paypal',
  templateUrl: './payment-paypal.component.html',
  styleUrls: ['./payment-paypal.component.scss'],
})
export class PaymentPaypalComponent implements OnInit, OnDestroy {
  @Output() loading: EventEmitter<null> = new EventEmitter();

  private subs: Subscription = new Subscription();

  public submitting = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderPaymentService: OrderPaymentService
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(
        this.orderPaymentService
          .getRedirectUrl$()
          .subscribe(this.getRedirectUrl)
      );
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private getRedirectUrl(redirectUrl: string): void {
    if (redirectUrl) {
      window.location.href = redirectUrl;
    }
  }

  get validatedPaymentButton(): boolean {
    return !this.submitting;
  }

  public onPaymentPaypal(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.submitting = true;
      this.orderPaymentService.paymentPayPal(
        this.route.snapshot.paramMap.get('orderID')
      );
      this.loading.emit();
    }
  }
}
