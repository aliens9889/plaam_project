import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsPreviewOneComponent } from './events-preview-one.component';

describe('EventsPreviewOneComponent', () => {
  let component: EventsPreviewOneComponent;
  let fixture: ComponentFixture<EventsPreviewOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsPreviewOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsPreviewOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
