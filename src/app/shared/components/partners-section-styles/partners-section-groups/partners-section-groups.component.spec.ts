import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersSectionGroupsComponent } from './partners-section-groups.component';

describe('PartnersSectionGroupsComponent', () => {
  let component: PartnersSectionGroupsComponent;
  let fixture: ComponentFixture<PartnersSectionGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersSectionGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersSectionGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
