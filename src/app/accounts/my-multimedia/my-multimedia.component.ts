import { Component, OnDestroy, OnInit } from '@angular/core';
import { Multimedia, MultimediasService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-multimedia',
  templateUrl: './my-multimedia.component.html',
  styleUrls: ['./my-multimedia.component.scss'],
})
export class MyMultimediaComponent implements OnInit, OnDestroy {
  public multimedias: Multimedia[] = [];

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private multimediasService: MultimediasService
  ) {
    this.enableLoading();
    this.allApp.toolbar.showBackButton();
  }

  ngOnInit(): void {
    this.subs.add(
      this.multimediasService.getMultimedias$().subscribe(this.getMultimedias)
    );
    this.multimediasService.getMultimedias();

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getMultimedias = (multimedias: Multimedia[]): void => {
    this.multimedias = multimedias;
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.multimediasService.getMultimedias();
  }
}
