export interface ConfirmationDialogData {
  title: string;
  message: string;

  cancelIcon?: string;
  cancelText?: string;

  confirmFaIcon: string;
  confirmText: string;
}
