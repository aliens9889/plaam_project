import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { AssociationsRoutingModule } from './associations-routing.module';
import { CompanyAddressFormComponent } from './components/company-address-form/company-address-form.component';
import { CompanyDataFormComponent } from './components/company-data-form/company-data-form.component';
import { DynamicFormComponent } from './components/dynamic-form/dynamic-form.component';
import { GroupCategoryPreviewComponent } from './components/group-category-preview/group-category-preview.component';
import { GroupPreviewComponent } from './components/group-preview/group-preview.component';
import { RequestCompanyPreviewComponent } from './components/request-company-preview/request-company-preview.component';
import { RequestMemberPreviewComponent } from './components/request-member-preview/request-member-preview.component';
import { UploadViewFileComponent } from './components/upload-view-file/upload-view-file.component';
import { UserAddressFormComponent } from './components/user-address-form/user-address-form.component';
import { UserDataFormComponent } from './components/user-data-form/user-data-form.component';
import { UserPartnerFormComponent } from './components/user-partner-form/user-partner-form.component';
import { EnrollmentGroupCompanyComponent } from './enrollments/enrollment-group-company/enrollment-group-company.component';
import { EnrollmentGroupMemberComponent } from './enrollments/enrollment-group-member/enrollment-group-member.component';
import { AllGroupsComponent } from './groups/all-groups/all-groups.component';
import { GroupsCategoriesComponent } from './groups/groups-categories/groups-categories.component';
import { GroupsCategoryComponent } from './groups/groups-category/groups-category.component';
import { CompleteRequestCompanyComponent } from './my-enrollments-requests/complete-request-company/complete-request-company.component';
import { CompleteRequestMemberComponent } from './my-enrollments-requests/complete-request-member/complete-request-member.component';
import { EditRequestCompanyComponent } from './my-enrollments-requests/edit-request-company/edit-request-company.component';
import { EditRequestMemberComponent } from './my-enrollments-requests/edit-request-member/edit-request-member.component';
import { MyEnrollmentRequestsComponent } from './my-enrollments-requests/my-enrollment-requests/my-enrollment-requests.component';
import { MyRequestsCompaniesComponent } from './my-enrollments-requests/my-requests-companies/my-requests-companies.component';
import { MyRequestsMembersComponent } from './my-enrollments-requests/my-requests-members/my-requests-members.component';
import { MyLicensesComponent } from './my-licenses/my-licenses/my-licenses.component';
import { AllPartnersComponent } from './partners/all-partners/all-partners.component';
import { PartnerPreviewComponent } from './partners/components/partner-preview/partner-preview.component';
import { PartnerDetailsComponent } from './partners/partner-details/partner-details.component';

@NgModule({
  declarations: [
    AllGroupsComponent,
    AllPartnersComponent,
    CompanyAddressFormComponent,
    CompanyDataFormComponent,
    CompleteRequestCompanyComponent,
    CompleteRequestMemberComponent,
    DynamicFormComponent,
    EditRequestCompanyComponent,
    EditRequestMemberComponent,
    EnrollmentGroupCompanyComponent,
    EnrollmentGroupMemberComponent,
    GroupCategoryPreviewComponent,
    GroupPreviewComponent,
    GroupsCategoriesComponent,
    GroupsCategoryComponent,
    MyEnrollmentRequestsComponent,
    MyLicensesComponent,
    MyRequestsCompaniesComponent,
    MyRequestsMembersComponent,
    PartnerDetailsComponent,
    PartnerPreviewComponent,
    RequestCompanyPreviewComponent,
    RequestMemberPreviewComponent,
    UploadViewFileComponent,
    UserAddressFormComponent,
    UserDataFormComponent,
    UserPartnerFormComponent,
  ],
  imports: [SharedModule, AssociationsRoutingModule, TranslateModule],
})
export class AssociationsModule {}
