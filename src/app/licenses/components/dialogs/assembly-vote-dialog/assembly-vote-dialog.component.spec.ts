import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssemblyVoteDialogComponent } from './assembly-vote-dialog.component';

describe('AssemblyVoteDialogComponent', () => {
  let component: AssemblyVoteDialogComponent;
  let fixture: ComponentFixture<AssemblyVoteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssemblyVoteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssemblyVoteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
