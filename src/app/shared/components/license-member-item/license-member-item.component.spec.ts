import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseMemberItemComponent } from './license-member-item.component';

describe('LicenseMemberItemComponent', () => {
  let component: LicenseMemberItemComponent;
  let fixture: ComponentFixture<LicenseMemberItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseMemberItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseMemberItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
