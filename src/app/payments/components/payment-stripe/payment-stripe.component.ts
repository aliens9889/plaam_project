import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StripeEnv } from '@qaroni-app/core/config';
import { OrderPaymentService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

declare const Stripe: any;

@Component({
  selector: 'qaroni-payment-stripe',
  templateUrl: './payment-stripe.component.html',
  styleUrls: ['./payment-stripe.component.scss'],
})
export class PaymentStripeComponent implements OnInit, OnDestroy {
  @Output() loading: EventEmitter<null> = new EventEmitter();

  private subs: Subscription = new Subscription();

  public stripeCheckoutId$: Observable<
    string
  > = this.orderPaymentService.getStripeCheckoutId$().pipe(shareReplay(1));

  public submitting = false;

  private tagScriptId = 'stripe-js-sdk';

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderPaymentService: OrderPaymentService
  ) {}

  ngOnInit(): void {
    if (this.hasPaymentStripe && this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(
        this.stripeCheckoutId$.subscribe(this.getPaymentStripeCheckoutId)
      );
      this.addScript();
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private getPaymentStripeCheckoutId(checkoutId: string): void {
    if (checkoutId) {
      const stripe = Stripe(StripeEnv.publicKey);
      stripe
        .redirectToCheckout({
          sessionId: checkoutId,
        })
        .then((result) => {});
    }
  }

  public onPaymentStripe(): void {
    this.submitting = true;
    this.orderPaymentService.getPaymentStripeCheckoutId(
      this.route.snapshot.paramMap.get('orderID')
    );
    this.loading.emit();
  }

  private addScript(): void {
    if (!this.hasPaymentStripe || document.getElementById(this.tagScriptId)) {
      return;
    }
    const script = document.createElement('script');
    script.id = this.tagScriptId;
    script.src = StripeEnv.scriptSrc;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  }

  get validatedPaymentButton(): boolean {
    return !this.submitting;
  }

  get hasPaymentStripe(): boolean {
    if (StripeEnv.publicKey) {
      return true;
    }
    return false;
  }
}
