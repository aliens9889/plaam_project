import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerCompaniesListComponent } from './partner-companies-list.component';

describe('PartnerCompaniesListComponent', () => {
  let component: PartnerCompaniesListComponent;
  let fixture: ComponentFixture<PartnerCompaniesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerCompaniesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerCompaniesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
