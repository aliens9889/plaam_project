import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Links } from '../../app';
import { News } from '../types/news';
import { NewsHttpService } from './news-http.service';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  private news$ = new Subject<News[]>();
  private newsItem$ = new Subject<News>();

  private links$ = new Subject<Links>();

  constructor(
    private allApp: AllAppService,
    private newsHttp: NewsHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getNextPage(links: Links): number {
    if (links && links.next) {
      const urlArr: string[] = links.next.split('?');
      if (urlArr.length === 2) {
        const page = JSON.parse(
          '{"' +
            decodeURI(urlArr[1].replace(/&/g, '","').replace(/=/g, '":"')) +
            '"}'
        );
        if (page.hasOwnProperty('page')) {
          return parseInt(page.page, 10);
        }
      }
    }

    return null;
  }

  // ==========================================================================================
  // Get News
  // ==========================================================================================

  public getNews$(): Observable<News[]> {
    return this.news$.asObservable();
  }

  public getNews(queryParams?: Params): void {
    this.enableLoading();
    this.newsHttp
      .getNews$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetNews, this.failureGetNews);
  }

  private successGetNews = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const news: News[] = data.body.result;
      const links: Links = data.body.links;
      this.news$.next(news);
      this.links$.next(links);
    } else {
      this.news$.next(null);
      this.links$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetNews = (error: HttpErrorResponse): void => {
    this.news$.next(null);
    this.links$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get News Item
  // ==========================================================================================

  public getNewsItem$(): Observable<News> {
    return this.newsItem$.asObservable();
  }

  public getNewsItem(newsID: number | string): void {
    this.enableLoading();
    this.newsHttp
      .getNewsItem$(newsID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetNewsItem, this.failureGetNewsItem);
  }

  private successGetNewsItem = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const newsItem: News = data.body.result[0];
      this.newsItem$.next(newsItem);
    } else {
      this.newsItem$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetNewsItem = (error: HttpErrorResponse): void => {
    this.newsItem$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get News Links
  // ==========================================================================================

  public getLinks$(): Observable<Links> {
    return this.links$.asObservable();
  }
}
