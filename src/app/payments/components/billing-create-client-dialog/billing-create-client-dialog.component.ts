import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  Client,
  ClientService,
  ClientTypeArray,
  ClientTypeEnum,
  UserDataUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { CountryEnum, CountryUtils } from '@qaroni-app/core/types';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

function provinceValidator(provinces: string[]): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (control.value !== undefined && !provinces.includes(control.value)) {
      return { provinceList: true };
    }
    return null;
  };
}

@Component({
  selector: 'qaroni-billing-create-client-dialog',
  templateUrl: './billing-create-client-dialog.component.html',
  styleUrls: ['./billing-create-client-dialog.component.scss'],
})
export class BillingCreateClientDialogComponent implements OnInit, OnDestroy {
  public provinces: string[] = [];
  public filteredProvinces: Observable<string[]>;

  public clientForm: FormGroup;
  private clientSkeleton = {
    type: [ClientTypeEnum.PHYSICAL, Validators.compose([Validators.required])],
    firstName: ['', Validators.compose([Validators.required])],
    lastName: ['', Validators.compose([Validators.required])],
    tradename: ['', Validators.compose([Validators.required])],
    document: ['', Validators.compose([Validators.required])],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    phone: [''],
    birthday: [''],
    gender: [''],
    line1: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    postalCode: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.postalCode.maxLength),
      ]),
    ],
    city: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.city.maxLength),
      ]),
    ],
    stateProvince: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.stateProvince.maxLength),
        provinceValidator(this.provinces),
      ]),
    ],
    country: [
      { value: CountryEnum.DEFAULT, disabled: true },
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.country.maxLength),
      ]),
    ],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    public dialogRef: MatDialogRef<BillingCreateClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public client: Client,
    private allApp: AllAppService,
    private fb: FormBuilder,
    private clientService: ClientService
  ) {
    this.createClientForm();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.clientService.getClients$().subscribe(this.getClients));
    this.subs.add(
      this.clientService.getProvinces$().subscribe(this.getProvinces)
    );
    this.clientService.getProvinces();

    this.filteredProvinces = this.stateProvince.valueChanges.pipe(
      startWith(''),
      map((value) => this.filterProvinces(value))
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createClientForm(): void {
    this.clientForm = this.fb.group(this.clientSkeleton);
    this.populateClientForm(this.client);
    this.onChangeClientType();
  }

  private populateClientForm(client: Client): void {
    if (this.client) {
      const clientForPatch = {
        type: client?.type,
        firstName: client?.firstName,
        lastName: client?.lastName,
        document: client?.document,
        email: client?.email,
        phone: client?.phone,
        birthday: client?.birthday,
        gender: client?.gender,
        tradename: client?.tradename,
        line1: client?.address?.line1,
        postalCode: client?.address?.postalCode,
        city: client?.address?.city,
        stateProvince: client?.address?.stateProvince,
      };
      this.clientForm.patchValue(clientForPatch);
      this.type.disable();
      this.firstName.disable();
      this.lastName.disable();
      this.document.disable();
      if (client?.birthday && client?.birthday !== '0000-00-00') {
        this.birthday.setValue(moment(client?.birthday));
      } else {
        this.birthday.setValue('');
      }
    }
  }

  get UserDataUtils() {
    return UserDataUtils;
  }

  get InputValidation() {
    return InputValidation;
  }

  get InputConfig() {
    return InputConfig;
  }

  get CountryUtils() {
    return CountryUtils;
  }

  get ClientTypeArray() {
    return ClientTypeArray;
  }

  get type(): AbstractControl {
    return this.clientForm.get('type');
  }

  get firstName(): AbstractControl {
    return this.clientForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.clientForm.get('lastName');
  }

  get tradename(): AbstractControl {
    return this.clientForm.get('tradename');
  }

  get document(): AbstractControl {
    return this.clientForm.get('document');
  }

  get email(): AbstractControl {
    return this.clientForm.get('email');
  }

  get phone(): AbstractControl {
    return this.clientForm.get('phone');
  }

  get birthday(): AbstractControl {
    return this.clientForm.get('birthday');
  }

  get gender(): AbstractControl {
    return this.clientForm.get('gender');
  }

  get line1(): AbstractControl {
    return this.clientForm.get('line1');
  }

  get postalCode(): AbstractControl {
    return this.clientForm.get('postalCode');
  }

  get city(): AbstractControl {
    return this.clientForm.get('city');
  }

  get stateProvince(): AbstractControl {
    return this.clientForm.get('stateProvince');
  }

  get country(): AbstractControl {
    return this.clientForm.get('country');
  }

  get validatedForm(): boolean {
    return this.clientForm.valid && !this.submitting;
  }

  private prepateFormToSend(): void {
    if (this.birthday.value && this.birthday.value.isValid()) {
      this.birthday.setValue(this.birthday.value.format('YYYY-MM-DD'));
    } else {
      this.birthday.setValue('');
    }
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.prepateFormToSend();
      if (this.client?.clientId) {
        this.clientService.editClient(
          this.client?.clientId,
          this.clientForm.getRawValue()
        );
      } else {
        this.clientService.createClient(this.clientForm.getRawValue());
      }
    }
  }

  get typeIsLegal(): boolean {
    if (this.type.value === ClientTypeEnum.LEGAL) {
      return true;
    }
    return false;
  }

  get typeIsPhysical(): boolean {
    if (this.type.value === ClientTypeEnum.PHYSICAL) {
      return true;
    }
    return false;
  }

  public onChangeClientType(): void {
    if (this.typeIsLegal) {
      this.lastName.clearValidators();
      this.tradename.setValidators(Validators.required);
      this.lastName.setValue('');
      this.birthday.setValue('');
      this.gender.setValue('');
    } else if (this.typeIsPhysical) {
      this.lastName.setValidators(Validators.required);
      this.tradename.clearValidators();
      this.tradename.setValue('');
    }
    this.lastName.updateValueAndValidity();
    this.tradename.updateValueAndValidity();
  }

  get getCol(): string {
    if (this.typeIsLegal) {
      return 'col-10 col-sm-6';
    } else if (this.typeIsPhysical) {
      return 'col-10 col-sm-6 col-md-4';
    }
  }

  get getColEmail(): string {
    if (this.typeIsLegal) {
      return 'col-10 col-sm-6';
    } else if (this.typeIsPhysical) {
      return 'col-10 col-sm-6 col-md-8';
    }
  }

  get firstNameLabel(): string {
    if (this.typeIsLegal) {
      return 'Fiscal name';
    } else if (this.typeIsPhysical) {
      return 'firstname';
    }
  }

  get submitMatIcon(): string {
    if (this.client) {
      return 'edit';
    }
    return 'save';
  }

  get submitText(): string {
    if (this.client) {
      return 'Update';
    }
    return 'Save';
  }

  private getClients = (clients: Client[]): void => {
    this.disableLoading();
    this.dialogRef.close(clients);
  }

  private getProvinces = (provinces): void => {
    for (const province of provinces) {
      if (province?.estado) {
        this.provinces.push(province?.estado);
      }
    }
    this.disableLoading();
    this.stateProvince.updateValueAndValidity();
  }

  private filterProvinces(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.provinces.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }
}
