import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Item, Order, OrderUpdateItem } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';
import { DeliveriesJson } from '../../shipping';

@Injectable({
  providedIn: 'root',
})
export class UserOrdersHttpService {
  constructor(private http: HttpClient) {}

  public getOrders$(
    userID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders`;

    const params: Params = Object.assign({}, queryParams);

    return this.http.get(url, { observe: 'response', params });
  }

  public createOrder$(
    userID: number | string,
    orderJSON: Order
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/addresses`;

    return this.http.post(url, orderJSON, { observe: 'response' });
  }

  public getOrder$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public updateOrder$(
    userID: number | string,
    orderID: number | string,
    orderJSON: Order
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}`;

    return this.http.patch(url, orderJSON, { observe: 'response' });
  }

  public deleteOrder$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}`;

    return this.http.delete(url, { observe: 'response' });
  }

  public checkoutOrder$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts`;

    return this.http.patch(url, null, { observe: 'response' });
  }

  public uncheckoutOrder$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/uncheckouts`;

    return this.http.patch(url, null, { observe: 'response' });
  }

  public createOrderItem$(
    userID: number | string,
    orderID: number | string,
    itemJSON: Order
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/items`;

    return this.http.post(url, itemJSON, { observe: 'response' });
  }

  public updateOrderItem$(
    userID: number | string,
    orderID: number | string,
    itemId: number | string,
    itemJSON: Item
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/items/${itemId}`;

    return this.http.patch(url, itemJSON, { observe: 'response' });
  }

  public deleteOrderItem$(
    userID: number | string,
    orderID: number | string,
    itemID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/items/${itemID}`;

    return this.http.delete(url, { observe: 'response' });
  }

  public emptyOrderItems$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/items`;

    return this.http.delete(url, { observe: 'response' });
  }

  public addOrderItemFromProductVariant$(
    userID: number | string,
    productID: number | string,
    variantID: number | string,
    dataJSON?: OrderUpdateItem
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/products/${productID}/variants/${variantID}`;

    return this.http.post(url, dataJSON, { observe: 'response' });
  }

  public updateOrderDeliveries$(
    userID: number | string,
    orderID: number | string,
    deliveriesJSON: DeliveriesJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/deliveries`;

    return this.http.patch(url, deliveriesJSON, { observe: 'response' });
  }

  public associateOrderToClient$(
    userID: number | string,
    orderID: number | string,
    clientID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/clients/${clientID}`;

    return this.http.patch(url, null, { observe: 'response' });
  }
}
