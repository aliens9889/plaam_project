import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  Invoice,
  Order,
  OrderStatusEnum,
  OrderUtils
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-order-resume-information',
  templateUrl: './order-resume-information.component.html',
  styleUrls: ['./order-resume-information.component.scss'],
})
export class OrderResumeInformationComponent implements OnInit {
  public OrderUtils = OrderUtils;

  @Input() order: Order;

  @Input() showInvoiceLink = true;

  @Output() invoice: EventEmitter<Invoice> = new EventEmitter();

  @Output() ticket: EventEmitter<Order> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  public onGetInvoice(): void {
    if (this.showInvoiceLink && this.hasInvoice) {
      this.invoice.emit(this.order.invoices[0]);
    }
  }

  public onGetTicket(): void {
    if (this.showInvoiceLink) {
      this.ticket.emit(this.order);
    }
  }

  get isOrderStatusAlert(): boolean {
    if (this.order && this.order.status === OrderStatusEnum.ALERT) {
      return true;
    } else {
      return false;
    }
  }

  get isOrderStatusPaid(): boolean {
    if (this.order?.status === OrderStatusEnum.PAID) {
      return true;
    } else {
      return false;
    }
  }

  get hasInvoice(): boolean {
    if (this.order && this.order.invoices && this.order.invoices.length) {
      return true;
    } else {
      return false;
    }
  }
}
