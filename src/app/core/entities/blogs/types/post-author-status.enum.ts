export enum PostAuthorStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export type PostAuthorStatusType =
  | PostAuthorStatusEnum.ACTIVE
  | PostAuthorStatusEnum.INACTIVE;

export const PostAuthorStatusArray = [
  PostAuthorStatusEnum.ACTIVE,
  PostAuthorStatusEnum.INACTIVE,
];

export const PostAuthorStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
