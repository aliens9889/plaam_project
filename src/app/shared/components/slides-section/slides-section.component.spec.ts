import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlidesSectionComponent } from './slides-section.component';

describe('SlidesSectionComponent', () => {
  let component: SlidesSectionComponent;
  let fixture: ComponentFixture<SlidesSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlidesSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlidesSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
