export enum ProductUnitTypeEnum {
  METRIC = 'METRIC',
  UNIT = 'UNIT',
}

export type ProductUnitType = 'METRIC' | 'UNIT';

export const ProductUnitTypeArray = ['METRIC', 'UNIT'];

export const ProductUnitTypeInfo = [
  {
    metric: {
      name: 'Por medida',
    },
  },
  {
    unit: {
      name: 'Por unidad',
    },
  },
];
