import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterCollectionsComponent } from './products-filter-collections.component';

describe('ProductsFilterCollectionsComponent', () => {
  let component: ProductsFilterCollectionsComponent;
  let fixture: ComponentFixture<ProductsFilterCollectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterCollectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterCollectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
