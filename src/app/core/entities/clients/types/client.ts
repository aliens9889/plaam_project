import { Address } from '../../address';
import { ClientTypeEnum } from './client-type.enum';

export interface Client {
  address: Address;
  bankInfo: BankInfo[];
  birthday: string;
  clientId: number;
  creationDate: string;
  document: string;
  email: string;
  firstName: string;
  gender: string;
  lastName: string;
  lastUpdateDate: string;
  merchantId: number;
  phone: string;
  status: string;
  tradename: string;
  type: ClientTypeEnum;
}

export interface BankInfo {
  entity: string;
  bic: string;
  iban: string;
}
