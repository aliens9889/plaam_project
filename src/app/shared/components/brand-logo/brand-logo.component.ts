import { Component, Input, OnInit } from '@angular/core';
import { ProductBrand } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-brand-logo',
  templateUrl: './brand-logo.component.html',
  styleUrls: ['./brand-logo.component.scss'],
})
export class BrandLogoComponent implements OnInit {
  @Input() brand: ProductBrand;

  constructor() {}

  ngOnInit(): void {}

  get hasLogo(): boolean {
    if (this.brand.logo && this.brand.logo !== '') {
      return true;
    }
    return false;
  }
}
