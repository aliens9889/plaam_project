import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '187',
};

export const appEnvironment = {
  title: 'Chuches Joali',
  basePath: 'https://joali.es/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: false,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 24,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '3:4',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plataforma.plaam.com/subido/empresas/187_20200511233129_Screenshot20200511at23.29.01.png',
  toolbar:
    'https://plataforma.plaam.com/subido/empresas/187_20200511233129_Screenshot20200511at23.29.01.png',
  footer:
    'https://plataforma.plaam.com/subido/empresas/187_20200511233129_Screenshot20200511at23.29.01.png',
};

export const urlsEnvironment = {
  website: 'https://joali.es/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@joali.es',
  facebook: 'https://www.facebook.com/Chuches-Joali-100691854910227',
  instagram: 'https://www.instagram.com/chuchesjoali/',
  privacy: 'https://joali.es/#/pages/1124/política-de-privacidad/',
  cookies: 'https://joali.es/#/pages/1125/política-de-cookies/',
  purchase: 'https://joali.es/#/pages/1123/condiciones-de-compra/',
};
