import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Scroll } from '@angular/router';
import { GoogleAnalyticsEnv } from '@qaroni-app/core/config';
import { AppService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';

declare let gtag: Function;

@Component({
  selector: 'qaroni-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'plaam-store';

  constructor(private allApp: AllAppService, private appService: AppService) {
    this.allApp.router.events.subscribe(this.catchRouterEvent);
  }

  ngOnInit(): void {
    this.appService.getNavbars();
  }

  private catchRouterEvent = (event): void => {
    if (event instanceof Scroll) {
      if (!event.routerEvent.urlAfterRedirects.includes('/products?')) {
        this.allApp.viewportScroller.scrollToPosition([0, 0]);
      }
    }
    if (event instanceof NavigationEnd) {
      if (GoogleAnalyticsEnv.TrackingID) {
        gtag('config', GoogleAnalyticsEnv.TrackingID, {
          page_path: event.urlAfterRedirects,
        });
      }
    }
  }
}
