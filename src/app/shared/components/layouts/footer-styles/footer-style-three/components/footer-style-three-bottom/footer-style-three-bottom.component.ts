import { Component, OnInit } from '@angular/core';
import { AppEnv, UrlsInfo } from '@qaroni-app/core/config';
import * as moment from 'moment';
import { urlsEnvironment } from 'src/environments/environment';

@Component({
  selector: 'qaroni-footer-style-three-bottom',
  templateUrl: './footer-style-three-bottom.component.html',
  styleUrls: ['./footer-style-three-bottom.component.scss'],
})
export class FooterStyleThreeBottomComponent implements OnInit {
  public year = moment().format('Y');

  constructor() {}

  ngOnInit(): void {}

  get UrlsInfo() {
    return UrlsInfo;
  }

  get AppEnv() {
    return AppEnv;
  }

  get keysUrlsEnvironment() {
    return Object.keys(urlsEnvironment);
  }
}
