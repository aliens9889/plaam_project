import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PathNotFoundComponent } from '@qaroni-app/views';
import { AllowShoppingCartGuard, UserGuard } from './core/guards';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@qaroni-app/landing/landing.module').then((m) => m.LandingModule),
  },
  {
    path: 'products',
    loadChildren: () =>
      import('@qaroni-app/products/products.module').then(
        (m) => m.ProductsModule
      ),
  },
  {
    path: 'pages',
    loadChildren: () =>
      import('@qaroni-app/pages/pages.module').then((m) => m.PagesModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('@qaroni-app/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'shopping-cart',
    loadChildren: () =>
      import('@qaroni-app/shopping-cart/shopping-cart.module').then(
        (m) => m.ShoppingCartModule
      ),
    canLoad: [UserGuard, AllowShoppingCartGuard],
  },
  {
    path: 'orders',
    loadChildren: () =>
      import('@qaroni-app/orders/orders.module').then((m) => m.OrdersModule),
    canLoad: [UserGuard],
  },
  {
    path: 'invoices',
    loadChildren: () =>
      import('@qaroni-app/invoices/invoices.module').then(
        (m) => m.InvoicesModule
      ),
    canLoad: [UserGuard],
  },
  {
    path: 'payments',
    loadChildren: () =>
      import('@qaroni-app/payments/payments.module').then(
        (m) => m.PaymentsModule
      ),
    canLoad: [UserGuard],
  },
  {
    path: 'my-account',
    loadChildren: () =>
      import('@qaroni-app/accounts/accounts.module').then(
        (m) => m.AccountsModule
      ),
    canLoad: [UserGuard],
  },
  {
    path: 'associations',
    loadChildren: () =>
      import('@qaroni-app/associations/associations.module').then(
        (m) => m.AssociationsModule
      ),
  },
  {
    path: 'licenses',
    loadChildren: () =>
      import('@qaroni-app/licenses/licenses.module').then(
        (m) => m.LicensesModule
      ),
  },
  {
    path: 'news',
    loadChildren: () =>
      import('@qaroni-app/news/news.module').then((m) => m.NewsModule),
  },
  {
    path: 'events',
    loadChildren: () =>
      import('@qaroni-app/events/events.module').then((m) => m.EventsModule),
  },
  {
    path: 'blogs',
    loadChildren: () =>
      import('@qaroni-app/blogs/blogs.module').then((m) => m.BlogsModule),
  },
  {
    path: 'applications',
    loadChildren: () =>
      import('@qaroni-app/applications/applications.module').then(
        (m) => m.ApplicationsModule
      ),
  },
  { path: '**', component: PathNotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // scrollPositionRestoration: 'top',
      useHash: true,
      // enableTracing: true
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
