import { Pipe, PipeTransform } from '@angular/core';
import {
  InvoicePaymentMethodEnum,
  InvoicePaymentMethodInfo
} from '@qaroni-app/core/entities';

@Pipe({
  name: 'invoicePaymentMethod',
})
export class InvoicePaymentMethodPipe implements PipeTransform {
  transform(value: InvoicePaymentMethodEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of InvoicePaymentMethodInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
