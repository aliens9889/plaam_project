import { Component, Input, OnInit } from '@angular/core';
import { Multimedia, MultimediasService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-multimedia-preview',
  templateUrl: './multimedia-preview.component.html',
  styleUrls: ['./multimedia-preview.component.scss']
})
export class MultimediaPreviewComponent implements OnInit {
  @Input() multimedia: Multimedia = null;

  constructor(public multimediasService: MultimediasService) {}

  ngOnInit(): void {}
}
