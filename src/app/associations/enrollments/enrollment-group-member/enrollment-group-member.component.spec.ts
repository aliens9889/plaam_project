import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentGroupMemberComponent } from './enrollment-group-member.component';

describe('EnrollmentGroupMemberComponent', () => {
  let component: EnrollmentGroupMemberComponent;
  let fixture: ComponentFixture<EnrollmentGroupMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentGroupMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentGroupMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
