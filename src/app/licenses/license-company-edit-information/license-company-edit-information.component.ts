import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute } from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import '@ckeditor/ckeditor5-build-classic/build/translations/es';
import {
  AssociationLicenseService,
  LicenseCompany
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-company-edit-information',
  templateUrl: './license-company-edit-information.component.html',
  styleUrls: ['./license-company-edit-information.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseCompanyEditInformationComponent
  implements OnInit, OnDestroy {
  private licenseCompanyID: string;

  public initLoaded = false;

  private licenseCompany$: Observable<
    LicenseCompany
  > = this.licenseService.getLicenseCompany$().pipe(shareReplay(1));
  public licenseCompany: LicenseCompany;

  private subs: Subscription = new Subscription();

  public companyDataForm: FormGroup;
  private companyDataSkeleton = {
    name: ['', [Validators.required]],
    tradename: ['', [Validators.required]],
    cif: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
    secondaryPhone: [''],
    automaticRenewal: [null],
    bankCharge: [null],
    iban: ['', Validators.compose([Validators.pattern(/^[A-Z]{2}\d{18,26}$/)])],
    bic: [''],
    description: [''],
  };

  private submitting = false;
  private formSent = false;

  public Editor = ClassicEditor;
  public configEditor: object = {
    language: 'es',
  };

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private licenseService: AssociationLicenseService
  ) {
    this.licenseCompanyID = route.snapshot.paramMap.get('licenseCompanyID');
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.createCompanyDataForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('licenseCompanyID')) {
      this.subs.add(this.licenseCompany$.subscribe(this.getLicenseCompany));
      this.licenseService.getLicenseCompany(this.licenseCompanyID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
    this.formSent = false;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private createCompanyDataForm(): void {
    this.companyDataForm = this.fb.group(this.companyDataSkeleton);
  }

  private populateCompanyDataForm(licenseCompany: LicenseCompany): void {
    if (licenseCompany) {
      this.companyDataForm.patchValue(licenseCompany);

      this.automaticRenewal.patchValue(
        licenseCompany?.automaticRenewal ? true : false
      );
      this.bankCharge.patchValue(licenseCompany?.bankCharge ? true : false);
      this.iban.patchValue(licenseCompany?.iban);
      this.bic.patchValue(licenseCompany?.bic);
      this.actionsRenewal();
    }
  }

  get InputValidation() {
    return InputValidation;
  }

  get name(): AbstractControl {
    return this.companyDataForm.get('name');
  }

  get tradename(): AbstractControl {
    return this.companyDataForm.get('tradename');
  }

  get cif(): AbstractControl {
    return this.companyDataForm.get('cif');
  }

  get email(): AbstractControl {
    return this.companyDataForm.get('email');
  }

  get phone(): AbstractControl {
    return this.companyDataForm.get('phone');
  }

  get secondaryPhone(): AbstractControl {
    return this.companyDataForm.get('secondaryPhone');
  }

  get automaticRenewal(): AbstractControl {
    return this.companyDataForm.get('automaticRenewal');
  }

  get bankCharge(): AbstractControl {
    return this.companyDataForm.get('bankCharge');
  }

  get iban(): AbstractControl {
    return this.companyDataForm.get('iban');
  }

  get bic(): AbstractControl {
    return this.companyDataForm.get('bic');
  }

  get description(): AbstractControl {
    return this.companyDataForm.get('description');
  }

  get validatedForm(): boolean {
    return (
      this.companyDataForm.dirty &&
      this.companyDataForm.valid &&
      !this.submitting
    );
  }

  private getLicenseCompany = (licenseCompany: LicenseCompany): void => {
    if (licenseCompany) {
      if (this.formSent) {
        this.onBack();
      } else {
        this.licenseCompany = licenseCompany;
        this.populateCompanyDataForm(this.licenseCompany);
      }
    }
    this.disableLoading();
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.licenseService.updateLicenseCompanyData(
        this.licenseCompanyID,
        this.companyDataForm.getRawValue()
      );
      this.formSent = true;
    }
  }

  get titlePage(): string {
    return this.licenseService.strings.getTitleLicenseCompanyEdit();
  }

  get titleCard(): string {
    return this.licenseService.strings.getStepTitleCompanyInfo();
  }

  public onBack(): void {
    this.allApp.location.back();
  }

  public onChangeRenewal(event: MatCheckboxChange): void {
    this.actionsRenewal();
  }

  private actionsRenewal(): void {
    if (this.automaticRenewal.value === true) {
      this.bankCharge.enable();
    } else if (this.automaticRenewal.value === false) {
      this.bankCharge.setValue(false);
      this.bankCharge.disable();
    }

    this.iban.clearValidators();
    if (this.bankCharge.value === true) {
      this.iban.setValidators(
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[A-Z]{2}\d{18,26}$/),
        ])
      );
    } else if (this.bankCharge.value === false) {
      this.iban.setValidators(Validators.pattern(/^[A-Z]{2}\d{18,26}$/));
    }
    this.iban.updateValueAndValidity();
  }
}
