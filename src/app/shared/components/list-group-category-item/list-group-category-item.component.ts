import { Component, Input, OnInit } from '@angular/core';
import { GroupCategory } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-list-group-category-item',
  templateUrl: './list-group-category-item.component.html',
  styleUrls: ['./list-group-category-item.component.scss'],
})
export class ListGroupCategoryItemComponent implements OnInit {
  @Input() category: GroupCategory;

  constructor() {}

  ngOnInit(): void {}

  get hasImage(): boolean {
    if (this.category && this.category.imageUrl) {
      return true;
    }
    return false;
  }
}
