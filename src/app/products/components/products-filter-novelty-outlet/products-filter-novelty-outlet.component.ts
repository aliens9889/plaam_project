import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'qaroni-products-filter-novelty-outlet',
  templateUrl: './products-filter-novelty-outlet.component.html',
  styleUrls: ['./products-filter-novelty-outlet.component.scss'],
})
export class ProductsFilterNoveltyOutletComponent implements OnInit {
  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {}

  get isNoveltyChecked(): boolean {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('novelty')) {
      if (queryParams.novelty === '1') {
        return true;
      }
    }
    return false;
  }

  public onNoveltyChange(event: MatSlideToggleChange): void {
    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );
    if (event.checked) {
      queryParamsEmit.novelty = 1;
    } else {
      delete queryParamsEmit.novelty;
    }
    this.queryParamsChange.emit(queryParamsEmit);
  }

  get isOutletChecked(): boolean {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('outlet')) {
      if (queryParams.outlet === '1') {
        return true;
      }
    }
    return false;
  }

  public onOutletChange(event: MatSlideToggleChange): void {
    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );
    if (event.checked) {
      queryParamsEmit.outlet = 1;
    } else {
      delete queryParamsEmit.outlet;
    }
    this.queryParamsChange.emit(queryParamsEmit);
  }
}
