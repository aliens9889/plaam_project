import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserSnackbars } from '@qaroni-app/core/entities/user/snackbars/user-snackbar.config';
import { ProgressBarService } from '@qaroni-app/core/services/layouts';

@Injectable({
  providedIn: 'root',
})
export class ErrorsHttpService {
  constructor(
    private progressBar: ProgressBarService,
    private router: Router,
    private snackbar: MatSnackBar,
    public translate: TranslateService
  ) {}

  public communication(error: HttpErrorResponse): void {
    console.error('ERROR HTTP', error);
    this.manageError(error);
    if (
      error &&
      error.status &&
      (error.status === 401 || error.status === 403)
    ) {
      this.snackbar.open(
        this.translate.instant(UserSnackbars.unauthorizedToken.message),
        this.translate.instant(UserSnackbars.unauthorizedToken.closeBtn),
        UserSnackbars.unauthorizedToken.config
      );
      this.router.navigate(['/auth', 'logout']);
    }
  }

  public apiInvalidResponse(error: any): void {
    console.error('INVALID API RESPONSE', error);
    this.manageError(error);
  }

  private manageError(error: any): void {
    this.progressBar.hide();
    // throw error;
  }

  public errorIsControlled(
    error: HttpErrorResponse,
    code: string,
    detail?: string
  ): boolean {
    if (
      error?.status === 400 &&
      error?.error?.errors?.length &&
      error?.error?.errors[0]?.code === code
    ) {
      if (!detail) {
        return true;
      } else {
        if (error?.error?.errors[0]?.detail === detail) {
          return true;
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }
}
