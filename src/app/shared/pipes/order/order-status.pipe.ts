import { Pipe, PipeTransform } from '@angular/core';
import { OrderStatusEnum, OrderStatusInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'orderStatus'
})
export class OrderStatusPipe implements PipeTransform {
  transform(value: OrderStatusEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of OrderStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
