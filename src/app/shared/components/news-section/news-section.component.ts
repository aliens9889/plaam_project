import { Component, OnDestroy, OnInit } from '@angular/core';
import { News, NewsService, NewsStatusEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-news-section',
  templateUrl: './news-section.component.html',
  styleUrls: ['./news-section.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class NewsSectionComponent implements OnInit, OnDestroy {
  public news$: Observable<News[]> = this.newsService
    .getNews$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(private allApp: AllAppService, private newsService: NewsService) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.newsService.getNews({ last: 3, status: NewsStatusEnum.ACTIVE });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.newsService.getNews({ last: 3, status: NewsStatusEnum.ACTIVE });
  }
}
