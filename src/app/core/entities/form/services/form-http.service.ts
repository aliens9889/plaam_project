import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FormHttpService {
  constructor(private http: HttpClient) {}

  public getFormFields$(
    formID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/forms/${formID}/fields`;

    return this.http.get(url, { observe: 'response' });
  }
}
