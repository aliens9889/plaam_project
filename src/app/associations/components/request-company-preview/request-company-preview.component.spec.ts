import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestCompanyPreviewComponent } from './request-company-preview.component';

describe('RequestCompanyPreviewComponent', () => {
  let component: RequestCompanyPreviewComponent;
  let fixture: ComponentFixture<RequestCompanyPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestCompanyPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestCompanyPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
