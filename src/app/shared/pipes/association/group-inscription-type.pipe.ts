import { Pipe, PipeTransform } from '@angular/core';
import {
  GroupInscriptionTypeEnum,
  GroupInscriptionTypeInfo
} from '@qaroni-app/core/entities';

@Pipe({
  name: 'groupInscriptionType',
})
export class GroupInscriptionTypePipe implements PipeTransform {
  transform(value: GroupInscriptionTypeEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of GroupInscriptionTypeInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
