import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterCategoriesComponent } from './products-filter-categories.component';

describe('ProductsFilterCategoriesComponent', () => {
  let component: ProductsFilterCategoriesComponent;
  let fixture: ComponentFixture<ProductsFilterCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
