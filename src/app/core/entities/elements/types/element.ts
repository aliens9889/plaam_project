import { LanguageEnum } from '@qaroni-app/core/types';
import { ElementStatusEnum } from './element-status.enum';
import { ElementTypeEnum } from './element-type.enum';

export interface Element {
  buttonAction: string;
  buttonName: string;
  creationDate: string;
  description: string;
  elementId: number;
  imageUrl: string;
  internal: boolean;
  language: LanguageEnum;
  lastUpdateDate: string;
  local: boolean;
  merchantId: number;
  position: number;
  status: ElementStatusEnum;
  title: string;
  type: ElementTypeEnum;
}
