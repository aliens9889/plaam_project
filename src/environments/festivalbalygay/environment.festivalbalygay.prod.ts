import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageEventsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '194',
};

export const appEnvironment = {
  title: 'VII FESTIVAL DE MÚSICA BAL Y GAY',
  basePath: 'https://my.festivalbalygay.com/#',
  languagesAllowed: [LanguageEnum.gl, LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  events: AppLandingPageEventsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  news: true,
  brands: false,
  groups: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plataforma.plaam.com/subido/empresas/194_20200626142817_LogoFestivalBalyGayMiguelPorlan2.png',
  toolbar:
    'https://plataforma.plaam.com/subido/empresas/194_20200626142817_LogoFestivalBalyGayMiguelPorlan2.png',
  footer:
    'https://plataforma.plaam.com/subido/empresas/194_20200626142817_LogoFestivalBalyGayMiguelPorlan2.png',
};

export const urlsEnvironment = {
  website: 'https://my.festivalbalygay.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'info@festivalbalygay.com',
  phone: '+34 982 13 61 32',
  facebook: 'https://www.facebook.com/festivalbalygay/',
  twitter: 'https://twitter.com/festivalbalygay',
  instagram: 'https://www.instagram.com/festivalbalygay/',
  privacy: 'https://my.festivalbalygay.com/#/pages/898/política-de-privacidad/',
  cookies: 'https://my.festivalbalygay.com/#/pages/910/política-de-cookies/',
  purchase: 'https://my.festivalbalygay.com/#/pages/985/condiciones-de-compra/',
};
