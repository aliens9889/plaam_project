import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { SendByEmailJson } from '@qaroni-app/core/types';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { SubscriptionsSnackbars } from '../snackbars/subscriptions-snackbar.config';
import { UserSubscription } from '../types/user-subscription';
import { SubscriptionsHttpService } from './subscriptions-http.service';

@Injectable({
  providedIn: 'root',
})
export class SubscriptionsService {
  private subscriptions$ = new Subject<UserSubscription[]>();

  constructor(
    private allApp: AllAppService,
    private subscriptionsHttp: SubscriptionsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getSubscriptionImageUrl(
    subscription: UserSubscription
  ): string | null {
    if (
      subscription.imagesURL &&
      subscription.imagesURL.length &&
      subscription.imagesURL[0]
    ) {
      return subscription.imagesURL[0];
    } else {
      return './assets/images/icons/subscriptions.png';
    }
  }

  // ==========================================================================================
  // Get Subscription
  // ==========================================================================================

  public getSubscriptions$(): Observable<UserSubscription[]> {
    return this.subscriptions$.asObservable();
  }

  public getSubscriptions(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.subscriptionsHttp
        .getSubscriptions$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetSubscriptions, this.failureGetSubscriptions);
    }
  }

  private successGetSubscriptions = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const subscriptions: UserSubscription[] = data.body.result;
      this.subscriptions$.next(subscriptions);
    } else {
      this.subscriptions$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetSubscriptions = (error: HttpErrorResponse): void => {
    this.subscriptions$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Send Subscription by email
  // ==========================================================================================

  public sendSubscriptionByEmail(
    subscriptionID: number | string,
    emailJSON: SendByEmailJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.subscriptionsHttp
        .sendSubscriptionByEmail$(
          this.oAuthService.getUserID,
          subscriptionID,
          emailJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successSendSubscriptionByEmail,
          this.failureSendSubscriptionByEmail
        );
    }
  }

  private successSendSubscriptionByEmail = (data: HttpResponse<any>): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        SubscriptionsSnackbars.successSendByEmail.message
      ),
      this.allApp.translate.instant(
        SubscriptionsSnackbars.successSendByEmail.closeBtn
      ),
      SubscriptionsSnackbars.successSendByEmail.config
    );
  }

  private failureSendSubscriptionByEmail = (error: HttpErrorResponse): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        SubscriptionsSnackbars.successSendByEmail.message
      ),
      this.allApp.translate.instant(
        SubscriptionsSnackbars.successSendByEmail.closeBtn
      ),
      SubscriptionsSnackbars.successSendByEmail.config
    );
  }
}
