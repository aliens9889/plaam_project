import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductCollection, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-products-filter-collections',
  templateUrl: './products-filter-collections.component.html',
  styleUrls: ['./products-filter-collections.component.scss'],
})
export class ProductsFilterCollectionsComponent implements OnInit, OnDestroy {
  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  public productCollections$: Observable<
    ProductCollection[]
  > = this.productService.getProductCollections$().pipe(shareReplay(1));

  public showFewOptions = true;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private productService: ProductService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.productService.getProductCollections();

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  public isOptionSelected(id: number): boolean {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('collections')) {
      const valuesArr: string[] = queryParams.collections.split(',');
      if (valuesArr.indexOf(`${id}`) !== -1) {
        return true;
      }
    }
    return false;
  }

  public onSelectionChange(list: MatSelectionListChange): void {
    const selectedValues: number[] = [];
    list.source.selectedOptions.selected.forEach((option) => {
      selectedValues.push(option.value);
    });
    const collections = selectedValues.sort().join(',');

    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );
    if (collections) {
      queryParamsEmit.collections = collections;
    } else {
      delete queryParamsEmit.collections;
    }
    this.queryParamsChange.emit(queryParamsEmit);
  }

  public onShowAll(): void {
    this.showFewOptions = !this.showFewOptions;
  }

  private languageChanged = (): void => {
    this.productService.getProductCollections();
  }
}
