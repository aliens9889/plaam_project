import { Component, Input, OnInit } from '@angular/core';
import { Item, OrderService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-order-item-price',
  templateUrl: './order-item-price.component.html',
  styleUrls: ['./order-item-price.component.scss'],
})
export class OrderItemPriceComponent implements OnInit {
  @Input() item: Item;

  constructor(private orderService: OrderService) {}

  ngOnInit(): void {}

  get hasItemDiscount(): boolean {
    return this.orderService.hasItemDiscount(this.item);
  }

  get getPriceDiscount(): number {
    if (this.hasItemDiscount) {
      return this.item.price * ((100 - parseFloat(this.item.discount)) / 100);
    } else {
      return this.item.price;
    }
  }
}
