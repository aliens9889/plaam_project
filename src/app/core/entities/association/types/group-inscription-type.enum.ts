export enum GroupInscriptionTypeEnum {
  INSCRIPTION = 'INSCRIPTION',
  REACTIVATION = 'REACTIVATION',
  RENEWAL = 'RENEWAL',
}

export type GroupInscriptionType = 'INSCRIPTION' | 'REACTIVATION' | 'RENEWAL';

export const GroupInscriptionTypeArray = [
  'INSCRIPTION',
  'REACTIVATION',
  'RENEWAL',
];

export const GroupInscriptionTypeInfo = [
  {
    inscription: {
      name: 'Inscription',
    },
  },
  {
    reactivation: {
      name: 'Reactivation',
    },
  },
  {
    renewal: {
      name: 'Renewal',
    },
  },
];
