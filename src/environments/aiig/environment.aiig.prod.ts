import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '119',
};

export const appEnvironment = {
  title: 'Asociación de Ingenieros Industriales de Galicia',
  basePath: 'https://my.aiig.es/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: false,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  products: AppLandingPageProductsEnum.ONE,
  brands: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: false,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/aiig/logos/aiig-logo.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/aiig/logos/aiig-logo.png',
  footer:
    'https://aiig.icoiig.es/wp-content/uploads/2018/01/Vieira-Mecanic_blanca2.png',
};

export const urlsEnvironment = {
  website: 'https://my.aiig.es/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'asociacion@icoiig.es',
  phone: '+34 981 22 58 26',
  linkedin:
    'https://www.linkedin.com/company/asociaci%C3%B3n-de-ingenieros-industriales-de-galicia/',
  legal: 'https://my.aiig.es/#/pages/925/aviso-legal/',
  privacy: 'https://my.aiig.es/#/pages/924/política-de-privacidad/',
  cookies: 'https://my.aiig.es/#/pages/923/política-de-cookies/',
};
