import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleOneFirstComponent } from './footer-style-one-first.component';

describe('FooterStyleOneFirstComponent', () => {
  let component: FooterStyleOneFirstComponent;
  let fixture: ComponentFixture<FooterStyleOneFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleOneFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleOneFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
