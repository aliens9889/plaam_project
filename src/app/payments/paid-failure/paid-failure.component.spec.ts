import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaidFailureComponent } from './paid-failure.component';

describe('PaidFailureComponent', () => {
  let component: PaidFailureComponent;
  let fixture: ComponentFixture<PaidFailureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaidFailureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaidFailureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
