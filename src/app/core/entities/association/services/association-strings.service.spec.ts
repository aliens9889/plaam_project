import { TestBed } from '@angular/core/testing';

import { AssociationStringsService } from './association-strings.service';

describe('AssociationStringsService', () => {
  let service: AssociationStringsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssociationStringsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
