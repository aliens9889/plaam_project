import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementBlockSectionOneComponent } from './element-block-section-one.component';

describe('ElementBlockSectionOneComponent', () => {
  let component: ElementBlockSectionOneComponent;
  let fixture: ComponentFixture<ElementBlockSectionOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementBlockSectionOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementBlockSectionOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
