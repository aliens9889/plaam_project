import { TestBed } from '@angular/core/testing';

import { StreamingsService } from './streamings.service';

describe('StreamingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StreamingsService = TestBed.inject(StreamingsService);
    expect(service).toBeTruthy();
  });
});
