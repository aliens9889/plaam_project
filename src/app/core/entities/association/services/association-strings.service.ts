import { Injectable } from '@angular/core';
import { AppAssociationTypeEnum } from '@qaroni-app/core/config';
import { associationsEnvironment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AssociationStringsService {
  constructor() {}

  public getTitleComponentString(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Join us';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Become a partner';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Become collegiate';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Become federated';
    } else {
      return 'Become a partner';
    }
  }

  public getTitleMyLicensesMembers(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'My licenses';
    } else {
      return 'Partner area';
    }
  }

  public getTitleMyLicensesCompanies(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Companies';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Companies';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academy/Club';
    } else {
      return 'Companies';
    }
  }

  public getTitleMyEnrollmentRequestsCompanies(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Companies requests';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Companies requests';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Companies requests';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academy/Club requests';
    } else {
      return 'Companies requests';
    }
  }

  public getStepTitlePersonalInfo(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner\'s personal information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner\'s personal information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate personal information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Federated personal information';
    } else {
      return 'Partner\'s personal information';
    }
  }

  public getStepTitleCompanyInfo(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Company basic information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner basic information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Company basic information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academy/Club basic information';
    } else {
      return 'Company basic information';
    }
  }

  public getStepTitleMemberAddress(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner\'s address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner\'s address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Address of the federated';
    } else {
      return 'Partner\'s address';
    }
  }

  public getStepTitleCompanyAddress(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Company address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Company address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academy/Club address';
    } else {
      return 'Company address';
    }
  }

  public getLabelPartner(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Collaborating entity';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Collaborating entity';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collaborating entity';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academy/Club';
    } else {
      return 'Collaborating entity';
    }
  }

  public getTitlePageEditMemberEnrollment(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Edit enrollment request';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Edit enrollment request';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Edit enrollment request';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Edit enrollment request';
    } else {
      return 'Edit enrollment request';
    }
  }

  public getTitleLicenseMemberDetails(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner details';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner details';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate details';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'License details';
    } else {
      return 'Partner details';
    }
  }

  public getTitleLicenseMemberUserEdit(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Edit partner information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Edit partner information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Edit collegiate information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Edit license information';
    } else {
      return 'Edit partner information';
    }
  }

  public getTitleLicenseMemberImageEdit(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner image';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner image';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate image';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Federated image';
    } else {
      return 'Partner image';
    }
  }

  public getTitleLicenseMemberAddress(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Edit partner address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Edit partner address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Edit collegiate address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Edit license address';
    } else {
      return 'Edit partner address';
    }
  }

  public getTitleLicenseCompanyDetails(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner details';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner details';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate details';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'License details';
    } else {
      return 'Partner details';
    }
  }

  public getTitleLicenseCompanyEdit(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Edit company information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Edit partner information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Edit collegiate information';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Edit academy/club information';
    } else {
      return 'Edit company information';
    }
  }

  public getTitleLicenseCompanyAddress(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Edit partner address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Edit partner address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Edit collegiate address';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Edit license address';
    } else {
      return 'Edit partner address';
    }
  }

  public getTitleLicenseCompanyImageEdit(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner logo';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner logo';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate logo';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academy/club logo';
    } else {
      return 'Partner logo';
    }
  }

  public getTitleAccountCardMyLicenses(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partner\'s area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner\'s area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'My licenses';
    } else {
      return 'Partner\'s area';
    }
  }

  public getTextAccountCardMyLicenses(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Manage partner area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Manage partner area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Manage collegiate area';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Manage my licenses';
    } else {
      return 'Manage partner area';
    }
  }

  public getTitleComponentPartners(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partners';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partner companies';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Partner collegiates';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Partner academies/clubs';
    } else {
      return 'Partners';
    }
  }

  public getTextAffiliatedMembers(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Affiliated partners';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Affiliated partners';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Affiliated collegiates';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Affiliated federates';
    } else {
      return 'Affiliated partners';
    }
  }

  public getTextPartnerMemberInscriptions(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Partners procedures';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Partners procedures';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Collegiate procedures';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Federated procedures';
    } else {
      return 'Partners procedures';
    }
  }

  public getTextAffiliatedCompanies(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Affiliated companies';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Affiliated companies';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Affiliated companies';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Affiliated academies/clubs';
    } else {
      return 'Affiliated companies';
    }
  }

  public getTextPartnerCompanyInscriptions(): string {
    if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.ASSOCIATION
    ) {
      return 'Companies procedures';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return 'Companies procedures';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.COLLEGIATE
    ) {
      return 'Companies procedures';
    } else if (
      associationsEnvironment.associationType ===
      AppAssociationTypeEnum.FEDERATION
    ) {
      return 'Academies/clubs procedures';
    } else {
      return 'Companies procedures';
    }
  }
}
