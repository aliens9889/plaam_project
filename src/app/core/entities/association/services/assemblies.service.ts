import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Assembly, AssemblyTopic } from '../types/assembly';
import { AssemblyDelegated } from '../types/assembly-delegated';
import { AssemblyVoteResultEnum } from '../types/assembly-vote-result.enum';
import { AssembliesHttpService } from './assemblies-http.service';
import { AssociationSnackbarsService } from './association-snackbars.service';

@Injectable({
  providedIn: 'root',
})
export class AssembliesService {
  private assembliesSubject = new Subject<Assembly[]>();
  private assemblySubject = new Subject<Assembly>();

  private delegatesSubject = new Subject<AssemblyDelegated[]>();
  private topicSubject = new Subject<AssemblyTopic>();

  constructor(
    private allApp: AllAppService,
    private assembliesHttp: AssembliesHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService,
    private associationSnackbars: AssociationSnackbarsService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Assemblies
  // ==========================================================================================
  public getAssemblies$(): Observable<Assembly[]> {
    return this.assembliesSubject.asObservable();
  }

  public getAssemblies(memberID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.assembliesHttp
        .getAssemblies$(this.oAuthService.getUserID, memberID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetAssemblies, this.failureGetAssemblies);
    }
  }

  private successGetAssemblies = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const assemblies: Assembly[] = data.body.result;
      this.assembliesSubject.next(assemblies);
    } else {
      this.assembliesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetAssemblies = (error: HttpErrorResponse): void => {
    this.assembliesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Assembly
  // ==========================================================================================
  public getAssembly$(): Observable<Assembly> {
    return this.assemblySubject.asObservable();
  }

  public getAssembly(
    memberID: number | string,
    assemblyID: number | string
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.assembliesHttp
        .getAssembly$(this.oAuthService.getUserID, memberID, assemblyID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetAssembly, this.failureGetAssembly);
    }
  }

  private successGetAssembly = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const assembly: Assembly = data.body.result[0];
      this.assemblySubject.next(assembly);
    } else {
      this.assemblySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetAssembly = (error: HttpErrorResponse): void => {
    this.assemblySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Assembly
  // ==========================================================================================
  public getAssemblyDelegates$(): Observable<AssemblyDelegated[]> {
    return this.delegatesSubject.asObservable();
  }

  public getAssemblyDelegates(
    memberID: number | string,
    assemblyID: number | string
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.assembliesHttp
        .getAssemblyDelegates$(
          this.oAuthService.getUserID,
          memberID,
          assemblyID
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetAssemblyDelegates,
          this.failureGetAssemblyDelegates
        );
    }
  }

  private successGetAssemblyDelegates = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const delegates: AssemblyDelegated[] = data.body.result;
      this.delegatesSubject.next(delegates);
    } else {
      this.delegatesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetAssemblyDelegates = (error: HttpErrorResponse): void => {
    this.delegatesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Assembly Topic
  // ==========================================================================================
  public getAssemblyTopic$(): Observable<AssemblyTopic> {
    return this.topicSubject.asObservable();
  }

  public getAssemblyTopic(
    memberID: number | string,
    assemblyID: number | string,
    topicID: number | string
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.assembliesHttp
        .getAssemblyTopic$(
          this.oAuthService.getUserID,
          memberID,
          assemblyID,
          topicID
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetAssemblytopic, this.failureGetAssemblyTopic);
    }
  }

  private successGetAssemblytopic = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const topic: AssemblyTopic = data.body.result[0];
      this.topicSubject.next(topic);
    } else {
      this.topicSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetAssemblyTopic = (error: HttpErrorResponse): void => {
    this.topicSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Vote Assembly Topic
  // ==========================================================================================

  public voteAssemblyTopic(
    memberID: number | string,
    assemblyID: number | string,
    topicID: number | string,
    vote: AssemblyVoteResultEnum
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.assembliesHttp
        .voteAssemblyTopic$(
          this.oAuthService.getUserID,
          memberID,
          assemblyID,
          topicID,
          vote
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successVoteAssemblytopic,
          this.failureVoteAssemblyTopic
        );
    }
  }

  private successVoteAssemblytopic = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const assembly: Assembly = data.body.result[0];
      this.assemblySubject.next(assembly);
    } else {
      this.assemblySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureVoteAssemblyTopic = (error: HttpErrorResponse): void => {
    this.assemblySubject.next(null);
    if (
      this.commonsHttp.errorsHttp.errorIsControlled(
        error,
        'E0013',
        'El punto no se encuentra disponible para votación'
      )
    ) {
      this.associationSnackbars.failureVoteTopicStatus();
    } else if (this.commonsHttp.errorsHttp.errorIsControlled(error, 'E0011')) {
      this.associationSnackbars.failureVoteTopicDelegate();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }
}
