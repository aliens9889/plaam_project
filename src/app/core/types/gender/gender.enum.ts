export enum GenderEnum {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
}

export type GenderType = 'MALE' | 'FEMALE';

export const GenderArray = ['MALE', 'FEMALE'];

export const GenderInfo = [
  {
    male: {
      name: 'Male',
    },
  },
  {
    female: {
      name: 'Female',
    },
  },
];
