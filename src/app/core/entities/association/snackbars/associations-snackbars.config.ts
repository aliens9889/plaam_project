import { SnackbarConfig } from '@qaroni-app/core/config';

export const AssociationsSnackbars = {
  failureAllowMember: {
    message: `Members registration requests are not allowed`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
  failureAllowCompany: {
    message: `Companies registration requests are not allowed`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
  failureEnrollmentType: {
    message: `Enrollment requests type are not allowed`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
  failureHasPartner: {
    message: `Enrollment requests are not allowed`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
  successEnrollmentRequest: {
    message: `Enrollment request was successful`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.success,
      duration: SnackbarConfig.durations.success,
    },
  },
  failureAllowedPartnerType: {
    message: `Partner type not allowed`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
  failureVoteTopicStatus: {
    message: `Voting for this point is not open`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
  failureVoteTopicDelegate: {
    message: `You cannot vote because you delegated your vote`,
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    },
  },
};
