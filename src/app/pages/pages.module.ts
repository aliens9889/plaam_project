import { NgModule } from '@angular/core';
import { SharedModule } from '@qaroni-app/shared';
import { PageShowComponent } from './page-show/page-show.component';
import { PagesRoutingModule } from './pages-routing.module';

@NgModule({
  declarations: [PageShowComponent],
  imports: [SharedModule, PagesRoutingModule],
})
export class PagesModule {}
