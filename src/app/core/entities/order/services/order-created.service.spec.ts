import { TestBed } from '@angular/core/testing';

import { OrderCreatedService } from './order-created.service';

describe('OrderCreatedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderCreatedService = TestBed.inject(OrderCreatedService);
    expect(service).toBeTruthy();
  });
});
