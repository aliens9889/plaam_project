import { TestBed } from '@angular/core/testing';

import { AssociationPartnerHttpService } from './association-partner-http.service';

describe('AssociationPartnerHttpService', () => {
  let service: AssociationPartnerHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssociationPartnerHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
