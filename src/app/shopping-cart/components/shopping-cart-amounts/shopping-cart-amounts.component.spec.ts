import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingCartAmountsComponent } from './shopping-cart-amounts.component';

describe('ShoppingCartAmountsComponent', () => {
  let component: ShoppingCartAmountsComponent;
  let fixture: ComponentFixture<ShoppingCartAmountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingCartAmountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingCartAmountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
