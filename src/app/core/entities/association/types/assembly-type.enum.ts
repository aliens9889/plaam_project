export enum AssemblyTypeEnum {
  ANONYMOUS = 'ANONYMOUS',
  NOT_ANONYMOUS = 'NOT_ANONYMOUS',
}

export type AssemblyType =
  | AssemblyTypeEnum.ANONYMOUS
  | AssemblyTypeEnum.NOT_ANONYMOUS;

export const AssemblyTypeArray = [
  AssemblyTypeEnum.ANONYMOUS,
  AssemblyTypeEnum.NOT_ANONYMOUS,
];

export const AssemblyTypeInfo = [
  {
    ANONYMOUS: {
      name: 'Anónimo',
    },
  },
  {
    NOT_ANONYMOUS: {
      name: 'No anónimo',
    },
  },
];
