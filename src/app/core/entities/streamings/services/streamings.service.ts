import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Streaming } from '../types/streaming';
import { StreamingsHttpService } from './streamings-http.service';

@Injectable({
  providedIn: 'root',
})
export class StreamingsService {
  private streamings$ = new Subject<Streaming[]>();

  constructor(
    private allApp: AllAppService,
    private streamingsHttp: StreamingsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Streamings
  // ==========================================================================================

  public getStreamings$(): Observable<Streaming[]> {
    return this.streamings$.asObservable();
  }

  public getStreamings(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.streamingsHttp
        .getStreamings$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetStreamings, this.failureGetStreamings);
    }
  }

  private successGetStreamings = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const streamings: Streaming[] = data.body.result;
      this.streamings$.next(streamings);
    } else {
      this.streamings$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetStreamings = (error: HttpErrorResponse): void => {
    this.streamings$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
