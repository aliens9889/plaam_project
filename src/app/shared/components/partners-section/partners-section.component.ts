import { Component, OnInit } from '@angular/core';
import { AppLandingPageGroupsEnum, LandingEnv } from '@qaroni-app/core/config';

@Component({
  selector: 'qaroni-partners-section',
  templateUrl: './partners-section.component.html',
  styleUrls: ['./partners-section.component.scss'],
})
export class PartnersSectionComponent implements OnInit {
  public LandingEnv = LandingEnv;
  public AppLandingPageGroupsEnum = AppLandingPageGroupsEnum;

  constructor() {}

  ngOnInit(): void {}
}
