import { Pipe, PipeTransform } from '@angular/core';
import {
  AssemblyStatusEnum,
  AssemblyStatusInfo
} from '@qaroni-app/core/entities';

@Pipe({
  name: 'assemblyStatus',
})
export class AssemblyStatusPipe implements PipeTransform {
  transform(value: AssemblyStatusEnum): string {
    if (value) {
      const key = value.trim().toUpperCase();
      for (const iterator of AssemblyStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
