import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleTwoFirstComponent } from './footer-style-two-first.component';

describe('FooterStyleTwoFirstComponent', () => {
  let component: FooterStyleTwoFirstComponent;
  let fixture: ComponentFixture<FooterStyleTwoFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleTwoFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleTwoFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
