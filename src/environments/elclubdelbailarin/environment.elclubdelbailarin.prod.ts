import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '112',
};

export const appEnvironment = {
  title: 'El Club del Bailarín',
  basePath: 'https://my.elclubdelbailarin.com/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: false,
  news: true,
  groups: AppLandingPageGroupsEnum.GROUPS,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: false,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://www.elclubdelbailarin.com/assets/base/img/layout/logos/logo-menta.png',
  toolbar:
    'https://www.elclubdelbailarin.com/assets/base/img/layout/logos/logo-menta.png',
  footer:
    'https://www.elclubdelbailarin.com/assets/base/img/layout/logos/logo-menta.png',
};

export const urlsEnvironment = {
  website: 'https://my.elclubdelbailarin.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'hola@elclubdelbailarin.com',
  phone: '+34 917 377 732',
  facebook:
    'https://www.facebook.com/%F0%9D%91%92%F0%9D%93%81-%F0%9D%92%9E%F0%9D%93%81%F0%9D%93%8A%F0%9D%92%B7-%F0%9D%92%B9%F0%9D%91%92%F0%9D%93%81-%F0%9D%92%B7%F0%9D%92%B6%F0%9D%92%BE%F0%9D%93%81%F0%9D%92%B6%F0%9D%93%87%F0%9D%92%BE%F0%9D%93%83-101803718017492/',
  instagram: 'https://www.instagram.com/elclubdelbailarin/',
  whatsapp: 'https://api.whatsapp.com/send?phone=34622476921',
  legal: 'https://my.elclubdelbailarin.com/#/',
  privacy: 'https://my.elclubdelbailarin.com/#/',
  cookies: 'https://my.elclubdelbailarin.com/#/',
};
