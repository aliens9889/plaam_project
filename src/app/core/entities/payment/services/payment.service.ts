import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { PaymentSnackbars } from '../snackbars/payment-snackbar.config';
import { PromotionalCodeJson } from '../types/promotional-code-json';
import { PaymentsHttpService } from './payments-http.service';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  private promotionalCode$ = new Subject<string>();

  constructor(
    private allApp: AllAppService,
    private paymentsHttp: PaymentsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Apply Promotional Code
  // ==========================================================================================
  public getPromotionalCode$(): Observable<string> {
    return this.promotionalCode$.asObservable();
  }

  public applyPromotionalCode(
    orderID: number | string,
    promotionalCodeJSON: PromotionalCodeJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .applyPromotionalCode$(
          this.oAuthService.getUserID,
          orderID,
          promotionalCodeJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successApplyPromotionalCode,
          this.failureApplyPromotionalCode
        );
    }
  }

  private successApplyPromotionalCode = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      this.promotionalCode$.next('success');
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          PaymentSnackbars.successPromotionalCode.message
        ),
        this.allApp.translate.instant(
          PaymentSnackbars.successPromotionalCode.closeBtn
        ),
        PaymentSnackbars.successPromotionalCode.config
      );
    } else {
      this.promotionalCode$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureApplyPromotionalCode = (error: HttpErrorResponse): void => {
    this.promotionalCode$.next(null);
    if (
      error.status === 400 &&
      error.error &&
      error.error.errors &&
      error.error.errors.length &&
      error.error.errors[0] &&
      error.error.errors[0].code
    ) {
      if (error.error.errors[0].code === 'E0004') {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(PaymentSnackbars.failureE0004.message),
          this.allApp.translate.instant(PaymentSnackbars.failureE0004.closeBtn),
          PaymentSnackbars.failureE0004.config
        );
      } else if (error.error.errors[0].code === 'E0010') {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(PaymentSnackbars.failureE0010.message),
          this.allApp.translate.instant(PaymentSnackbars.failureE0010.closeBtn),
          PaymentSnackbars.failureE0010.config
        );
      } else {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(
            PaymentSnackbars.failurePromotionalCode.message
          ),
          this.allApp.translate.instant(
            PaymentSnackbars.failurePromotionalCode.closeBtn
          ),
          PaymentSnackbars.failurePromotionalCode.config
        );
      }
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          PaymentSnackbars.failurePromotionalCode.message
        ),
        this.allApp.translate.instant(
          PaymentSnackbars.failurePromotionalCode.closeBtn
        ),
        PaymentSnackbars.failurePromotionalCode.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }
}
