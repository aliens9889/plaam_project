import { Component, Input, OnInit } from '@angular/core';
import { Shipping } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-shipping-radio',
  templateUrl: './shipping-radio.component.html',
  styleUrls: ['./shipping-radio.component.scss'],
})
export class ShippingRadioComponent implements OnInit {
  @Input() shipping: Shipping;

  constructor() {}

  ngOnInit(): void {}

  get hasLogoUrl(): boolean {
    if (this.shipping && this.shipping.logoUrl) {
      return true;
    }
    return false;
  }

  get hasDescription(): boolean {
    if (this.shipping && this.shipping.description) {
      return true;
    }
    return false;
  }
}
