import { Component, Input, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { SubscriptionsService, UserSubscription } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { SendByEmailJson } from '@qaroni-app/core/types';
import { SendEmailDialogComponent } from '@qaroni-app/shared/components/dialogs';

@Component({
  selector: 'qaroni-subscription-preview',
  templateUrl: './subscription-preview.component.html',
  styleUrls: ['./subscription-preview.component.scss']
})
export class SubscriptionPreviewComponent implements OnInit {
  @Input() subscription: UserSubscription = null;

  constructor(
    private allApp: AllAppService,
    public subscriptionsService: SubscriptionsService
  ) {}

  ngOnInit(): void {}

  public onSendByEmail(): void {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '700px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;

    const dialog = this.allApp.dialog.open(
      SendEmailDialogComponent,
      matDialogConfig
    );

    dialog.afterClosed().subscribe((emailJSON: SendByEmailJson) => {
      if (emailJSON && emailJSON.email) {
        this.subscriptionsService.sendSubscriptionByEmail(
          this.subscription.subscriptionId,
          emailJSON
        );
      }
    });
  }
}
