import { NgModule } from '@angular/core';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { ProductDetailGalleryComponent } from './components/product-detail-gallery/product-detail-gallery.component';
import { ProductDetailOptionsComponent } from './components/product-detail-options/product-detail-options.component';
import { ProductStockStringComponent } from './components/product-stock-string/product-stock-string.component';
import { ProductsFilterAttributeComponent } from './components/products-filter-attribute/products-filter-attribute.component';
import { ProductsFilterAttributesComponent } from './components/products-filter-attributes/products-filter-attributes.component';
import { ProductsFilterBrandsComponent } from './components/products-filter-brands/products-filter-brands.component';
import { ProductsFilterCategoriesComponent } from './components/products-filter-categories/products-filter-categories.component';
import { ProductsFilterCollectionsComponent } from './components/products-filter-collections/products-filter-collections.component';
import { ProductsFilterNoveltyOutletComponent } from './components/products-filter-novelty-outlet/products-filter-novelty-outlet.component';
import { ProductsFilterPricesComponent } from './components/products-filter-prices/products-filter-prices.component';
import { ProductsFilterSearchComponent } from './components/products-filter-search/products-filter-search.component';
import { ProductsFilterTypeComponent } from './components/products-filter-type/products-filter-type.component';
import { ProductsFiltersComponent } from './components/products-filters/products-filters.component';
import { ProductsStyleOneComponent } from './components/products-page-styles/products-style-one/products-style-one.component';
import { ProductsStyleTwoComponent } from './components/products-page-styles/products-style-two/products-style-two.component';
import { ProductsPreviewSizeComponent } from './components/products-preview-size/products-preview-size.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products/products.component';

@NgModule({
  declarations: [
    ProductDetailGalleryComponent,
    ProductDetailOptionsComponent,
    ProductDetailsComponent,
    ProductsComponent,
    ProductsFilterAttributeComponent,
    ProductsFilterAttributesComponent,
    ProductsFilterBrandsComponent,
    ProductsFilterCategoriesComponent,
    ProductsFilterCollectionsComponent,
    ProductsFilterNoveltyOutletComponent,
    ProductsFilterPricesComponent,
    ProductsFiltersComponent,
    ProductsFilterSearchComponent,
    ProductsFilterTypeComponent,
    ProductsPreviewSizeComponent,
    ProductsStyleOneComponent,
    ProductsStyleTwoComponent,
    ProductStockStringComponent,
  ],
  imports: [
    NgxGalleryModule,
    ProductsRoutingModule,
    SharedModule,
    TranslateModule,
  ],
})
export class ProductsModule {}
