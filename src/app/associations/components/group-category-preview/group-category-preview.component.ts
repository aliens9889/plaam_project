import { Component, Input, OnInit } from '@angular/core';
import { GroupCategory, AssociationService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-group-category-preview',
  templateUrl: './group-category-preview.component.html',
  styleUrls: ['./group-category-preview.component.scss'],
})
export class GroupCategoryPreviewComponent implements OnInit {
  @Input() category: GroupCategory;

  constructor(private associationService: AssociationService) {}

  ngOnInit(): void {}
}
