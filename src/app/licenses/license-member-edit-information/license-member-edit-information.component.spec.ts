import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseMemberEditInformationComponent } from './license-member-edit-information.component';

describe('LicenseMemberEditInformationComponent', () => {
  let component: LicenseMemberEditInformationComponent;
  let fixture: ComponentFixture<LicenseMemberEditInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseMemberEditInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseMemberEditInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
