import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentPaycometComponent } from './payment-paycomet.component';

describe('PaymentPaycometComponent', () => {
  let component: PaymentPaycometComponent;
  let fixture: ComponentFixture<PaymentPaycometComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentPaycometComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPaycometComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
