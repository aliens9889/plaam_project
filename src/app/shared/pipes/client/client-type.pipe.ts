import { Pipe, PipeTransform } from '@angular/core';
import { ClientTypeEnum, ClientTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'clientType',
})
export class ClientTypePipe implements PipeTransform {
  transform(value: ClientTypeEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of ClientTypeInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
