import { Address } from '../../address';
import { Group } from './group';
import { LicenseCompanyStatusEnum } from './license-company-status.enum';

export interface LicenseCompany {
  addresses: Address[];
  automaticRenewal: boolean;
  bankCharge: boolean;
  bic: string;
  cif: string;
  clientId: number;
  code: string;
  companyId: number;
  contact: LicenseCompanyContact;
  creationDate: string;
  email: string;
  expirationDate: string;
  groupId: number;
  groupInfo: Group;
  iban: string;
  imageUrl: string;
  inscriptionId: number;
  isPartner: boolean;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  partnerId: number;
  phone: string;
  renewalDate: string;
  secondaryPhone: string;
  status: LicenseCompanyStatusEnum;
  tradename: string;
  userId: number;
  walletUrl: string;
}

export interface LicenseCompanyContact {
  addresses: Address[];
  birthday: string;
  companyId: number;
  contactId: number;
  creationDate: string;
  document: string;
  documentType: string;
  email: string;
  firstName: string;
  gender: string;
  language: string;
  lastName: string;
  lastUpdateDate: string;
  phone: string;
}
