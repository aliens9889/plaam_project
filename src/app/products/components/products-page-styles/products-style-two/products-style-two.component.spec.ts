import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsStyleTwoComponent } from './products-style-two.component';

describe('ProductsStyleTwoComponent', () => {
  let component: ProductsStyleTwoComponent;
  let fixture: ComponentFixture<ProductsStyleTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsStyleTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsStyleTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
