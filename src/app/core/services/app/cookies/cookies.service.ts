import { Injectable } from '@angular/core';
import { AppStorageService } from '../app-storage/app-storage.service';

@Injectable({
  providedIn: 'root',
})
export class CookiesService {
  constructor(private appStorage: AppStorageService) {}

  get hasCookies(): boolean {
    return this.appStorage.hasCookies;
  }

  public setCookies(cookies: boolean): void {
    this.appStorage.setCookies(cookies);
  }
}
