import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';
import { ChangePasswordJSON, UserData } from '../types/user-data';

@Injectable({
  providedIn: 'root',
})
export class UserDataHttpService {
  constructor(private http: HttpClient) {}

  public getUserData$(userID: number | string): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public updateUserData$(
    userID: number | string,
    userDataJSON: UserData
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}`;

    return this.http.patch(url, userDataJSON, { observe: 'response' });
  }

  public changePassword$(
    userID: number | string,
    changePasswordJSON: ChangePasswordJSON
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/passwords`;

    return this.http.patch(url, changePasswordJSON, { observe: 'response' });
  }
}
