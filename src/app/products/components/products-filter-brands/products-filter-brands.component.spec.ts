import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterBrandsComponent } from './products-filter-brands.component';

describe('ProductsFilterBrandsComponent', () => {
  let component: ProductsFilterBrandsComponent;
  let fixture: ComponentFixture<ProductsFilterBrandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterBrandsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterBrandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
