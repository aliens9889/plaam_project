import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { OrderResumeComponent } from './components/order-resume/order-resume.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersComponent } from './orders/orders.component';
import { OrderResumeInformationComponent } from './components/order-resume-information/order-resume-information.component';
import { OrderInvoicesComponent } from './components/order-invoices/order-invoices.component';

@NgModule({
  declarations: [OrderDetailsComponent, OrderResumeComponent, OrdersComponent, OrderResumeInformationComponent, OrderInvoicesComponent],
  imports: [SharedModule, OrdersRoutingModule, TranslateModule]
})
export class OrdersModule {}
