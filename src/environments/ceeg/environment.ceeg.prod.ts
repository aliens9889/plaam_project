import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '216',
};

export const appEnvironment = {
  title: 'Club de Empresarios y Ejecutivos Galicia',
  basePath: 'https://www.clubempresariosgalicia.gal/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  boxes: AppLandingPageBoxesEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  news: true,
  brands: false,
  products: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/ceeg/logos/logo-ceeg-transparent.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/ceeg/logos/logo-ceeg-transparent.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/ceeg/logos/logo-ceeg-transparent.png',
};

export const urlsEnvironment = {
  website: 'https://www.clubempresariosgalicia.gal/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@clubempresariosgalicia.gal',
  phone: '+34 698 141 051',
  facebook: 'https://www.facebook.com/ClubdeEmpresariosyEjecutivosdeGalicia',
  twitter: 'https://twitter.com/ceeg_galicia',
  instagram: 'https://www.instagram.com/ceeg_galicia',
  linkedin:
    'https://www.linkedin.com/company/ceeg-club-de-empresarios-y-ejecutivos-de-galicia',
  privacy:
    'https://www.clubempresariosgalicia.gal/#/pages/1111/política-de-privacidad/',
  cookies:
    'https://www.clubempresariosgalicia.gal/#/pages/1110/política-de-cookies/',
};
