import { Invoice } from '../../invoice';
import { Item } from './item';
import { OrderStatusEnum } from './order-status.enum';
import { OrderTypeEnum } from './order-type.enum';

export interface Order {
  amount: number;
  clientId: number;
  code: number | string;
  creationDate: string;
  deliveryAmount: number;
  deliveryId: number;
  description: string;
  invoices: Invoice[];
  items: Item[];
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  number: number;
  orderId: number;
  serieId: number;
  status: OrderStatusEnum;
  trackingNumber: string;
  type: OrderTypeEnum;
  userId: string;
}
