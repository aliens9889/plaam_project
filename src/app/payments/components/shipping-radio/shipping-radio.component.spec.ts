import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingRadioComponent } from './shipping-radio.component';

describe('ShippingRadioComponent', () => {
  let component: ShippingRadioComponent;
  let fixture: ComponentFixture<ShippingRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippingRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
