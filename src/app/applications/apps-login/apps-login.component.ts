import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OAuth, OAuthService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';

@Component({
  selector: 'qaroni-apps-login',
  templateUrl: './apps-login.component.html',
  styleUrls: ['./apps-login.component.scss'],
})
export class AppsLoginComponent implements OnInit {
  private token: string;
  private userID: number;
  private url: string;

  constructor(
    private allApp: AllAppService,
    private oAuthService: OAuthService,
    private route: ActivatedRoute
  ) {
    this.enableLoading();
    this.token = route.snapshot.queryParamMap.get('token');
    this.userID = parseInt(route.snapshot.queryParamMap.get('userID'), 10);
    this.url = route.snapshot.queryParamMap.get('url');
  }

  ngOnInit(): void {
    if (!this.oAuthService.hasOAuth) {
      if (this.token && this.userID) {
        const oAuth: OAuth = {
          access_token: this.token,
          userId: this.userID,
          expires: null,
          issued: null,
        };
        this.oAuthService.oAuthStorage.set(oAuth);
        this.checkNavigateUrl();
      } else {
        this.redirect();
      }
    } else {
      this.checkNavigateUrl();
    }
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private checkNavigateUrl(): void {
    if (this.url) {
      this.allApp.router.navigate([this.url]);
    } else {
      this.redirect();
    }
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }
}
