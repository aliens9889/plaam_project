import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material';
import { PipesModule } from '../pipes';
import { AddressStringComponent } from './address-string/address-string.component';
import { ApplyPromotionalCodeComponent } from './apply-promotional-code/apply-promotional-code.component';
import { BrandLogoComponent } from './brand-logo/brand-logo.component';
import { BrandsSectionOneComponent } from './brands-section-styles/brands-section-one/brands-section-one.component';
import { BrandsSectionTwoComponent } from './brands-section-styles/brands-section-two/brands-section-two.component';
import { BrandsSectionComponent } from './brands-section/brands-section.component';
import { CardEmptyContentComponent } from './card-empty-content/card-empty-content.component';
import { CardLoadingContentComponent } from './card-loading-content/card-loading-content.component';
import { AddressFormDialogComponent } from './dialogs/address-form-dialog/address-form-dialog.component';
import { ConfirmationDialogComponent } from './dialogs/confirmation-dialog/confirmation-dialog.component';
import { ErrorsHttpComponent } from './dialogs/errors-http/errors-http.component';
import { SendEmailDialogComponent } from './dialogs/send-email-dialog/send-email-dialog.component';
import { ElementBlockSectionOneComponent } from './element-block-section-styles/element-block-section-one/element-block-section-one.component';
import { ElementBlockSectionComponent } from './element-block-section/element-block-section.component';
import { ElementBoxesSectionOneComponent } from './element-boxes-section-styles/element-boxes-section-one/element-boxes-section-one.component';
import { ElementBoxesSectionComponent } from './element-boxes-section/element-boxes-section.component';
import { ElementButtonCasesComponent } from './element-button-cases/element-button-cases.component';
import { EventsPreviewOneComponent } from './events-preview-styles/events-preview-one/events-preview-one.component';
import { EventsSectionOneComponent } from './events-section-styles/events-section-one/events-section-one.component';
import { EventsSectionComponent } from './events-section/events-section.component';
import { FormFieldComponent } from './form-field/form-field.component';
import { GrecaptchaComponent } from './grecaptcha/grecaptcha.component';
import { ImageAspectRatioComponent } from './image-aspect-ratio/image-aspect-ratio.component';
import { InfoLabelValueComponent } from './info-label-value/info-label-value.component';
import { InputErrorMessagesComponent } from './input-error-messages/input-error-messages.component';
import { InputPreviewComponent } from './input-preview/input-preview.component';
import { CookiesBannerComponent } from './layouts/cookies-banner/cookies-banner.component';
import { FooterStyleFourBottomComponent } from './layouts/footer-styles/footer-style-four/components/footer-style-four-bottom/footer-style-four-bottom.component';
import { FooterStyleFourFirstComponent } from './layouts/footer-styles/footer-style-four/components/footer-style-four-first/footer-style-four-first.component';
import { FooterStyleFourComponent } from './layouts/footer-styles/footer-style-four/footer-style-four/footer-style-four.component';
import { FooterStyleOneBottomComponent } from './layouts/footer-styles/footer-style-one/components/footer-style-one-bottom/footer-style-one-bottom.component';
import { FooterStyleOneFirstComponent } from './layouts/footer-styles/footer-style-one/components/footer-style-one-first/footer-style-one-first.component';
import { FooterStyleOneComponent } from './layouts/footer-styles/footer-style-one/footer-style-one/footer-style-one.component';
import { FooterStyleThreeBottomComponent } from './layouts/footer-styles/footer-style-three/components/footer-style-three-bottom/footer-style-three-bottom.component';
import { FooterStyleThreeFirstComponent } from './layouts/footer-styles/footer-style-three/components/footer-style-three-first/footer-style-three-first.component';
import { FooterStyleThreeComponent } from './layouts/footer-styles/footer-style-three/footer-style-three/footer-style-three.component';
import { FooterStyleTwoBottomComponent } from './layouts/footer-styles/footer-style-two/components/footer-style-two-bottom/footer-style-two-bottom.component';
import { FooterStyleTwoFirstComponent } from './layouts/footer-styles/footer-style-two/components/footer-style-two-first/footer-style-two-first.component';
import { FooterStyleTwoComponent } from './layouts/footer-styles/footer-style-two/footer-style-two/footer-style-two.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { HeaderToolbarComponent } from './layouts/header-toolbar/header-toolbar.component';
import { HeaderComponent } from './layouts/header/header.component';
import { MainComponent } from './layouts/main/main.component';
import { LicenseCompanyItemComponent } from './license-company-item/license-company-item.component';
import { LicenseMemberItemComponent } from './license-member-item/license-member-item.component';
import { ListGroupCategoryItemComponent } from './list-group-category-item/list-group-category-item.component';
import { ListGroupItemComponent } from './list-group-item/list-group-item.component';
import { NewsPreviewComponent } from './news-preview/news-preview.component';
import { NewsSectionComponent } from './news-section/news-section.component';
import { OrderCofidisCalculatorComponent } from './order-cofidis-calculator/order-cofidis-calculator.component';
import { OrderItemImageComponent } from './order-item-image/order-item-image.component';
import { OrderItemPriceComponent } from './order-item-price/order-item-price.component';
import { OrderResumeAmountsComponent } from './order-resume-amounts/order-resume-amounts.component';
import { OrderResumeItemComponent } from './order-resume-item/order-resume-item.component';
import { PageTitleStyleMaterialComponent } from './page-title-styles/page-title-style-material/page-title-style-material.component';
import { PageTitleStyleOneComponent } from './page-title-styles/page-title-style-one/page-title-style-one.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { PartnersSectionCategoriesComponent } from './partners-section-styles/partners-section-categories/partners-section-categories.component';
import { PartnersSectionGroupsComponent } from './partners-section-styles/partners-section-groups/partners-section-groups.component';
import { PartnersSectionComponent } from './partners-section/partners-section.component';
import { PostsPreviewComponent } from './posts-preview/posts-preview.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductsSectionOneComponent } from './products-section-styles/products-section-one/products-section-one.component';
import { ProductsSectionTwoComponent } from './products-section-styles/products-section-two/products-section-two.component';
import { ProductsSectionComponent } from './products-section/products-section.component';
import { RequiredIndicatorComponent } from './required-indicator/required-indicator.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { SlidesCarouselComponent } from './slides-carousel/slides-carousel.component';
import { SlidesSectionComponent } from './slides-section/slides-section.component';

@NgModule({
  declarations: [
    AddressFormDialogComponent,
    AddressStringComponent,
    ApplyPromotionalCodeComponent,
    BrandLogoComponent,
    BrandsSectionComponent,
    BrandsSectionOneComponent,
    BrandsSectionTwoComponent,
    CardEmptyContentComponent,
    CardLoadingContentComponent,
    ConfirmationDialogComponent,
    CookiesBannerComponent,
    ElementBlockSectionComponent,
    ElementBlockSectionOneComponent,
    ElementBoxesSectionComponent,
    ElementBoxesSectionOneComponent,
    ElementButtonCasesComponent,
    ErrorsHttpComponent,
    EventsPreviewOneComponent,
    EventsSectionComponent,
    EventsSectionOneComponent,
    FooterComponent,
    FooterStyleFourBottomComponent,
    FooterStyleFourComponent,
    FooterStyleFourFirstComponent,
    FooterStyleOneBottomComponent,
    FooterStyleOneComponent,
    FooterStyleOneFirstComponent,
    FooterStyleThreeBottomComponent,
    FooterStyleThreeComponent,
    FooterStyleThreeFirstComponent,
    FooterStyleTwoBottomComponent,
    FooterStyleTwoComponent,
    FooterStyleTwoFirstComponent,
    FormFieldComponent,
    GrecaptchaComponent,
    HeaderComponent,
    HeaderToolbarComponent,
    ImageAspectRatioComponent,
    InfoLabelValueComponent,
    InputErrorMessagesComponent,
    InputPreviewComponent,
    LicenseCompanyItemComponent,
    LicenseMemberItemComponent,
    ListGroupCategoryItemComponent,
    ListGroupItemComponent,
    MainComponent,
    NewsPreviewComponent,
    NewsSectionComponent,
    OrderCofidisCalculatorComponent,
    OrderItemImageComponent,
    OrderItemPriceComponent,
    OrderResumeAmountsComponent,
    OrderResumeItemComponent,
    PageTitleComponent,
    PageTitleStyleMaterialComponent,
    PageTitleStyleOneComponent,
    PartnersSectionCategoriesComponent,
    PartnersSectionComponent,
    PartnersSectionGroupsComponent,
    PostsPreviewComponent,
    ProductCardComponent,
    ProductsSectionComponent,
    ProductsSectionOneComponent,
    ProductsSectionTwoComponent,
    RequiredIndicatorComponent,
    ScrollTopComponent,
    SendEmailDialogComponent,
    SlidesCarouselComponent,
    SlidesSectionComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    MaterialModule,
    PipesModule,
    RouterModule,
  ],
  exports: [
    AddressStringComponent,
    ApplyPromotionalCodeComponent,
    BrandLogoComponent,
    BrandsSectionComponent,
    CardEmptyContentComponent,
    CardLoadingContentComponent,
    CookiesBannerComponent,
    ElementBlockSectionComponent,
    ElementBoxesSectionComponent,
    EventsPreviewOneComponent,
    EventsSectionComponent,
    FooterComponent,
    FormFieldComponent,
    GrecaptchaComponent,
    HeaderComponent,
    ImageAspectRatioComponent,
    InfoLabelValueComponent,
    InputErrorMessagesComponent,
    InputPreviewComponent,
    LicenseCompanyItemComponent,
    LicenseMemberItemComponent,
    ListGroupCategoryItemComponent,
    ListGroupItemComponent,
    MainComponent,
    NewsPreviewComponent,
    NewsSectionComponent,
    OrderCofidisCalculatorComponent,
    OrderItemImageComponent,
    OrderItemPriceComponent,
    OrderResumeAmountsComponent,
    OrderResumeItemComponent,
    PageTitleComponent,
    PartnersSectionComponent,
    PostsPreviewComponent,
    ProductCardComponent,
    ProductsSectionComponent,
    RequiredIndicatorComponent,
    ScrollTopComponent,
    SlidesCarouselComponent,
    SlidesSectionComponent,
  ],
})
export class ComponentsModule {}
