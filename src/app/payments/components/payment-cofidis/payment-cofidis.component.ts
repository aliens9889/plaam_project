import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CofidisData, OrderPaymentService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-payment-cofidis',
  templateUrl: './payment-cofidis.component.html',
  styleUrls: ['./payment-cofidis.component.scss'],
})
export class PaymentCofidisComponent implements OnInit, OnDestroy {
  @Output() loading: EventEmitter<null> = new EventEmitter();

  private subs: Subscription = new Subscription();

  public cofidisData$: Observable<
    CofidisData
  > = this.orderPaymentService.getCofidisData$().pipe(shareReplay(1));

  public paymentForm: FormGroup;
  private paymentSkeleton = {
    apellido1: ['', Validators.required],
    apellido2: ['', Validators.required],
    apellidos: ['', Validators.required],
    cantidadCompra1: ['', Validators.required],
    carencia: ['', Validators.required],
    descCompra1: ['', Validators.required],
    importe: ['', Validators.required],
    nombre: ['', Validators.required],
    precioCompra1: ['', Validators.required],
    precioTotal: ['', Validators.required],
    producto: ['', Validators.required],
    referencia: ['', Validators.required],
    url_acept: ['', Validators.required],
    url_cancel: ['', Validators.required],
    url_confirm: ['', Validators.required],
    url_error: ['', Validators.required],
    url_rechaz: ['', Validators.required],
    vendedor: ['', Validators.required],
  };

  public submitting = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderPaymentService: OrderPaymentService,
    private fb: FormBuilder
  ) {
    this.createPaymentForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(this.cofidisData$.subscribe(this.getPaymentCofidisData));
      this.orderPaymentService.getPaymentCofidisData(
        this.route.snapshot.paramMap.get('orderID')
      );
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private createPaymentForm(): void {
    this.paymentForm = this.fb.group(this.paymentSkeleton);
  }

  get apellido1(): AbstractControl {
    return this.paymentForm.get('apellido1');
  }

  get apellido2(): AbstractControl {
    return this.paymentForm.get('apellido2');
  }

  get apellidos(): AbstractControl {
    return this.paymentForm.get('apellidos');
  }

  get cantidadCompra1(): AbstractControl {
    return this.paymentForm.get('cantidadCompra1');
  }

  get carencia(): AbstractControl {
    return this.paymentForm.get('carencia');
  }

  get descCompra1(): AbstractControl {
    return this.paymentForm.get('descCompra1');
  }

  get importe(): AbstractControl {
    return this.paymentForm.get('importe');
  }

  get nombre(): AbstractControl {
    return this.paymentForm.get('nombre');
  }

  get precioCompra1(): AbstractControl {
    return this.paymentForm.get('precioCompra1');
  }

  get precioTotal(): AbstractControl {
    return this.paymentForm.get('precioTotal');
  }

  get producto(): AbstractControl {
    return this.paymentForm.get('producto');
  }

  get referencia(): AbstractControl {
    return this.paymentForm.get('referencia');
  }

  get url_acept(): AbstractControl {
    return this.paymentForm.get('url_acept');
  }

  get url_cancel(): AbstractControl {
    return this.paymentForm.get('url_cancel');
  }

  get url_confirm(): AbstractControl {
    return this.paymentForm.get('url_confirm');
  }

  get url_error(): AbstractControl {
    return this.paymentForm.get('url_error');
  }

  get url_rechaz(): AbstractControl {
    return this.paymentForm.get('url_rechaz');
  }

  get vendedor(): AbstractControl {
    return this.paymentForm.get('vendedor');
  }

  get validatedForm(): boolean {
    return this.paymentForm.valid && !this.submitting;
  }

  private getPaymentCofidisData = (cofidisData: CofidisData): void => {
    this.paymentForm.patchValue(cofidisData);
  }

  public onPaymentCofidis(): void {
    this.submitting = true;
    this.loading.emit();
  }
}
