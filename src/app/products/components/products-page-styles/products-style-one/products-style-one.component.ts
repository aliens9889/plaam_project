import { Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {
  Links,
  Product,
  ProductCategory,
  ProductService
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-products-style-one',
  templateUrl: './products-style-one.component.html',
  styleUrls: ['./products-style-one.component.scss'],
})
export class ProductsStyleOneComponent implements OnInit, OnDestroy {
  public products$: Observable<
    Product[]
  > = this.productService.getProducts$().pipe(shareReplay(1));

  public productLinks$: Observable<Links> = this.productService
    .getLinks$()
    .pipe(shareReplay(1));

  public productCategory$: Observable<ProductCategory> = this.productService
    .getProductCategory$()
    .pipe(shareReplay(1));

  public language$ = this.allApp.language.getLanguage$().pipe(shareReplay(1));

  isSmall$: Observable<boolean> = this.allApp.breakpointObserver
    .observe([Breakpoints.XSmall, Breakpoints.Small])
    .pipe(
      map((result) => result.matches),
      shareReplay(1)
    );

  public products: Product[] = [];

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  public title = 'All products';
  private currentCategoryName: string;

  private nextPage = 1;

  private endGenealogy = false;

  public sortPriceDirection: string;
  public sortAlphabetically: string;
  public btnAlphabeticalName: string;

  constructor(
    private allApp: AllAppService,
    private productService: ProductService,
    private route: ActivatedRoute
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.products$.subscribe(this.getProducts));
    this.subs.add(this.productLinks$.subscribe(this.getLinks));
    this.subs.add(this.language$.subscribe(this.languageChanged));
    this.route.queryParams.subscribe(this.getQueryParams);
    this.subs.add(this.productCategory$.subscribe(this.getProductCategory));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.initLoaded = false;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private languageChanged = (): void => {
    this.getQueryParams(this.route.snapshot.queryParams);
  }

  private getQueryParams = (queryParams: Params) => {
    this.nextPage = 1;
    this.products = [];
    const params: Params = Object.assign({}, queryParams);
    params.page = this.nextPage;

    if (!queryParams.sortField) {
      this.sortPriceDirection = '';
      this.sortAlphabetically = '';
      this.btnAlphabeticalName = 'A - Z';
    }

    if (queryParams.sortField === 'productName') {
      params.sortField = queryParams.sortField;
      params.sortDirection = queryParams.sortDirection;
    }

    if (queryParams.sortField === 'pricePosition') {
      params.sortField = queryParams.sortField;
      params.sortDirection = queryParams.sortDirection;
    }

    this.productsCalls(params);
  }

  private productsCalls(queryParams: Params): void {
    this.enableLoading();
    this.productService.getProducts(queryParams);
  }

  private getProducts = (products: Product[]): void => {
    this.products.push(...products);
    this.disableLoading();
  }

  public onGetQueryParams(queryParams: Params): void {
    this.allApp.router.navigate(['/products'], { queryParams });
  }

  get getPageTitle(): string {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('categories') && this.currentCategoryName) {
      return this.currentCategoryName;
    } else if (Object.keys(queryParams).length === 1) {
      if (queryParams.hasOwnProperty('novelty')) {
        return this.allApp.translate.instant('Novelties');
      } else if (queryParams.hasOwnProperty('outlet')) {
        return this.allApp.translate.instant('Outlet');
      } else if (queryParams.hasOwnProperty('collections')) {
        return this.allApp.translate.instant('Products by collections');
      } else if (queryParams.hasOwnProperty('brands')) {
        return this.allApp.translate.instant('Products by brand');
      }
    } else if (Object.keys(queryParams).length > 1) {
      return this.allApp.translate.instant('Products by your search criteria');
    }
    return this.allApp.translate.instant('All products');
  }

  get getSortByPriceIcon(): string {
    if (this.sortPriceDirection === 'ASC') {
      return 'fas fa-sort-down mr-1';
    } else if (this.sortPriceDirection === 'DESC') {
      return 'fas fa-sort-up mr-1';
    } else {
      return 'fas fa-sort mr-1';
    }
  }

  get getSortAlphabeticallyIcon(): string {
    if (this.sortAlphabetically === 'ASC') {
      return 'fas fa-sort-down mr-1';
    } else if (this.sortAlphabetically === 'DESC') {
      return 'fas fa-sort-up mr-1';
    } else {
      return 'fas fa-sort mr-1';
    }
  }

  public onSortByPriceProduct(): void {
    this.changePriceSortDirection();
  }

  private changePriceSortDirection(): void {
    if (this.sortPriceDirection === 'ASC') {
      this.sortPriceDirection = 'DESC';
    } else {
      this.sortPriceDirection = 'ASC';
    }

    this.sortAlphabetically = '';
    const queryParams: Params = { ...this.route.snapshot.queryParams };
    queryParams.sortField = 'pricePosition';
    queryParams.sortDirection = this.sortPriceDirection;
    this.allApp.router.navigate(['/products'], { queryParams });
  }

  public onSortAlphabetically(): void {
    this.changeAlphabeticSortDirection();
  }

  private changeAlphabeticSortDirection(): void {
    if (this.sortAlphabetically === 'ASC') {
      this.sortAlphabetically = 'DESC';
      this.btnAlphabeticalName = 'A - Z';
    } else {
      this.sortAlphabetically = 'ASC';
      this.btnAlphabeticalName = 'Z - A';
    }

    this.sortPriceDirection = '';
    const queryParams: Params = { ...this.route.snapshot.queryParams };
    queryParams.sortField = 'productName';
    queryParams.sortDirection = this.sortAlphabetically;
    this.allApp.router.navigate(['/products'], { queryParams });
  }

  get showLoadMoreProducts(): boolean {
    if (!this.nextPage || this.nextPage === 1) {
      return false;
    }
    return true;
  }

  public onLoadMoreProducts(): void {
    const params: Params = Object.assign({}, this.route.snapshot.queryParams);
    params.page = this.nextPage;
    this.productsCalls(params);
  }

  private getLinks = (links: Links): void => {
    this.nextPage = this.productService.getNextPage(links);
  }

  public trackByFn(index, product: Product) {
    return product.productId;
  }

  private getProductCategory = (category: ProductCategory): void => {
    if (this.endGenealogy === false) {
      this.currentCategoryName = category?.name;
      this.endGenealogy = true;
    }
    if (category?.parentId === 0) {
      this.endGenealogy = false;
    }
  }
}
