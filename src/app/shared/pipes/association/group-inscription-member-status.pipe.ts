import { Pipe, PipeTransform } from '@angular/core';
import { GroupInscriptionMemberStatusEnum, GroupInscriptionMemberStatusInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'groupInscriptionMemberStatus',
})
export class GroupInscriptionMemberStatusPipe implements PipeTransform {
  transform(value: GroupInscriptionMemberStatusEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of GroupInscriptionMemberStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
