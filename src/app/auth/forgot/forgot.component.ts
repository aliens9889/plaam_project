import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { OAuthService, UserAccount } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ForgotComponent implements OnInit, OnDestroy {
  public recoverySent = false;

  public InputValidation = InputValidation;

  public forgotForm: FormGroup;
  private forgotSkeleton = {
    username: ['', Validators.compose([Validators.required, Validators.email])],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private oAuthService: OAuthService
  ) {
    this.allApp.toolbar.showBackButton();
    this.createForgotForm();
  }

  ngOnInit(): void {
    this.subs.add(
      this.oAuthService.getUserAccount$().subscribe(this.getUserAccount)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createForgotForm(): void {
    this.forgotForm = this.fb.group(this.forgotSkeleton);
    if (this.hasReCaptcha) {
      const captchaCtl: FormControl = new FormControl('', Validators.required);
      this.forgotForm.addControl('captcha', captchaCtl);
    }
  }

  get username(): AbstractControl {
    return this.forgotForm.get('username');
  }

  get captcha(): AbstractControl {
    return this.forgotForm.get('captcha');
  }

  get validatedForm(): boolean {
    return this.forgotForm.valid && !this.submitting;
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.oAuthService.forgotPassword(this.forgotForm.getRawValue());
    }
  }

  private getUserAccount = (userAccount: UserAccount): void => {
    this.disableLoading();
    this.recoverySent = true;
    // this.allApp.router.navigate(['/auth', 'login']);
  }

  get hasReCaptcha(): boolean {
    return this.allApp.hasReCaptcha;
  }

  public onCaptchaResponse(token: string): void {
    this.captcha.setValue(token);
  }

  public onCaptchaExpired(): void {
    this.captcha.setValue('');
  }
}
