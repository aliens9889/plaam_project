import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRequestMemberComponent } from './edit-request-member.component';

describe('EditRequestMemberComponent', () => {
  let component: EditRequestMemberComponent;
  let fixture: ComponentFixture<EditRequestMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRequestMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRequestMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
