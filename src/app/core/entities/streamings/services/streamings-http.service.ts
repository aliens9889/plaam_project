import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';
import { StreamingStatusEnum } from '../types/streaming-status.enum';

@Injectable({
  providedIn: 'root',
})
export class StreamingsHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getStreamings$(
    userID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/users/${userID}/streamings`;

    const params: Params = Object.assign({}, queryParams);
    params.status = StreamingStatusEnum.ENABLED;
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params });
  }
}
