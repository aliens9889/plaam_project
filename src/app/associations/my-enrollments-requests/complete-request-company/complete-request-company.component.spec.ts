import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteRequestCompanyComponent } from './complete-request-company.component';

describe('CompleteRequestCompanyComponent', () => {
  let component: CompleteRequestCompanyComponent;
  let fixture: ComponentFixture<CompleteRequestCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteRequestCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteRequestCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
