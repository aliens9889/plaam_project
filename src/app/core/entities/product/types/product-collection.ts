export interface ProductCollection {
  articleId: number;
  collectionId: number;
  creationDate: string;
  description: string;
  headerImage: string;
  headerOriginalImage: string;
  image: string;
  importId: number;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  originalImage: string;
  position: number;
  status: number;
  urlBanner: string;
}
