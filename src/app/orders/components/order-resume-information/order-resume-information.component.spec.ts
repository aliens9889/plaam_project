import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderResumeInformationComponent } from './order-resume-information.component';

describe('OrderResumeInformationComponent', () => {
  let component: OrderResumeInformationComponent;
  let fixture: ComponentFixture<OrderResumeInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderResumeInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderResumeInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
