import { TestBed } from '@angular/core/testing';

import { MultimediasService } from './multimedias.service';

describe('MultimediasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MultimediasService = TestBed.inject(MultimediasService);
    expect(service).toBeTruthy();
  });
});
