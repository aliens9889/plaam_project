import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerCompaniesTableComponent } from './partner-companies-table.component';

describe('PartnerCompaniesTableComponent', () => {
  let component: PartnerCompaniesTableComponent;
  let fixture: ComponentFixture<PartnerCompaniesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerCompaniesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerCompaniesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
