import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AssociationService,
  GroupInscriptionMember,
  Order,
  OrderCreatedService,
  OrderUpdateItem,
  OrderUpdateItemEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-requests-members',
  templateUrl: './my-requests-members.component.html',
  styleUrls: ['./my-requests-members.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class MyRequestsMembersComponent implements OnInit, OnDestroy {
  private inscriptions$: Observable<
    GroupInscriptionMember[]
  > = this.associationService.getInscriptionsMembers$().pipe(shareReplay(1));
  public inscriptions: GroupInscriptionMember[];

  private subs: Subscription = new Subscription();

  private addInscription = false;

  public initLoaded = false;

  constructor(
    private allApp: AllAppService,
    private associationService: AssociationService,
    private orderCreatedService: OrderCreatedService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.inscriptions$.subscribe(this.getInscriptions));
    this.associationService.getInscriptionsMembers();

    this.subs.add(
      this.orderCreatedService.getOrderCreated$().subscribe(this.getOrder)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getInscriptions = (inscriptions: GroupInscriptionMember[]): void => {
    this.inscriptions = inscriptions;
    this.disableLoading();
  }

  public onPayInscription(inscription: GroupInscriptionMember): void {
    if (
      inscription &&
      this.associationService.hasProductVariantIDs(inscription)
    ) {
      this.addInscription = true;
      const dataJSON: OrderUpdateItem = {
        action: OrderUpdateItemEnum.REPLACE,
        quantity: 1,
        typeId: inscription?.inscriptionId,
      };
      this.orderCreatedService.updateOrderItemFromProductVariant(
        inscription.group.productId,
        inscription.group.variantId,
        dataJSON
      );
    }
  }

  private getOrder = (order: Order): void => {
    if (this.addInscription && order && order.orderId) {
      this.allApp.router.navigate(['/shopping-cart']);
    }
  }
}
