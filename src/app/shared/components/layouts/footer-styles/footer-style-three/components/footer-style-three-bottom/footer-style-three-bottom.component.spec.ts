import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleThreeBottomComponent } from './footer-style-three-bottom.component';

describe('FooterStyleThreeBottomComponent', () => {
  let component: FooterStyleThreeBottomComponent;
  let fixture: ComponentFixture<FooterStyleThreeBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleThreeBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleThreeBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
