export enum LanguageEnum {
  default = 'es',
  en = 'en',
  es = 'es',
  gl = 'gl',
  ca = 'ca',
}

export type LanguageType =
  | LanguageEnum.en
  | LanguageEnum.es
  | LanguageEnum.gl
  | LanguageEnum.ca;

export const LanguageArray = [
  LanguageEnum.en,
  LanguageEnum.es,
  LanguageEnum.gl,
  LanguageEnum.ca,
];

export const LanguageInfo = [
  {
    en: {
      name: 'English',
    },
  },
  {
    es: {
      name: 'Español',
    },
  },
  {
    gl: {
      name: 'Galego',
    },
  },
  {
    ca: {
      name: 'Català',
    },
  },
];
