import { Component, Input, OnInit } from '@angular/core';
import { Order, OrderUtils } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-order-resume-amounts',
  templateUrl: './order-resume-amounts.component.html',
  styleUrls: ['./order-resume-amounts.component.scss'],
})
export class OrderResumeAmountsComponent implements OnInit {
  public OrderUtils = OrderUtils;

  @Input() order: Order = null;
  @Input() showShippingCost = true;

  constructor() {}

  ngOnInit(): void {}
}
