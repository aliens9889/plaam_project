import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address, UserAddressService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-address-card',
  templateUrl: './address-card.component.html',
  styleUrls: ['./address-card.component.scss'],
})
export class AddressCardComponent implements OnInit {
  @Input() address: Address = null;

  @Output() editAddress: EventEmitter<Address> = new EventEmitter();
  @Output() deleteAddress: EventEmitter<Address> = new EventEmitter();

  constructor(private userAddressService: UserAddressService) {}

  ngOnInit(): void {}

  public onEditAddress(): void {
    this.editAddress.emit(this.address);
  }

  public onDeleteAddress(): void {
    if (!this.isAddressDefault) {
      this.deleteAddress.emit(this.address);
    }
  }

  get isAddressDefault(): boolean {
    return this.userAddressService.isAddressDefault(this.address);
  }
}
