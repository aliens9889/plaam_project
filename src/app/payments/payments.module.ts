import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { BankChargeSuccessComponent } from './bank-charge-success/bank-charge-success.component';
import { BillingInformationComponent } from './billing-information/billing-information.component';
import { BillingClientPreviewComponent } from './components/billing-client-preview/billing-client-preview.component';
import { BillingCreateClientDialogComponent } from './components/billing-create-client-dialog/billing-create-client-dialog.component';
import { PaymentAplazameComponent } from './components/payment-aplazame/payment-aplazame.component';
import { PaymentBankChargeDialogComponent } from './components/payment-bank-charge-dialog/payment-bank-charge-dialog.component';
import { PaymentBankChargeComponent } from './components/payment-bank-charge/payment-bank-charge.component';
import { PaymentCecabankComponent } from './components/payment-cecabank/payment-cecabank.component';
import { PaymentCofidisComponent } from './components/payment-cofidis/payment-cofidis.component';
import { PaymentPaycometComponent } from './components/payment-paycomet/payment-paycomet.component';
import { PaymentPaypalComponent } from './components/payment-paypal/payment-paypal.component';
import { PaymentRedsysComponent } from './components/payment-redsys/payment-redsys.component';
import { PaymentStripeComponent } from './components/payment-stripe/payment-stripe.component';
import { ShippingRadioComponent } from './components/shipping-radio/shipping-radio.component';
import { PaidFailureComponent } from './paid-failure/paid-failure.component';
import { PaidSuccessComponent } from './paid-success/paid-success.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { PaymentsRoutingModule } from './payments-routing.module';
import { ShippingInformationComponent } from './shipping-information/shipping-information.component';

@NgModule({
  declarations: [
    BankChargeSuccessComponent,
    BillingClientPreviewComponent,
    BillingCreateClientDialogComponent,
    BillingInformationComponent,
    PaidFailureComponent,
    PaidSuccessComponent,
    PaymentAplazameComponent,
    PaymentBankChargeComponent,
    PaymentBankChargeDialogComponent,
    PaymentCecabankComponent,
    PaymentCofidisComponent,
    PaymentMethodComponent,
    PaymentPaycometComponent,
    PaymentPaypalComponent,
    PaymentRedsysComponent,
    PaymentStripeComponent,
    ShippingInformationComponent,
    ShippingRadioComponent,
  ],
  imports: [SharedModule, PaymentsRoutingModule, TranslateModule],
})
export class PaymentsModule {}
