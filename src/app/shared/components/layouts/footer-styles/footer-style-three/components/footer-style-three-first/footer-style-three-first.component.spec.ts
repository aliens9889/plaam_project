import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleThreeFirstComponent } from './footer-style-three-first.component';

describe('FooterStyleThreeFirstComponent', () => {
  let component: FooterStyleThreeFirstComponent;
  let fixture: ComponentFixture<FooterStyleThreeFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleThreeFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleThreeFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
