import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {
  AssembliesService,
  Assembly,
  AssemblyDelegated,
  AssemblyStatusEnum,
  AssemblyTopic,
  AssemblyVoteResultEnum,
  AssociationLicenseService,
  AssociationSnackbarsService,
  LicenseMember
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AssemblyVoteDialogComponent } from '../components/dialogs/assembly-vote-dialog/assembly-vote-dialog.component';

@Component({
  selector: 'qaroni-assembly-details',
  templateUrl: './assembly-details.component.html',
  styleUrls: ['./assembly-details.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class AssemblyDetailsComponent implements OnInit, OnDestroy {
  private hasLicenseMemberID: boolean = this.route.snapshot.paramMap.has(
    'licenseMemberID'
  );

  private licenseMemberID: string = this.route.snapshot.paramMap.get(
    'licenseMemberID'
  );

  private hasAssemblyID: boolean = this.route.snapshot.paramMap.has(
    'assemblyID'
  );

  private assemblyID: string = this.route.snapshot.paramMap.get('assemblyID');

  public initLoaded = false;

  public assembly$: Observable<Assembly> = this.assembliesService
    .getAssembly$()
    .pipe(shareReplay(1));

  public assembly: Assembly;

  public topic$: Observable<AssemblyTopic> = this.assembliesService
    .getAssemblyTopic$()
    .pipe(shareReplay(1));

  public delegates$: Observable<
    AssemblyDelegated[]
  > = this.assembliesService.getAssemblyDelegates$().pipe(shareReplay(1));
  public delegates: AssemblyDelegated[];
  public delegatesString: string;

  private licenseMember$: Observable<LicenseMember> = this.licenseService
    .getLicenseMember$()
    .pipe(shareReplay(1));
  public licenseMember: LicenseMember;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private assembliesService: AssembliesService,
    private associationSnackbars: AssociationSnackbarsService,
    private licenseService: AssociationLicenseService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.hasLicenseMemberID && this.hasAssemblyID) {
      this.subs.add(this.assembly$.subscribe(this.getAssembly));
      this.assembliesService.getAssembly(this.licenseMemberID, this.assemblyID);
      this.subs.add(this.delegates$.subscribe(this.getAssemblyDelegates));
      this.assembliesService.getAssemblyDelegates(
        this.licenseMemberID,
        this.assemblyID
      );
      this.subs.add(this.licenseMember$.subscribe(this.getLicenseMember));
      this.licenseService.getLicenseMember(this.licenseMemberID);
      this.subs.add(this.topic$.subscribe(this.getAssemblyTopic));
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private getAssembly = (assembly: Assembly): void => {
    if (assembly) {
      this.assembly = assembly;
    }
    this.disableLoading();
  }

  private getAssemblyDelegates = (delegates: any): void => {
    if (delegates) {
      this.delegates = delegates;
      this.getDelegatesString();
    }
    this.disableLoading();
  }

  private getDelegatesString(): void {
    if (this.delegates?.length) {
      this.delegatesString = '';
      for (const [index, delegated] of this.delegates.entries()) {
        this.delegatesString += delegated.firstName + ' ' + delegated.lastName;
        if (index < this.delegates.length - 1) {
          this.delegatesString += ', ';
        } else {
          this.delegatesString += '.';
        }
      }
    }
    return null;
  }

  private getLicenseMember = (licenseMember: LicenseMember): void => {
    if (licenseMember) {
      this.licenseMember = licenseMember;
    }
    this.disableLoading();
  }

  public canVoteTopic(topic: AssemblyTopic): boolean {
    if (this.assembly?.hasAttended && topic?.topicId && !topic?.hasVoted) {
      if (
        topic?.status === AssemblyStatusEnum.CREATED ||
        topic?.status === AssemblyStatusEnum.OPEN
      ) {
        return true;
      }
    }
    return false;
  }

  public onVoteTopic(topic: AssemblyTopic): void {
    if (this.hasLicenseMemberID && this.hasAssemblyID && topic?.topicId) {
      this.assembliesService.getAssemblyTopic(
        this.licenseMemberID,
        this.assemblyID,
        topic?.topicId
      );
    }
  }

  private getAssemblyTopic = (topic: AssemblyTopic): void => {
    if (topic) {
      if (topic?.status === AssemblyStatusEnum.OPEN) {
        const matDialogConfig = new MatDialogConfig();
        matDialogConfig.width = '700px';
        matDialogConfig.maxWidth = '90vw';
        matDialogConfig.panelClass = 'style-confirm-dialog';
        matDialogConfig.autoFocus = false;
        matDialogConfig.data = topic;

        const dialog = this.allApp.dialog.open<
          AssemblyVoteDialogComponent,
          AssemblyTopic,
          AssemblyVoteResultEnum
        >(AssemblyVoteDialogComponent, matDialogConfig);

        this.subs.add(
          dialog.afterClosed().subscribe((vote: AssemblyVoteResultEnum) => {
            if (
              this.hasLicenseMemberID &&
              this.hasAssemblyID &&
              topic?.topicId &&
              vote
            ) {
              this.assembliesService.voteAssemblyTopic(
                this.licenseMemberID,
                this.assemblyID,
                topic?.topicId,
                vote
              );
            }
          })
        );
      } else {
        this.associationSnackbars.failureVoteTopicStatus();
      }
    }
  }
}
