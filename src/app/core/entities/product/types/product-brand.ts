export interface ProductBrand {
  brandId: number;
  code: string;
  creationDate: string;
  image: string;
  lastUpdateDate: string;
  logo: string;
  longDescription: string;
  merchantId: number;
  name: string;
  position: number;
  shortDescription: string;
  showOnCover: number;
  status: number;
  url: string;
}

// export enum HeaderImage {
//   Empty = '',
//   The1837_20190403121253_GarminportadaJpg = '1837_20190403121253_garminportada.jpg',
// }

// export enum HeaderOriginalImage {
//   AlpcrossPNG = 'alpcross.png',
//   Empty = '',
//   GarminPortadaJpg = 'garmin_portada.jpg',
// }
