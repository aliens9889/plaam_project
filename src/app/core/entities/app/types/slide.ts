export interface Slide {
  buttonAction: string;
  buttonName: string;
  creationDate: string;
  description: string;
  imageUrl: string;
  internal: boolean;
  language: string;
  lastUpdateDate: string;
  local: boolean;
  merchantId: number;
  position: number;
  slideId: number;
  status: string;
  title: string;
  type: string;
}
