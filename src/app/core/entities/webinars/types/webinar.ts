import { LanguageEnum } from '@qaroni-app/core/types';
import { Address } from '../../address';

export interface Webinar {
  address: Address;
  capacity: number;
  categoryId: number;
  code: number;
  creationDate: string;
  eventId: number;
  finishDate: string;
  imagesURL: string[];
  language: LanguageEnum;
  largeDescription: string;
  lastUpdateDate: string;
  location: Location;
  merchantId: number;
  place: string;
  shortDescription: string;
  startDate: string;
  state: string;
  title: string;
  transmissionId: string;
  type: string;
}
