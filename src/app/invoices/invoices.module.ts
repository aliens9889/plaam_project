import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { InvoicesRoutingModule } from './invoices-routing.module';
import { OrderInvoiceComponent } from './order-invoice/order-invoice.component';

@NgModule({
  declarations: [OrderInvoiceComponent],
  imports: [SharedModule, InvoicesRoutingModule, TranslateModule]
})
export class InvoicesModule {}
