import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qaroni-page-title-style-material',
  templateUrl: './page-title-style-material.component.html',
  styleUrls: ['./page-title-style-material.component.scss'],
})
export class PageTitleStyleMaterialComponent implements OnInit {
  @Input() title: string;

  @Input() titleClass: string;

  constructor() {}

  ngOnInit(): void {}
}
