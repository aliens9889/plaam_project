import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FormField } from '../types/form-field';
import { FormHttpService } from './form-http.service';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  private formFields$ = new Subject<FormField[]>();

  constructor(
    private allApp: AllAppService,
    private formHttp: FormHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Form Fields
  // ==========================================================================================
  public getFormFields$(): Observable<FormField[]> {
    return this.formFields$.asObservable();
  }

  public getFormFields(formID: number | string): void {
    this.enableLoading();
    this.formHttp
      .getFormFields$(formID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetFormFields, this.failureGetFormFields);
  }

  private successGetFormFields = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const formFields: FormField[] = data.body.result;
      this.formFields$.next(formFields);
    } else {
      this.formFields$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetFormFields = (error: HttpErrorResponse): void => {
    this.formFields$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
