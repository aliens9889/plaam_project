import { NgModule } from '@angular/core';
import { SharedModule } from '@qaroni-app/shared';
import { ShoppingCartAmountsComponent } from './components/shopping-cart-amounts/shopping-cart-amounts.component';
import { ShoppingCartOrderItemComponent } from './components/shopping-cart-order-item/shopping-cart-order-item.component';
import { ShoppingCartOrderComponent } from './components/shopping-cart-order/shopping-cart-order.component';
import { ShoppingCartRoutingModule } from './shopping-cart-routing.module';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ShoppingCartAmountsComponent,
    ShoppingCartComponent,
    ShoppingCartOrderComponent,
    ShoppingCartOrderItemComponent
  ],
  imports: [SharedModule, ShoppingCartRoutingModule, TranslateModule]
})
export class ShoppingCartModule {}
