import { Component, Input, OnInit } from '@angular/core';
import { LandingEnv } from '@qaroni-app/core/config';
import { Slide } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-slides-carousel',
  templateUrl: './slides-carousel.component.html',
  styleUrls: ['./slides-carousel.component.scss'],
})
export class SlidesCarouselComponent implements OnInit {
  @Input() slides: Slide[];

  public LandingEnv = LandingEnv;

  constructor() {}

  ngOnInit(): void {}

  public getIfActive(i: number): boolean {
    if (i === 0) {
      return true;
    }
    return false;
  }

  public getBgImageSrc(slide: Slide): string | null {
    return 'url(' + slide.imageUrl + ')';
  }

  public getButtonAction(action: string): string[] {
    return [action];
  }

  get showSlideButton(): boolean {
    return LandingEnv.showSlideButton;
  }
}
