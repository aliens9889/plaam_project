import { PostAuthorStatusEnum } from './post-author-status.enum';
import { PostCategoryStatusEnum } from './post-category-status.enum';
import { PostStatusEnum } from './post-status.enum';
import { PostTagStatusEnum } from './post-tag-status.enum';

export interface Post {
  authors: PostAuthor[];
  categories: PostCategory[];
  creationDate: string;
  date: string;
  descriptions: PostDescription[];
  featured: boolean;
  imageUrl: string;
  lastUpdateDate: string;
  merchantId: number;
  postId: number;
  status: PostStatusEnum;
  tags: PostTag[];
}

export interface PostAuthor {
  authorId: number;
  creationDate: string;
  description: string;
  firstName: string;
  imageUrl: null;
  lastName: string;
  lastUpdateDate: string;
  merchantId: number;
  position: string;
  status: PostAuthorStatusEnum;
}

export interface PostCategory {
  categoryId?: number;
  creationDate: string;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  status: PostCategoryStatusEnum;
  tagId?: number;
}

export interface PostDescription {
  creationDate: string;
  language: string;
  largeDescription: string;
  lastUpdateDate: string;
  shortDescription: string;
  subtitle: string;
  title: string;
}

export interface PostTag {
  categoryId?: number;
  creationDate: string;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  status: PostTagStatusEnum;
  tagId?: number;
}
