import { TestBed } from '@angular/core/testing';

import { UserDataHttpService } from './user-data-http.service';

describe('UserDataHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserDataHttpService = TestBed.inject(UserDataHttpService);
    expect(service).toBeTruthy();
  });
});
