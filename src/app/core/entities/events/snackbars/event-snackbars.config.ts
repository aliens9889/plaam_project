import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { SnackbarConfig } from '@qaroni-app/core/config';

export const EventSnackbars = {
  warningRegister: {
    message: `To register you need to login`,
    closeBtn: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.warning,
      duration: SnackbarConfig.durations.warning,
    } as MatSnackBarConfig,
  },
};
