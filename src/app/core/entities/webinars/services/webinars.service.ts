import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Webinar } from '../types/webinar';
import { WebinarsHttpService } from './webinars-http.service';

@Injectable({
  providedIn: 'root',
})
export class WebinarsService {
  private webinars$ = new Subject<Webinar[]>();

  constructor(
    private allApp: AllAppService,
    private webinarsHttp: WebinarsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getWebinarImageUrl(webinar: Webinar): string | null {
    if (webinar.imagesURL && webinar.imagesURL.length && webinar.imagesURL[0]) {
      return webinar.imagesURL[0];
    } else {
      return null;
    }
  }

  // ==========================================================================================
  // Get Webinars
  // ==========================================================================================

  public getWebinars$(): Observable<Webinar[]> {
    return this.webinars$.asObservable();
  }

  public getWebinars(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.webinarsHttp
        .getWebinars$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetWebinars, this.failureGetWebinars);
    }
  }

  private successGetWebinars = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const webinars: Webinar[] = data.body.result;
      this.webinars$.next(webinars);
    } else {
      this.webinars$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetWebinars = (error: HttpErrorResponse): void => {
    this.webinars$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
