import { TestBed } from '@angular/core/testing';

import { SubscriptionsHttpService } from './subscriptions-http.service';

describe('SubscriptionsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubscriptionsHttpService = TestBed.inject(SubscriptionsHttpService);
    expect(service).toBeTruthy();
  });
});
