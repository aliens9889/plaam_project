import { TestBed } from '@angular/core/testing';

import { LanguageHttpService } from './language-http.service';

describe('LanguageHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LanguageHttpService = TestBed.inject(LanguageHttpService);
    expect(service).toBeTruthy();
  });
});
