import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { NgxGalleryImage } from '@kolkov/ngx-gallery';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Links } from '../../app';
import { OAuthService } from '../../o-auth';
import { Product } from '../types/product';
import { ProductAttribute } from '../types/product-attribute';
import { ProductBrand } from '../types/product-brand';
import {
  ProductCategory,
  SectionCategoryProducts
} from '../types/product-category';
import { ProductCollection } from '../types/product-collection';
import { ProductUnitTypeEnum } from '../types/product-unit-type';
import { ProductsHttpService } from './products-http.service';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private products$ = new Subject<Product[]>();
  private product$ = new Subject<Product>();

  private links$ = new Subject<Links>();

  private productBrands$ = new Subject<ProductBrand[]>();
  private productBrand$ = new Subject<ProductBrand>();
  private productCategories$ = new Subject<ProductCategory[]>();
  private productCategory$ = new Subject<ProductCategory>();
  private productCollections$ = new Subject<ProductCollection[]>();
  private productAttributes$ = new Subject<ProductAttribute[]>();

  private sectionCategoryProducts$ = new Subject<SectionCategoryProducts>();

  constructor(
    private allApp: AllAppService,
    private productsHttp: ProductsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getOneImageUrl(product: Product): string {
    if (
      product &&
      product.variant &&
      product.variant.imagesURL &&
      product.variant.imagesURL.length &&
      product.variant.imagesURL[0]
    ) {
      return product.variant.imagesURL[0];
    } else if (
      product &&
      product.imagesURL &&
      product.imagesURL.length &&
      product.imagesURL[0]
    ) {
      return product.imagesURL[0];
    } else {
      return null;
    }
  }

  public getAllImagesUrl(product: Product): string[] {
    const images: string[] = [];
    if (
      product &&
      product.variant &&
      product.variant.imagesURL &&
      product.variant.imagesURL.length
    ) {
      for (const imageUrl of product.variant.imagesURL) {
        if (images.indexOf(imageUrl) === -1) {
          images.push(imageUrl);
        }
      }
    }
    if (product && product.imagesURL && product.imagesURL.length) {
      for (const imageUrl of product.imagesURL) {
        if (images.indexOf(imageUrl) === -1) {
          images.push(imageUrl);
        }
      }
    }
    return images;
  }

  public getNgxGalleryImages(product: Product): NgxGalleryImage[] {
    const images: string[] = this.getAllImagesUrl(product);
    const galleryImages: NgxGalleryImage[] = [];
    if (images.length) {
      images.forEach((image: string) => {
        galleryImages.push({
          small: image,
          medium: image,
          big: image,
        });
      });
    }
    return galleryImages;
  }

  public getUrlName(product: Product): string {
    if (product && product.name) {
      const urlName = product.name
        .replace(/  +/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase();
      return urlName;
    }
    return '';
  }

  public hasVariantOffer(product: Product): boolean {
    if (product && product.variant && product.variant.offer) {
      if (product.variant.offer !== 0) {
        return true;
      }
    }
    return false;
  }

  public getLabelOfferString(product: Product): string {
    if (this.hasVariantOffer(product)) {
      return (
        (-100 + (product.variant.offer * 100) / product.variant.price).toFixed(
          0
        ) + ' %'
      );
    } else {
      return null;
    }
  }

  public isProductOutlet(product: Product): boolean {
    if (product && product.outlet) {
      if (product.outlet === 1) {
        return true;
      }
    }
    return false;
  }

  public isProductNovelty(product: Product): boolean {
    if (product && product.novelty) {
      if (product.novelty === 1) {
        return true;
      }
    }
    return false;
  }

  public isUnitTypeMetric(product: Product): boolean {
    if (
      product &&
      product.unitType &&
      product.unitType === ProductUnitTypeEnum.METRIC
    ) {
      return true;
    }
    return false;
  }

  public hasUnitName(product: Product): boolean {
    if (product && product.unitName && product.unitName !== '') {
      return true;
    }
    return false;
  }

  public getNextPage(links: Links): number {
    if (links && links.next) {
      const urlArr: string[] = links.next.split('?');
      if (urlArr.length === 2) {
        const page = JSON.parse(
          '{"' +
            decodeURI(urlArr[1].replace(/&/g, '","').replace(/=/g, '":"')) +
            '"}'
        );
        if (page.hasOwnProperty('page')) {
          return parseInt(page.page, 10);
        }
      }
    }
    return null;
  }

  // ==========================================================================================
  // Get Products
  // ==========================================================================================

  public getProducts$(): Observable<Product[]> {
    return this.products$.asObservable();
  }

  public getProducts(queryParams?: Params): void {
    this.enableLoading();
    this.productsHttp
      .getProducts$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetProducts, this.failureGetProducts);
  }

  private successGetProducts = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const products: Product[] = data.body.result;
      this.products$.next(products);
      const links: Links = data.body.links;
      this.links$.next(links);
    } else {
      this.products$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProducts = (error: HttpErrorResponse): void => {
    this.products$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  public getProductsCategorySection$(): Observable<SectionCategoryProducts> {
    return this.sectionCategoryProducts$.asObservable();
  }

  public getProductsCategorySection(queryParams?: Params): void {
    this.enableLoading();
    this.productsHttp
      .getProducts$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(
        this.successGetProductsCategorySection,
        this.failureGetProductsCategorySection
      );
  }

  private successGetProductsCategorySection = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      let category = null;
      if (
        data.body.included &&
        data.body.included.categories &&
        data.body.included.categories.length
      ) {
        category = data.body.included.categories[0];
      }
      const section: SectionCategoryProducts = {
        category,
        products: data.body.result,
      };
      this.sectionCategoryProducts$.next(section);
    } else {
      this.sectionCategoryProducts$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductsCategorySection = (
    error: HttpErrorResponse
  ): void => {
    this.sectionCategoryProducts$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product
  // ==========================================================================================
  public getProduct$(): Observable<Product> {
    return this.product$.asObservable();
  }

  public getProduct(productID: number | string): void {
    this.enableLoading();
    this.productsHttp
      .getProduct$(productID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetProduct, this.failureGetProduct);
  }

  public getProductByOptions(
    productID: number | string,
    queryParams?: Params
  ): void {
    this.enableLoading();
    this.productsHttp
      .getProductByOptions$(productID, queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetProduct, this.failureGetProduct);
  }

  private successGetProduct = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const product: Product = data.body.result[0];
      this.product$.next(product);
    } else {
      this.product$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProduct = (error: HttpErrorResponse): void => {
    this.product$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product Links
  // ==========================================================================================
  public getLinks$(): Observable<Links> {
    return this.links$.asObservable();
  }

  // ==========================================================================================
  // Get Product Brands
  // ==========================================================================================
  public getProductBrands$(): Observable<ProductBrand[]> {
    return this.productBrands$.asObservable();
  }

  public getProductBrands(queryParams?: Params): void {
    this.enableLoading();
    this.productsHttp
      .getProductBrands$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetProductBrands, this.failureGetProductBrands);
  }

  private successGetProductBrands = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const productBrands: ProductBrand[] = data.body.result;
      this.productBrands$.next(productBrands);
    } else {
      this.productBrands$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductBrands = (error: HttpErrorResponse): void => {
    this.productBrands$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product Brand
  // ==========================================================================================
  public getProductBrand$(): Observable<ProductBrand> {
    return this.productBrand$.asObservable();
  }

  public getProductBrand(brandId: number | string): void {
    this.enableLoading();
    this.productsHttp
      .getProductBrand$(brandId)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetProductBrand, this.failureGetProductBrand);
  }

  private successGetProductBrand = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const productBrand: ProductBrand = data.body.result[0];
      this.productBrand$.next(productBrand);
    } else {
      this.productBrand$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductBrand = (error: HttpErrorResponse): void => {
    this.productBrand$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product Categories
  // ==========================================================================================
  public getProductCategories$(): Observable<ProductCategory[]> {
    return this.productCategories$.asObservable();
  }

  public getProductCategories(queryParams?: Params): void {
    this.enableLoading();
    this.productsHttp
      .getProductCategories$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(
        this.successGetProductCategories,
        this.failureGetProductCategories
      );
  }

  private successGetProductCategories = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const productCategories: ProductCategory[] = data.body.result;
      this.productCategories$.next(productCategories);
    } else {
      this.productCategories$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductCategories = (error: HttpErrorResponse): void => {
    this.productCategories$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product Category
  // ==========================================================================================
  public getProductCategory$(): Observable<ProductCategory> {
    return this.productCategory$.asObservable();
  }

  public getProductCategory(categoryId: number | string): void {
    this.enableLoading();
    this.productsHttp
      .getProductCategory$(categoryId)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(
        this.successGetProductCategory,
        this.failureGetProductCategory
      );
  }

  private successGetProductCategory = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const productCategory: ProductCategory = data.body.result[0];
      this.productCategory$.next(productCategory);
    } else {
      this.productCategory$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductCategory = (error: HttpErrorResponse): void => {
    this.productCategory$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product Collections
  // ==========================================================================================
  public getProductCollections$(): Observable<ProductCollection[]> {
    return this.productCollections$.asObservable();
  }

  public getProductCollections(queryParams?: Params): void {
    this.enableLoading();
    this.productsHttp
      .getProductCollections$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(
        this.successGetProductCollections,
        this.failureGetProductCollections
      );
  }

  private successGetProductCollections = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const productCollections: ProductCollection[] = data.body.result;
      this.productCollections$.next(productCollections);
    } else {
      this.productCollections$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductCollections = (error: HttpErrorResponse): void => {
    this.productCollections$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Product Attributes
  // ==========================================================================================
  public getProductAttributes$(): Observable<ProductAttribute[]> {
    return this.productAttributes$.asObservable();
  }

  public getProductAttributes(queryParams?: Params): void {
    this.enableLoading();
    this.productsHttp
      .getProductAttributes$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(
        this.successGetProductAttributes,
        this.failureGetProductAttributes
      );
  }

  private successGetProductAttributes = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const productAttributes: ProductAttribute[] = data.body.result;
      this.productAttributes$.next(productAttributes);
    } else {
      this.productAttributes$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProductAttributes = (error: HttpErrorResponse): void => {
    this.productAttributes$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
