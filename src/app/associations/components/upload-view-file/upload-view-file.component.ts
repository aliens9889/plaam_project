import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GroupInscriptionFile } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { FileSnackbars } from '@qaroni-app/core/types';

@Component({
  selector: 'qaroni-upload-view-file',
  templateUrl: './upload-view-file.component.html',
  styleUrls: ['./upload-view-file.component.scss'],
})
export class UploadViewFileComponent implements OnInit {
  @Input() file: GroupInscriptionFile;

  @Input() submitting: boolean;

  @Output() fileChanged: EventEmitter<File[]> = new EventEmitter();

  constructor(private allApp: AllAppService) {}

  ngOnInit(): void {}

  public onFileChanged(event) {
    if (event.target.files.length) {
      const rawArrayFiles = Array.from(
        (event.target as HTMLInputElement).files
      );

      if (
        this.allApp.FilesValidations.validateDocuments(rawArrayFiles) ||
        this.allApp.FilesValidations.validateImages(rawArrayFiles)
      ) {
        const theFile = this.allApp.FilesValidations.allowOneFile(
          rawArrayFiles
        );

        this.fileChanged.emit(theFile);
      } else {
        this.allApp.snackbar.open(
          FileSnackbars.failureFile.message,
          FileSnackbars.failureFile.action,
          FileSnackbars.failureFile.config
        );
      }
    }
  }

  get hasFile(): boolean {
    if (this.file && this.file.fileUrl) {
      return true;
    }
    return false;
  }

  get fileIsDocument(): boolean {
    if (this.hasFile) {
      const fileExt = this.file.fileUrl.split('.').pop();
      if (fileExt === 'pdf') {
        return true;
      }
    }
    return false;
  }
}
