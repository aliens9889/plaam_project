import { TestBed } from '@angular/core/testing';

import { AssociationHttpService } from './association-http.service';

describe('AssociationHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssociationHttpService = TestBed.inject(AssociationHttpService);
    expect(service).toBeTruthy();
  });
});
