import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from '@qaroni-app/core/guards';
import { BankChargeSuccessComponent } from './bank-charge-success/bank-charge-success.component';
import { BillingInformationComponent } from './billing-information/billing-information.component';
import { PaidFailureComponent } from './paid-failure/paid-failure.component';
import { PaidSuccessComponent } from './paid-success/paid-success.component';
import { PaymentMethodComponent } from './payment-method/payment-method.component';
import { ShippingInformationComponent } from './shipping-information/shipping-information.component';

const routes: Routes = [
  {
    path: ':orderID/shippings',
    component: ShippingInformationComponent,
    canActivate: [UserGuard],
  },
  {
    path: ':orderID/billing',
    component: BillingInformationComponent,
    canActivate: [UserGuard],
  },
  {
    path: ':orderID/methods',
    component: PaymentMethodComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'success',
    component: PaidSuccessComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'bank-charge/success',
    component: BankChargeSuccessComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'failure',
    component: PaidFailureComponent,
    canActivate: [UserGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentsRoutingModule {}
