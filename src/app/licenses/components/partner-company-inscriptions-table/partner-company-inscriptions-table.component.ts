import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import {
  AssociationPartnerService,
  GroupInscriptionCompany,
  PartnerCompanyInscriptionDataSource
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-partner-company-inscriptions-table',
  templateUrl: './partner-company-inscriptions-table.component.html',
  styleUrls: ['./partner-company-inscriptions-table.component.scss'],
})
export class PartnerCompanyInscriptionsTableComponent
  implements OnInit, AfterViewInit {
  @Input() showPaginator = true;

  @Input() partnerID: number | string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) table: MatTable<GroupInscriptionCompany>;
  public dataSource: PartnerCompanyInscriptionDataSource;

  public displayedColumns: string[] = [
    'creationDate',
    'name',
    'tradename',
    'cif',
    'email',
    'phone',
    'type',
    'status',
  ];

  constructor(private partnerService: AssociationPartnerService) {}

  ngOnInit(): void {
    this.dataSource = new PartnerCompanyInscriptionDataSource(
      this.partnerID,
      this.partnerService
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
