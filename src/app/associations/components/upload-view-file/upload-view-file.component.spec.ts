import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadViewFileComponent } from './upload-view-file.component';

describe('UploadViewFileComponent', () => {
  let component: UploadViewFileComponent;
  let fixture: ComponentFixture<UploadViewFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadViewFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadViewFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
