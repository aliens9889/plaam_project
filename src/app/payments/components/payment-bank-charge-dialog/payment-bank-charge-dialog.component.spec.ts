import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentBankChargeDialogComponent } from './payment-bank-charge-dialog.component';

describe('PaymentBankChargeDialogComponent', () => {
  let component: PaymentBankChargeDialogComponent;
  let fixture: ComponentFixture<PaymentBankChargeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentBankChargeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentBankChargeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
