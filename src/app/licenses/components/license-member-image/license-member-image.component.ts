import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import {
  AssociationLicenseService,
  LicenseMember,
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData, FileSnackbars } from '@qaroni-app/core/types';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { ConfirmationDialogComponent } from '@qaroni-app/shared/components/dialogs';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-member-image',
  templateUrl: './license-member-image.component.html',
  styleUrls: ['./license-member-image.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseMemberImageComponent implements OnInit, OnDestroy {
  @Input() licenseMember: LicenseMember;

  public images: any = null;
  public imageUrl: string = null;

  private licenseMember$: Observable<
    LicenseMember
  > = this.licenseService.getLicenseMember$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  private submitting = false;

  constructor(
    private allApp: AllAppService,
    private licenseService: AssociationLicenseService
  ) {}

  ngOnInit(): void {
    this.subs.add(this.licenseMember$.subscribe(this.getLicenseMember));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private getLicenseMember = (licenseMember: LicenseMember): void => {
    this.disableLoading();
  }

  get validatedForm(): boolean {
    return !this.submitting;
  }

  public onFileChanged(event): void {
    if (event.target.files.length) {
      const rawArrayImages = Array.from(
        (event.target as HTMLInputElement).files
      );
      if (this.allApp.FilesValidations.validateImages(rawArrayImages)) {
        this.images = this.allApp.FilesValidations.allowOneFile(rawArrayImages);

        let dataMultipart = new FormData();
        dataMultipart = this.allApp.BuildHttpData.buildMultipartWithImages(
          dataMultipart,
          'image',
          this.images
        );

        this.enableLoading();

        this.licenseService.uploadLicenseMemberImage(
          this.licenseMember?.memberId,
          dataMultipart
        );
      } else {
        this.allApp.snackbar.open(
          FileSnackbars.failureImage.message,
          FileSnackbars.failureImage.action,
          FileSnackbars.failureImage.config
        );
      }
    }
  }

  public onDeleteImage(imageUrl: string): void {
    const confirmationData: ConfirmationDialogData = {
      title: this.allApp.translate.instant(`Delete image?`),
      message: this.allApp.translate.instant(
        `If you delete the image you will not be able to recover it, you can upload a new one`
      ),
      confirmFaIcon: `far fa-trash-alt`,
      confirmText: this.allApp.translate.instant(`Yes, delete`),
    };

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '700px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.data = confirmationData;

    const dialog = this.allApp.dialog.open<
      ConfirmationDialogComponent,
      ConfirmationDialogData,
      boolean
    >(ConfirmationDialogComponent, matDialogConfig);

    dialog.afterClosed().subscribe((confirmation) => {
      if (confirmation === true) {
        this.enableLoading();
        this.licenseService.deleteLicenseMemberImage(
          this.licenseMember?.memberId
        );
      }
    });
  }

  get title(): string {
    return this.licenseService.strings.getTitleLicenseMemberImageEdit();
  }
}
