import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';
import { AssemblyVoteResultEnum } from '../types/assembly-vote-result.enum';

@Injectable({
  providedIn: 'root',
})
export class AssembliesHttpService {
  constructor(private http: HttpClient) {}

  // ==========================================================================================
  // Get Assemblies
  // ==========================================================================================

  public getAssemblies$(
    userID: number | string,
    memberID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/votes/assemblies`;

    return this.http.get(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Get Assembly
  // ==========================================================================================

  public getAssembly$(
    userID: number | string,
    memberID: number | string,
    assemblyID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/votes/assemblies/${assemblyID}`;

    return this.http.get(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Get Assembly Delegates
  // ==========================================================================================

  public getAssemblyDelegates$(
    userID: number | string,
    memberID: number | string,
    assemblyID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/votes/assemblies/${assemblyID}/delegates`;

    return this.http.get(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Get Assembly Topic
  // ==========================================================================================

  public getAssemblyTopic$(
    userID: number | string,
    memberID: number | string,
    assemblyID: number | string,
    topicID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/votes/assemblies/${assemblyID}/topics/${topicID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public voteAssemblyTopic$(
    userID: number | string,
    memberID: number | string,
    assemblyID: number | string,
    topicID: number | string,
    vote: AssemblyVoteResultEnum
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/votes/assemblies/${assemblyID}/topics/${topicID}`;

    return this.http.post(url, { result: vote }, { observe: 'response' });
  }
}
