import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsSectionTwoComponent } from './products-section-two.component';

describe('ProductsSectionTwoComponent', () => {
  let component: ProductsSectionTwoComponent;
  let fixture: ComponentFixture<ProductsSectionTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsSectionTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsSectionTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
