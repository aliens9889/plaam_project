import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qaroni-input-preview',
  templateUrl: './input-preview.component.html',
  styleUrls: ['./input-preview.component.scss']
})
export class InputPreviewComponent implements OnInit {
  @Input() label: string = null;
  @Input() input: string = null;

  constructor() {}

  ngOnInit(): void {}
}
