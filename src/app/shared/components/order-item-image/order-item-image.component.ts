import { Component, Input, OnInit } from '@angular/core';
import { Item, OrderService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-order-item-image',
  templateUrl: './order-item-image.component.html',
  styleUrls: ['./order-item-image.component.scss'],
})
export class OrderItemImageComponent implements OnInit {
  @Input() item: Item;

  @Input() linkToProduct: boolean;

  constructor(private orderService: OrderService) {}

  ngOnInit(): void {}

  get getItemImageUrl(): string {
    return this.orderService.getItemImageUrl(this.item);
  }

  get getUrlItemName(): string {
    if (this.item?.name) {
      return this.orderService.getUrlItemName(this.item.name);
    }
  }
}
