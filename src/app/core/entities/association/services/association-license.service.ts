import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Address } from '../../address';
import { OAuthService } from '../../o-auth';
import {
  LicenseCompany,
  LicenseCompanyContact
} from '../types/license-company';
import { LicenseMember, LicenseMemberUserInfo } from '../types/license-member';
import { AssociationLicenseHttpService } from './association-license-http.service';
import { AssociationStringsService } from './association-strings.service';

@Injectable({
  providedIn: 'root',
})
export class AssociationLicenseService {
  private memberSubject = new Subject<LicenseMember>();
  private companySubject = new Subject<LicenseCompany>();
  private membersSubject = new Subject<LicenseMember[]>();
  private companiesSubject = new Subject<LicenseCompany[]>();

  constructor(
    private allApp: AllAppService,
    private licenseHttp: AssociationLicenseHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService,
    public strings: AssociationStringsService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Association Groups
  // ==========================================================================================
  public getGroupsCompanies(queryParams?: Params): void {
    this.enableLoading();
    this.licenseHttp
      .getGroupsCompanies$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(
        this.successGetGroupsCompanies,
        this.failureGetGroupsCompanies
      );
  }

  private successGetGroupsCompanies = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const companies: LicenseCompany[] = data.body.result;
      this.companiesSubject.next(companies);
    } else {
      this.companiesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetGroupsCompanies = (error: HttpErrorResponse): void => {
    this.companiesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get License Member
  // ==========================================================================================
  public getLicenseMember$(): Observable<LicenseMember> {
    return this.memberSubject.asObservable();
  }

  public getLicenseMember(memberID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .getLicenseMember$(this.oAuthService.getUserID, memberID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetLicenseMember, this.failureGetLicenseMember);
    }
  }

  private successGetLicenseMember = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const member: LicenseMember = data.body.result[0];
      this.memberSubject.next(member);
    } else {
      this.memberSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetLicenseMember = (error: HttpErrorResponse): void => {
    this.memberSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update License Member User Info
  // ==========================================================================================
  public updateLicenseMemberUserInfo(
    memberID: number | string,
    userInfo: LicenseMemberUserInfo
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .updateLicenseMemberUserInfo$(
          this.oAuthService.getUserID,
          memberID,
          userInfo
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateLicenseMemberUserInfo,
          this.failureUpdateLicenseMemberUserInfo
        );
    }
  }

  private successUpdateLicenseMemberUserInfo = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const member: LicenseMember = data.body.result[0];
      this.memberSubject.next(member);
    } else {
      this.memberSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateLicenseMemberUserInfo = (
    error: HttpErrorResponse
  ): void => {
    this.memberSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update License Member User Address
  // ==========================================================================================
  public updateLicenseMemberAddress(
    memberID: number | string,
    address: Address
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .updateLicenseMemberAddress$(
          this.oAuthService.getUserID,
          memberID,
          address
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateLicenseMemberAddress,
          this.failureUpdateLicenseMemberAddress
        );
    }
  }

  private successUpdateLicenseMemberAddress = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const member: LicenseMember = data.body.result[0];
      this.memberSubject.next(member);
    } else {
      this.memberSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateLicenseMemberAddress = (
    error: HttpErrorResponse
  ): void => {
    this.memberSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Upload License Member Image
  // ==========================================================================================
  public uploadLicenseMemberImage(
    memberID: number | string,
    dataMultipart: FormData
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .uploadLicenseMemberImage$(
          this.oAuthService.getUserID,
          memberID,
          dataMultipart
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUploadLicenseMemberImage,
          this.failureUploadLicenseMemberImage
        );
    }
  }

  private successUploadLicenseMemberImage = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const member: LicenseMember = data.body.result[0];
      this.memberSubject.next(member);
    } else {
      this.memberSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUploadLicenseMemberImage = (
    error: HttpErrorResponse
  ): void => {
    this.memberSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Delete License Member Image
  // ==========================================================================================
  public deleteLicenseMemberImage(memberID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .deleteLicenseMemberImage$(this.oAuthService.getUserID, memberID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successDeleteLicenseMemberImage,
          this.failureDeleteLicenseMemberImage
        );
    }
  }

  private successDeleteLicenseMemberImage = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const member: LicenseMember = data.body.result[0];
      this.memberSubject.next(member);
    } else {
      this.memberSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureDeleteLicenseMemberImage = (
    error: HttpErrorResponse
  ): void => {
    this.memberSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Licenses Members
  // ==========================================================================================
  public getLicensesMembers$(): Observable<LicenseMember[]> {
    return this.membersSubject.asObservable();
  }

  public getLicensesMembers(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .getLicensesMembers$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetLicensesMembers,
          this.failureGetLicensesMembers
        );
    }
  }

  private successGetLicensesMembers = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const members: LicenseMember[] = data.body.result;
      this.membersSubject.next(members);
    } else {
      this.membersSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetLicensesMembers = (error: HttpErrorResponse): void => {
    this.membersSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get License Company
  // ==========================================================================================
  public getLicenseCompany$(): Observable<LicenseCompany> {
    return this.companySubject.asObservable();
  }

  public getLicenseCompany(companyID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .getLicenseCompany$(this.oAuthService.getUserID, companyID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetLicenseCompany,
          this.failureGetLicenseCompany
        );
    }
  }

  private successGetLicenseCompany = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetLicenseCompany = (error: HttpErrorResponse): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update License Company
  // ==========================================================================================
  public updateLicenseCompanyData(
    companyID: number | string,
    license: LicenseCompany
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .updateLicenseCompanyData$(
          this.oAuthService.getUserID,
          companyID,
          license
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateLicenseCompanyData,
          this.failureUpdateLicenseCompanyData
        );
    }
  }

  private successUpdateLicenseCompanyData = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateLicenseCompanyData = (
    error: HttpErrorResponse
  ): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update License Company Address
  // ==========================================================================================
  public updateLicenseCompanyAddress(
    companyID: number | string,
    address: Address
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .updateLicenseCompanyAddress$(
          this.oAuthService.getUserID,
          companyID,
          address
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateLicenseCompanyAddress,
          this.failureUpdateLicenseCompanyAddress
        );
    }
  }

  private successUpdateLicenseCompanyAddress = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateLicenseCompanyAddress = (
    error: HttpErrorResponse
  ): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update License Company Contact
  // ==========================================================================================
  public updateLicenseCompanyContactInfo(
    companyID: number | string,
    contact: LicenseCompanyContact
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .updateLicenseCompanyContactInfo$(
          this.oAuthService.getUserID,
          companyID,
          contact
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateLicenseCompanyContactInfo,
          this.failureUpdateLicenseCompanyContactInfo
        );
    }
  }

  private successUpdateLicenseCompanyContactInfo = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateLicenseCompanyContactInfo = (
    error: HttpErrorResponse
  ): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update License Company Contact Address
  // ==========================================================================================
  public updateLicenseCompanyContactAddress(
    companyID: number | string,
    address: Address
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .updateLicenseCompanyContactAddress$(
          this.oAuthService.getUserID,
          companyID,
          address
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateLicenseCompanyContactAddress,
          this.failureUpdateLicenseCompanyContactAddress
        );
    }
  }

  private successUpdateLicenseCompanyContactAddress = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateLicenseCompanyContactAddress = (
    error: HttpErrorResponse
  ): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Upload License Company Image
  // ==========================================================================================
  public uploadLicenseCompanyImage(
    companyID: number | string,
    dataMultipart: FormData
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .uploadLicenseCompanyImage$(
          this.oAuthService.getUserID,
          companyID,
          dataMultipart
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUploadLicenseCompanyImage,
          this.failureUploadLicenseCompanyImage
        );
    }
  }

  private successUploadLicenseCompanyImage = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUploadLicenseCompanyImage = (
    error: HttpErrorResponse
  ): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Delete License Company Image
  // ==========================================================================================
  public deleteLicenseCompanyImage(companyID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .deleteLicenseCompanyImage$(this.oAuthService.getUserID, companyID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successDeleteLicenseCompanyImage,
          this.failureDeleteLicenseCompanyImage
        );
    }
  }

  private successDeleteLicenseCompanyImage = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const company: LicenseCompany = data.body.result[0];
      this.companySubject.next(company);
    } else {
      this.companySubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureDeleteLicenseCompanyImage = (
    error: HttpErrorResponse
  ): void => {
    this.companySubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Licenses Companies
  // ==========================================================================================
  public getLicensesCompanies$(): Observable<LicenseCompany[]> {
    return this.companiesSubject.asObservable();
  }

  public getLicensesCompanies(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.licenseHttp
        .getLicensesCompanies$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetLicensesCompanies,
          this.failureGetLicensesCompanies
        );
    }
  }

  private successGetLicensesCompanies = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const companies: LicenseCompany[] = data.body.result;
      this.companiesSubject.next(companies);
    } else {
      this.companiesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetLicensesCompanies = (error: HttpErrorResponse): void => {
    this.companiesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
