export enum ProductTypeEnum {
  ASSOCIATION = 'ASSOCIATION',
  EVENT = 'EVENT',
  MERCHANDISE = 'MERCHANDISE',
  MULTIMEDIA = 'MULTIMEDIA',
  SERVICE = 'SERVICE',
  SUBSCRIPTION = 'SUBSCRIPTION',
  RESERVATION = 'RESERVATION',
}

export type ProductType =
| 'ASSOCIATION'
  | 'EVENT'
  | 'MERCHANDISE'
  | 'MULTIMEDIA'
  | 'SERVICE'
  | 'SUBSCRIPTION'
  | 'RESERVATION';

export const ProductTypeArray = [
  'ASSOCIATION',
  'EVENT',
  'MERCHANDISE',
  'MULTIMEDIA',
  'SERVICE',
  'SUBSCRIPTION',
  'RESERVATION',
];

export const ProductTypeInfo = [
  {
    association: {
      name: 'Asociación',
    },
  },
  {
    event: {
      name: 'Eventos',
    },
  },
  {
    merchandise: {
      name: 'Merchandise',
    },
  },
  {
    multimedia: {
      name: 'Multimedia',
    },
  },
  {
    service: {
      name: 'Servicios',
    },
  },
  {
    subscription: {
      name: 'Suscripciones',
    },
  },
  {
    reservation: {
      name: 'Reservaciones',
    },
  },
];
