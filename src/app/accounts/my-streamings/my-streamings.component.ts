import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppEnv } from '@qaroni-app/core/config';
import { Streaming, StreamingsService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-streamings',
  templateUrl: './my-streamings.component.html',
  styleUrls: ['./my-streamings.component.scss']
})
export class MyStreamingsComponent implements OnInit, OnDestroy {
  public AppEnv = AppEnv;

  public streamings$: Observable<
    Streaming[]
  > = this.streamingsService.getStreamings$().pipe(shareReplay(1));

  public initLoaded = false;

  constructor(
    private allApp: AllAppService,
    private streamingsService: StreamingsService
  ) {
    this.enableLoading();
    this.allApp.toolbar.showBackButton();
  }

  ngOnInit(): void {
    this.streamingsService.getStreamings();
    this.streamings$.subscribe().unsubscribe();
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }
}
