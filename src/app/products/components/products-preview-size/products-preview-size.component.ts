import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'qaroni-products-preview-size',
  templateUrl: './products-preview-size.component.html',
  styleUrls: ['./products-preview-size.component.scss'],
})
export class ProductsPreviewSizeComponent implements OnInit {
  @Output() size: EventEmitter<string> = new EventEmitter();

  private productSize: string;

  private colNormal = 'col-12 col-sm-6 col-md-4 col-lg-4 col-3xl-3 mb-3';
  private colBig = 'col-12 col-md-6 mb-3';

  constructor() {
    this.productSize = this.colNormal;
  }

  ngOnInit(): void {
    this.size.emit(this.productSize);
  }

  get getProductPreviewSizeIcon(): string {
    if (this.productSize === this.colBig) {
      return 'fas fa-th';
    } else {
      return 'fas fa-th-large';
    }
  }

  public changeProductPreviewSize(): void {
    if (this.productSize === this.colBig) {
      this.productSize = this.colNormal;
    } else {
      this.productSize = this.colBig;
    }
    this.size.emit(this.productSize);
  }
}
