import { Component, Input, OnInit } from '@angular/core';
import {
  AssociationService,
  Group,
  GroupMonthTypeEnum,
  GroupTypeEnum
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-list-group-item',
  templateUrl: './list-group-item.component.html',
  styleUrls: ['./list-group-item.component.scss'],
})
export class ListGroupItemComponent implements OnInit {
  @Input() group: Group;
  @Input() partnerID: string;

  constructor(private associationService: AssociationService) {}

  ngOnInit(): void {}

  get hasImage(): boolean {
    if (this.group && this.group.imageUrl) {
      return true;
    }
    return false;
  }

  public booleanToYesOrNo(status: boolean): string {
    return this.associationService.booleanToYesOrNo(status);
  }

  public timeUnitString(months: number): string {
    if (this.group.monthType === GroupMonthTypeEnum.MONTH) {
      return this.associationService.monthsString(months);
    } else if (this.group.monthType === GroupMonthTypeEnum.NATURAL_YEAR) {
      return this.associationService.yearsString(months);
    }
  }

  get getEnrollmentLink(): string[] {
    if (this.group?.type) {
      if (this.partnerID) {
        if (this.group?.type === GroupTypeEnum.ENTITY) {
          return [
            '/associations',
            'groups',
            this.group?.groupId?.toString(),
            'companies',
            'partners',
            this.partnerID,
          ];
        } else if (this.group?.type === GroupTypeEnum.PERSON) {
          return [
            '/associations',
            'groups',
            this.group?.groupId?.toString(),
            'members',
            'partners',
            this.partnerID,
          ];
        } else {
          return ['/associations', 'groups', 'partners', this.partnerID];
        }
      } else {
        if (this.group?.type === GroupTypeEnum.ENTITY) {
          return [
            '/associations',
            'groups',
            this.group?.groupId?.toString(),
            'companies',
          ];
        } else if (this.group?.type === GroupTypeEnum.PERSON) {
          return [
            '/associations',
            'groups',
            this.group?.groupId?.toString(),
            'members',
          ];
        } else {
          return ['/associations', 'groups'];
        }
      }
    }
  }
}
