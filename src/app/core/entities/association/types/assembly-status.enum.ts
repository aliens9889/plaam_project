export enum AssemblyStatusEnum {
  CLOSED = 'CLOSED',
  CREATED = 'CREATED',
  NOT_USED = 'NOT_USED',
  OPEN = 'OPEN',
}

export type AssemblyStatusType =
  | AssemblyStatusEnum.CLOSED
  | AssemblyStatusEnum.CREATED
  | AssemblyStatusEnum.NOT_USED
  | AssemblyStatusEnum.OPEN;

export const AssemblyStatusArray = [
  AssemblyStatusEnum.CLOSED,
  AssemblyStatusEnum.CREATED,
  AssemblyStatusEnum.NOT_USED,
  AssemblyStatusEnum.OPEN,
];

export const AssemblyStatusInfo = [
  {
    CLOSED: {
      name: 'Cerrada',
    },
  },
  {
    CREATED: {
      name: 'Pendiente',
    },
  },
  {
    NOT_USED: {
      name: 'No usada',
    },
  },
  {
    OPEN: {
      name: 'Abierta',
    },
  },
];
