import { Component, Input, OnInit } from '@angular/core';
import { Item, OrderService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-order-resume-item',
  templateUrl: './order-resume-item.component.html',
  styleUrls: ['./order-resume-item.component.scss'],
})
export class OrderResumeItemComponent implements OnInit {
  @Input() item: Item = null;

  constructor(private orderService: OrderService) {}

  ngOnInit(): void {}

  get hasItemDiscount(): boolean {
    return this.orderService.hasItemDiscount(this.item);
  }
}
