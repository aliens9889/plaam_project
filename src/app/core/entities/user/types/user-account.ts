export interface UserAccount {
  creationDate: string;
  lastUpdateDate: string;
  merchantId: number;
  status: string;
  userId: number;
  username: string;
}
