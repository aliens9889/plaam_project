import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Address, InvoiceService, UserAddressService, UserData, UserDataService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { AddressFormDialogComponent } from '@qaroni-app/shared/components/dialogs';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-order-invoice',
  templateUrl: './order-invoice.component.html',
  styleUrls: ['./order-invoice.component.scss'],
})
export class OrderInvoiceComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;

  public addresses: Address[] = [];
  public selectedAddress: Address = null;

  public initLoaded = false;

  private submitting = false;

  private subs: Subscription = new Subscription();

  public invoiceForm: FormGroup;
  private invoiceSkeleton = {
    orderId: ['', [Validators.required]],
    addressId: ['', [Validators.required]],
    name: ['', [Validators.required]],
    nif: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
  };

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private invoiceService: InvoiceService,
    private userAddressService: UserAddressService,
    private userDataService: UserDataService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(
        this.invoiceService
          .getOrderInvoiceUrl$()
          .subscribe(this.getOrderInvoiceUrl)
      );
      this.invoiceService.getOrderInvoice(
        this.route.snapshot.paramMap.get('orderID')
      );
      this.createInvoiceForm();
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
  }

  private getOrderInvoiceUrl = (invoiceUrl: string): void => {
    if (invoiceUrl) {
      window.location.href = invoiceUrl;
      this.allApp.router.navigate(['/orders']);
    } else if (invoiceUrl === null) {
      this.subs.add(
        this.userDataService.getUserData$().subscribe(this.getUserData)
      );
      this.userDataService.getUserData();
      this.subs.add(
        this.userAddressService
          .getUserAddresses$()
          .subscribe(this.getUserAddresses)
      );
      this.userAddressService.getUserAddresses();
    }
  }

  private createInvoiceForm(): void {
    this.invoiceForm = this.fb.group(this.invoiceSkeleton);
    this.orderId.setValue(this.route.snapshot.paramMap.get('orderID'));
  }

  private populateInvoiceFormWithUserData(userData: UserData): void {
    if (userData.firstName && userData.lastName) {
      this.name.setValue(userData.firstName + ' ' + userData.lastName);
    }
    if (userData.documentType && userData.document) {
      this.nif.setValue(userData.documentType + ' ' + userData.document);
    }
    if (userData.email) {
      this.email.setValue(userData.email);
    }
    if (userData.phone) {
      this.phone.setValue(userData.phone);
    }
  }

  private populateInvoiceFormWithAddress(addresses: Address[]): void {
    if (addresses.length) {
      this.addressId.setValue(addresses[0].addressId);
      this.selectedAddress = addresses[0];
    }
  }

  get orderId(): AbstractControl {
    return this.invoiceForm.get('orderId');
  }

  get addressId(): AbstractControl {
    return this.invoiceForm.get('addressId');
  }

  get name(): AbstractControl {
    return this.invoiceForm.get('name');
  }

  get nif(): AbstractControl {
    return this.invoiceForm.get('nif');
  }

  get email(): AbstractControl {
    return this.invoiceForm.get('email');
  }

  get phone(): AbstractControl {
    return this.invoiceForm.get('phone');
  }

  get validatedForm(): boolean {
    return this.invoiceForm.valid && !this.submitting;
  }

  private getUserAddresses = (addresses: Address[]): void => {
    if (addresses && addresses.length) {
      this.addresses = addresses.sort(
        this.userAddressService.sortAddressesDefault
      );
      this.populateInvoiceFormWithAddress(addresses);
    }
    this.disableLoading();
  }

  public createAddress(): void {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '500px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;

    const dialog = this.allApp.dialog.open(
      AddressFormDialogComponent,
      matDialogConfig
    );

    dialog.afterClosed().subscribe((addressResponse: Address) => {
      if (addressResponse) {
        this.userAddressService.getUserAddresses();
      }
    });
  }

  private getUserData = (userData: UserData): void => {
    this.disableLoading();
    this.populateInvoiceFormWithUserData(userData);
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.invoiceService.createOrderInvoice(
        this.route.snapshot.paramMap.get('orderID'),
        this.invoiceForm.getRawValue()
      );
    }
  }

  public onChangeAddress(event) {
    if (this.addresses.length) {
      this.selectedAddress = this.getAddressInfo(event.value);
    }
  }

  public getAddressInfo(addressID: number | string): Address {
    if (this.addresses.length) {
      return this.addresses.find(
        (address: Address) => address.addressId === addressID
      );
    }
  }
}
