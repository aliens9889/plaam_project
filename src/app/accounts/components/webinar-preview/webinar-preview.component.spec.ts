import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebinarPreviewComponent } from './webinar-preview.component';

describe('WebinarPreviewComponent', () => {
  let component: WebinarPreviewComponent;
  let fixture: ComponentFixture<WebinarPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebinarPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebinarPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
