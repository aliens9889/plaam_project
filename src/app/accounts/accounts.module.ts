import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { AccountPanelComponent } from './account-panel/account-panel.component';
import { AccountsRoutingModule } from './accounts-routing.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AddressCardComponent } from './components/address-card/address-card.component';
import { ManageItemCardComponent } from './components/manage-item-card/manage-item-card.component';
import { MultimediaPreviewComponent } from './components/multimedia-preview/multimedia-preview.component';
import { StreamingPreviewComponent } from './components/streaming-preview/streaming-preview.component';
import { SubscriptionPreviewComponent } from './components/subscription-preview/subscription-preview.component';
import { TicketPreviewComponent } from './components/ticket-preview/ticket-preview.component';
import { WebinarPreviewComponent } from './components/webinar-preview/webinar-preview.component';
import { MyAddressesComponent } from './my-addresses/my-addresses.component';
import { MyInformationEditComponent } from './my-information-edit/my-information-edit.component';
import { MyInformationComponent } from './my-information/my-information.component';
import { MyMultimediaComponent } from './my-multimedia/my-multimedia.component';
import { MyStreamingsComponent } from './my-streamings/my-streamings.component';
import { MySubscriptionsComponent } from './my-subscriptions/my-subscriptions.component';
import { MyTicketsComponent } from './my-tickets/my-tickets.component';
import { MyWebinarsComponent } from './my-webinars/my-webinars.component';

@NgModule({
  declarations: [
    AccountPanelComponent,
    AddressCardComponent,
    ChangePasswordComponent,
    ManageItemCardComponent,
    MultimediaPreviewComponent,
    MyAddressesComponent,
    MyInformationComponent,
    MyInformationEditComponent,
    MyMultimediaComponent,
    MyStreamingsComponent,
    MySubscriptionsComponent,
    MyTicketsComponent,
    MyWebinarsComponent,
    StreamingPreviewComponent,
    SubscriptionPreviewComponent,
    TicketPreviewComponent,
    WebinarPreviewComponent,
  ],
  imports: [SharedModule, AccountsRoutingModule, TranslateModule],
})
export class AccountsModule {}
