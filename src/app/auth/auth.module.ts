import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { AuthRoutingModule } from './auth-routing.module';
import { ForgotChangePassComponent } from './forgot-change-pass/forgot-change-pass.component';
import { ForgotComponent } from './forgot/forgot.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { SignUpPosterComponent } from './sign-up-poster/sign-up-poster.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ValidateComponent } from './validate/validate.component';

@NgModule({
  declarations: [
    ForgotChangePassComponent,
    ForgotComponent,
    LoginComponent,
    LogoutComponent,
    SignUpComponent,
    SignUpPosterComponent,
    ValidateComponent
  ],
  imports: [SharedModule, AuthRoutingModule, TranslateModule],
  exports: []
})
export class AuthModule {}
