export enum GroupInscriptionCompanyStatusEnum {
  ACTIVE = 'ACTIVE',
  ADMITTED = 'ADMITTED',
  BANK_CHARGE = 'BANK_CHARGE',
  CANCELLED = 'CANCELLED',
  INCOMPLETE = 'INCOMPLETE',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
  REVISION = 'REVISION',
}

export type GroupInscriptionCompanyStatusType =
  | 'ACTIVE'
  | 'ADMITTED'
  | 'BANK_CHARGE'
  | 'CANCELLED'
  | 'INCOMPLETE'
  | 'PENDING'
  | 'REJECTED'
  | 'REVISION';

export const GroupInscriptionCompanyStatusArray = [
  'ACTIVE',
  'ADMITTED',
  'BANK_CHARGE',
  'CANCELLED',
  'INCOMPLETE',
  'PENDING',
  'REJECTED',
  'REVISION',
];

export const GroupInscriptionCompanyStatusInfo = [
  {
    active: {
      name: 'Activa',
    },
  },
  {
    admitted: {
      name: 'Admitida',
    },
  },
  {
    bank_charge: {
      name: 'Cargo bancario',
    },
  },
  {
    cancelled: {
      name: 'Cancelada',
    },
  },
  {
    incomplete: {
      name: 'Incompleta',
    },
  },
  {
    pending: {
      name: 'Pendiente',
    },
  },
  {
    rejected: {
      name: 'Rechazada',
    },
  },
  {
    revision: {
      name: 'En revisión',
    },
  },
];
