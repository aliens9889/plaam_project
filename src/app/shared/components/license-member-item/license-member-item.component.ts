import { Component, Input, OnInit } from '@angular/core';
import { LicenseMember } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-license-member-item',
  templateUrl: './license-member-item.component.html',
  styleUrls: ['./license-member-item.component.scss'],
})
export class LicenseMemberItemComponent implements OnInit {
  @Input() member: LicenseMember;
  @Input() cardLink: boolean;

  constructor() {}

  ngOnInit(): void {}

  get hasImage(): boolean {
    if (this.member?.groupInfo?.imageUrl) {
      return true;
    }
    return false;
  }

  public onDownloadWallet(event): void {
    event.stopPropagation();
  }
}
