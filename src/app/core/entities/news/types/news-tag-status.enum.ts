export enum NewsTagStatusEnum {
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
  INACTIVE = 'INACTIVE',
}

export type NewsTagStatusType =
  | NewsTagStatusEnum.ACTIVE
  | NewsTagStatusEnum.CREATED
  | NewsTagStatusEnum.INACTIVE;

export const NewsTagStatusArray = [
  NewsTagStatusEnum.ACTIVE,
  NewsTagStatusEnum.CREATED,
  NewsTagStatusEnum.INACTIVE,
];

export const NewsTagStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    created: {
      name: 'Creado',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
