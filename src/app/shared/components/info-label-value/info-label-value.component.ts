import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qaroni-info-label-value',
  templateUrl: './info-label-value.component.html',
  styleUrls: ['./info-label-value.component.scss']
})
export class InfoLabelValueComponent implements OnInit {
  @Input() label: string = null;
  @Input() value: string = null;

  constructor() {}

  ngOnInit(): void {}
}
