import { LanguageEnum } from '../language/language.enum';

export interface AppConfig {
  cookies?: boolean;
  language?: LanguageEnum;
}
