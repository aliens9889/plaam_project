export interface Zone {
  capacity: number;
  creationDate: string;
  eventId: number;
  lastUpdateDate: string;
  name: string;
  number: number;
  preffix: string;
  zoneId: number;
}
