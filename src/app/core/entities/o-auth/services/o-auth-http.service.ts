import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';
import {
  ForgotChangePasswordJson,
  ForgotJson,
  LoginJson,
  OtpJson,
  SignUpJson
} from '../types/authentication';

@Injectable({
  providedIn: 'root',
})
export class OAuthHttpService {
  constructor(private http: HttpClient) {}

  public registerUser$(signUpJSON: SignUpJson): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/users/registers`;

    return this.http.post(url, signUpJSON, { observe: 'response' });
  }

  public getOAuth$(loginJSON: LoginJson): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/users/logins`;

    return this.http.post(url, loginJSON, { observe: 'response' });
  }

  public validate$(
    userID: number | string,
    otpJSON: OtpJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/users/${userID}/validates`;

    return this.http.patch(url, otpJSON, { observe: 'response' });
  }

  public forgotPassword$(
    forgotJSON: ForgotJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/users/forgots`;

    return this.http.patch(url, forgotJSON, { observe: 'response' });
  }

  public forgotChangePassword$(
    userID: number | string,
    forgotChangePasswordJSON: ForgotChangePasswordJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/users/${userID}/forgots/validates`;

    return this.http.patch(url, forgotChangePasswordJSON, {
      observe: 'response',
    });
  }
}
