import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRequestsMembersComponent } from './my-requests-members.component';

describe('MyRequestsMembersComponent', () => {
  let component: MyRequestsMembersComponent;
  let fixture: ComponentFixture<MyRequestsMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyRequestsMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRequestsMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
