import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterAttributeComponent } from './products-filter-attribute.component';

describe('ProductsFilterAttributeComponent', () => {
  let component: ProductsFilterAttributeComponent;
  let fixture: ComponentFixture<ProductsFilterAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
