import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyEditContactAddressComponent } from './license-company-edit-contact-address.component';

describe('LicenseCompanyEditContactAddressComponent', () => {
  let component: LicenseCompanyEditContactAddressComponent;
  let fixture: ComponentFixture<LicenseCompanyEditContactAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyEditContactAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyEditContactAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
