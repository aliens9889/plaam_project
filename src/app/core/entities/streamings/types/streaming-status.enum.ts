export enum StreamingStatusEnum {
  DISABLED = 'DISABLED',
  ENABLED = 'ENABLED',
}

export type StreamingStatusType = 'DISABLED' | 'ENABLED';

export const StreamingStatusArray = ['DISABLED', 'ENABLED'];

export const StreamingStatusInfo = [
  {
    disabled: {
      name: 'Inactivo',
    },
  },
  {
    enabled: {
      name: 'Activo',
    },
  },
];
