export enum AssemblyVoteResultEnum {
  ABSTENTION = 'ABSTENTION',
  NO = 'NO',
  YES = 'YES',
}

export type AssemblyVoteResultType =
  | AssemblyVoteResultEnum.ABSTENTION
  | AssemblyVoteResultEnum.NO
  | AssemblyVoteResultEnum.YES;

export const AssemblyVoteResultArray = [
  AssemblyVoteResultEnum.YES,
  AssemblyVoteResultEnum.NO,
  AssemblyVoteResultEnum.ABSTENTION,
];

export const AssemblyVoteResultInfo = [
  {
    ABSTENTION: {
      name: 'Abstención',
    },
  },
  {
    NO: {
      name: 'No',
    },
  },
  {
    YES: {
      name: 'Sí',
    },
  },
];
