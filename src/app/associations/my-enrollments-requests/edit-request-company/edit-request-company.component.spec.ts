import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRequestCompanyComponent } from './edit-request-company.component';

describe('EditRequestCompanyComponent', () => {
  let component: EditRequestCompanyComponent;
  let fixture: ComponentFixture<EditRequestCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditRequestCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRequestCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
