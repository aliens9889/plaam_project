import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectZoneDialogComponent } from './select-zone-dialog.component';

describe('SelectZoneDialogComponent', () => {
  let component: SelectZoneDialogComponent;
  let fixture: ComponentFixture<SelectZoneDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectZoneDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectZoneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
