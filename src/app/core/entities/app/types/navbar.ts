import { LanguageEnum } from '@qaroni-app/core/types';

export interface Navbar {
  creationDate: string;
  descriptions: NavbarDescription[];
  internal: boolean;
  lastUpdateDate: string;
  merchantId: number;
  navbarId: number;
  parentId: number;
  position: number;
  status: string;
  url: string;
}

export interface NavbarDescription {
  description: string;
  language: LanguageEnum;
  name: string;
}
