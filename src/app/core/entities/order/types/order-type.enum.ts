export enum OrderTypeEnum {
  BRICK = 'BRICK',
  ONLINE = 'ONLINE'
}

export type OrderType = 'BRICK' | 'ONLINE';

export const OrderTypeArray = ['BRICK', 'ONLINE'];

export const OrderTypeInfo = [
  {
    brick: {
      name: 'Fisico'
    }
  },
  {
    online: {
      name: 'Online'
    }
  }
];
