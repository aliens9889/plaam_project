import { Component, OnInit } from '@angular/core';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-paid-failure',
  templateUrl: './paid-failure.component.html',
  styleUrls: ['./paid-failure.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PaidFailureComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
