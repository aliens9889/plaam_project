import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

interface PriceInput {
  value: number;
  min: number;
  max: number;
}

@Component({
  selector: 'qaroni-products-filter-prices',
  templateUrl: './products-filter-prices.component.html',
  styleUrls: ['./products-filter-prices.component.scss'],
})
export class ProductsFilterPricesComponent implements OnInit {
  public from: PriceInput = {
    value: 0,
    min: 0,
    max: 5000,
  };
  public to: PriceInput = {
    value: 0,
    min: 0,
    max: 5000,
  };

  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(this.getQueryParams);
  }

  private getQueryParams = (queryParams: Params) => {
    if (queryParams.hasOwnProperty('priceFrom')) {
      this.from.value = parseInt(queryParams.priceFrom, 10) / 100;
    } else {
      this.from.value = 0;
    }
    if (queryParams.hasOwnProperty('priceTo')) {
      this.to.value = parseInt(queryParams.priceTo, 10) / 100;
    } else {
      this.to.value = 0;
    }
  }

  get canFilterByPrice(): boolean {
    if (
      typeof this.from.value === 'number' &&
      typeof this.to.value === 'number' &&
      (this.from.value !== 0 || this.to.value !== 0)
    ) {
      return true;
    }
    return false;
  }

  public onFilterByPrice(): void {
    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );

    let from = 0;
    let to = 0;

    if (this.from.value > this.to.value) {
      from = this.to.value;
      to = this.from.value;
    } else {
      from = this.from.value;
      to = this.to.value;
    }

    queryParamsEmit.priceFrom = (from * 100).toFixed(0);
    queryParamsEmit.priceTo = (to * 100).toFixed(0);
    this.queryParamsChange.emit(queryParamsEmit);
  }
}
