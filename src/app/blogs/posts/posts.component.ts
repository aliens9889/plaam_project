import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Links } from '@qaroni-app/core/entities';
import {
  Post,
  PostsService,
  PostStatusEnum
} from '@qaroni-app/core/entities/blogs';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PostsComponent implements OnInit, OnDestroy {
  public posts: Post[] = [];

  private posts$: Observable<Post[]> = this.postsService
    .getPosts$()
    .pipe(shareReplay(1));

  private postsLinks$: Observable<Links> = this.postsService
    .getLinks$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  private nextPage = 1;

  constructor(
    private allApp: AllAppService,
    private postsService: PostsService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.posts$.subscribe(this.getPosts));
    this.subs.add(this.postsLinks$.subscribe(this.getLinks));
    this.subs.add(this.route.queryParams.subscribe(this.getQueryParams));
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get showMorePosts(): boolean {
    if (!this.nextPage || this.nextPage === 1) {
      return false;
    }
    return true;
  }

  public onShowMorePosts(): void {
    const allParams: Params = {
      page: this.nextPage,
      status: PostStatusEnum.ACTIVE,
    };
    this.callPosts(allParams);
  }

  private getLinks = (links: Links): void => {
    this.nextPage = this.postsService.getNextPage(links);
  }

  private getQueryParams = (queryParams: Params) => {
    this.posts = [];
    this.nextPage = 1;
    const allParams: Params = {
      page: this.nextPage,
      status: PostStatusEnum.ACTIVE,
    };
    this.callPosts(allParams);
  }

  private callPosts(queryParams: Params): void {
    this.enableLoading();
    this.postsService.getPosts(queryParams);
  }

  private getPosts = (posts: Post[]): void => {
    this.posts.push(...posts);
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.nextPage = 1;
    this.posts = [];
    const allParams: Params = {
      page: this.nextPage,
      status: PostStatusEnum.ACTIVE,
    };
    this.callPosts(allParams);
  }

  public onQueryParamsChange(queryParams: Params): void {
    this.allApp.router.navigate(['/blogs'], { queryParams });
  }
}
