import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import {
  AssociationLicenseService,
  LicenseCompany
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-user-partner-form',
  templateUrl: './user-partner-form.component.html',
  styleUrls: ['./user-partner-form.component.scss'],
})
export class UserPartnerFormComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;

  public userPartnerForm: FormGroup;
  private userPartnerSkeleton = {
    partnerId: [''],
  };

  public companies$: Observable<
    LicenseCompany[]
  > = this.licenseService.getLicensesCompanies$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private licenseService: AssociationLicenseService
  ) {
    this.enableLoading();
    this.createUserPartnerForm();
  }

  ngOnInit(): void {
    this.subs.add(this.companies$.subscribe(this.getCompanies));
    this.licenseService.getGroupsCompanies();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private createUserPartnerForm(): void {
    this.userPartnerForm = this.fb.group(this.userPartnerSkeleton);
  }

  public populateUserPartnerForm(partnerId: number | string): void {
    if (partnerId) {
      this.userPartnerForm.patchValue({ partnerId });
    }
  }

  get partnerId(): AbstractControl {
    return this.userPartnerForm.get('partnerId');
  }

  get validatedForm(): boolean {
    return this.userPartnerForm.valid;
  }

  private getCompanies = (companies: LicenseCompany[]): void => {
    this.disableLoading();
  }

  get labelPartner(): string {
    return this.licenseService.strings.getLabelPartner();
  }
}
