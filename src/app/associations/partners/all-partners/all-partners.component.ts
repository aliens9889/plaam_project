import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AssociationPartnerService,
  AssociationStringsService,
  CompanyProfile
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-all-partners',
  templateUrl: './all-partners.component.html',
  styleUrls: ['./all-partners.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class AllPartnersComponent implements OnInit, OnDestroy {
  public partners$: Observable<
    CompanyProfile[]
  > = this.partnerService.getPartners$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  public initLoaded = false;

  constructor(
    private allApp: AllAppService,
    private partnerService: AssociationPartnerService,
    private stringsService: AssociationStringsService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.partners$.subscribe(this.getPartners));
    this.partnerService.getPartners();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  get title(): string {
    return this.stringsService.getTitleComponentPartners();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getPartners = (partners: CompanyProfile[]): void => {
    this.disableLoading();
    this.initLoaded = true;
  }
}
