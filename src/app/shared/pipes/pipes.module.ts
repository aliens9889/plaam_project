import { NgModule } from '@angular/core';
import { AssemblyStatusPipe } from './association/assembly-status.pipe';
import { AssemblyTypePipe } from './association/assembly-type.pipe';
import { AssemblyVoteResultPipe } from './association/assembly-vote-result.pipe';
import { GroupCompanyStatusPipe } from './association/group-company-status.pipe';
import { GroupInscriptionMemberStatusPipe } from './association/group-inscription-member-status.pipe';
import { GroupInscriptionTypePipe } from './association/group-inscription-type.pipe';
import { GroupMemberStatusPipe } from './association/group-member-status.pipe';
import { ClientTypePipe } from './client/client-type.pipe';
import { CountryPipe } from './country/country.pipe';
import { IntegerToCurrencyPipe } from './currency/integer-to-currency.pipe';
import { DiscountPipe } from './discount/discount.pipe';
import { EventStatePipe } from './event/event-state.pipe';
import { EventTypePipe } from './event/event-type.pipe';
import { GenderPipe } from './gender/gender.pipe';
import { InvoicePaymentMethodPipe } from './invoice/invoice-payment-method.pipe';
import { InvoiceStatusPipe } from './invoice/invoice-status.pipe';
import { IvaPipe } from './iva/iva.pipe';
import { LanguagePipe } from './language/language.pipe';
import { ItemUnitTypePipe } from './order/item-unit-type.pipe';
import { OrderStatusPipe } from './order/order-status.pipe';
import { OrderTypePipe } from './order/order-type.pipe';
import { ProductTypePipe } from './product/product-type.pipe';
import { RouterLinkBasePipe } from './router-link-base/router-link-base.pipe';
import { RouterLinkQueryParamsPipe } from './router-link-query-params/router-link-query-params.pipe';
import { SanitizeHtmlPipe } from './sanitize-html/sanitize-html.pipe';
import { SanitizeUrlPipe } from './sanitize-url/sanitize-url.pipe';
import { TicketStatusPipe } from './ticket/ticket-status.pipe';
import { DocumentTypePipe } from './user/document-type.pipe';

@NgModule({
  declarations: [
    AssemblyStatusPipe,
    AssemblyTypePipe,
    AssemblyVoteResultPipe,
    ClientTypePipe,
    CountryPipe,
    DiscountPipe,
    DocumentTypePipe,
    EventStatePipe,
    EventTypePipe,
    GenderPipe,
    GroupCompanyStatusPipe,
    GroupInscriptionMemberStatusPipe,
    GroupInscriptionTypePipe,
    GroupMemberStatusPipe,
    IntegerToCurrencyPipe,
    InvoicePaymentMethodPipe,
    InvoiceStatusPipe,
    ItemUnitTypePipe,
    IvaPipe,
    LanguagePipe,
    OrderStatusPipe,
    OrderTypePipe,
    ProductTypePipe,
    RouterLinkBasePipe,
    RouterLinkQueryParamsPipe,
    SanitizeHtmlPipe,
    SanitizeUrlPipe,
    TicketStatusPipe,
  ],
  imports: [],
  exports: [
    AssemblyStatusPipe,
    AssemblyTypePipe,
    AssemblyVoteResultPipe,
    ClientTypePipe,
    CountryPipe,
    DiscountPipe,
    DocumentTypePipe,
    EventStatePipe,
    EventTypePipe,
    GenderPipe,
    GroupCompanyStatusPipe,
    GroupInscriptionMemberStatusPipe,
    GroupInscriptionTypePipe,
    GroupMemberStatusPipe,
    IntegerToCurrencyPipe,
    InvoicePaymentMethodPipe,
    InvoiceStatusPipe,
    ItemUnitTypePipe,
    IvaPipe,
    LanguagePipe,
    OrderStatusPipe,
    OrderTypePipe,
    ProductTypePipe,
    RouterLinkBasePipe,
    RouterLinkQueryParamsPipe,
    SanitizeHtmlPipe,
    SanitizeUrlPipe,
    TicketStatusPipe,
  ],
})
export class PipesModule {}
