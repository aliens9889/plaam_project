import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { SelectZoneDialogComponent } from './components/dialogs/select-zone-dialog/select-zone-dialog.component';
import { EventShowComponent } from './event-show/event-show.component';
import { EventsRoutingModule } from './events-routing.module';
import { EventsComponent } from './events/events.component';
import { FreeTicketFailureComponent } from './free-ticket-failure/free-ticket-failure.component';
import { FreeTicketSuccessComponent } from './free-ticket-success/free-ticket-success.component';
import { FreeTicketComponent } from './free-ticket/free-ticket.component';

@NgModule({
  declarations: [
    EventsComponent,
    EventShowComponent,
    FreeTicketComponent,
    FreeTicketFailureComponent,
    FreeTicketSuccessComponent,
    SelectZoneDialogComponent,
  ],
  imports: [CommonModule, EventsRoutingModule, SharedModule, TranslateModule],
})
export class EventsModule {}
