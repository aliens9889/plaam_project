import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppLandingPageBrandsEnum, LandingEnv } from '@qaroni-app/core/config';
import { ProductBrand, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-brands-section',
  templateUrl: './brands-section.component.html',
  styleUrls: ['./brands-section.component.scss'],
})
export class BrandsSectionComponent implements OnInit {
  public brands$: Observable<
    ProductBrand[]
  > = this.productService.getProductBrands$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private productService: ProductService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.productService.getProductBrands({ showOnCover: 1 });
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get LandingEnv() {
    return LandingEnv;
  }

  get AppLandingPageBrandsEnum() {
    return AppLandingPageBrandsEnum;
  }
}
