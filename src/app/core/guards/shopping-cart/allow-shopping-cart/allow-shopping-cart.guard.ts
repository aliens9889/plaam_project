import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { AppEnv } from '@qaroni-app/core/config';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AllowShoppingCartGuard implements CanActivate, CanLoad {
  private redirectTo = ['/'];

  constructor(private allApp: AllAppService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (AppEnv.allowShoppingCart) {
      return true;
    }
    this.allApp.router.navigate(this.redirectTo);
    return false;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (AppEnv.allowShoppingCart) {
      return true;
    }
    this.allApp.router.navigate(this.redirectTo);
    return false;
  }
}
