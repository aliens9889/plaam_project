import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppService, Slide } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-slides-section',
  templateUrl: './slides-section.component.html',
  styleUrls: ['./slides-section.component.scss'],
})
export class SlidesSectionComponent implements OnInit, OnDestroy {
  public slides$: Observable<Slide[]> = this.appService
    .getSlides$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(private allApp: AllAppService, private appService: AppService) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.appService.getSlides({ status: 'ACTIVE' });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.appService.getSlides({ status: 'ACTIVE' });
  }
}
