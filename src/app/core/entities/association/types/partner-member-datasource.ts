import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Params } from '@angular/router';
import { PaginationLinks } from '@qaroni-app/core/types';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AssociationPartnerService } from '../services/association-partner.service';
import { LicenseMember } from './license-member';

export class PartnerMemberDataSource extends DataSource<LicenseMember> {
  paginator: MatPaginator;

  public members$: Observable<
    LicenseMember[]
  > = this.partnerService.getPartnerMembers$().pipe(shareReplay(1));

  private paginationLinks$: Observable<
    PaginationLinks
  > = this.partnerService.getPaginationLinks$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private partnerID: number | string,
    private partnerService: AssociationPartnerService
  ) {
    super();
  }

  connect(): Observable<LicenseMember[]> {
    this.paginator.pageSize = 60;

    this.subs.add(this.paginationLinks$.subscribe(this.getPaginationLinks));
    this.subs.add(this.paginator.page.subscribe(this.getPage));

    if (this.partnerID) {
      const queryParams: Params = {
        pagination: this.paginator.pageSize,
        page: 1,
      };
      this.partnerService.getPartnerMembers(this.partnerID, queryParams);
    }

    return this.members$;
  }

  disconnect(): void {
    this.subs.unsubscribe();
  }

  private getPaginationLinks = (paginationLinks: PaginationLinks): void => {
    if (paginationLinks) {
      this.paginator.length = paginationLinks?.total;
    }
  }

  private getPage = (pageEvent: PageEvent): void => {
    if (this.partnerID && pageEvent) {
      this.paginator.pageSize = pageEvent?.pageSize;

      const params: Params = {
        pagination: pageEvent?.pageSize,
        page: pageEvent?.pageIndex + 1,
      };

      this.partnerService.resetPartnerMembers();
      this.partnerService.getPartnerMembers(this.partnerID, params);
    }
  }
}
