import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {
  Event,
  EventService,
  EventStateEnum,
  Links
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class EventsComponent implements OnInit, OnDestroy {
  public events: Event[] = [];

  private events$: Observable<Event[]> = this.eventService
    .getEvents$()
    .pipe(shareReplay(1));

  private eventsLinks$: Observable<Links> = this.eventService
    .getLinks$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  private nextPage = 1;

  constructor(
    private allApp: AllAppService,
    private eventService: EventService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.events$.subscribe(this.getEvents));
    this.subs.add(this.eventsLinks$.subscribe(this.getLinks));
    this.subs.add(this.route.queryParams.subscribe(this.getQueryParams));
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get showMoreEvents(): boolean {
    if (!this.nextPage || this.nextPage === 1) {
      return false;
    }
    return true;
  }

  public onShowMoreEvents(): void {
    const allParams: Params = {
      page: this.nextPage,
      status: EventStateEnum.ACTIVE,
    };
    this.callEvents(allParams);
  }

  private getLinks = (links: Links): void => {
    this.nextPage = this.eventService.getNextPage(links);
  }

  private getQueryParams = (queryParams: Params) => {
    this.nextPage = 1;
    this.events = [];
    const allParams: Params = {
      page: this.nextPage,
      status: EventStateEnum.ACTIVE,
    };
    this.callEvents(allParams);
  }

  private callEvents(queryParams: Params): void {
    this.enableLoading();
    this.eventService.getEvents(queryParams);
  }

  private getEvents = (events: Event[]): void => {
    this.events.push(...events);
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.nextPage = 1;
    this.events = [];
    const allParams: Params = {
      page: this.nextPage,
      status: EventStateEnum.ACTIVE,
    };
    this.callEvents(allParams);
  }
}
