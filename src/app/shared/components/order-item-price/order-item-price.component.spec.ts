import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderItemPriceComponent } from './order-item-price.component';

describe('OrderItemPriceComponent', () => {
  let component: OrderItemPriceComponent;
  let fixture: ComponentFixture<OrderItemPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderItemPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
