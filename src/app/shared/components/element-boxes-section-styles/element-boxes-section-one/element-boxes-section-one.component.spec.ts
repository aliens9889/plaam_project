import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementBoxesSectionOneComponent } from './element-boxes-section-one.component';

describe('ElementBoxesSectionOneComponent', () => {
  let component: ElementBoxesSectionOneComponent;
  let fixture: ComponentFixture<ElementBoxesSectionOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementBoxesSectionOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementBoxesSectionOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
