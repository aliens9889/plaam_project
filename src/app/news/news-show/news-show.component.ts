import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { News, NewsService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-news-show',
  templateUrl: './news-show.component.html',
  styleUrls: ['./news-show.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class NewsShowComponent implements OnInit, OnDestroy {
  public newsItem$: Observable<News> = this.newsService
    .getNewsItem$()
    .pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private newsService: NewsService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('newsID')) {
      this.newsService.getNewsItem(this.route.snapshot.paramMap.get('newsID'));
    } else {
      this.allApp.router.navigate(['/news']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public checkTagInfo(tagName: string): void {
    const queryParams: Params = { ...this.route.snapshot.queryParams };
    queryParams.tag = tagName.trim();
    this.allApp.router.navigate(['/news'], { queryParams });
  }
}
