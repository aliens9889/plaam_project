import { Component, Input, OnInit } from '@angular/core';
import { LicenseCompany } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-license-company-item',
  templateUrl: './license-company-item.component.html',
  styleUrls: ['./license-company-item.component.scss'],
})
export class LicenseCompanyItemComponent implements OnInit {
  @Input() company: LicenseCompany;
  @Input() cardLink: boolean;

  constructor() {}

  ngOnInit(): void {}

  get hasImage(): boolean {
    if (this.company?.groupInfo?.imageUrl) {
      return true;
    }
    return false;
  }

  public onDownloadWallet(event): void {
    event.stopPropagation();
  }
}
