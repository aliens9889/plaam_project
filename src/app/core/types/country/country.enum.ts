export enum CountryEnum {
  DEFAULT = 'ES',
  ES = 'ES'
}

export type CountryType = 'ES';

export const CountryArray = ['ES'];

export const CountryInfo = [
  {
    es: {
      name: 'España'
    }
  }
];
