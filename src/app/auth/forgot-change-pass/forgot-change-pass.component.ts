import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ForgotChangePasswordJson, OAuthService, UserAccount, UserSnackbars } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { PasswordValidator } from '@qaroni-app/shared/validators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-forgot-change-pass',
  templateUrl: './forgot-change-pass.component.html',
  styleUrls: ['./forgot-change-pass.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ForgotChangePassComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;
  public InputConfig = InputConfig;

  public changePasswordForm: FormGroup;
  private changePasswordSkeleton = {
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(InputConfig.user.password.minLength),
      ],
    ],
    passwordConfirmation: [
      '',
      [
        Validators.required,
        Validators.minLength(InputConfig.user.password.minLength),
      ],
    ],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private oAuthService: OAuthService
  ) {
    this.createChangePasswordForm();
  }

  ngOnInit(): void {
    if (
      this.route.snapshot.paramMap.has('userID') &&
      this.route.snapshot.paramMap.has('otp')
    ) {
      this.subs.add(
        this.oAuthService.getUserAccount$().subscribe(this.getUserAccount)
      );
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(UserSnackbars.failureValidate.message),
        this.allApp.translate.instant(UserSnackbars.failureValidate.closeBtn),
        UserSnackbars.failureValidate.config
      );
      this.allApp.router.navigate(['/']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createChangePasswordForm(): void {
    this.changePasswordForm = this.fb.group(this.changePasswordSkeleton, {
      validators: PasswordValidator.matchPassword,
    });
  }

  get password(): AbstractControl {
    return this.changePasswordForm.get('password');
  }

  get passwordConfirmation(): AbstractControl {
    return this.changePasswordForm.get('passwordConfirmation');
  }

  get validatedForm(): boolean {
    return this.changePasswordForm.valid && !this.submitting;
  }

  public onSubmit() {
    if (this.validatedForm) {
      this.enableLoading();
      const forgotChangePasswordJSON: ForgotChangePasswordJson = {
        otp: this.route.snapshot.paramMap.get('otp'),
        password: this.password.value,
      };
      this.oAuthService.forgotChangePassword(
        this.route.snapshot.paramMap.get('userID'),
        forgotChangePasswordJSON
      );
    }
  }

  private getUserAccount = (userAccount: UserAccount): void => {
    this.disableLoading();
    if (userAccount) {
      this.allApp.router.navigate(['/auth', 'login']);
    }
  }
}
