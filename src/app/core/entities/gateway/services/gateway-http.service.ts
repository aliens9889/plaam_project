import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GatewayHttpService {
  constructor(private http: HttpClient) {}

  public getPaymentGateways$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/payments/gateways`;

    return this.http.get(url, { observe: 'response' });
  }
}
