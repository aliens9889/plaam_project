import { TestBed } from '@angular/core/testing';

import { PaymentsHttpService } from './payments-http.service';

describe('PaymentsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentsHttpService = TestBed.inject(PaymentsHttpService);
    expect(service).toBeTruthy();
  });
});
