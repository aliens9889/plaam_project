import { Component, OnInit } from '@angular/core';
import { OAuthStorageService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';

@Component({
  selector: 'qaroni-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  constructor(
    private allApp: AllAppService,
    private oAuthStorage: OAuthStorageService
  ) {
    this.oAuthStorage.reset();

    this.allApp.router.navigate(['/']);
  }

  ngOnInit(): void {}
}
