import { Element } from './element';
import { ElementTypeEnum } from './element-type.enum';

export interface ElementBox extends Element {
  type: ElementTypeEnum.BOX;
}
