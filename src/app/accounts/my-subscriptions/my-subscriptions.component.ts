import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  SubscriptionsService,
  UserSubscription
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.scss'],
})
export class MySubscriptionsComponent implements OnInit, OnDestroy {
  public subscriptions: UserSubscription[] = [];

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private subscriptionsService: SubscriptionsService
  ) {
    this.enableLoading();
    this.allApp.toolbar.showBackButton();
  }

  ngOnInit(): void {
    this.subs.add(
      this.subscriptionsService
        .getSubscriptions$()
        .subscribe(this.getSubscriptions)
    );
    this.subscriptionsService.getSubscriptions();

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getSubscriptions = (subscriptions: UserSubscription[]): void => {
    this.subscriptions = subscriptions;
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.subscriptionsService.getSubscriptions();
  }
}
