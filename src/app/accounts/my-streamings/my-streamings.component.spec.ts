import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyStreamingsComponent } from './my-streamings.component';

describe('MyStreamingsComponent', () => {
  let component: MyStreamingsComponent;
  let fixture: ComponentFixture<MyStreamingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyStreamingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyStreamingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
