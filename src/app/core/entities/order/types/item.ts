import { Currency } from '@qaroni-app/core/types';
import { ProductTypeEnum } from '../../product';
import { ItemUnitTypeEnum } from './item-unit-type.enum';

export interface Item {
  code: string;
  creationDate: string;
  currency: Currency;
  description: string;
  discount: string;
  imagesURL: string[];
  itemId: number;
  iva: number;
  lastUpdateDate: string;
  name: string;
  options: any;
  orderId: number;
  price: number;
  productId: number;
  quantity: number;
  type: ProductTypeEnum;
  unitName: string;
  unitType: ItemUnitTypeEnum;
  variantId: number;
}
