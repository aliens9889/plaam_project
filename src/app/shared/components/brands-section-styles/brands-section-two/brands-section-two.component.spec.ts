import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandsSectionTwoComponent } from './brands-section-two.component';

describe('BrandsSectionTwoComponent', () => {
  let component: BrandsSectionTwoComponent;
  let fixture: ComponentFixture<BrandsSectionTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandsSectionTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandsSectionTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
