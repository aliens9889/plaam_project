import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderResumeAmountsComponent } from './order-resume-amounts.component';

describe('OrderResumeAmountsComponent', () => {
  let component: OrderResumeAmountsComponent;
  let fixture: ComponentFixture<OrderResumeAmountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderResumeAmountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderResumeAmountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
