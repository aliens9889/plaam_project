import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsSectionOneComponent } from './events-section-one.component';

describe('EventsSectionOneComponent', () => {
  let component: EventsSectionOneComponent;
  let fixture: ComponentFixture<EventsSectionOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsSectionOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsSectionOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
