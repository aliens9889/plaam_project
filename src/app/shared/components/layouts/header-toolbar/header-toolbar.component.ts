import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { AppEnv, ImagesInfo, UrlsInfo } from '@qaroni-app/core/config';
import {
  AppService,
  Navbar,
  OAuthService,
  Order,
  OrderCreatedService,
  OrderUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { LanguageEnum } from '@qaroni-app/core/types';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-header-toolbar',
  templateUrl: './header-toolbar.component.html',
  styleUrls: ['./header-toolbar.component.scss'],
})
export class HeaderToolbarComponent implements OnInit, OnDestroy {
  public bpLg: BreakpointState;

  public stateProgressBar = false;
  public stateBackButton = false;

  private subs: Subscription = new Subscription();

  public OrderUtils = OrderUtils;
  public LanguageEnum = LanguageEnum;
  public ImagesInfo = ImagesInfo;
  public UrlsInfo = UrlsInfo;

  public shoppingCartItems = 0;

  public navbars: Navbar[] = [];

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor(
    private allApp: AllAppService,
    private oAuthService: OAuthService,
    private breakpointObserver: BreakpointObserver,
    private orderCreatedService: OrderCreatedService,
    private appService: AppService
  ) {}

  ngOnInit(): void {
    this.subs.add(
      this.allApp.progressBar
        .getProgressBar$()
        .subscribe(this.setProgressBarState)
    );

    this.subs.add(
      this.allApp.toolbar.getBackButton$().subscribe(this.setBackButtonState)
    );

    this.subs.add(
      this.breakpointObserver
        .observe(['(min-width: 1200px)'])
        .subscribe(this.updateBreakpointState)
    );

    this.subs.add(
      this.orderCreatedService
        .getOrderCreated$()
        .subscribe(this.successUpdateOrderCreated)
    );

    this.subs.add(this.appService.getNavbars$().subscribe(this.getNavbars));

    if (this.hasUser) {
      this.orderCreatedService.getOrderCreated();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private setProgressBarState = (state: boolean): void => {
    this.stateProgressBar = state;
  }

  private setBackButtonState = (state: boolean): void => {
    this.stateBackButton = state;
  }

  private updateBreakpointState = (state: BreakpointState): void => {
    this.bpLg = state;
  }

  get hasUser(): boolean {
    return this.oAuthService.hasOAuth;
  }

  public onBackButton(): void {
    if (this.allApp.toolbar.hasConfigBackUrl()) {
      this.allApp.router.navigate(this.allApp.toolbar.getConfigBackUrl());
    } else {
      this.allApp.location.back();
    }
  }

  public switchLang(language: LanguageEnum) {
    this.allApp.language.setLanguage(language);
    this.appService.getNavbars();
  }

  private successUpdateOrderCreated = (order: Order): void => {
    if (order && order.items && order.items.length) {
      this.shoppingCartItems = OrderUtils.totalItems(order);
    } else {
      this.shoppingCartItems = 0;
    }
  }

  private getNavbars = (navbars: Navbar[]): void => {
    this.navbars = navbars;
  }

  public getRouterLinkAction(action: string): string[] {
    return [action];
  }

  get getAllowShoppingCart(): boolean {
    return AppEnv.allowShoppingCart;
  }

  // ==========================================================================================
  // Languages
  // ==========================================================================================

  get showLanguageButton(): boolean {
    return this.allApp.language.showLanguageButton;
  }

  get showLanguageOptionEn(): boolean {
    return this.allApp.language.showLanguageOptionEn;
  }

  get showLanguageOptionEs(): boolean {
    return this.allApp.language.showLanguageOptionEs;
  }

  get showLanguageOptionGl(): boolean {
    return this.allApp.language.showLanguageOptionGl;
  }

  get showLanguageOptionCa(): boolean {
    return this.allApp.language.showLanguageOptionCa;
  }
}
