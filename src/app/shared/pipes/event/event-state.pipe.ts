import { Pipe, PipeTransform } from '@angular/core';
import { EventStateEnum, EventStateInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'eventState',
})
export class EventStatePipe implements PipeTransform {
  transform(value: EventStateEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of EventStateInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
