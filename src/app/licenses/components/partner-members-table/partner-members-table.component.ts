import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import {
  AssociationPartnerService,
  LicenseMember,
  PartnerMemberDataSource
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-partner-members-table',
  templateUrl: './partner-members-table.component.html',
  styleUrls: ['./partner-members-table.component.scss'],
})
export class PartnerMembersTableComponent implements OnInit, AfterViewInit {
  @Input() showPaginator = true;

  @Input() partnerID: number | string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) table: MatTable<LicenseMember>;
  public dataSource: PartnerMemberDataSource;

  public displayedColumns: string[] = [
    'creationDate',
    'code',
    'name',
    'document',
    'email',
    'phone',
    'status',
  ];

  constructor(private partnerService: AssociationPartnerService) {}

  ngOnInit(): void {
    this.dataSource = new PartnerMemberDataSource(
      this.partnerID,
      this.partnerService
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
