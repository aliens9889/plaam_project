import { Component, OnDestroy, OnInit } from '@angular/core';
import { AssociationService, GroupCategory, GroupCategoryStatusEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-groups-categories',
  templateUrl: './groups-categories.component.html',
  styleUrls: ['./groups-categories.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class GroupsCategoriesComponent implements OnInit, OnDestroy {
  private categories$: Observable<
    GroupCategory[]
  > = this.associationService.getCategories$().pipe(shareReplay(1));
  public categories: GroupCategory[];

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private associationService: AssociationService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.categories$.subscribe(this.getCategories));
    this.associationService.getCategories({
      status: GroupCategoryStatusEnum.ACTIVE,
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  get title(): string {
    return this.associationService.strings.getTitleComponentString();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getCategories = (categories: GroupCategory[]): void => {
    if (categories && categories.length) {
      this.categories = categories;
    } else {
      this.allApp.router.navigate(['/associations', 'groups']);
    }
  }
}
