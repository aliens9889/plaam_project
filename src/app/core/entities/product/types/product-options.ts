import { ProductOptionValues } from './product-option-values';

export interface ProductOptions {
  id: number;
  label: string;
  values: ProductOptionValues[];
}
