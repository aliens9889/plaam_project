import { Component, Input, OnInit } from '@angular/core';
import { ElementBlock } from '@qaroni-app/core/entities';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-element-block-section-one',
  templateUrl: './element-block-section-one.component.html',
  styleUrls: ['./element-block-section-one.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ElementBlockSectionOneComponent implements OnInit {
  @Input() blocks: ElementBlock[];

  constructor() {}

  ngOnInit(): void {}
}
