import { Pipe, PipeTransform } from '@angular/core';
import { EventTypeEnum, EventTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'eventType',
})
export class EventTypePipe implements PipeTransform {
  transform(value: EventTypeEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of EventTypeInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
