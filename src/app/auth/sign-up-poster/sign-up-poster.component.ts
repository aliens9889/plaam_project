import { Component, OnInit } from '@angular/core';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-sign-up-poster',
  templateUrl: './sign-up-poster.component.html',
  styleUrls: ['./sign-up-poster.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class SignUpPosterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
