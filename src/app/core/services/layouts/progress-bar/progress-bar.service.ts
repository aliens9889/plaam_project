import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {
  private progressBar$ = new Subject<boolean>();
  private state = false;

  constructor() {}

  public getProgressBar$(): Observable<boolean> {
    return this.progressBar$.asObservable();
  }

  public show(): void {
    this.state = true;
    this.progressBar$.next(this.state);
  }

  public hide(): void {
    this.state = false;
    this.progressBar$.next(this.state);
  }

  public toggle(): void {
    this.state = !this.state;
    this.progressBar$.next(this.state);
  }
}
