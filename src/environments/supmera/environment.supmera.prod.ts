import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '191',
};

export const appEnvironment = {
  title: 'Escuela de Paddle Surf - SUP Mera',
  basePath: 'https://my.supmera.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  blocks: AppLandingPageBlocksEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  brands: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: false,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plataforma.staging.plaam.com/subido/empresas/197_20200608145414_LogoSUPMera.jpg',
  toolbar:
    'https://plataforma.staging.plaam.com/subido/empresas/197_20200608145414_LogoSUPMera.jpg',
  footer:
    'https://plataforma.staging.plaam.com/subido/empresas/197_20200608145414_LogoSUPMera.jpg',
};

export const urlsEnvironment = {
  website: 'https://my.supmera.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'reservas@supmera.com',
  phone: '+34 681 223 850',
  facebook: 'https://www.facebook.com/supmera/',
  instagram: 'https://www.instagram.com/supmera/',
  privacy: 'https://my.supmera.com/#/',
  tsAndCs: 'https://my.supmera.com/#/',
  purchase: 'https://my.supmera.com/#/',
};
