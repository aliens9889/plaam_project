import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'routerLinkQueryParams'
})
export class RouterLinkQueryParamsPipe implements PipeTransform {
  transform(url: string): object {
    if (url.includes('?')) {
      const queryParams = url.split('?');
      const queryParam = queryParams[1].split('&');

      let objQueryParam = '{';
      for (let indexQP = 0; indexQP < queryParam.length; indexQP++) {
        const elementQP = queryParam[indexQP];
        const keyVal = elementQP.split('=');
        for (let indexKV = 0; indexKV < keyVal.length; indexKV++) {
          const elementKV = keyVal[indexKV];
          objQueryParam += '"' + elementKV + '"';
          if (!(indexKV % 2)) {
            objQueryParam += ':';
          }
        }
        if (indexQP !== queryParam.length - 1) {
          objQueryParam += ',';
        }
      }
      objQueryParam += '}';

      return JSON.parse(objQueryParam);
    } else {
      return null;
    }
  }
}
