import { LanguageEnum } from '@qaroni-app/core/types';
import { StreamingTypeEnum } from './streaming-type.enum';

export interface Streaming {
  creationDate: string;
  descriptions: StreamingDescription[];
  lastUpdateDate: string;
  merchantId: string;
  name: string;
  status: string;
  streamingId: number;
  type: StreamingTypeEnum;
  url: string;
}

export interface StreamingDescription {
  creationDate: string;
  description: string;
  language: LanguageEnum;
  lastUpdateDate: string;
  name: string;
  streamingId: number;
}
