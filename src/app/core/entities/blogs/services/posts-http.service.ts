import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PostsHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getPosts$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/blogs/posts`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  public getPost$(postID: number | string): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/blogs/posts/${postID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }
}
