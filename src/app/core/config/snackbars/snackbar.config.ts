export const SnackbarConfig = {
  strings: {
    close: 'Close'
  },
  classes: {
    primary: ['style-snackbars', 'primary-snackbar'],
    success: ['style-snackbars', 'success-snackbar'],
    warning: ['style-snackbars', 'warning-snackbar'],
    danger: ['style-snackbars', 'danger-snackbar']
  },
  durations: {
    primary: 6000,
    success: 6000,
    warning: 6000,
    danger: 15000
  }
};
