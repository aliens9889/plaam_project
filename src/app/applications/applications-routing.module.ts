import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppsLoginComponent } from './apps-login/apps-login.component';

const routes: Routes = [{ path: 'login', component: AppsLoginComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplicationsRoutingModule {}
