import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'routerLinkBase'
})
export class RouterLinkBasePipe implements PipeTransform {
  transform(url: string): string[] {
    if (url.includes('?')) {
      const split = url.split('?');
      return [split[0]];
    } else {
      return [url];
    }
  }
}
