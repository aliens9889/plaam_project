import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyItemComponent } from './license-company-item.component';

describe('LicenseCompanyItemComponent', () => {
  let component: LicenseCompanyItemComponent;
  let fixture: ComponentFixture<LicenseCompanyItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
