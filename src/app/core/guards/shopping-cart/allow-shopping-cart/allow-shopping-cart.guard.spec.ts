import { TestBed } from '@angular/core/testing';

import { AllowShoppingCartGuard } from './allow-shopping-cart.guard';

describe('AllowShoppingCartGuard', () => {
  let guard: AllowShoppingCartGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AllowShoppingCartGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
