import { LanguageEnum } from '@qaroni-app/core/types';
import { Group } from './group';
import { GroupCategoryStatusEnum } from './group-category-status.enum';

export interface GroupCategory {
  categoryId: number;
  creationDate: string;
  description: string;
  groups: Group[];
  imageUrl: string;
  language: LanguageEnum;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  status: GroupCategoryStatusEnum;
}
