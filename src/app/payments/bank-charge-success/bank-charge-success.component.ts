import { Component, OnInit } from '@angular/core';
import { OrderCreatedService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-bank-charge-success',
  templateUrl: './bank-charge-success.component.html',
  styleUrls: ['./bank-charge-success.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class BankChargeSuccessComponent implements OnInit {
  constructor(
    private allApp: AllAppService,
    private orderCreatedService: OrderCreatedService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.orderCreatedService.getOrderCreated();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }
}
