import { Component, Input, OnInit } from '@angular/core';
import { ProductBrand } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';

@Component({
  selector: 'qaroni-brands-section-two',
  templateUrl: './brands-section-two.component.html',
  styleUrls: ['./brands-section-two.component.scss'],
})
export class BrandsSectionTwoComponent implements OnInit {
  @Input() brands$: Observable<ProductBrand[]>;

  constructor() {}

  ngOnInit(): void {}
}
