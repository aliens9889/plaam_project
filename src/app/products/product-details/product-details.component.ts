import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Params } from '@angular/router';
import { AppEnv, ProductsEnv } from '@qaroni-app/core/config';
import {
  OAuthService,
  Order,
  OrderCreatedService,
  OrderSnackbars,
  OrderUpdateItem,
  OrderUpdateItemEnum,
  Product,
  ProductBrand,
  ProductCategory,
  ProductService,
  ProductUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  public product: Product;
  public products: Product[];
  public currentRoute = '';

  public initLoaded = false;

  private submitting = false;

  private subs: Subscription = new Subscription();

  public ProductUtils = ProductUtils;

  public quantity = 1;

  public product$: Observable<Product> = this.productService
    .getProduct$()
    .pipe(shareReplay(1));

  public brand$: Observable<
    ProductBrand
  > = this.productService.getProductBrand$().pipe(shareReplay(1));

  public productCategory$: Observable<
    ProductCategory
  > = this.productService.getProductCategory$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private productService: ProductService,
    private oAuthService: OAuthService,
    private orderCreatedService: OrderCreatedService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.allApp.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
  }

  ngOnInit(): void {
    this.subs.add(this.product$.subscribe(this.getProduct));
    this.subs.add(
      this.productService.getProducts$().subscribe(this.getProducts)
    );
    this.subs.add(
      this.orderCreatedService.getOrderCreated$().subscribe(this.getOrder)
    );
    this.productsCalls();
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.currentRoute = this.encodeURI(
      AppEnv.basePath + this.allApp.router.url
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  get getShowPrices(): boolean {
    return AppEnv.showPrices;
  }

  get getAllowShoppingCart(): boolean {
    return AppEnv.allowShoppingCart;
  }

  get showProductReference(): boolean {
    return ProductsEnv.showProductReference;
  }

  get changeTitleByCategory(): boolean {
    return ProductsEnv.detailsChangeTitleByCategory;
  }

  private productsCalls(): void {
    if (this.route.snapshot.paramMap.has('productID')) {
      this.productService.getProduct(
        this.route.snapshot.paramMap.get('productID')
      );
      this.orderCreatedService.getOrderCreated();
    } else {
      this.allApp.router.navigate(['/']);
    }
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
  }

  public onSelectOption(queryParams: Params): void {
    this.enableLoading();
    this.productService.getProductByOptions(
      this.product.productId,
      queryParams
    );
  }

  private getProduct = (product: Product): void => {
    this.product = product;
    this.disableLoading();
    this.updateQuantity();
    if (product?.categoryId) {
      this.productService.getProductCategory(product?.categoryId);
      this.productService.getProducts({
        categories: product.categoryId,
        last: 10,
      });
    } else {
      this.productService.getProducts({ last: 10 });
    }
    if (product?.brandId) {
      this.productService.getProductBrand(product.brandId);
    }
  }

  private getOrder = (order: Order): void => {
    this.disableLoading();
  }

  private getProducts = (products: Product[]): void => {
    this.products = products
      .filter((product: Product) => {
        return (
          product.productId.toString() !==
          this.route.snapshot.paramMap.get('productID')
        );
      })
      .sort(() => Math.random() - 0.5)
      .slice(0, 4);
  }

  get validatedBuyProduct(): boolean {
    return !this.submitting && (this.stockControl ? this.hasStock : true);
  }

  public onBuyProduct(product: Product): void {
    if (this.oAuthService.hasOAuth) {
      if (this.getAllowShoppingCart) {
        this.enableLoading();
        let variantID = product.productId;
        if (product.variant.variantId) {
          variantID = product.variant.variantId;
        }
        const dataJSON: OrderUpdateItem = {
          action: OrderUpdateItemEnum.ADD,
          quantity: this.quantity,
        };
        this.orderCreatedService.updateOrderItemFromProductVariant(
          product.productId,
          variantID,
          dataJSON
        );
      }
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.addProductMustBeLogin.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.addProductMustBeLogin.closeBtn
        ),
        OrderSnackbars.addProductMustBeLogin.config
      );
      const queryParams: Params = {
        redirectTo: this.allApp.router.url,
      };
      const navigationExtras: NavigationExtras = {
        queryParams,
      };
      this.allApp.router.navigate(['/auth', 'login'], navigationExtras);
    }
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.product = null;
    this.initLoaded = false;
    this.submitting = false;
    this.productsCalls();
  }

  get hasVariantOffer(): boolean {
    return this.productService.hasVariantOffer(this.product);
  }

  get isUnitTypeMetric(): boolean {
    return this.productService.isUnitTypeMetric(this.product);
  }

  get hasUnitName(): boolean {
    return this.productService.hasUnitName(this.product);
  }

  get getLabelOfferString(): string {
    return this.productService.getLabelOfferString(this.product);
  }

  public hasBrandLogo(brand: ProductBrand): boolean {
    if (brand.logo && brand.logo !== '') {
      return true;
    }
    return false;
  }

  public encodeURI(uri: number | string | boolean): string {
    return encodeURIComponent(uri);
  }

  public scrollToDescription(): void {
    const el = document.getElementsByClassName('card-product-large-info');
    if (el.length) {
      el[0].scrollIntoView({ block: 'center', behavior: 'smooth' });
    }
  }

  get showTabsInfo(): boolean {
    if (
      this.product?.description ||
      this.product?.attributes?.length ||
      this.product?.descriptionTags?.length
    ) {
      return true;
    }
    return false;
  }

  get stockControl(): boolean {
    return this.product?.variant?.stockControl;
  }

  get hasStock(): boolean {
    if (this.stockControl) {
      if (this.product?.variant?.stock > 0) {
        return true;
      }
      return false;
    }
    return false;
  }

  private updateQuantity(): void {
    if (
      this.stockControl &&
      this.hasStock &&
      this.quantity > this.product?.variant?.stock
    ) {
      this.quantity = this.product?.variant?.stock;
    }
  }

  public shareOnFacebook(): false {
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' + this.currentRoute,
      'facebook-share-dialog',
      'status=1,width=575,height=400'
    );
    return false;
  }
}
