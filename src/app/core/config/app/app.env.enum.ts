export enum AppTitleStyleEnum {
  ONE = 'ONE',
  MATERIAL = 'MATERIAL',
}

export enum AppFooterStyleEnum {
  ONE = 'ONE',
  TWO = 'TWO',
  THREE = 'THREE',
  FOUR = 'FOUR',
}

export enum AppLandingPageBrandsEnum {
  ONE = 'ONE',
  TWO = 'TWO',
}

export enum AppLandingPageProductsEnum {
  ONE = 'ONE',
  TWO = 'TWO',
}

export enum AppLandingPageGroupsEnum {
  CATEGORIES = 'CATEGORIES',
  GROUPS = 'GROUPS',
}

export enum AppLandingPageEventsEnum {
  ONE = 'ONE',
}

export enum AppLandingPageBlocksEnum {
  ONE = 'ONE',
}

export enum AppLandingPageBoxesEnum {
  ONE = 'ONE',
}

export enum AppProductsPageStyleEnum {
  ONE = 'ONE',
  TWO = 'TWO',
}

export enum AppAssociationTypeEnum {
  ASSOCIATION = 'ASSOCIATION',
  CHAMBER_OF_COMMERCE = 'CHAMBER_OF_COMMERCE',
  COLLEGIATE = 'COLLEGIATE',
  FEDERATION = 'FEDERATION',
}
