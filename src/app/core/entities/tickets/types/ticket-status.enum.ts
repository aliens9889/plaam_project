export enum TicketStatusEnum {
  DISABLED = 'DISABLED',
  ENABLED = 'ENABLED',
  USED = 'USED',
  EXPIRED = 'EXPIRED',
}

export type TicketStatusType = 'DISABLED' | 'ENABLED' | 'USED' | 'EXPIRED';

export const TicketStatusArray = ['DISABLED', 'ENABLED', 'USED', 'EXPIRED'];

export const TicketStatusInfo = [
  {
    disabled: {
      name: 'Disabled',
    },
  },
  {
    enabled: {
      name: 'Enabled',
    },
  },
  {
    used: {
      name: 'Used',
    },
  },
  {
    expired: {
      name: 'Expired',
    },
  },
];
