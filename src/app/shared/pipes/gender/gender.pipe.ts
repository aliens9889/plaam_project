import { Pipe, PipeTransform } from '@angular/core';
import { GenderInfo } from '@qaroni-app/core/types';

@Pipe({
  name: 'gender',
})
export class GenderPipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      value = value.trim().toLowerCase();
      for (const iterator of GenderInfo) {
        if (Object.keys(iterator)[0] === value) {
          return iterator[value].name;
        }
      }
    }
    return null;
  }
}
