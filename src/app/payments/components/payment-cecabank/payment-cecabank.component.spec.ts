import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCecabankComponent } from './payment-cecabank.component';

describe('PaymentCecabankComponent', () => {
  let component: PaymentCecabankComponent;
  let fixture: ComponentFixture<PaymentCecabankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentCecabankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCecabankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
