import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { PromotionalCodeJson } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';
import { BankChargeJson } from '../types/bank-charge-json';

@Injectable({
  providedIn: 'root',
})
export class PaymentsHttpService {
  constructor(private http: HttpClient) {}

  public paymentPayPal$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/paypal`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public paymentPaycomet$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/paycomet`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public paymentCecabank$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/cecabank`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public getPaymentRedsysData$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/redsys`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public getPaymentCofidisData$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/cofidis`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public getCalculatorCofidisUrl$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/calculators/cofidis`;

    return this.http.get(url, { observe: 'response' });
  }

  public getPaymentStripeCheckoutId$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/stripe`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public getPaymentAplazameCheckoutId$(
    userID: number | string,
    orderID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/checkouts/aplazame`;

    return this.http.post(url, null, { observe: 'response' });
  }

  public paymentBankCharge$(
    userID: number | string,
    orderID: number | string,
    bankChargeJSON: BankChargeJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/transactions`;

    return this.http.post(url, bankChargeJSON, { observe: 'response' });
  }

  public applyPromotionalCode$(
    userID: number | string,
    orderID: number | string,
    promotionalCodeJSON: PromotionalCodeJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/codes`;

    return this.http.post(url, promotionalCodeJSON, { observe: 'response' });
  }
}
