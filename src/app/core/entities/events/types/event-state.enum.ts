export enum EventStateEnum {
  INACTIVE = 'INACTIVE',
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
}

export type EventStateType = 'INACTIVE' | 'ACTIVE' | 'CREATED';

export const EventStateArray = ['INACTIVE', 'ACTIVE', 'CREATED'];

export const EventStateInfo = [
  {
    inactive: {
      name: 'Inactivo',
    },
  },
  {
    active: {
      name: 'Activo',
    },
  },
  {
    created: {
      name: 'Creado',
    },
  },
];
