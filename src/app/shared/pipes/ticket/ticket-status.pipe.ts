import { Pipe, PipeTransform } from '@angular/core';
import { TicketStatusEnum, TicketStatusInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'ticketStatus',
})
export class TicketStatusPipe implements PipeTransform {
  transform(value: TicketStatusEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of TicketStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return 'null';
  }
}
