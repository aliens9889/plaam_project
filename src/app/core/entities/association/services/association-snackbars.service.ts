import { Injectable } from '@angular/core';
import { AllAppService } from '@qaroni-app/core/services';
import { AssociationsSnackbars } from '../snackbars/associations-snackbars.config';

@Injectable({
  providedIn: 'root',
})
export class AssociationSnackbarsService {
  constructor(private allApp: AllAppService) {}

  public failureAllowMember(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureAllowMember.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureAllowMember.action
      ),
      AssociationsSnackbars.failureAllowMember.config
    );
  }

  public failureAllowCompany(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureAllowCompany.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureAllowCompany.action
      ),
      AssociationsSnackbars.failureAllowCompany.config
    );
  }

  public failureEnrollmentType(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureEnrollmentType.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureEnrollmentType.action
      ),
      AssociationsSnackbars.failureEnrollmentType.config
    );
  }

  public successEnrollmentRequest(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.successEnrollmentRequest.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.successEnrollmentRequest.action
      ),
      AssociationsSnackbars.successEnrollmentRequest.config
    );
  }

  public failureHasPartner(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureHasPartner.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureHasPartner.action
      ),
      AssociationsSnackbars.failureHasPartner.config
    );
  }

  public failureAllowedPartnerType(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureAllowedPartnerType.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureAllowedPartnerType.action
      ),
      AssociationsSnackbars.failureAllowedPartnerType.config
    );
  }

  public failureVoteTopicStatus(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureVoteTopicStatus.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureVoteTopicStatus.action
      ),
      AssociationsSnackbars.failureVoteTopicStatus.config
    );
  }

  public failureVoteTopicDelegate(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        AssociationsSnackbars.failureVoteTopicDelegate.message
      ),
      this.allApp.translate.instant(
        AssociationsSnackbars.failureVoteTopicDelegate.action
      ),
      AssociationsSnackbars.failureVoteTopicDelegate.config
    );
  }
}
