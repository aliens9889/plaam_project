import { CountryArray } from './country.enum';

export class CountryUtils {
  public static getCountryArray(): string[] {
    return CountryArray;
  }
}
