import { Component, Input, OnInit } from '@angular/core';
import { Webinar, WebinarsService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-webinar-preview',
  templateUrl: './webinar-preview.component.html',
  styleUrls: ['./webinar-preview.component.scss']
})
export class WebinarPreviewComponent implements OnInit {
  @Input() webinar: Webinar = null;

  constructor(public webinarService: WebinarsService) {}

  ngOnInit(): void {}
}
