import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import {
  AssociationLicenseService,
  LicenseCompany
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData, FileSnackbars } from '@qaroni-app/core/types';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { ConfirmationDialogComponent } from '@qaroni-app/shared/components/dialogs';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-company-image',
  templateUrl: './license-company-image.component.html',
  styleUrls: ['./license-company-image.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseCompanyImageComponent implements OnInit, OnDestroy {
  @Input() licenseCompany: LicenseCompany;

  public images: any = null;
  public imageUrl: string = null;

  private licenseCompany$: Observable<
    LicenseCompany
  > = this.licenseService.getLicenseCompany$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  private submitting = false;

  constructor(
    private allApp: AllAppService,
    private licenseService: AssociationLicenseService
  ) {}

  ngOnInit(): void {
    this.subs.add(this.licenseCompany$.subscribe(this.getLicenseCompany));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private getLicenseCompany = (licenseCompany: LicenseCompany): void => {
    this.disableLoading();
  }

  get validatedForm(): boolean {
    return !this.submitting;
  }

  public onFileChanged(event): void {
    if (event.target.files.length) {
      const rawArrayImages = Array.from(
        (event.target as HTMLInputElement).files
      );
      if (this.allApp.FilesValidations.validateImages(rawArrayImages)) {
        this.images = this.allApp.FilesValidations.allowOneFile(rawArrayImages);

        let dataMultipart = new FormData();
        dataMultipart = this.allApp.BuildHttpData.buildMultipartWithImages(
          dataMultipart,
          'image',
          this.images
        );

        this.enableLoading();

        this.licenseService.uploadLicenseCompanyImage(
          this.licenseCompany?.companyId,
          dataMultipart
        );
      } else {
        this.allApp.snackbar.open(
          FileSnackbars.failureImage.message,
          FileSnackbars.failureImage.action,
          FileSnackbars.failureImage.config
        );
      }
    }
  }

  public onDeleteImage(imageUrl: string): void {
    const confirmationData: ConfirmationDialogData = {
      title: this.allApp.translate.instant(`Delete image?`),
      message: this.allApp.translate.instant(
        `If you delete the image you will not be able to recover it, you can upload a new one`
      ),
      confirmFaIcon: `far fa-trash-alt`,
      confirmText: this.allApp.translate.instant(`Yes, delete`),
    };

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '700px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.data = confirmationData;

    const dialog = this.allApp.dialog.open<
      ConfirmationDialogComponent,
      ConfirmationDialogData,
      boolean
    >(ConfirmationDialogComponent, matDialogConfig);

    dialog.afterClosed().subscribe((confirmation) => {
      if (confirmation === true) {
        this.enableLoading();
        this.licenseService.deleteLicenseCompanyImage(
          this.licenseCompany?.companyId
        );
      }
    });
  }

  get title(): string {
    return this.licenseService.strings.getTitleLicenseCompanyImageEdit();
  }
}
