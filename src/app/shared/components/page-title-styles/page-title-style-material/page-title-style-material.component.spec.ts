import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTitleStyleMaterialComponent } from './page-title-style-material.component';

describe('PageTitleStyleMaterialComponent', () => {
  let component: PageTitleStyleMaterialComponent;
  let fixture: ComponentFixture<PageTitleStyleMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTitleStyleMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTitleStyleMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
