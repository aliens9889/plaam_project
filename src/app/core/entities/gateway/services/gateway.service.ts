import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { GatewayEnum } from '../types/gateway.enum';
import { GatewayHttpService } from './gateway-http.service';

@Injectable({
  providedIn: 'root',
})
export class GatewayService {
  private gatewaysSubject = new Subject<GatewayEnum[]>();

  constructor(
    private allApp: AllAppService,
    private gatewayHttp: GatewayHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Payment Gateways
  // ==========================================================================================
  public getGateways$(): Observable<GatewayEnum[]> {
    return this.gatewaysSubject.asObservable();
  }

  public getPaymentGateways(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.gatewayHttp
        .getPaymentGateways$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPaymentGateways,
          this.failureGetPaymentGateways
        );
    }
  }

  private successGetPaymentGateways = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const gateways: GatewayEnum[] = data.body.result;
      this.gatewaysSubject.next(gateways);
    } else {
      this.gatewaysSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPaymentGateways = (error: HttpErrorResponse): void => {
    this.gatewaysSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
