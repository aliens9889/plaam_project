import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoLabelValueComponent } from './info-label-value.component';

describe('InfoLabelValueComponent', () => {
  let component: InfoLabelValueComponent;
  let fixture: ComponentFixture<InfoLabelValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoLabelValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoLabelValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
