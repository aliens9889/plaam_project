export enum LicenseCompanyStatusEnum {
  ACTIVE = 'ACTIVE',
  CANCELLED = 'CANCELLED',
  EXPIRED = 'EXPIRED',
  LOCKED = 'LOCKED',
}

export type LicenseCompanyStatusType =
  | 'ACTIVE'
  | 'CANCELLED'
  | 'EXPIRED'
  | 'LOCKED';

export const LicenseCompanyStatusArray = [
  'ACTIVE',
  'CANCELLED',
  'EXPIRED',
  'LOCKED',
];

export const LicenseCompanyStatusInfo = [
  {
    active: {
      name: 'Active',
    },
  },
  {
    cancelled: {
      name: 'Cancelled',
    },
  },
  {
    expired: {
      name: 'Expired',
    },
  },
  {
    locked: {
      name: 'Locked',
    },
  },
];
