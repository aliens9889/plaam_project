import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from '@qaroni-app/core/guards';
import { OrderInvoiceComponent } from './order-invoice/order-invoice.component';

const routes: Routes = [
  {
    path: 'orders/:orderID',
    component: OrderInvoiceComponent,
    canActivate: [UserGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule {}
