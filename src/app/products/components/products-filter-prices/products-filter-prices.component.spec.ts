import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterPricesComponent } from './products-filter-prices.component';

describe('ProductsFilterPricesComponent', () => {
  let component: ProductsFilterPricesComponent;
  let fixture: ComponentFixture<ProductsFilterPricesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterPricesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterPricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
