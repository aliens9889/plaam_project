import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import {
  AppAssociationTypeEnum,
  AssociationEnv
} from '@qaroni-app/core/config';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { PaginationLinks } from '@qaroni-app/core/types';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { CompanyProfile } from '../types/company';
import { GroupInscriptionCompany } from '../types/group-inscription-company';
import { GroupInscriptionMember } from '../types/group-inscription-member';
import { LicenseCompany } from '../types/license-company';
import { LicenseMember } from '../types/license-member';
import { AssociationPartnerHttpService } from './association-partner-http.service';

@Injectable({
  providedIn: 'root',
})
export class AssociationPartnerService {
  private companyProfileSubject = new Subject<CompanyProfile>();
  private companiesProfilesSubject = new Subject<CompanyProfile[]>();

  private partnerMembersSubject = new Subject<LicenseMember[]>();
  private partnerCompaniesSubject = new Subject<LicenseCompany[]>();

  private memberInscriptionsSubject = new Subject<GroupInscriptionMember[]>();
  private companyInscriptionsSubject = new Subject<GroupInscriptionCompany[]>();

  private paginationLinksSubject = new Subject<PaginationLinks>();

  constructor(
    private allApp: AllAppService,
    private partnerHttp: AssociationPartnerHttpService,
    private oAuthService: OAuthService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get allowPartnerMember(): boolean {
    if (
      AssociationEnv.associationType === AppAssociationTypeEnum.ASSOCIATION ||
      AssociationEnv.associationType === AppAssociationTypeEnum.FEDERATION ||
      AssociationEnv.associationType === AppAssociationTypeEnum.COLLEGIATE
    ) {
      return true;
    }
    return false;
  }

  get allowPartnerCompany(): boolean {
    if (
      AssociationEnv.associationType === AppAssociationTypeEnum.ASSOCIATION ||
      AssociationEnv.associationType ===
        AppAssociationTypeEnum.CHAMBER_OF_COMMERCE
    ) {
      return true;
    }
    return false;
  }

  public getPartnerProfileUrlTradename(partner: CompanyProfile): string {
    if (partner?.tradename) {
      const urlName = partner.tradename
        .replace(/  +/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase();
      return urlName;
    }
    return '';
  }

  // ==========================================================================================
  // Get Groups Partners
  // ==========================================================================================
  public getPartners$(): Observable<CompanyProfile[]> {
    return this.companiesProfilesSubject.asObservable();
  }

  public getPartners(queryParams?: Params): void {
    this.enableLoading();
    this.partnerHttp
      .getPartners$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetPartners, this.failureGetPartners);
  }

  private successGetPartners = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const partners: CompanyProfile[] = data.body.result;
      this.companiesProfilesSubject.next(partners);
    } else {
      this.companiesProfilesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPartners = (error: HttpErrorResponse): void => {
    this.companiesProfilesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Group Partner
  // ==========================================================================================
  public getPartner$(): Observable<CompanyProfile> {
    return this.companyProfileSubject.asObservable();
  }

  public getPartner(partnerID: number | string): void {
    this.enableLoading();
    this.partnerHttp
      .getPartner$(partnerID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetPartner, this.failureGetPartner);
  }

  private successGetPartner = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const partner: CompanyProfile = data.body.result[0];
      this.companyProfileSubject.next(partner);
    } else {
      this.companyProfileSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPartner = (error: HttpErrorResponse): void => {
    this.companyProfileSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Pagination Links
  // ==========================================================================================

  public getPaginationLinks$(): Observable<PaginationLinks> {
    return this.paginationLinksSubject.asObservable();
  }

  // ==========================================================================================
  // Get Licence Company Partners Members
  // ==========================================================================================
  public getPartnerMembers$(): Observable<LicenseMember[]> {
    return this.partnerMembersSubject.asObservable();
  }

  public resetPartnerMembers(): void {
    return this.partnerMembersSubject.next(null);
  }

  public getPartnerMembers(
    partnerID: number | string,
    queryParams?: Params
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.partnerHttp
        .getLicenseCompanyPartnerMembers$(
          this.oAuthService.getUserID,
          partnerID,
          queryParams
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPartnerMembers,
          this.failureGetPartnerMembers
        );
    }
  }

  private successGetPartnerMembers = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const members: LicenseMember[] = data.body.result;
      this.partnerMembersSubject.next(members);
      if (data.body?.links) {
        const links: PaginationLinks = data.body.links;
        this.paginationLinksSubject.next(links);
      }
    } else {
      this.partnerMembersSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPartnerMembers = (error: HttpErrorResponse): void => {
    this.partnerMembersSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Licence Company Partner Member Inscriptions
  // ==========================================================================================
  public getMemberInscriptions$(): Observable<GroupInscriptionMember[]> {
    return this.memberInscriptionsSubject.asObservable();
  }

  public resetMemberInscriptions(): void {
    return this.memberInscriptionsSubject.next(null);
  }

  public getMemberInscriptions(
    partnerID: number | string,
    queryParams?: Params
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.partnerHttp
        .getLicenseCompanyPartnerMemberInscriptions$(
          this.oAuthService.getUserID,
          partnerID,
          queryParams
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetMemberInscriptions,
          this.failureGetMemberInscriptions
        );
    }
  }

  private successGetMemberInscriptions = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const memberInscriptions: GroupInscriptionMember[] = data.body.result;
      this.memberInscriptionsSubject.next(memberInscriptions);
      if (data.body?.links) {
        const links: PaginationLinks = data.body.links;
        this.paginationLinksSubject.next(links);
      }
    } else {
      this.memberInscriptionsSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetMemberInscriptions = (error: HttpErrorResponse): void => {
    this.memberInscriptionsSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Licence Company Partners Companies
  // ==========================================================================================
  public getPartnerCompanies$(): Observable<LicenseCompany[]> {
    return this.partnerCompaniesSubject.asObservable();
  }

  public resetPartnerCompanies(): void {
    return this.partnerCompaniesSubject.next(null);
  }

  public getPartnerCompanies(
    partnerID: number | string,
    queryParams?: Params
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.partnerHttp
        .getLicenseCompanyPartnerCompanies$(
          this.oAuthService.getUserID,
          partnerID,
          queryParams
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPartnerCompanies,
          this.failureGetPartnerCompanies
        );
    }
  }

  private successGetPartnerCompanies = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const companies: LicenseCompany[] = data.body.result;
      this.partnerCompaniesSubject.next(companies);
      if (data.body?.links) {
        const links: PaginationLinks = data.body.links;
        this.paginationLinksSubject.next(links);
      }
    } else {
      this.partnerCompaniesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPartnerCompanies = (error: HttpErrorResponse): void => {
    this.partnerCompaniesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Licence Company Partner Company Inscriptions
  // ==========================================================================================
  public getCompanyInscriptions$(): Observable<GroupInscriptionCompany[]> {
    return this.companyInscriptionsSubject.asObservable();
  }

  public resetCompanyInscriptions(): void {
    return this.companyInscriptionsSubject.next(null);
  }

  public getCompanyInscriptions(
    partnerID: number | string,
    queryParams?: Params
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.partnerHttp
        .getLicenseCompanyPartnerCompanyInscriptions$(
          this.oAuthService.getUserID,
          partnerID,
          queryParams
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetCompanyInscriptions,
          this.failureGetCompanyInscriptions
        );
    }
  }

  private successGetCompanyInscriptions = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const companyInscriptions: GroupInscriptionCompany[] = data.body.result;
      this.companyInscriptionsSubject.next(companyInscriptions);
      if (data.body?.links) {
        const links: PaginationLinks = data.body.links;
        this.paginationLinksSubject.next(links);
      }
    } else {
      this.companyInscriptionsSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetCompanyInscriptions = (error: HttpErrorResponse): void => {
    this.companyInscriptionsSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
