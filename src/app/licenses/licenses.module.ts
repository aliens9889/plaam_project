import { NgModule } from '@angular/core';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { AssembliesComponent } from './assemblies/assemblies.component';
import { AssemblyDetailsComponent } from './assembly-details/assembly-details.component';
import { LicenseCompanyImageComponent } from './components/license-company-image/license-company-image.component';
import { LicenseMemberImageComponent } from './components/license-member-image/license-member-image.component';
import { PartnerCompaniesTableComponent } from './components/partner-companies-table/partner-companies-table.component';
import { PartnerCompanyInscriptionsTableComponent } from './components/partner-company-inscriptions-table/partner-company-inscriptions-table.component';
import { PartnerMemberInscriptionsTableComponent } from './components/partner-member-inscriptions-table/partner-member-inscriptions-table.component';
import { PartnerMembersTableComponent } from './components/partner-members-table/partner-members-table.component';
import { LicenseCompanyDetailsComponent } from './license-company-details/license-company-details.component';
import { LicenseCompanyEditAddressComponent } from './license-company-edit-address/license-company-edit-address.component';
import { LicenseCompanyEditContactAddressComponent } from './license-company-edit-contact-address/license-company-edit-contact-address.component';
import { LicenseCompanyEditContactInformationComponent } from './license-company-edit-contact-information/license-company-edit-contact-information.component';
import { LicenseCompanyEditInformationComponent } from './license-company-edit-information/license-company-edit-information.component';
import { LicenseMemberDetailsComponent } from './license-member-details/license-member-details.component';
import { LicenseMemberEditAddressComponent } from './license-member-edit-address/license-member-edit-address.component';
import { LicenseMemberEditInformationComponent } from './license-member-edit-information/license-member-edit-information.component';
import { LicensesRoutingModule } from './licenses-routing.module';
import { PartnerCompaniesListComponent } from './partner-companies-list/partner-companies-list.component';
import { PartnerCompanyInscriptionsListComponent } from './partner-company-inscriptions-list/partner-company-inscriptions-list.component';
import { PartnerMemberInscriptionsListComponent } from './partner-member-inscriptions-list/partner-member-inscriptions-list.component';
import { PartnerMembersListComponent } from './partner-members-list/partner-members-list.component';
import { AssemblyVoteDialogComponent } from './components/dialogs/assembly-vote-dialog/assembly-vote-dialog.component';

@NgModule({
  declarations: [
    AssembliesComponent,
    AssemblyDetailsComponent,
    LicenseCompanyDetailsComponent,
    LicenseCompanyEditAddressComponent,
    LicenseCompanyEditContactAddressComponent,
    LicenseCompanyEditContactInformationComponent,
    LicenseCompanyEditInformationComponent,
    LicenseCompanyImageComponent,
    LicenseMemberDetailsComponent,
    LicenseMemberEditAddressComponent,
    LicenseMemberEditInformationComponent,
    LicenseMemberImageComponent,
    PartnerCompaniesListComponent,
    PartnerCompaniesTableComponent,
    PartnerCompanyInscriptionsListComponent,
    PartnerCompanyInscriptionsTableComponent,
    PartnerMemberInscriptionsListComponent,
    PartnerMemberInscriptionsTableComponent,
    PartnerMembersListComponent,
    PartnerMembersTableComponent,
    AssemblyVoteDialogComponent,
  ],
  imports: [
    SharedModule,
    LicensesRoutingModule,
    TranslateModule,
    CKEditorModule,
  ],
})
export class LicensesModule {}
