import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from '@qaroni-app/core/guards';
import { EnrollmentGroupCompanyComponent } from './enrollments/enrollment-group-company/enrollment-group-company.component';
import { EnrollmentGroupMemberComponent } from './enrollments/enrollment-group-member/enrollment-group-member.component';
import { AllGroupsComponent } from './groups/all-groups/all-groups.component';
import { GroupsCategoriesComponent } from './groups/groups-categories/groups-categories.component';
import { GroupsCategoryComponent } from './groups/groups-category/groups-category.component';
import { CompleteRequestCompanyComponent } from './my-enrollments-requests/complete-request-company/complete-request-company.component';
import { CompleteRequestMemberComponent } from './my-enrollments-requests/complete-request-member/complete-request-member.component';
import { EditRequestCompanyComponent } from './my-enrollments-requests/edit-request-company/edit-request-company.component';
import { EditRequestMemberComponent } from './my-enrollments-requests/edit-request-member/edit-request-member.component';
import { MyEnrollmentRequestsComponent } from './my-enrollments-requests/my-enrollment-requests/my-enrollment-requests.component';
import { MyLicensesComponent } from './my-licenses/my-licenses/my-licenses.component';
import { AllPartnersComponent } from './partners/all-partners/all-partners.component';
import { PartnerDetailsComponent } from './partners/partner-details/partner-details.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'groups' },
  {
    path: 'groups',
    component: AllGroupsComponent,
  },
  {
    path: 'groups/partners/:partnerID',
    component: AllGroupsComponent,
  },
  {
    path: 'groups-categories',
    component: GroupsCategoriesComponent,
  },
  {
    path: 'groups-categories/:categoryID',
    component: GroupsCategoryComponent,
  },
  {
    path: 'groups/:groupID',
    pathMatch: 'full',
    redirectTo: 'groups',
  },
  {
    path: 'groups/:groupID/members',
    component: EnrollmentGroupMemberComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'groups/:groupID/members/partners/:partnerID',
    component: EnrollmentGroupMemberComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'groups/:groupID/companies',
    component: EnrollmentGroupCompanyComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'groups/:groupID/companies/partners/:partnerID',
    component: EnrollmentGroupCompanyComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-enrollment-requests',
    component: MyEnrollmentRequestsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-enrollment-requests/:inscriptionID/complete/members',
    component: CompleteRequestMemberComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'groups/:groupID/my-enrollment-requests/:inscriptionID/edit/members',
    component: EditRequestMemberComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-enrollment-requests/:inscriptionID/complete/companies',
    component: CompleteRequestCompanyComponent,
    canActivate: [UserGuard],
  },
  {
    path:
      'groups/:groupID/my-enrollment-requests/:inscriptionID/edit/companies',
    component: EditRequestCompanyComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-licenses',
    component: MyLicensesComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'partners',
    component: AllPartnersComponent,
  },
  {
    path: 'partners/:partnerID',
    component: PartnerDetailsComponent,
  },
  {
    path: 'partners/:partnerID/:partnerTradename',
    component: PartnerDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssociationsRoutingModule {}
