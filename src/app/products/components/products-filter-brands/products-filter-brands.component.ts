import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductBrand, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-products-filter-brands',
  templateUrl: './products-filter-brands.component.html',
  styleUrls: ['./products-filter-brands.component.scss'],
})
export class ProductsFilterBrandsComponent implements OnInit, OnDestroy {
  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  public productBrands$: Observable<
    ProductBrand[]
  > = this.productService.getProductBrands$().pipe(shareReplay(1));

  public showFewOptions = true;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private productService: ProductService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(this.getQueryParams);
    this.productService.getProductBrands();

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private getQueryParams = (queryParams: Params) => {
    console.log('Entre');

    const allParams: Params = { ...this.route.snapshot.params };
    console.log('que tengo de QueryParams', queryParams);
    console.log('que tengo en allParams', allParams);

    if (queryParams.categories) {
      console.log('AQUI ESTOY');
    }

    if (queryParams.hasOwnProperty('categories')) {
      console.log('In if');
      const valuesArr: string[] = queryParams.categories.split(',');
      console.log('valuesArr', valuesArr);
      console.log('valuesArr[0]', valuesArr[0]);
      if (valuesArr[0] !== '0') {
        queryParams.categories = valuesArr[0];
      } else {
        delete queryParams.categories;
      }
      this.productService.getProductBrands(queryParams);
    }
  }

  public isOptionSelected(id: number): boolean {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('brands')) {
      const valuesArr: string[] = queryParams.brands.split(',');
      if (valuesArr.indexOf(`${id}`) !== -1) {
        return true;
      }
    }
    return false;
  }

  public onSelectionChange(list: MatSelectionListChange): void {
    const selectedValues: number[] = [];
    list.source.selectedOptions.selected.forEach((option) => {
      selectedValues.push(option.value);
    });
    const brands = selectedValues.sort().join(',');

    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );
    if (brands) {
      queryParamsEmit.brands = brands;
    } else {
      delete queryParamsEmit.brands;
    }
    this.queryParamsChange.emit(queryParamsEmit);
  }

  public onShowAll(): void {
    this.showFewOptions = !this.showFewOptions;
  }

  private languageChanged = (): void => {
    this.productService.getProductBrands();
  }
}
