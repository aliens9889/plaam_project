import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssociationEnv } from '@qaroni-app/core/config';
import { AssociationService, GroupInscriptionFile, GroupInscriptionMember, GroupTypeEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-complete-request-member',
  templateUrl: './complete-request-member.component.html',
  styleUrls: ['./complete-request-member.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class CompleteRequestMemberComponent implements OnInit, OnDestroy {
  public inscription$: Observable<
    GroupInscriptionMember
  > = this.associationService.getInscriptionMember$().pipe(shareReplay(1));
  public inscription: GroupInscriptionMember;

  private subs: Subscription = new Subscription();

  public submitting = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private associationService: AssociationService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (
      this.route.snapshot.paramMap.has('inscriptionID') &&
      AssociationEnv.allowsMembers === true
    ) {
      this.subs.add(this.inscription$.subscribe(this.getInscription));
      this.associationService.getInscriptionMember(
        this.route.snapshot.paramMap.get('inscriptionID')
      );
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private redirect(): void {
    this.allApp.router.navigate(['/associations', 'my-enrollment-requests']);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getInscription = (inscription: GroupInscriptionMember): void => {
    this.submitting = false;
    if (inscription && inscription?.group?.type === GroupTypeEnum.PERSON) {
    } else {
      this.redirect();
    }
  }

  public onFileChanged(theFile: File[], file: GroupInscriptionFile) {
    if (theFile && file) {
      let dataMultipart = new FormData();
      dataMultipart = this.allApp.BuildHttpData.buildMultipartWithImages(
        dataMultipart,
        file.name,
        theFile
      );

      this.submitting = true;
      if (this.route.snapshot.paramMap.has('inscriptionID')) {
        this.associationService.addFileToInscriptionMember(
          this.route.snapshot.paramMap.get('inscriptionID'),
          dataMultipart
        );
      }
    }
  }
}
