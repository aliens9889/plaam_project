export enum InvoicePaymentMethodEnum {
  APLAZAME = 'APLAZAME',
  BANK_CHARGE = 'BANK_CHARGE',
  CARD = 'CARD',
  CASH = 'CASH',
  CECABANK = 'CECABANK',
  CETELEM = 'CETELEM',
  COFIDIS = 'COFIDIS',
  PAYCOMET = 'PAYCOMET',
  PAYPAL = 'PAYPAL',
  REDSYS = 'REDSYS',
  STRIPE = 'STRIPE',
  TRANSFER = 'TRANSFER',
}

export type InvoicePaymentMethodType =
  | 'APLAZAME'
  | 'BANK_CHARGE'
  | 'CARD'
  | 'CASH'
  | 'CECABANK'
  | 'CETELEM'
  | 'COFIDIS'
  | 'PAYCOMET'
  | 'PAYPAL'
  | 'REDSYS'
  | 'STRIPE'
  | 'TRANSFER';

export const InvoicePaymentMethodArray = [
  'APLAZAME',
  'BANK_CHARGE',
  'CARD',
  'CASH',
  'CECABANK',
  'CETELEM',
  'COFIDIS',
  'PAYCOMET',
  'PAYPAL',
  'REDSYS',
  'STRIPE',
  'TRANSFER',
];

export const InvoicePaymentMethodInfo = [
  {
    aplazame: {
      name: 'Aplazame',
    },
  },
  {
    bank_charge: {
      name: 'Recibo bancario',
    },
  },
  {
    card: {
      name: 'Card',
    },
  },
  {
    cash: {
      name: 'Cash',
    },
  },
  {
    cecabank: {
      name: 'Cecabank',
    },
  },
  {
    cetelem: {
      name: 'Cetelem',
    },
  },
  {
    cofidis: {
      name: 'Cofidis',
    },
  },
  {
    paycomet: {
      name: 'Paycomet',
    },
  },
  {
    paypal: {
      name: 'PayPal',
    },
  },
  {
    redsys: {
      name: 'Redsys',
    },
  },
  {
    stripe: {
      name: 'Stripe',
    },
  },
  {
    transfer: {
      name: 'Transfer',
    },
  },
];
