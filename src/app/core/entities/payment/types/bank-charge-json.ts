import { GatewayEnum } from '../../gateway';

export interface BankChargeJson {
  bic: string;
  iban: string;
  name: string;
  paymentMethod: GatewayEnum.BANK_CHARGE;
}
