import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { UrlsInfo } from '@qaroni-app/core/config';
import { OAuthService, UserAccount } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { LanguageEnum } from '@qaroni-app/core/types';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { PasswordValidator } from '@qaroni-app/shared/validators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class SignUpComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;

  private subs: Subscription = new Subscription();

  public signUpForm: FormGroup;
  private signUpSkeleton = {
    username: ['', Validators.compose([Validators.required, Validators.email])],
    password: [
      '',
      Validators.compose([Validators.required, Validators.minLength(8)]),
    ],
    passwordConfirmation: [
      '',
      Validators.compose([Validators.required, Validators.minLength(8)]),
    ],
    acceptLOPD: [
      false,
      Validators.compose([Validators.required, Validators.requiredTrue]),
    ],
    language: ['', Validators.compose([Validators.required])],
  };

  private submitting = false;

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private oAuthService: OAuthService
  ) {
    this.createSignUpForm();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.updateLanguageValue)
    );
    this.subs.add(
      this.oAuthService.getUserAccount$().subscribe(this.getUserAccount)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createSignUpForm(): void {
    this.signUpForm = this.fb.group(this.signUpSkeleton, {
      validators: PasswordValidator.matchPassword,
    });
    if (this.hasReCaptcha) {
      const captchaCtl: FormControl = new FormControl('', Validators.required);
      this.signUpForm.addControl('captcha', captchaCtl);
    }
    this.populateSignUpForm();
  }

  private populateSignUpForm(): void {
    this.language.patchValue(this.allApp.appStorage.getLanguage());
  }

  private updateLanguageValue = (language: LanguageEnum): void => {
    this.language.patchValue(language);
  }

  get username(): AbstractControl {
    return this.signUpForm.get('username');
  }

  get password(): AbstractControl {
    return this.signUpForm.get('password');
  }

  get passwordConfirmation(): AbstractControl {
    return this.signUpForm.get('passwordConfirmation');
  }

  get acceptLOPD(): AbstractControl {
    return this.signUpForm.get('acceptLOPD');
  }

  get language(): AbstractControl {
    return this.signUpForm.get('language');
  }

  get captcha(): AbstractControl {
    return this.signUpForm.get('captcha');
  }

  get validatedForm(): boolean {
    return this.signUpForm.valid && !this.submitting;
  }

  public onSubmit() {
    if (this.validatedForm) {
      this.enableLoading();
      this.oAuthService.registerUser(this.signUpForm.getRawValue());
    }
  }

  private getUserAccount = (userAccount: UserAccount): void => {
    if (userAccount) {
      this.allApp.router.navigate(['/auth', 'sign-up', 'poster']);
    } else {
      this.disableLoading();
    }
  }

  get getPrivacyUrl(): string {
    if (UrlsInfo.privacy) {
      return UrlsInfo.privacy;
    } else {
      return '#/';
    }
  }

  get hasReCaptcha(): boolean {
    return this.allApp.hasReCaptcha;
  }

  public onCaptchaResponse(token: string): void {
    this.captcha.setValue(token);
  }

  public onCaptchaExpired(): void {
    this.captcha.setValue('');
  }
}
