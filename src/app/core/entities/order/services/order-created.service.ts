import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { OrderSnackbars } from '../snackbars/order-snackbar.config';
import { Order } from '../types/order';
import { OrderStatusEnum } from '../types/order-status.enum';
import { OrderTypeEnum } from '../types/order-type.enum';
import { OrderUpdateItem } from '../types/order-update-item';
import { UserOrdersHttpService } from './user-orders-http.service';

@Injectable({
  providedIn: 'root',
})
export class OrderCreatedService {
  private orderCreated$ = new Subject<Order>();

  constructor(
    private allApp: AllAppService,
    private ordersHttp: UserOrdersHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Order Created
  // ==========================================================================================
  public getOrderCreated$(): Observable<Order> {
    return this.orderCreated$.asObservable();
  }

  public getOrderCreated(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      const params: Params = {
        status: OrderStatusEnum.CREATED,
        type: OrderTypeEnum.ONLINE,
      };
      this.ordersHttp
        .getOrders$(this.oAuthService.getUserID, params)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetOrdersCreated, this.failureGetOrdersCreated);
    }
  }

  private successGetOrdersCreated = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const ordersCreated: Order[] = data.body.result;
      let order: Order = null;
      if (ordersCreated.length) {
        order = ordersCreated[0];
      } else {
        order = {
          amount: 0,
          clientId: null,
          code: null,
          creationDate: null,
          deliveryAmount: 0,
          deliveryId: null,
          description: null,
          invoices: [],
          items: [],
          lastUpdateDate: null,
          merchantId: null,
          name: null,
          number: null,
          orderId: null,
          serieId: null,
          status: OrderStatusEnum.CREATED,
          trackingNumber: null,
          type: OrderTypeEnum.ONLINE,
          userId: null,
        };
      }
      this.orderCreated$.next(order);
    } else {
      this.orderCreated$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetOrdersCreated = (error: HttpErrorResponse): void => {
    this.orderCreated$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Empty Order Created
  // ==========================================================================================
  public emptyOrderItems(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .emptyOrderItems$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successEmptyOrder, this.failureEmptyOrder);
    }
  }

  private successEmptyOrder = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderCreated$.next(order);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(OrderSnackbars.successEmptyOrder.message),
        this.allApp.translate.instant(
          OrderSnackbars.successEmptyOrder.closeBtn
        ),
        OrderSnackbars.successEmptyOrder.config
      );
    } else {
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureEmptyOrder = (error: HttpErrorResponse): void => {
    this.getOrderCreated();
    this.allApp.snackbar.open(
      this.allApp.translate.instant(OrderSnackbars.failureEmptyOrder.message),
      this.allApp.translate.instant(OrderSnackbars.failureEmptyOrder.closeBtn),
      OrderSnackbars.failureEmptyOrder.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Delete Item in Order Created
  // ==========================================================================================
  public deleteOrderItem(
    orderID: number | string,
    itemID: number | string
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .deleteOrderItem$(this.oAuthService.getUserID, orderID, itemID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successDeleteItem, this.failureDeleteItem);
    }
  }

  private successDeleteItem = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderCreated$.next(order);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(OrderSnackbars.successDeleteItem.message),
        this.allApp.translate.instant(
          OrderSnackbars.successDeleteItem.closeBtn
        ),
        OrderSnackbars.successDeleteItem.config
      );
    } else {
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureDeleteItem = (error: HttpErrorResponse): void => {
    this.getOrderCreated();
    this.allApp.snackbar.open(
      this.allApp.translate.instant(OrderSnackbars.failureDeleteItem.message),
      this.allApp.translate.instant(OrderSnackbars.failureDeleteItem.closeBtn),
      OrderSnackbars.failureDeleteItem.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Empty Order Created
  // ==========================================================================================
  public updateOrderItemFromProductVariant(
    productID: number | string,
    variantID: number | string,
    dataJSON?: OrderUpdateItem
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .addOrderItemFromProductVariant$(
          this.oAuthService.getUserID,
          productID,
          variantID,
          dataJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successUpdateItem, this.failureUpdateItem);
    }
  }

  private successUpdateItem = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderCreated$.next(order);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(OrderSnackbars.successUpdateItem.message),
        this.allApp.translate.instant(
          OrderSnackbars.successUpdateItem.closeBtn
        ),
        OrderSnackbars.successUpdateItem.config
      );
    } else {
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateItem = (error: HttpErrorResponse): void => {
    this.getOrderCreated();
    if (
      this.commonsHttp.errorsHttp.errorIsControlled(
        error,
        'E0011',
        'No hay capacidad para las entradas del pedido'
      )
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.failureUpdateItemAvailability.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.failureUpdateItemAvailability.closeBtn
        ),
        OrderSnackbars.failureUpdateItemAvailability.config
      );
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(OrderSnackbars.failureUpdateItem.message),
        this.allApp.translate.instant(
          OrderSnackbars.failureUpdateItem.closeBtn
        ),
        OrderSnackbars.failureUpdateItem.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Set Order Deliveries
  // ==========================================================================================
  public updateOrderDeliveries(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ordersHttp
        .updateOrderDeliveries$(this.oAuthService.getUserID, orderID, null)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateOrderDeliveries,
          this.failureUpdateOrderDeliveries
        );
    }
  }

  private successUpdateOrderDeliveries = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.orderCreated$.next(order);
    } else {
      this.orderCreated$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateOrderDeliveries = (error: HttpErrorResponse): void => {
    this.orderCreated$.next(null);
    if (
      error.status === 400 &&
      error.error &&
      error.error.errors &&
      error.error.errors.length &&
      error.error.errors[0] &&
      error.error.errors[0].code &&
      error.error.errors[0].code === 'E0015'
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.failureDeliveriesE0015.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.failureDeliveriesE0015.closeBtn
        ),
        OrderSnackbars.failureDeliveriesE0015.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }
}
