import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDetailOptionsComponent } from './product-detail-options.component';

describe('ProductDetailOptionsComponent', () => {
  let component: ProductDetailOptionsComponent;
  let fixture: ComponentFixture<ProductDetailOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDetailOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
