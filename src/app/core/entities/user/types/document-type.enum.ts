export enum DocumentTypeEnum {
  DNI = 'DNI',
  NIE = 'NIE',
  CIF = 'CIF',
  PASSPORT = 'PASSPORT'
}

export type DocumentType = 'DNI' | 'NIE' | 'CIF' | 'PASSPORT';

export const DocumentTypeArray = ['DNI', 'NIE', 'CIF', 'PASSPORT'];

export const DocumentTypeInfo = [
  {
    dni: {
      name: 'DNI'
    }
  },
  {
    nie: {
      name: 'NIE'
    }
  },
  {
    cif: {
      name: 'CIF'
    }
  },
  {
    passport: {
      name: 'Pasaporte'
    }
  }
];
