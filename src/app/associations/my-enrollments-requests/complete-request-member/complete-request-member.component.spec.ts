import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteRequestMemberComponent } from './complete-request-member.component';

describe('CompleteRequestMemberComponent', () => {
  let component: CompleteRequestMemberComponent;
  let fixture: ComponentFixture<CompleteRequestMemberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteRequestMemberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteRequestMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
