import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import {
  AssociationPartnerService,
  GroupInscriptionMember,
  PartnerMemberInscriptionDataSource
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-partner-member-inscriptions-table',
  templateUrl: './partner-member-inscriptions-table.component.html',
  styleUrls: ['./partner-member-inscriptions-table.component.scss'],
})
export class PartnerMemberInscriptionsTableComponent
  implements OnInit, AfterViewInit {
  @Input() showPaginator = true;

  @Input() partnerID: number | string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) table: MatTable<GroupInscriptionMember>;
  public dataSource: PartnerMemberInscriptionDataSource;

  public displayedColumns: string[] = [
    'creationDate',
    'name',
    'document',
    'email',
    'phone',
    'type',
    'status',
  ];

  constructor(private partnerService: AssociationPartnerService) {}

  ngOnInit(): void {
    this.dataSource = new PartnerMemberInscriptionDataSource(
      this.partnerID,
      this.partnerService
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
