import { GoogleReCaptchaEnvironment } from 'src/environments/environment';

export const GoogleReCaptchaEnv = {
  scriptSrc:
    'https://www.google.com/recaptcha/api.js?onload=gReCaptchaOnLoadCallback&amp;render=explicit',
  sitekey: GoogleReCaptchaEnvironment.hasOwnProperty('sitekey')
    ? GoogleReCaptchaEnvironment.sitekey
    : null,
};
