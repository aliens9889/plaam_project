import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  UserData,
  UserDataService,
  UserDataUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-user-data-form',
  templateUrl: './user-data-form.component.html',
  styleUrls: ['./user-data-form.component.scss'],
})
export class UserDataFormComponent implements OnInit, OnDestroy {
  @Input() precharge = false;

  public InputValidation = InputValidation;
  public UserDataUtils = UserDataUtils;

  public userDataForm: FormGroup;
  private userDataSkeleton = {
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    documentType: ['', [Validators.required]],
    document: ['', [Validators.required]],
    birthday: ['', [Validators.required]],
    gender: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
  };

  private userData$: Observable<
    UserData
  > = this.userDataService.getUserData$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private userDataService: UserDataService
  ) {
    this.enableLoading();
    this.createUserDataForm();
  }

  ngOnInit(): void {
    if (this.precharge === true) {
      this.subs.add(this.userData$.subscribe(this.getUserData));
      this.userDataService.getUserData();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private createUserDataForm(): void {
    this.userDataForm = this.fb.group(this.userDataSkeleton);
  }

  public populateUserDataForm(userData: UserData): void {
    this.userDataForm.patchValue(userData);
    // if (userData.firstName) {
    //   this.firstName.disable();
    // }
    // if (userData.lastName) {
    //   this.lastName.disable();
    // }
    // if (userData.documentType) {
    //   this.documentType.disable();
    // }
    // if (userData.document) {
    //   this.document.disable();
    // }
    if (userData.birthday) {
      this.birthday.setValue(moment(userData.birthday));
      // this.birthday.disable();
    }
  }

  get firstName(): AbstractControl {
    return this.userDataForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.userDataForm.get('lastName');
  }

  get documentType(): AbstractControl {
    return this.userDataForm.get('documentType');
  }

  get document(): AbstractControl {
    return this.userDataForm.get('document');
  }

  get birthday(): AbstractControl {
    return this.userDataForm.get('birthday');
  }

  get gender(): AbstractControl {
    return this.userDataForm.get('gender');
  }

  get email(): AbstractControl {
    return this.userDataForm.get('email');
  }

  get phone(): AbstractControl {
    return this.userDataForm.get('phone');
  }

  get validatedForm(): boolean {
    return this.userDataForm.valid;
  }

  private getUserData = (userData: UserData): void => {
    if (userData) {
      this.populateUserDataForm(userData);
    }
  }

  public prepateFormToSend(): void {
    if (this.birthday.value && this.birthday.value.isValid()) {
      this.birthday.setValue(this.birthday.value.format('YYYY-MM-DD'));
    } else {
      this.birthday.patchValue('');
    }
  }
}
