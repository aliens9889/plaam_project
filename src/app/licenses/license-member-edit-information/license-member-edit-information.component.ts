import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ActivatedRoute } from '@angular/router';
import {
  AssociationLicenseService,
  LicenseMember,
  UserDataUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-member-edit-information',
  templateUrl: './license-member-edit-information.component.html',
  styleUrls: ['./license-member-edit-information.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseMemberEditInformationComponent
  implements OnInit, OnDestroy {
  private licenseMemberID: string;

  public initLoaded = false;

  private licenseMember$: Observable<
    LicenseMember
  > = this.licenseService.getLicenseMember$().pipe(shareReplay(1));
  public licenseMember: LicenseMember;

  private subs: Subscription = new Subscription();

  public userInfoForm: FormGroup;
  private userInfoSkeleton = {
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    documentType: ['', [Validators.required]],
    document: ['', [Validators.required]],
    birthday: ['', [Validators.required]],
    gender: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
    automaticRenewal: [null],
    bankCharge: [null],
    iban: ['', Validators.compose([Validators.pattern(/^[A-Z]{2}\d{18,26}$/)])],
    bic: [''],
  };

  private submitting = false;
  private formSent = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private licenseService: AssociationLicenseService
  ) {
    this.licenseMemberID = route.snapshot.paramMap.get('licenseMemberID');
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.createUserInfoForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('licenseMemberID')) {
      this.subs.add(this.licenseMember$.subscribe(this.getLicenseMember));
      this.licenseService.getLicenseMember(this.licenseMemberID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
    this.formSent = false;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private createUserInfoForm(): void {
    this.userInfoForm = this.fb.group(this.userInfoSkeleton);
  }

  public populateUserInfoForm(licenseMember: LicenseMember): void {
    if (licenseMember) {
      if (licenseMember?.userInfo) {
        this.userInfoForm.patchValue(licenseMember?.userInfo);
        if (licenseMember?.userInfo?.birthday) {
          this.birthday.setValue(moment(licenseMember?.userInfo?.birthday));
        }
      }

      this.automaticRenewal.patchValue(
        licenseMember?.automaticRenewal ? true : false
      );
      this.bankCharge.patchValue(licenseMember?.bankCharge ? true : false);
      this.iban.patchValue(licenseMember?.iban);
      this.bic.patchValue(licenseMember?.bic);
      this.actionsRenewal();
    }
  }

  get InputValidation() {
    return InputValidation;
  }

  get UserDataUtils() {
    return UserDataUtils;
  }

  get firstName(): AbstractControl {
    return this.userInfoForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.userInfoForm.get('lastName');
  }

  get documentType(): AbstractControl {
    return this.userInfoForm.get('documentType');
  }

  get document(): AbstractControl {
    return this.userInfoForm.get('document');
  }

  get birthday(): AbstractControl {
    return this.userInfoForm.get('birthday');
  }

  get gender(): AbstractControl {
    return this.userInfoForm.get('gender');
  }

  get email(): AbstractControl {
    return this.userInfoForm.get('email');
  }

  get phone(): AbstractControl {
    return this.userInfoForm.get('phone');
  }

  get automaticRenewal(): AbstractControl {
    return this.userInfoForm.get('automaticRenewal');
  }

  get bankCharge(): AbstractControl {
    return this.userInfoForm.get('bankCharge');
  }

  get iban(): AbstractControl {
    return this.userInfoForm.get('iban');
  }

  get bic(): AbstractControl {
    return this.userInfoForm.get('bic');
  }

  get validatedForm(): boolean {
    return (
      this.userInfoForm.dirty && this.userInfoForm.valid && !this.submitting
    );
  }

  private getLicenseMember = (licenseMember: LicenseMember): void => {
    if (licenseMember) {
      if (this.formSent) {
        this.onBack();
      } else {
        this.licenseMember = licenseMember;
        this.populateUserInfoForm(this.licenseMember);
      }
    }
    this.disableLoading();
  }

  private prepateFormToSend(): void {
    if (this.birthday.value && this.birthday.value.isValid()) {
      this.birthday.setValue(this.birthday.value.format('YYYY-MM-DD'));
    } else {
      this.birthday.patchValue('');
    }
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.prepateFormToSend();
      this.licenseService.updateLicenseMemberUserInfo(
        this.licenseMemberID,
        this.userInfoForm.getRawValue()
      );
      this.formSent = true;
    }
  }

  get titlePage(): string {
    return this.licenseService.strings.getTitleLicenseMemberUserEdit();
  }

  get titleCard(): string {
    return this.licenseService.strings.getStepTitlePersonalInfo();
  }

  public onBack(): void {
    this.allApp.location.back();
  }

  public onChangeRenewal(event: MatCheckboxChange): void {
    this.actionsRenewal();
  }

  private actionsRenewal(): void {
    if (this.automaticRenewal.value === true) {
      this.bankCharge.enable();
    } else if (this.automaticRenewal.value === false) {
      this.bankCharge.setValue(false);
      this.bankCharge.disable();
    }

    this.iban.clearValidators();
    if (this.bankCharge.value === true) {
      this.iban.setValidators(
        Validators.compose([
          Validators.required,
          Validators.pattern(/^[A-Z]{2}\d{18,26}$/),
        ])
      );
    } else if (this.bankCharge.value === false) {
      this.iban.setValidators(Validators.pattern(/^[A-Z]{2}\d{18,26}$/));
    }
    this.iban.updateValueAndValidity();
  }
}
