import { Breakpoints } from '@angular/cdk/layout';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Invoice, Order } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-order-invoices',
  templateUrl: './order-invoices.component.html',
  styleUrls: ['./order-invoices.component.scss'],
})
export class OrderInvoicesComponent implements OnInit {
  @Input() order: Order;

  @Output() invoice: EventEmitter<Invoice> = new EventEmitter();

  isSmall$: Observable<boolean> = this.allApp.breakpointObserver
    .observe([Breakpoints.XSmall, Breakpoints.Small])
    .pipe(
      map((result) => result.matches),
      shareReplay(1)
    );

  constructor(private allApp: AllAppService) {}

  ngOnInit(): void {}

  public onGetInvoice(invoice: Invoice): void {
    if (invoice?.userId && invoice?.orderId && invoice?.invoiceId) {
      this.invoice.emit(invoice);
    }
  }
}
