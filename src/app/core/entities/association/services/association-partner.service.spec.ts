import { TestBed } from '@angular/core/testing';

import { AssociationPartnerService } from './association-partner.service';

describe('AssociationPartnerService', () => {
  let service: AssociationPartnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssociationPartnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
