export interface CofidisData {
  apellido1: string;
  apellido2: string;
  apellidos: string;
  cantidadCompra1: number;
  carencia: string;
  descCompra1: number;
  importe: number;
  nombre: string;
  postUrl: string;
  precioCompra1: number;
  precioTotal: number;
  producto: string;
  referencia: string;
  url_acept: string;
  url_cancel: string;
  url_confirm: string;
  url_error: string;
  url_rechaz: string;
  vendedor: string;
}
