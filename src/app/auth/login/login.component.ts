import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {
  OAuth,
  OAuthService,
  OrderCreatedService,
  UserData,
  UserDataService
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LoginComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;

  public loginForm: FormGroup;
  private loginSkeleton = {
    username: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private oAuthService: OAuthService,
    private userDataService: UserDataService,
    private orderCreatedService: OrderCreatedService,
    private route: ActivatedRoute
  ) {
    this.createLoginForm();
  }

  ngOnInit(): void {
    this.subs.add(this.oAuthService.getOAuth$().subscribe(this.getOAuth));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createLoginForm(): void {
    this.loginForm = this.fb.group(this.loginSkeleton);
  }

  get username(): AbstractControl {
    return this.loginForm.get('username');
  }

  get password(): AbstractControl {
    return this.loginForm.get('password');
  }

  get validatedForm(): boolean {
    return this.loginForm.valid && !this.submitting;
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.oAuthService.getOAuth(this.loginForm.getRawValue());
    }
  }

  private getOAuth = (oAuth: OAuth): void => {
    if (oAuth) {
      this.subs.add(
        this.userDataService.getUserData$().subscribe(this.successGetUserData)
      );
      this.userDataService.getUserData();
    } else {
      this.disableLoading();
    }
  }

  private successGetUserData = (userData: UserData): void => {
    this.allApp.language.setLanguage(userData.language);
    this.orderCreatedService.getOrderCreated();
    this.route.queryParamMap.subscribe(this.getQueryParamMap);
  }

  private getQueryParamMap = (queryParamMap: ParamMap) => {
    if (queryParamMap.has('redirectTo')) {
      this.allApp.router.navigate([queryParamMap.get('redirectTo')]);
    } else {
      this.allApp.router.navigate(['/']);
    }
  }
}
