import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '121',
};

export const appEnvironment = {
  title: 'Federación Catalana de Surf',
  basePath: 'https://my.fsurf.cat/#',
  languagesAllowed: [LanguageEnum.ca, LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  groups: AppLandingPageGroupsEnum.GROUPS,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  brands: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '5:4',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.FEDERATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: 'https://fsurf.cat/wp-content/uploads/2018/01/ucs-logo.png',
  toolbar: 'https://fsurf.cat/wp-content/uploads/2018/01/ucs-logo.png',
  footer:
    'https://fsurf.cat/wp-content/uploads/2018/01/ucs-logo-retina-white.png',
};

export const urlsEnvironment = {
  website: 'https://my.fsurf.cat/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@fsurf.cat',
  facebook: 'https://www.facebook.com/uniocatalanadesurf/',
  instagram: 'https://www.instagram.com/uniocatalanadesurf/',
  privacy: 'https://my.fsurf.cat/#/',
  cookies: 'https://my.fsurf.cat/#/',
  tsAndCs: 'https://my.fsurf.cat/#/',
};
