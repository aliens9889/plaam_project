import { LanguageEnum, Location } from '@qaroni-app/core/types';
import { Address } from '../../address';
import { Zone } from '../../events';
import { Attendee } from './attendee';
import { TicketStatusEnum } from './ticket-status.enum';

export interface Ticket {
  address: Address;
  capacity: number;
  categoryId: number;
  code: string;
  creationDate: string;
  document: string;
  email: string;
  eventId: number;
  finishDate: string;
  firstName: string;
  imagesURL: string[];
  isPaid: number;
  language: LanguageEnum;
  largeDescription: string;
  lastName: string;
  lastUpdateDate: string;
  location: Location;
  merchantId: number;
  novelty: number;
  phone: string;
  place: string;
  shortDescription: string;
  startDate: string;
  state: string;
  status: TicketStatusEnum;
  templateUUID: string;
  ticketUUID: string;
  title: string;
  type: string;
  walletUrl: string;
  zone: Zone;
}
export interface FreeTicketJson {
  quantity: number;
  zoneId: string;
  attendees: Attendee[];
}
