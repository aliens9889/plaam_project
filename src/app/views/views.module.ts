import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { PathNotFoundComponent } from './path-not-found/path-not-found.component';

export { PathNotFoundComponent };

@NgModule({
  declarations: [PathNotFoundComponent],
  imports: [SharedModule, TranslateModule],
  exports: []
})
export class ViewsModule {}
