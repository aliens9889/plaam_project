import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Page } from '../types/page';
import { PageHttpService } from './page-http.service';

@Injectable({
  providedIn: 'root',
})
export class PageService {
  private page$ = new Subject<Page>();

  constructor(
    private allApp: AllAppService,
    private pageHttp: PageHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Page
  // ==========================================================================================

  public getPage$(): Observable<Page> {
    return this.page$.asObservable();
  }

  public getPage(pageID: number | string): void {
    this.enableLoading();
    this.pageHttp
      .getPage$(pageID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetPage, this.failureGetPage);
  }

  private successGetPage = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const page: Page = data.body.result[0];
      this.page$.next(page);
    } else {
      this.page$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPage = (error: HttpErrorResponse): void => {
    this.page$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
