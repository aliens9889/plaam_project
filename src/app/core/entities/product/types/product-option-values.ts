export interface ProductOptionValues {
  backgroundColor: string;
  imageURL: string;
  value: string;
}
