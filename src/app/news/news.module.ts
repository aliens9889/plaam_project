import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { NewsRoutingModule } from './news-routing.module';
import { NewsShowComponent } from './news-show/news-show.component';
import { NewsComponent } from './news/news.component';

@NgModule({
  declarations: [NewsComponent, NewsShowComponent],
  imports: [SharedModule, NewsRoutingModule, TranslateModule],
})
export class NewsModule {}
