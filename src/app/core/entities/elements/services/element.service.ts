import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ElementBlock } from '../types/element-block';
import { ElementBox } from '../types/element-box';
import { ElementHttpService } from './element-http.service';

@Injectable({
  providedIn: 'root',
})
export class ElementService {
  private blocksSubject = new Subject<ElementBlock[]>();
  private blockSubject = new Subject<ElementBlock>();

  private boxesSubject = new Subject<ElementBox[]>();
  private boxesubject = new Subject<ElementBox>();

  constructor(
    private allApp: AllAppService,
    private elementHttp: ElementHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Blocks
  // ==========================================================================================

  public getBlocks$(): Observable<ElementBlock[]> {
    return this.blocksSubject.asObservable();
  }

  public getBlocks(queryParams?: Params): void {
    this.enableLoading();
    this.elementHttp
      .getElements$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetBlocks, this.failureGetBlocks);
  }

  private successGetBlocks = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const blocks: ElementBlock[] = data.body.result;
      this.blocksSubject.next(blocks);
    } else {
      this.blocksSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetBlocks = (error: HttpErrorResponse): void => {
    this.blocksSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Element Block
  // ==========================================================================================

  public getBlock$(): Observable<ElementBlock> {
    return this.blockSubject.asObservable();
  }

  public getBlock(elementID: number | string): void {
    this.enableLoading();
    this.elementHttp
      .getElement$(elementID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetBlock, this.failureGetBlock);
  }

  private successGetBlock = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const block: ElementBlock = data.body.result[0];
      this.blockSubject.next(block);
    } else {
      this.blockSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetBlock = (error: HttpErrorResponse): void => {
    this.blockSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Boxes
  // ==========================================================================================

  public getBoxes$(): Observable<ElementBox[]> {
    return this.boxesSubject.asObservable();
  }

  public getBoxes(queryParams?: Params): void {
    this.enableLoading();
    this.elementHttp
      .getElements$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetBoxes, this.failureGetBoxes);
  }

  private successGetBoxes = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const box: ElementBox[] = data.body.result;
      this.boxesSubject.next(box);
    } else {
      this.boxesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetBoxes = (error: HttpErrorResponse): void => {
    this.boxesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Element Box
  // ==========================================================================================

  public getBox$(): Observable<ElementBox> {
    return this.boxesubject.asObservable();
  }

  public getBox(elementID: number | string): void {
    this.enableLoading();
    this.elementHttp
      .getElement$(elementID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetBox, this.failureGetBox);
  }

  private successGetBox = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const box: ElementBox = data.body.result[0];
      this.boxesubject.next(box);
    } else {
      this.boxesubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetBox = (error: HttpErrorResponse): void => {
    this.boxesubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
