import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllAppService } from '@qaroni-app/core/services';
import { CountryEnum, CountryUtils } from '@qaroni-app/core/types';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';

@Component({
  selector: 'qaroni-company-address-form',
  templateUrl: './company-address-form.component.html',
  styleUrls: ['./company-address-form.component.scss'],
})
export class CompanyAddressFormComponent implements OnInit {
  public InputValidation = InputValidation;
  public InputConfig = InputConfig;
  public CountryUtils = CountryUtils;

  public companyAddressForm: FormGroup;
  private companyAddressSkeleton = {
    line1: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    line2: [
      '',
      Validators.compose([
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    postalCode: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.postalCode.maxLength),
      ]),
    ],
    city: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.city.maxLength),
      ]),
    ],
    stateProvince: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.stateProvince.maxLength),
      ]),
    ],
    country: [
      { value: CountryEnum.DEFAULT, disabled: true },
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.country.maxLength),
      ]),
    ],
  };

  constructor(private allApp: AllAppService, private fb: FormBuilder) {
    this.enableLoading();
    this.createAddressForm();
  }

  ngOnInit(): void {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private createAddressForm(): void {
    this.companyAddressForm = this.fb.group(this.companyAddressSkeleton);
  }

  get line1(): AbstractControl {
    return this.companyAddressForm.get('line1');
  }

  get line2(): AbstractControl {
    return this.companyAddressForm.get('line2');
  }

  get postalCode(): AbstractControl {
    return this.companyAddressForm.get('postalCode');
  }

  get city(): AbstractControl {
    return this.companyAddressForm.get('city');
  }

  get stateProvince(): AbstractControl {
    return this.companyAddressForm.get('stateProvince');
  }

  get country(): AbstractControl {
    return this.companyAddressForm.get('country');
  }

  get validatedForm(): boolean {
    return this.companyAddressForm.valid;
  }
}
