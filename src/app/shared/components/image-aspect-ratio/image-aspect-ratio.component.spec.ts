import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageAspectRatioComponent } from './image-aspect-ratio.component';

describe('ImageAspectRatioComponent', () => {
  let component: ImageAspectRatioComponent;
  let fixture: ComponentFixture<ImageAspectRatioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageAspectRatioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageAspectRatioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
