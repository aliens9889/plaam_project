import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { ImagesInfo } from '@qaroni-app/core/config';
import { Address, UserAddressService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData } from '@qaroni-app/core/types';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import {
  AddressFormDialogComponent,
  ConfirmationDialogComponent
} from '@qaroni-app/shared/components/dialogs';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-my-addresses',
  templateUrl: './my-addresses.component.html',
  styleUrls: ['./my-addresses.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class MyAddressesComponent implements OnInit, OnDestroy {
  public ImagesInfo = ImagesInfo;

  public userAddresses: Address[] = null;

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private userAddressService: UserAddressService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.userAddressService
        .getUserAddresses$()
        .subscribe(this.getUserAddresses)
    );
    this.userAddressService.getUserAddresses();
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getUserAddresses = (addresses: Address[]): void => {
    if (addresses && addresses.length) {
      this.userAddresses = addresses.sort(
        this.userAddressService.sortAddressesDefault
      );
    }
    this.initLoaded = true;
  }

  public createAddress(): void {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '500px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;

    const dialog = this.allApp.dialog.open(
      AddressFormDialogComponent,
      matDialogConfig
    );

    dialog.afterClosed().subscribe((addressResponse: Address) => {
      if (addressResponse) {
        this.userAddressService.getUserAddresses();
      }
    });
  }

  public onEditAddress(address: Address): void {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '500px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;
    matDialogConfig.data = address;

    const dialog = this.allApp.dialog.open(
      AddressFormDialogComponent,
      matDialogConfig
    );

    dialog.afterClosed().subscribe((addressResponse: Address) => {
      if (addressResponse) {
        this.userAddressService.getUserAddresses();
      }
    });
  }

  public onDeleteAddress(address: Address): void {
    if (!this.userAddressService.isAddressDefault(address)) {
      const confirmationData: ConfirmationDialogData = {
        title: this.allApp.translate.instant('Delete address'),
        message:
          this.allApp.translate.instant(
            'Delete the address is an action that cannot be undone.'
          ) +
          '<br><br>' +
          this.allApp.translate.instant(
            'Do you really want to delete the address?'
          ),
        confirmFaIcon: 'far fa-trash-alt',
        confirmText: this.allApp.translate.instant('yes-delete'),
      };

      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = '700px';
      matDialogConfig.maxWidth = '90vw';
      matDialogConfig.panelClass = 'style-confirm-dialog';
      matDialogConfig.autoFocus = false;
      matDialogConfig.data = confirmationData;

      const dialog = this.allApp.dialog.open(
        ConfirmationDialogComponent,
        matDialogConfig
      );

      dialog.afterClosed().subscribe((confirmation) => {
        if (confirmation === true) {
          this.subs.add(
            this.userAddressService
              .getDeletedUserAddressID$()
              .subscribe(this.deleteUserAddress)
          );
          this.userAddressService.deleteUserAddress(address);
        }
      });
    }
  }

  private deleteUserAddress = (addressID: number) => {
    this.userAddresses = this.userAddresses.filter((address: Address) => {
      return address.addressId !== addressID;
    });
  }
}
