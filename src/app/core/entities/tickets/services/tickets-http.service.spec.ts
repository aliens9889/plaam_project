import { TestBed } from '@angular/core/testing';

import { TicketsHttpService } from './tickets-http.service';

describe('TicketsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TicketsHttpService = TestBed.inject(TicketsHttpService);
    expect(service).toBeTruthy();
  });
});
