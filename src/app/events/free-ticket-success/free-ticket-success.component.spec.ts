import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeTicketSuccessComponent } from './free-ticket-success.component';

describe('FreeTicketSuccessComponent', () => {
  let component: FreeTicketSuccessComponent;
  let fixture: ComponentFixture<FreeTicketSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTicketSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTicketSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
