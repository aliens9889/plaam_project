import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Client } from '../types/client';
import { ClientRequest } from '../types/client-request';
import { ClientHttpService } from './client-http.service';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  private clientSubject = new Subject<Client>();
  private clientsSubject = new Subject<Client[]>();
  private provincesSubject = new Subject<any>();

  constructor(
    private allApp: AllAppService,
    private clientHttp: ClientHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Clients
  // ==========================================================================================

  public getClients$(): Observable<Client[]> {
    return this.clientsSubject.asObservable();
  }

  public getClients(orderId: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.clientHttp
        .getClients$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetClients, this.failureGetClients);
    }
  }

  private successGetClients = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const clients: Client[] = data.body.result;
      this.clientsSubject.next(clients);
    } else {
      this.clientsSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetClients = (error: HttpErrorResponse): void => {
    this.clientsSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create Client
  // ==========================================================================================
  public createClient(clientJSON: ClientRequest): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.clientHttp
        .createClient$(this.oAuthService.getUserID, clientJSON)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successCreateClient, this.failureCreateClient);
    }
  }

  private successCreateClient = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus201(data)) {
      const clients: Client[] = data.body.result;
      this.clientsSubject.next(clients);
    } else {
      this.clientsSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureCreateClient = (error: HttpErrorResponse): void => {
    this.clientsSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Edit Client
  // ==========================================================================================
  public editClient(
    clientID: number | string,
    clientJSON: ClientRequest
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.clientHttp
        .editClient$(this.oAuthService.getUserID, clientID, clientJSON)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successEditClient, this.failureEditClient);
    }
  }

  private successEditClient = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const clients: Client[] = data.body.result;
      this.clientsSubject.next(clients);
    } else {
      this.clientsSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureEditClient = (error: HttpErrorResponse): void => {
    this.clientsSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Delete Client
  // ==========================================================================================
  public deleteClient(clientID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.clientHttp
        .deleteClient$(this.oAuthService.getUserID, clientID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successDeleteClient, this.failureDeleteClient);
    }
  }

  private successDeleteClient = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const clients: Client[] = data.body.result;
      this.clientsSubject.next(clients);
    } else {
      this.clientsSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureDeleteClient = (error: HttpErrorResponse): void => {
    this.clientsSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Client
  // ==========================================================================================

  public getClient$(): Observable<Client> {
    return this.clientSubject.asObservable();
  }

  public getClient(clientID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.clientHttp
        .getClient$(this.oAuthService.getUserID, clientID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetClient, this.failureGetClient);
    }
  }

  private successGetClient = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const client: Client = data.body.result[0];
      this.clientSubject.next(client);
    } else {
      this.clientSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetClient = (error: HttpErrorResponse): void => {
    this.clientSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Provinces
  // ==========================================================================================

  public getProvinces$(): Observable<any> {
    return this.provincesSubject.asObservable();
  }

  public getProvinces(): void {
    this.enableLoading();
    this.clientHttp
      .getProvinces$()
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetProvinces, this.failureGetProvinces);
  }

  private successGetProvinces = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const provinces = data.body.result;
      this.provincesSubject.next(provinces);
    } else {
      this.provincesSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetProvinces = (error: HttpErrorResponse): void => {
    this.provincesSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
