import { Component, Input, OnInit } from '@angular/core';
import {
  AssociationPartnerService,
  CompanyProfile
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-partner-preview',
  templateUrl: './partner-preview.component.html',
  styleUrls: ['./partner-preview.component.scss'],
})
export class PartnerPreviewComponent implements OnInit {
  @Input() partner: CompanyProfile;

  constructor(private partnerService: AssociationPartnerService) {}

  ngOnInit(): void {}

  get hasImage(): boolean {
    if (this.partner?.imageUrl) {
      return true;
    }
    return false;
  }

  get getPartnerProfileUrlTradename(): string {
    if (this.partner) {
      return this.partnerService.getPartnerProfileUrlTradename(this.partner);
    }
  }
}
