import { AplazameEnvironment } from 'src/environments/environment';

export const AplazameEnv = {
  scriptSrc: AplazameEnvironment.hasOwnProperty('scriptSrc')
    ? AplazameEnvironment.scriptSrc
    : null,
  publicKey: AplazameEnvironment.hasOwnProperty('publicKey')
    ? AplazameEnvironment.publicKey
    : null,
};
