import { TestBed } from '@angular/core/testing';

import { ShippingsHttpService } from './shippings-http.service';

describe('ShippingsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShippingsHttpService = TestBed.inject(ShippingsHttpService);
    expect(service).toBeTruthy();
  });
});
