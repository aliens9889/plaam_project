import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandsSectionOneComponent } from './brands-section-one.component';

describe('BrandsSectionOneComponent', () => {
  let component: BrandsSectionOneComponent;
  let fixture: ComponentFixture<BrandsSectionOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandsSectionOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandsSectionOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
