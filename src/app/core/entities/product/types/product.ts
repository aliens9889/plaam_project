import { Currency } from '@qaroni-app/core/types';
import { ProductAttributes } from './product-attributes';
import { ProductTypeEnum } from './product-type.enum';
import { ProductUnitTypeEnum } from './product-unit-type';
import { Variant } from './variant';

export interface Product {
  attributes: ProductAttributes[];
  brandId: number;
  categoryId: number;
  collectionId: number;
  creationDate: string;
  currency: Currency;
  description: string;
  descriptionTags: ProductDescriptionTag[];
  imagesURL: string[];
  iva: number;
  keywords: string;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  novelty: number;
  options: ProductOption[];
  outlet: number;
  productId: number;
  type: ProductTypeEnum;
  unitName: string;
  unitType: ProductUnitTypeEnum;
  variant: Variant;
}
export interface ProductOption {
  id: number;
  label: string;
  values: ProductOptionValue[];
}

export interface ProductOptionValue {
  backgroundColor: string;
  imageURL: string;
  value: string;
}

export interface ProductDescriptionTag {
  title: string;
  content: string;
}
