// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '217',
};

export const appEnvironment = {
  title: 'Club de Empresarios y Ejecutivos Galicia',
  basePath: 'https://ceeg.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  boxes: AppLandingPageBoxesEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  news: true,
  brands: false,
  products: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/ceeg/logos/logo-ceeg-transparent.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/ceeg/logos/logo-ceeg-transparent.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/ceeg/logos/logo-ceeg-transparent.png',
};

export const urlsEnvironment = {
  website: 'https://ceeg.staging.plaam.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@clubempresariosgalicia.gal',
  phone: '+34 698 141 051',
  facebook: 'https://www.facebook.com/ClubdeEmpresariosyEjecutivosdeGalicia',
  twitter: 'https://twitter.com/ceeg_galicia',
  instagram: 'https://www.instagram.com/ceeg_galicia',
  linkedin:
    'https://www.linkedin.com/company/ceeg-club-de-empresarios-y-ejecutivos-de-galicia',
  privacy: 'https://ceeg.staging.plaam.com/#/',
  cookies: 'https://ceeg.staging.plaam.com/#/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
