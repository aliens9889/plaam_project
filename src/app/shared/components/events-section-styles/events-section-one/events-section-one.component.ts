import { Component, Input, OnInit } from '@angular/core';
import { Event } from '@qaroni-app/core/entities';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable } from 'rxjs';

@Component({
  selector: 'qaroni-events-section-one',
  templateUrl: './events-section-one.component.html',
  styleUrls: ['./events-section-one.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class EventsSectionOneComponent implements OnInit {
  @Input() events$: Observable<Event[]>;

  constructor() {}

  ngOnInit(): void {}
}
