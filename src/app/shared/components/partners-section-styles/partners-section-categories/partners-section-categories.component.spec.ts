import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersSectionCategoriesComponent } from './partners-section-categories.component';

describe('PartnersSectionCategoriesComponent', () => {
  let component: PartnersSectionCategoriesComponent;
  let fixture: ComponentFixture<PartnersSectionCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnersSectionCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersSectionCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
