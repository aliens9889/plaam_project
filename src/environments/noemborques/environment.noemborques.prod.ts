import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '66',
};

export const appEnvironment = {
  title: 'Greloland',
  basePath: 'https://my.noemborques.com/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 16,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: 'G-PPZNXJWYQV',
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: 'https://www.noemborques.com/assets/images/logo.png',
  toolbar: 'https://www.noemborques.com/assets/images/logo.png',
  footer: 'https://www.noemborques.com/assets/images/logo.png',
};

export const urlsEnvironment = {
  website: 'https://my.noemborques.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'info@greloland.es',
  facebook: 'https://www.facebook.com/pages/No-emborques/214852265236865',
  twitter: 'https://twitter.com/Noemborques',
  linkedin:
    'https://www.linkedin.com/pub/carlos-mari%C3%B1as-d%C3%ADaz/4a/983/93b',
  youtube: 'https://www.youtube.com/channel/UC3Of_Yb2ImMmzhKsTnGFt5Q',
  instagram: 'https://www.instagram.com/noemborques/',
  tsAndCs: 'https://my.noemborques.com/#/pages/883/términos-y-condiciones/',
  privacy: 'https://my.noemborques.com/#/pages/884/política-de-privacidad/',
  cookies: 'https://my.noemborques.com/#/pages/888/política-de-cookies/',
  purchase: 'https://my.noemborques.com/#/pages/887/condiciones-de-compra/',
};
