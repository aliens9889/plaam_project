export enum PostTagStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export type PostTagStatusType =
  | PostTagStatusEnum.ACTIVE
  | PostTagStatusEnum.INACTIVE;

export const BlogTagStatusArray = [
  PostTagStatusEnum.ACTIVE,
  PostTagStatusEnum.INACTIVE,
];

export const PostTagStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
