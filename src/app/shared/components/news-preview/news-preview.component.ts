import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { News, NewsService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-news-preview',
  templateUrl: './news-preview.component.html',
  styleUrls: ['./news-preview.component.scss'],
})
export class NewsPreviewComponent implements OnInit {
  @Output() queryParamsTag: EventEmitter<Params> = new EventEmitter();
  @Input() newsItem: News;

  constructor(
    private newsService: NewsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {}

  get getUrlTitle(): string {
    if (
      this.newsItem?.descriptions?.length &&
      this.newsItem?.descriptions[0]?.title
    ) {
      const urlTitle = this.newsItem.descriptions[0].title
        .replace(/  +/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase();
      return urlTitle;
    }
    return '';
  }

  public checkTagInfo(tagName: string): void {
    this.onSearchTag(tagName);
  }

  private onSearchTag(tagName: string): void {
    const queryParamsEmit: Params = { ...this.route.snapshot.queryParams };
    queryParamsEmit.tag = tagName.trim();
    this.queryParamsTag.emit(queryParamsEmit);
  }

  public checkRoute(): boolean {
    if (this.router.url !== '/') {
      return true;
    }
    return false;
  }
}
