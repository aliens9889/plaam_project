export interface ProductAttributes {
  label: string;
  value: string;
}
