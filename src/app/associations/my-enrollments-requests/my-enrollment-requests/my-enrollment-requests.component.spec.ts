import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyEnrollmentRequestsComponent } from './my-enrollment-requests.component';

describe('MyEnrollmentRequestsComponent', () => {
  let component: MyEnrollmentRequestsComponent;
  let fixture: ComponentFixture<MyEnrollmentRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyEnrollmentRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyEnrollmentRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
