import { TestBed } from '@angular/core/testing';

import { OAuthHttpService } from './o-auth-http.service';

describe('OAuthHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OAuthHttpService = TestBed.inject(OAuthHttpService);
    expect(service).toBeTruthy();
  });
});
