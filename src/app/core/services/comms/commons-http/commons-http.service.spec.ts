import { TestBed } from '@angular/core/testing';

import { CommonsHttpService } from './commons-http.service';

describe('CommonsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommonsHttpService = TestBed.inject(CommonsHttpService);
    expect(service).toBeTruthy();
  });
});
