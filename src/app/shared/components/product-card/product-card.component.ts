import { Component, Input, OnInit } from '@angular/core';
import { AppEnv, ProductsEnv } from '@qaroni-app/core/config';
import { Product, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';

@Component({
  selector: 'qaroni-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;

  constructor(
    private allApp: AllAppService,
    public productService: ProductService
  ) {}

  ngOnInit(): void {}

  get getProductCardImageAspectRatio(): string {
    return ProductsEnv.cardImageAspectRatio;
  }

  get getProductCardShowPriceFrom(): boolean {
    return ProductsEnv.cardShowPriceFrom;
  }

  get getShowPrices(): boolean {
    return AppEnv.showPrices;
  }

  get hasVariantOffer(): boolean {
    return this.productService.hasVariantOffer(this.product);
  }

  get isProductOutlet(): boolean {
    return this.productService.isProductOutlet(this.product);
  }

  get isProductNovelty(): boolean {
    return this.productService.isProductNovelty(this.product);
  }

  get getRouteToProductDetails(): (number | string)[] {
    return [
      '/products',
      this.product.productId,
      this.productService.getUrlName(this.product),
    ];
  }

  get getOneImageUrl(): string {
    return this.productService.getOneImageUrl(this.product);
  }

  get hasLabelOffer(): boolean {
    if (this.hasVariantOffer) {
      return true;
    } else {
      return false;
    }
  }

  get getLabelOfferString(): string {
    return this.productService.getLabelOfferString(this.product);
  }

  get hasLabel(): boolean {
    if (this.isProductOutlet || this.isProductNovelty) {
      return true;
    }
    return false;
  }

  get getLabelString(): string {
    if (this.isProductOutlet) {
      return this.allApp.translate.instant('Is outlet');
    } else if (this.isProductNovelty) {
      return this.allApp.translate.instant('Is novelty');
    }
  }

  get getLabelClass(): string {
    if (this.isProductOutlet) {
      return 'label-outlet';
    } else if (this.isProductNovelty) {
      return 'label-novelty';
    }
  }
}
