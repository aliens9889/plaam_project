import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsCategoryComponent } from './groups-category.component';

describe('GroupsCategoryComponent', () => {
  let component: GroupsCategoryComponent;
  let fixture: ComponentFixture<GroupsCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupsCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
