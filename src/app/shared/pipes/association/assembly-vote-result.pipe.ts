import { Pipe, PipeTransform } from '@angular/core';
import {
  AssemblyVoteResultEnum,
  AssemblyVoteResultInfo
} from '@qaroni-app/core/entities';

@Pipe({
  name: 'assemblyVoteResult',
})
export class AssemblyVoteResultPipe implements PipeTransform {
  transform(value: AssemblyVoteResultEnum): string {
    if (value) {
      const key = value.trim().toUpperCase();
      for (const iterator of AssemblyVoteResultInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
