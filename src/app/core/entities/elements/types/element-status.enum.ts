export enum ElementStatusEnum {
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
  INACTIVE = 'INACTIVE',
}

export type ElementStatusType = 'ACTIVE' | 'CREATED' | 'INACTIVE';

export const ElementStatusArray = ['ACTIVE', 'CREATED', 'INACTIVE'];

export const ElementStatusInfo = [
  {
    active: {
      name: 'Activa',
    },
  },
  {
    created: {
      name: 'Creada',
    },
  },
  {
    inactive: {
      name: 'Inactiva',
    },
  },
];
