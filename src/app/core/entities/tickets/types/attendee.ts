export interface Attendee {
  document: string;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
}
