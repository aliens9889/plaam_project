import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsShowComponent } from './news-show/news-show.component';
import { NewsComponent } from './news/news.component';

const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
  },
  {
    path: ':newsID',
    component: NewsShowComponent,
  },
  {
    path: ':newsID/:newsTitle',
    component: NewsShowComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsRoutingModule {}
