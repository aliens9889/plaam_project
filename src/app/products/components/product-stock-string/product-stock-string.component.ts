import { Component, Input, OnInit } from '@angular/core';
import { ProductsEnv } from '@qaroni-app/core/config';
import { Product } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-product-stock-string',
  templateUrl: './product-stock-string.component.html',
  styleUrls: ['./product-stock-string.component.scss'],
})
export class ProductStockStringComponent implements OnInit {
  @Input() product: Product;

  constructor() {}

  ngOnInit(): void {}

  get stockControl(): boolean {
    return this.product?.variant?.stockControl;
  }

  get stockString(): string {
    if (this.stockControl) {
      if (this.product?.variant?.stock === 0) {
        return 'Unavailable';
      } else if (
        this.product?.variant?.stock > 0 &&
        this.product?.variant?.stock <= ProductsEnv.stockFewUnitsQuantity
      ) {
        return 'Few units';
      } else if (
        this.product?.variant?.stock > ProductsEnv.stockFewUnitsQuantity
      ) {
        return 'Available';
      } else {
        return 'No stock information';
      }
    }
  }

  get stockClass(): string {
    if (this.stockControl) {
      if (this.product?.variant?.stock === 0) {
        return 'font-weight-bold text-danger';
      } else if (
        this.product?.variant?.stock > 0 &&
        this.product?.variant?.stock <= ProductsEnv.stockFewUnitsQuantity
      ) {
        return 'font-weight-normal text-danger';
      } else if (
        this.product?.variant?.stock > ProductsEnv.stockFewUnitsQuantity
      ) {
        return 'font-weight-light';
      } else {
        return 'font-weight-bold text-warning';
      }
    }
  }
}
