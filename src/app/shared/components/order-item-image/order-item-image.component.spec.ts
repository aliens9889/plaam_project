import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderItemImageComponent } from './order-item-image.component';

describe('OrderItemImageComponent', () => {
  let component: OrderItemImageComponent;
  let fixture: ComponentFixture<OrderItemImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderItemImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
