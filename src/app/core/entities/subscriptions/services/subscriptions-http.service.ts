import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { SendByEmailJson } from '@qaroni-app/core/types';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SubscriptionsHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getSubscriptions$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/users/${userID}/subscriptions`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public sendSubscriptionByEmail$(
    userID: number | string,
    subscriptionID: number | string,
    emailJSON: SendByEmailJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/subscriptions/${subscriptionID}/emails`;

    return this.http.post(url, emailJSON, { observe: 'response' });
  }
}
