import { StripeEnvironment } from 'src/environments/environment';

export const StripeEnv = {
  scriptSrc: 'https://js.stripe.com/v3/',
  publicKey: StripeEnvironment.hasOwnProperty('publicKey')
    ? StripeEnvironment.publicKey
    : null,
};
