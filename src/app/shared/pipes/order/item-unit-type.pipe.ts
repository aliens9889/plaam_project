import { Pipe, PipeTransform } from '@angular/core';
import { ItemUnitTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'itemUnitType'
})
export class ItemUnitTypePipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      value = value.trim().toLowerCase();
      for (const iterator of ItemUnitTypeInfo) {
        if (Object.keys(iterator)[0] === value) {
          return iterator[value].name;
        }
      }
    }
    return null;
  }
}
