import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing/landing.component';

@NgModule({
  declarations: [LandingComponent],
  imports: [SharedModule, LandingRoutingModule, TranslateModule],
})
export class LandingModule {}
