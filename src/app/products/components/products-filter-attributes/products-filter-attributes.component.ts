import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductAttribute, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-products-filter-attributes',
  templateUrl: './products-filter-attributes.component.html',
  styleUrls: ['./products-filter-attributes.component.scss'],
})
export class ProductsFilterAttributesComponent implements OnInit, OnDestroy {
  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  public productAttributes$: Observable<
    ProductAttribute[]
  > = this.productService.getProductAttributes$().pipe(shareReplay(1));

  public showFewOptions = true;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private productService: ProductService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.subs.add(this.route.queryParams.subscribe(this.getQueryParams));

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private getQueryParams = (queryParams: Params) => {
    if (queryParams.hasOwnProperty('categories')) {
      const categoryArr: string[] = queryParams.categories.split(',');
      const categoryParams: Params = { categories: categoryArr[0] };
      this.productService.getProductAttributes(categoryParams);
    } else {
      this.productService.getProductAttributes();
    }
  }

  public onGetQueryParams(queryParamsEmit: Params): void {
    this.queryParamsChange.emit(queryParamsEmit);
  }

  private languageChanged = (): void => {
    this.getQueryParams(this.route.snapshot.queryParams);
  }
}
