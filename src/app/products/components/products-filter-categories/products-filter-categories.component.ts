import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductCategory, ProductService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-products-filter-categories',
  templateUrl: './products-filter-categories.component.html',
  styleUrls: ['./products-filter-categories.component.scss'],
})
export class ProductsFilterCategoriesComponent implements OnInit, OnDestroy {
  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  public productCategories$: Observable<
    ProductCategory[]
  > = this.productService.getProductCategories$().pipe(shareReplay(1));

  public productCategory$: Observable<
    ProductCategory
  > = this.productService.getProductCategory$().pipe(shareReplay(1));
  public categoriesGenealogy: ProductCategory[] = [];
  public showCategoriesGenealogy = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private productService: ProductService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(this.getQueryParams);

    this.subs.add(
      this.productCategory$
        .pipe(shareReplay(1))
        .subscribe(this.getProductCategory)
    );

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private getQueryParams = (queryParams: Params) => {
    this.resetCategoriesGenealogy();
    if (queryParams.hasOwnProperty('categories')) {
      const valuesArr: string[] = queryParams.categories.split(',');
      if (valuesArr[0] !== '0') {
        this.productService.getProductCategory(valuesArr[0]);
        const categoriesParams: Params = { parentId: valuesArr[0] };
        this.productService.getProductCategories(categoriesParams);
      } else {
        this.onSelectCategory(valuesArr[0]);
      }
    } else {
      const categoriesParams: Params = { parentId: 0 };
      this.productService.getProductCategories(categoriesParams);
    }
  }

  public onSelectCategory(categoryId: number | string): void {
    this.resetCategoriesGenealogy();
    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );
    if (categoryId) {
      if (categoryId !== '0') {
        queryParamsEmit.categories = categoryId;
      } else {
        delete queryParamsEmit.categories;
      }
    } else {
      delete queryParamsEmit.categories;
    }
    this.queryParamsChange.emit(queryParamsEmit);
  }

  private languageChanged = (): void => {
    const queryParams: Params = this.route.snapshot.queryParams;
    this.getQueryParams(queryParams);
  }

  private resetCategoriesGenealogy(): void {
    this.categoriesGenealogy = [];
    this.showCategoriesGenealogy = false;
  }

  private getProductCategory = (category: ProductCategory): void => {
    this.categoriesGenealogy.unshift(category);
    if (category?.parentId) {
      this.productService.getProductCategory(category?.parentId);
    } else if (category?.parentId === 0) {
      this.showCategoriesGenealogy = true;
    } else {
      this.resetCategoriesGenealogy();
    }
  }
}
