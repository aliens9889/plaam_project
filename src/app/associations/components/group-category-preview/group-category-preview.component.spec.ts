import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCategoryPreviewComponent } from './group-category-preview.component';

describe('GroupCategoryPreviewComponent', () => {
  let component: GroupCategoryPreviewComponent;
  let fixture: ComponentFixture<GroupCategoryPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCategoryPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCategoryPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
