import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerMemberInscriptionsListComponent } from './partner-member-inscriptions-list.component';

describe('PartnerMemberInscriptionsListComponent', () => {
  let component: PartnerMemberInscriptionsListComponent;
  let fixture: ComponentFixture<PartnerMemberInscriptionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerMemberInscriptionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerMemberInscriptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
