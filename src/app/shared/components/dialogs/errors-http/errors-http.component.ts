import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'qaroni-errors-http',
  templateUrl: './errors-http.component.html',
  styleUrls: ['./errors-http.component.scss'],
})
export class ErrorsHttpComponent implements OnInit {
  public showErrorDetails = false;

  constructor(
    public dialogRef: MatDialogRef<ErrorsHttpComponent>,
    @Inject(MAT_DIALOG_DATA) public error: HttpErrorResponse
  ) {}

  ngOnInit(): void {}

  public onShowErrorDetails(): void {
    this.showErrorDetails = true;
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  get isControlledError(): boolean {
    if (
      this.error.status === 400 &&
      this.error.error &&
      this.error.error.errors &&
      this.error.error.errors.length &&
      this.error.error.errors[0] &&
      this.error.error.errors[0].code &&
      this.error.error.errors[0].title
    ) {
      return true;
    } else {
      return false;
    }
  }
}
