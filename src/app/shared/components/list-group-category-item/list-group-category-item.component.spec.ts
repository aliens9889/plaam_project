import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGroupCategoryItemComponent } from './list-group-category-item.component';

describe('ListGroupCategoryItemComponent', () => {
  let component: ListGroupCategoryItemComponent;
  let fixture: ComponentFixture<ListGroupCategoryItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGroupCategoryItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGroupCategoryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
