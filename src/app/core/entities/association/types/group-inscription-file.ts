export interface GroupInscriptionFile {
  creationDate: string;
  file: string;
  fileId: number;
  fileUrl: string;
  formId: number;
  inscriptionId: number;
  label: string;
  lastUpdateDate: string;
  name: string;
  required: number;
}
