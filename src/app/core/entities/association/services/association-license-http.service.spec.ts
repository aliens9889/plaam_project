import { TestBed } from '@angular/core/testing';

import { AssociationLicenseHttpService } from './association-license-http.service';

describe('AssociationLicenseHttpService', () => {
  let service: AssociationLicenseHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssociationLicenseHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
