export interface OAuth {
  access_token: string;
  issued: string;
  expires: string;
  userId: number;
}
