import { Component, OnInit } from '@angular/core';
import { AppProductsPageStyleEnum, ProductsEnv } from '@qaroni-app/core/config';
import { AllAppService } from '@qaroni-app/core/services';

@Component({
  selector: 'qaroni-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  public ProductsEnv = ProductsEnv;
  public AppProductsPageStyleEnum = AppProductsPageStyleEnum;

  constructor(private allApp: AllAppService) {
    this.enableLoading();
  }

  ngOnInit(): void {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }
}
