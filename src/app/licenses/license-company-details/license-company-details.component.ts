import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {
  AssociationLicenseService,
  AssociationPartnerService,
  AssociationService,
  GroupInscriptionCompany,
  GroupInscriptionTypeEnum,
  LicenseCompany,
  LicenseCompanyStatusEnum,
  RenewalInscriptionCompanyJson
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData } from '@qaroni-app/core/types';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { ConfirmationDialogComponent } from '@qaroni-app/shared/components/dialogs';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-company-details',
  templateUrl: './license-company-details.component.html',
  styleUrls: ['./license-company-details.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseCompanyDetailsComponent implements OnInit, OnDestroy {
  private hasLicenseCompanyID: boolean = this.route.snapshot.paramMap.has(
    'licenseCompanyID'
  );
  private licenseCompanyID: string = this.route.snapshot.paramMap.get(
    'licenseCompanyID'
  );

  public initLoaded = false;

  private licenseCompany$: Observable<
    LicenseCompany
  > = this.licenseService.getLicenseCompany$().pipe(shareReplay(1));
  public licenseCompany: LicenseCompany;

  private subs: Subscription = new Subscription();

  public GroupInscriptionTypeEnum = GroupInscriptionTypeEnum;

  public inscription$: Observable<
    GroupInscriptionCompany
  > = this.associationService.getInscriptionCompany$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private licenseService: AssociationLicenseService,
    private associationService: AssociationService,
    private partnerService: AssociationPartnerService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.hasLicenseCompanyID) {
      this.subs.add(this.licenseCompany$.subscribe(this.getLicenseCompany));
      this.licenseService.getLicenseCompany(this.licenseCompanyID);
      this.subs.add(this.inscription$.subscribe(this.getInscription));
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private getLicenseCompany = (licenseCompany: LicenseCompany): void => {
    if (licenseCompany) {
      this.licenseCompany = licenseCompany;
    }
    this.disableLoading();
  }

  public onEditInformation(): void {
    this.allApp.router.navigate([
      '/licenses',
      'companies',
      this.licenseCompanyID,
      'edit',
      'information',
    ]);
  }

  public onEditAddress(): void {
    this.allApp.router.navigate([
      '/licenses',
      'companies',
      this.licenseCompanyID,
      'edit',
      'address',
    ]);
  }

  public onEditContactInformation(): void {
    this.allApp.router.navigate([
      '/licenses',
      'companies',
      this.licenseCompanyID,
      'edit',
      'information',
      'contacts',
    ]);
  }

  public onEditContactAddress(): void {
    this.allApp.router.navigate([
      '/licenses',
      'companies',
      this.licenseCompanyID,
      'edit',
      'address',
      'contacts',
    ]);
  }

  public onDownloadWallet(): void {
    if (this.licenseCompany.walletUrl) {
      window.location.href = this.licenseCompany.walletUrl;
    }
  }

  get titleLicenseDetails(): string {
    return this.licenseService.strings.getTitleLicenseCompanyDetails();
  }

  get groupHasRenewal(): boolean {
    if (this.licenseCompany?.groupInfo?.hasRenewal === true) {
      return true;
    }
    return false;
  }

  get groupHasAlertDays(): boolean {
    if (
      this.licenseCompany?.groupInfo?.alertDays &&
      this.licenseCompany?.groupInfo?.alertDays > 0
    ) {
      return true;
    }
    return false;
  }

  get showRenew(): boolean {
    if (
      this.groupHasRenewal &&
      this.groupHasAlertDays &&
      this.licenseCompany?.status === LicenseCompanyStatusEnum.ACTIVE &&
      this.licenseCompany?.expirationDate &&
      moment(this.licenseCompany?.expirationDate).isValid() &&
      moment().isAfter(
        moment(this.licenseCompany?.expirationDate)
          .subtract(this.licenseCompany?.groupInfo?.alertDays, 'days')
          .startOf('day')
      ) &&
      moment().isBefore(
        moment(this.licenseCompany?.expirationDate).endOf('day')
      )
    ) {
      return true;
    }
    return false;
  }

  get showReactivateDates(): boolean {
    if (
      this.groupHasRenewal &&
      this.licenseCompany?.expirationDate &&
      moment(this.licenseCompany?.expirationDate).isValid() &&
      moment().isAfter(moment(this.licenseCompany?.expirationDate))
    ) {
      return true;
    }
    return false;
  }

  get showReactivateStatus(): boolean {
    if (
      this.groupHasRenewal &&
      (this.licenseCompany?.status === LicenseCompanyStatusEnum.CANCELLED ||
        this.licenseCompany?.status === LicenseCompanyStatusEnum.EXPIRED)
    ) {
      return true;
    }
    return false;
  }

  public renewReactivateLicenseCompany(type: GroupInscriptionTypeEnum): void {
    if (
      this.licenseCompany &&
      type &&
      (type === GroupInscriptionTypeEnum.RENEWAL ||
        type === GroupInscriptionTypeEnum.REACTIVATION) &&
      ((type === GroupInscriptionTypeEnum.RENEWAL && this.showRenew) ||
        (type === GroupInscriptionTypeEnum.REACTIVATION &&
          (this.showReactivateDates || this.showReactivateStatus)))
    ) {
      const confirmationData: ConfirmationDialogData = {
        title: this.allApp.translate.instant(`Renew license`),
        message: this.allApp.translate.instant(
          `Create license renewal request`
        ),
        confirmFaIcon: 'fas fa-check',
        confirmText: this.allApp.translate.instant(`Yes, create request`),
      };

      if (type === GroupInscriptionTypeEnum.REACTIVATION) {
        confirmationData.title = this.allApp.translate.instant(
          `Reactivate license`
        );
        confirmationData.message = this.allApp.translate.instant(
          `Create license reactivate request`
        );
      }

      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = '700px';
      matDialogConfig.maxWidth = '90vw';
      matDialogConfig.panelClass = 'style-confirm-dialog';
      matDialogConfig.autoFocus = false;
      matDialogConfig.data = confirmationData;

      const dialog = this.allApp.dialog.open<
        ConfirmationDialogComponent,
        ConfirmationDialogData,
        boolean
      >(ConfirmationDialogComponent, matDialogConfig);

      dialog.afterClosed().subscribe((confirmation) => {
        if (confirmation === true) {
          this.enableLoading();
          const createInscription: RenewalInscriptionCompanyJson = {
            companyJson: this.buildCompany,
            companyAddressJson: this.licenseCompany?.addresses?.length
              ? this.licenseCompany?.addresses[0]
              : null,
            userJson: this.buildContact,
            addressJson: this.licenseCompany?.contact?.addresses?.length
              ? this.licenseCompany?.contact?.addresses[0]
              : null,
            modelId: this.licenseCompany?.companyId,
            partnerId: this.licenseCompany?.partnerId
              ? this.licenseCompany.partnerId
              : null,
            type,
          };

          this.associationService.createGroupInscriptionCompany(
            this.licenseCompany?.groupId,
            createInscription
          );
        }
      });
    }
  }

  get buildCompany(): object {
    if (this.licenseCompany) {
      const company = {
        name: this.licenseCompany?.name,
        tradename: this.licenseCompany?.tradename,
        cif: this.licenseCompany?.cif,
        email: this.licenseCompany?.email,
        phone: this.licenseCompany?.phone,
        secondaryPhone: this.licenseCompany?.secondaryPhone,
      };
      return company;
    }
    return null;
  }

  get buildContact(): object {
    if (this.licenseCompany?.contact) {
      const address = {
        firstName: this.licenseCompany?.contact?.firstName,
        lastName: this.licenseCompany?.contact?.lastName,
        documentType: this.licenseCompany?.contact?.documentType,
        document: this.licenseCompany?.contact?.document,
        birthday: this.licenseCompany?.contact?.birthday,
        gender: this.licenseCompany?.contact?.gender,
        email: this.licenseCompany?.contact?.email,
        phone: this.licenseCompany?.contact?.phone,
      };
      return address;
    }
    return null;
  }

  private getInscription = (inscription: GroupInscriptionCompany): void => {
    if (inscription) {
      this.allApp.router.navigate(['/associations', 'my-enrollment-requests']);
    }
    this.disableLoading();
  }

  get isPartner(): boolean {
    if (this.licenseCompany?.isPartner === true) {
      return true;
    }
    return false;
  }

  public onRequestRegistration(): void {
    if (this.isPartner && this.licenseCompanyID) {
      this.allApp.router.navigate([
        '/associations',
        'groups',
        'partners',
        this.licenseCompanyID,
      ]);
    }
  }

  get textListPartnerMembers(): string {
    return this.licenseService.strings.getTextAffiliatedMembers();
  }

  public onListPartnerMembers(): void {
    if (this.isPartner && this.licenseCompanyID) {
      this.allApp.router.navigate([
        '/licenses',
        'companies',
        this.licenseCompanyID,
        'partners',
        'members',
      ]);
    }
  }

  get textListPartnerMembersInscriptions(): string {
    return this.licenseService.strings.getTextPartnerMemberInscriptions();
  }

  public onListPartnerMembersInscriptions(): void {
    if (this.isPartner && this.licenseCompanyID) {
      this.allApp.router.navigate([
        '/licenses',
        'companies',
        this.licenseCompanyID,
        'partners',
        'members',
        'inscriptions',
      ]);
    }
  }

  get textListPartnerCompanies(): string {
    return this.licenseService.strings.getTextAffiliatedCompanies();
  }

  public onListPartnerCompanies(): void {
    if (this.isPartner && this.licenseCompanyID) {
      this.allApp.router.navigate([
        '/licenses',
        'companies',
        this.licenseCompanyID,
        'partners',
        'companies',
      ]);
    }
  }

  get textListPartnerCompanyInscriptions(): string {
    return this.licenseService.strings.getTextPartnerCompanyInscriptions();
  }

  public onListPartnerCompanyInscriptions(): void {
    if (this.isPartner && this.licenseCompanyID) {
      this.allApp.router.navigate([
        '/licenses',
        'companies',
        this.licenseCompanyID,
        'partners',
        'companies',
        'inscriptions',
      ]);
    }
  }

  get allowPartnerMember(): boolean {
    return this.partnerService.allowPartnerMember;
  }

  get allowPartnerCompany(): boolean {
    return this.partnerService.allowPartnerCompany;
  }
}
