import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DynamicFormComponent } from '@qaroni-app/associations/components/dynamic-form/dynamic-form.component';
import { UserAddressFormComponent } from '@qaroni-app/associations/components/user-address-form/user-address-form.component';
import { UserDataFormComponent } from '@qaroni-app/associations/components/user-data-form/user-data-form.component';
import { UserPartnerFormComponent } from '@qaroni-app/associations/components/user-partner-form/user-partner-form.component';
import { AssociationEnv } from '@qaroni-app/core/config';
import {
  AssociationService,
  FormField,
  FormService,
  Group,
  GroupInscriptionMember,
  GroupInscriptionMemberStatusEnum,
  GroupInscriptionTypeEnum,
  GroupTypeEnum,
  InscriptionMemberJson
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-edit-request-member',
  templateUrl: './edit-request-member.component.html',
  styleUrls: ['./edit-request-member.component.scss'],
})
export class EditRequestMemberComponent implements OnInit, OnDestroy {
  private formFields$: Observable<
    FormField[]
  > = this.formService.getFormFields$().pipe(shareReplay(1));
  public formFields: FormField[];

  private group$: Observable<Group> = this.associationService
    .getGroup$()
    .pipe(shareReplay(1));
  public group: Group;

  public inscription$: Observable<
    GroupInscriptionMember
  > = this.associationService.getInscriptionMember$().pipe(shareReplay(1));
  public inscription: GroupInscriptionMember;

  private subs: Subscription = new Subscription();

  public initLoaded = false;

  public submitting = false;
  private formSent = false;

  private groupID: string = this.route.snapshot.paramMap.get('groupID');
  private inscriptionID: string = this.route.snapshot.paramMap.get(
    'inscriptionID'
  );

  @ViewChild(UserDataFormComponent)
  userDataComponent: UserDataFormComponent;

  @ViewChild(UserAddressFormComponent)
  userAddressComponent: UserAddressFormComponent;

  @ViewChild(UserPartnerFormComponent)
  userPartnerComponent: UserPartnerFormComponent;

  @ViewChild(DynamicFormComponent)
  dynamicFormComponent: DynamicFormComponent;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private associationService: AssociationService,
    private formService: FormService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (
      this.route.snapshot.paramMap.has('groupID') &&
      this.route.snapshot.paramMap.has('inscriptionID') &&
      AssociationEnv.allowsMembers === true
    ) {
      this.subs.add(this.group$.subscribe(this.getGroup));
      this.associationService.getGroup(this.groupID);

      this.subs.add(this.inscription$.subscribe(this.getInscription));
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private redirect(): void {
    this.allApp.router.navigate(['/associations', 'my-enrollment-requests']);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
    this.formSent = false;
  }

  private getGroup = (group: Group): void => {
    if (group?.type === GroupTypeEnum.PERSON) {
      this.group = group;
      if (this.group?.formId) {
        this.subs.add(this.formFields$.subscribe(this.getFormFields));
        this.formService.getFormFields(this.group?.formId);
      } else {
        this.associationService.getInscriptionMember(this.inscriptionID);
      }
    } else {
      this.redirect();
    }
  }

  private getFormFields = (formFields: FormField[]): void => {
    if (formFields?.length) {
      this.formFields = formFields;
      this.associationService.getInscriptionMember(this.inscriptionID);
    }
  }

  get hasPartnerForm(): boolean {
    if (this.group?.hasPartner === true) {
      return true;
    }
    return false;
  }

  get hasDynamicForm(): boolean {
    if (this.formFields?.length) {
      return true;
    }
    return false;
  }

  private getInscription = (inscription: GroupInscriptionMember): void => {
    if (this.formSent || !this.isIncomplete(inscription)) {
      this.redirect();
    } else {
      this.disableLoading();
      if (inscription?.group?.type !== GroupTypeEnum.PERSON) {
        this.redirect();
      }
      this.userDataComponent.populateUserDataForm(inscription?.user);
      this.userAddressComponent.populateUserAddressForm(inscription?.address);
      if (this.hasPartnerForm) {
        this.userPartnerComponent.populateUserPartnerForm(
          inscription?.partnerId
        );
      }
      if (this.hasDynamicForm) {
        this.dynamicFormComponent.populateWithGetData(inscription?.formJson);
      }
    }
  }

  public updateInscription(): void {
    if (this.groupID) {
      this.enableLoading();
      this.userDataComponent.prepateFormToSend();
      if (this.hasDynamicForm) {
        this.dynamicFormComponent.prepateFormToSend();
      }

      const updateInscription: InscriptionMemberJson = {
        addressJson: this.userAddressComponent.userAddressForm.getRawValue(),
        clubId: null,
        formId: this.group?.formId,
        formJson: this.hasDynamicForm
          ? this.dynamicFormComponent.dynamicForm.getRawValue()
          : null,
        partnerId: this.hasPartnerForm
          ? this.userPartnerComponent.partnerId.value
          : null,
        type: GroupInscriptionTypeEnum.INSCRIPTION,
        userJson: this.userDataComponent.userDataForm.getRawValue(),
      };
      this.associationService.updateInscriptionMember(
        this.inscriptionID,
        updateInscription
      );

      this.userDataComponent.populateUserDataForm(
        this.userDataComponent.userDataForm.getRawValue()
      );
      if (this.hasDynamicForm) {
        this.dynamicFormComponent.populateDynamicForm(
          this.dynamicFormComponent.dynamicForm.getRawValue()
        );
      }
      this.formSent = true;
    }
  }

  get validatedFormUserData(): boolean {
    if (this.userDataComponent?.userDataForm) {
      return this.userDataComponent.userDataForm.valid;
    }
    return false;
  }

  get validatedFormUserAddress(): boolean {
    if (this.userAddressComponent?.userAddressForm) {
      return this.userAddressComponent.userAddressForm.valid;
    }
    return false;
  }

  get validatedDynamicForm(): boolean {
    if (this.dynamicFormComponent?.dynamicForm) {
      return this.dynamicFormComponent.dynamicForm.valid;
    }
    return false;
  }

  get validatedForm(): boolean {
    if (this.hasDynamicForm) {
      return (
        !this.submitting &&
        this.validatedFormUserData &&
        this.validatedFormUserAddress &&
        this.validatedDynamicForm
      );
    } else {
      return (
        !this.submitting &&
        this.validatedFormUserData &&
        this.validatedFormUserAddress
      );
    }
  }

  get titlePage(): string {
    return this.associationService.strings.getTitlePageEditMemberEnrollment();
  }

  get titlePersonalInfo(): string {
    return this.associationService.strings.getStepTitlePersonalInfo();
  }

  get titleAddress(): string {
    return this.associationService.strings.getStepTitleMemberAddress();
  }

  public isIncomplete(inscription: GroupInscriptionMember): boolean {
    if (inscription) {
      return inscription.status === GroupInscriptionMemberStatusEnum.INCOMPLETE;
    }
    return false;
  }
}
