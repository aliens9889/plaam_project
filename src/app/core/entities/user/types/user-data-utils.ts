import { AppEnv } from '@qaroni-app/core/config';
import { GenderArray, LanguageEnum } from '@qaroni-app/core/types';
import { DocumentTypeArray } from './document-type.enum';

export class UserDataUtils {
  public static getDocumentTypeArray(): string[] {
    return DocumentTypeArray;
  }

  public static getGenderArray(): string[] {
    return GenderArray;
  }

  public static getLanguagesAllowed(): string[] {
    if (
      Array.isArray(AppEnv.languagesAllowed) &&
      AppEnv.languagesAllowed.length
    ) {
      return AppEnv.languagesAllowed;
    } else {
      return [LanguageEnum.default];
    }
  }
}
