import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseMemberDetailsComponent } from './license-member-details.component';

describe('LicenseMemberDetailsComponent', () => {
  let component: LicenseMemberDetailsComponent;
  let fixture: ComponentFixture<LicenseMemberDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseMemberDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseMemberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
