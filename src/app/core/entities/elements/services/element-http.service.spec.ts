import { TestBed } from '@angular/core/testing';

import { ElementHttpService } from './element-http.service';

describe('ElementHttpService', () => {
  let service: ElementHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ElementHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
