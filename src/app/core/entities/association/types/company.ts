import { Address } from '../../address';

export interface Company {
  cif: string;
  creationDate: string;
  description: string;
  email: string;
  isPartner: boolean;
  lastUpdateDate: string;
  name: string;
  phone: string;
  secondaryPhone: string;
  tradename: string;
  userId: number;
}

export interface CompanyProfile {
  address: Address;
  description: string;
  imageUrl: string;
  name: string;
  profileId: number;
  tradename: string;
}
