import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { BankChargeJson, CofidisData, RedsysData } from '../../payment';
import { PaymentsHttpService } from '../../payment/services/payments-http.service';
import { OrderSnackbars } from '../snackbars/order-snackbar.config';
import { Order } from '../types/order';

@Injectable({
  providedIn: 'root',
})
export class OrderPaymentService {
  private redirectUrl$ = new Subject<string>();
  private redsysData$ = new Subject<RedsysData>();
  private cofidisData$ = new Subject<CofidisData>();
  private cofidisCalculator$ = new Subject<string>();
  private stripeCheckoutId$ = new Subject<string>();
  private aplazameCheckoutId$ = new Subject<string>();
  private bankChargeOrderSubject = new Subject<Order>();

  constructor(
    private allApp: AllAppService,
    private paymentsHttp: PaymentsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public orderAmountAllowCofidis(order: Order): boolean {
    if (order && order.amount >= 15000 && order.amount <= 600000) {
      return true;
    }
    return false;
  }

  private errorIsControlled(error: HttpErrorResponse): boolean {
    return this.commonsHttp.errorsHttp.errorIsControlled(
      error,
      'E0011',
      'No hay capacidad para las entradas del pedido'
    );
  }

  private noAvailabilityActions(): void {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        OrderSnackbars.failureCheckoutAvailability.message
      ),
      this.allApp.translate.instant(
        OrderSnackbars.failureCheckoutAvailability.closeBtn
      ),
      OrderSnackbars.failureCheckoutAvailability.config
    );
    this.allApp.router.navigate(['/shopping-cart']);
  }

  // ==========================================================================================
  // Get Redirect Url
  // ==========================================================================================
  public getRedirectUrl$(): Observable<string> {
    return this.redirectUrl$.asObservable();
  }

  // ==========================================================================================
  // Payment with PayPal
  // ==========================================================================================
  public paymentPayPal(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .paymentPayPal$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successPaymentPayPal, this.failurePaymentPayPal);
    }
  }

  private successPaymentPayPal = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      if (data.body.result[0].url) {
        const redirectUrl: string = data.body.result[0].url;
        this.redirectUrl$.next(redirectUrl);
      } else {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(
            OrderSnackbars.redirectErrorPayPal.message
          ),
          this.allApp.translate.instant(
            OrderSnackbars.redirectErrorPayPal.closeBtn
          ),
          OrderSnackbars.redirectErrorPayPal.config
        );
        this.redirectUrl$.next(null);
      }
    } else {
      this.redirectUrl$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failurePaymentPayPal = (error: HttpErrorResponse): void => {
    this.redirectUrl$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.redirectErrorPayPal.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.redirectErrorPayPal.closeBtn
        ),
        OrderSnackbars.redirectErrorPayPal.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Paycomet
  // ==========================================================================================
  public paymentPaycomet(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .paymentPaycomet$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successPaymentPaycomet, this.failurePaymentPaycomet);
    }
  }

  private successPaymentPaycomet = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      if (data.body.result[0].url) {
        const redirectUrl: string = data.body.result[0].url;
        this.redirectUrl$.next(redirectUrl);
      } else {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(
            OrderSnackbars.redirectErrorPaycomet.message
          ),
          this.allApp.translate.instant(
            OrderSnackbars.redirectErrorPaycomet.closeBtn
          ),
          OrderSnackbars.redirectErrorPaycomet.config
        );
        this.redirectUrl$.next(null);
      }
    } else {
      this.redirectUrl$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failurePaymentPaycomet = (error: HttpErrorResponse): void => {
    this.redirectUrl$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.redirectErrorPaycomet.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.redirectErrorPaycomet.closeBtn
        ),
        OrderSnackbars.redirectErrorPaycomet.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Cecabank
  // ==========================================================================================
  public paymentCecabank(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .paymentCecabank$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successPaymentCecabank, this.failurePaymentCecabank);
    }
  }

  private successPaymentCecabank = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      if (data.body.result[0].url) {
        const redirectUrl: string = data.body.result[0].url;
        this.redirectUrl$.next(redirectUrl);
      } else {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(
            OrderSnackbars.redirectErrorCecabank.message
          ),
          this.allApp.translate.instant(
            OrderSnackbars.redirectErrorCecabank.closeBtn
          ),
          OrderSnackbars.redirectErrorCecabank.config
        );
        this.redirectUrl$.next(null);
      }
    } else {
      this.redirectUrl$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failurePaymentCecabank = (error: HttpErrorResponse): void => {
    this.redirectUrl$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          OrderSnackbars.redirectErrorCecabank.message
        ),
        this.allApp.translate.instant(
          OrderSnackbars.redirectErrorCecabank.closeBtn
        ),
        OrderSnackbars.redirectErrorCecabank.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Redsys
  // ==========================================================================================
  public getRedsysData$(): Observable<RedsysData> {
    return this.redsysData$.asObservable();
  }

  public getPaymentRedsysData(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.paymentsHttp
        .getPaymentRedsysData$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPaymentRedsysData,
          this.failureGetPaymentRedsysData
        );
    }
  }

  private successGetPaymentRedsysData = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const redsysData: RedsysData = data.body.result;
      this.redsysData$.next(redsysData);
    } else {
      this.redsysData$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPaymentRedsysData = (error: HttpErrorResponse): void => {
    this.redsysData$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Cofidis
  // ==========================================================================================
  public getCofidisData$(): Observable<CofidisData> {
    return this.cofidisData$.asObservable();
  }

  public getPaymentCofidisData(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.paymentsHttp
        .getPaymentCofidisData$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPaymentCofidisData,
          this.failureGetPaymentCofidisData
        );
    }
  }

  private successGetPaymentCofidisData = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const cofidisData: CofidisData = data.body.result;
      this.cofidisData$.next(cofidisData);
    } else {
      this.cofidisData$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPaymentCofidisData = (error: HttpErrorResponse): void => {
    this.cofidisData$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Cofidis Calculator
  // ==========================================================================================
  public getCofidisCalculator$(): Observable<string> {
    return this.cofidisCalculator$.asObservable();
  }

  public getCalculatorCofidisUrl(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.paymentsHttp
        .getCalculatorCofidisUrl$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetCalculatorCofidisUrl,
          this.failureGetCalculatorCofidisUrl
        );
    }
  }

  private successGetCalculatorCofidisUrl = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const calculatorUrl: string = data.body.result[0].url;
      this.cofidisCalculator$.next(calculatorUrl);
    } else {
      this.cofidisCalculator$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetCalculatorCofidisUrl = (error: HttpErrorResponse): void => {
    this.cofidisCalculator$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Stripe
  // ==========================================================================================
  public getStripeCheckoutId$(): Observable<string> {
    return this.stripeCheckoutId$.asObservable();
  }

  public getPaymentStripeCheckoutId(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .getPaymentStripeCheckoutId$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPaymentStripeCheckoutId,
          this.failureGetPaymentStripeCheckoutId
        );
    }
  }

  private successGetPaymentStripeCheckoutId = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const stripeCheckoutId: string = data.body.result[0].checkoutId;
      this.stripeCheckoutId$.next(stripeCheckoutId);
    } else {
      this.stripeCheckoutId$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPaymentStripeCheckoutId = (
    error: HttpErrorResponse
  ): void => {
    this.stripeCheckoutId$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Aplázame
  // ==========================================================================================
  public getAplazameCheckoutId$(): Observable<string> {
    return this.aplazameCheckoutId$.asObservable();
  }

  public getPaymentAplazameCheckoutId(orderID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .getPaymentAplazameCheckoutId$(this.oAuthService.getUserID, orderID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetPaymentAplazameCheckoutId,
          this.failureGetPaymentAplazameCheckoutId
        );
    }
  }

  private successGetPaymentAplazameCheckoutId = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const aplazameCheckoutId: string = data.body.result[0].checkoutId;
      this.aplazameCheckoutId$.next(aplazameCheckoutId);
    } else {
      this.aplazameCheckoutId$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPaymentAplazameCheckoutId = (
    error: HttpErrorResponse
  ): void => {
    this.aplazameCheckoutId$.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Payment with Bank Charge
  // ==========================================================================================
  public getBankChargeOrder$(): Observable<Order> {
    return this.bankChargeOrderSubject.asObservable();
  }

  public paymentBankCharge(
    orderID: number | string,
    bankChargeJSON: BankChargeJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.paymentsHttp
        .paymentBankCharge$(
          this.oAuthService.getUserID,
          orderID,
          bankChargeJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successPaymentBankCharge,
          this.failurePaymentBankCharge
        );
    }
  }

  private successPaymentBankCharge = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const order: Order = data.body.result[0];
      this.bankChargeOrderSubject.next(order);
    } else {
      this.bankChargeOrderSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failurePaymentBankCharge = (error: HttpErrorResponse): void => {
    this.bankChargeOrderSubject.next(null);
    if (this.errorIsControlled(error)) {
      this.noAvailabilityActions();
    }
    this.commonsHttp.errorsHttp.communication(error);
  }
}
