import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyEditAddressComponent } from './license-company-edit-address.component';

describe('LicenseCompanyEditAddressComponent', () => {
  let component: LicenseCompanyEditAddressComponent;
  let fixture: ComponentFixture<LicenseCompanyEditAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyEditAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyEditAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
