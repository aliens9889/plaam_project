import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { SendByEmailJson } from '@qaroni-app/core/types';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { TicketsSnackbars } from '../snackbars/tickets-snackbar.config';
import { FreeTicketJson, Ticket } from '../types/ticket';
import { TicketsHttpService } from './tickets-http.service';

@Injectable({
  providedIn: 'root',
})
export class TicketsService {
  private tickets$ = new Subject<Ticket[]>();
  private freeTicketSubject = new Subject<string>();

  constructor(
    private allApp: AllAppService,
    private ticketsHttp: TicketsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getTicketImageUrl(ticket: Ticket): string | null {
    if (ticket.imagesURL && ticket.imagesURL.length && ticket.imagesURL[0]) {
      return ticket.imagesURL[0];
    } else {
      return null;
    }
  }

  // ==========================================================================================
  // Get Tickets
  // ==========================================================================================

  public getTickets$(): Observable<Ticket[]> {
    return this.tickets$.asObservable();
  }

  public getTickets(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ticketsHttp
        .getTickets$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetTickets, this.failureGetTickets);
    }
  }

  private successGetTickets = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const tickets: Ticket[] = data.body.result;
      this.tickets$.next(tickets);
    } else {
      this.tickets$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetTickets = (error: HttpErrorResponse): void => {
    this.tickets$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Send Ticket by email
  // ==========================================================================================

  public sendTicketCardByEmail(
    eventID: number | string,
    ticketUUID: string,
    emailJSON: SendByEmailJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.ticketsHttp
        .sendTicketCardByEmail$(
          this.oAuthService.getUserID,
          eventID,
          ticketUUID,
          emailJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successSendTicketByEmail,
          this.failureSendTicketByEmail
        );
    }
  }

  private successSendTicketByEmail = (data: HttpResponse<any>): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        TicketsSnackbars.successSendByEmail.message
      ),
      this.allApp.translate.instant(
        TicketsSnackbars.successSendByEmail.closeBtn
      ),
      TicketsSnackbars.successSendByEmail.config
    );
  }

  private failureSendTicketByEmail = (error: HttpErrorResponse): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        TicketsSnackbars.successSendByEmail.message
      ),
      this.allApp.translate.instant(
        TicketsSnackbars.successSendByEmail.closeBtn
      ),
      TicketsSnackbars.successSendByEmail.config
    );
  }

  // ==========================================================================================
  // Download ticket pdf
  // ==========================================================================================

  public getDownloadTicketPDF(
    eventID: number | string,
    ticketUUID: string
  ): string {
    if (this.oAuthService.hasOAuth) {
      return this.ticketsHttp.buildDownloadTicketCardPDF(
        this.oAuthService.getUserID,
        eventID,
        ticketUUID
      );
    } else {
      return '';
    }
  }

  // ==========================================================================================
  // Free Ticket
  // ==========================================================================================

  public getFreeTicket$(): Observable<string> {
    return this.freeTicketSubject.asObservable();
  }

  public freeTicket(
    eventID: number | string,
    freeTicketJSON: FreeTicketJson
  ): void {
    this.enableLoading();
    this.ticketsHttp
      .freeTicket$(eventID, freeTicketJSON)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successFreeTicket, this.failureFreeTicket);
  }

  private successFreeTicket = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const free: string = data.body.result[0].information;
      this.freeTicketSubject.next(free);
    } else {
      this.freeTicketSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureFreeTicket = (error: HttpErrorResponse): void => {
    this.freeTicketSubject.next(null);
    if (
      this.commonsHttp.errorsHttp.errorIsControlled(
        error,
        'E0011',
        'No hay capacidad para la cantidad solicitada'
      )
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          TicketsSnackbars.failureFreeByCapacity.message
        ),
        this.allApp.translate.instant(
          TicketsSnackbars.failureFreeByCapacity.closeBtn
        ),
        TicketsSnackbars.failureFreeByCapacity.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }
}
