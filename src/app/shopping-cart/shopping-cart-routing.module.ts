import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllowShoppingCartGuard, UserGuard } from '@qaroni-app/core/guards';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

const routes: Routes = [
  {
    path: '',
    component: ShoppingCartComponent,
    canActivate: [UserGuard, AllowShoppingCartGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShoppingCartRoutingModule {}
