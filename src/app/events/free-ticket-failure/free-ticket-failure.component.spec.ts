import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeTicketFailureComponent } from './free-ticket-failure.component';

describe('FreeTicketFailureComponent', () => {
  let component: FreeTicketFailureComponent;
  let fixture: ComponentFixture<FreeTicketFailureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeTicketFailureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeTicketFailureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
