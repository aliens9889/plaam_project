import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssociationService, Group, GroupCategory, GroupStatusEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-groups-category',
  templateUrl: './groups-category.component.html',
  styleUrls: ['./groups-category.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class GroupsCategoryComponent implements OnInit, OnDestroy {
  private category$: Observable<
    GroupCategory
  > = this.associationService.getCategory$().pipe(shareReplay(1));
  public category: GroupCategory;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private associationService: AssociationService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('categoryID')) {
      this.subs.add(this.category$.subscribe(this.getCategory));
      this.associationService.getCategory(
        this.route.snapshot.paramMap.get('categoryID')
      );
    } else {
      this.allApp.router.navigate(['/associations', 'groups-categories']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  get title(): string {
    return this.associationService.strings.getTitleComponentString();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getCategory = (category: GroupCategory): void => {
    if (category) {
      this.category = category;
    }
  }

  public hasImage(category: GroupCategory): boolean {
    if (category && category.imageUrl) {
      return true;
    }
    return false;
  }

  public isGroupActive(group: Group): boolean {
    if (group?.status === GroupStatusEnum.ACTIVE) {
      return true;
    }
    return false;
  }
}
