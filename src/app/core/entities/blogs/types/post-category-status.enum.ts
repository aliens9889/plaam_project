export enum PostCategoryStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export type PostCategoryStatusType =
  | PostCategoryStatusEnum.ACTIVE
  | PostCategoryStatusEnum.INACTIVE;

export const PostCategoryStatusArray = [
  PostCategoryStatusEnum.ACTIVE,
  PostCategoryStatusEnum.INACTIVE,
];

export const PostCategoryStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
