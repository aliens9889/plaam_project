import { LanguageEnum } from '@qaroni-app/core/types';
import { NewsAuthorStatusEnum } from './news-author-status.enum';
import { NewsCategoryStatusEnum } from './news-category-status.enum';
import { NewsStatusEnum } from './news-status.enum';
import { NewsTagStatusEnum } from './news-tag-status.enum';

export interface News {
  authors: NewsAuthor[];
  categories: NewsCategory[];
  creationDate: string;
  date: string;
  descriptions: NewsDescription[];
  featured: boolean;
  imageUrl: string;
  lastUpdateDate: string;
  merchantId: number;
  newId: number;
  status: NewsStatusEnum;
  tags: NewsTag[];
}

export interface NewsAuthor {
  authorId: number;
  creationDate: string;
  description: string;
  firstName: string;
  imageUrl: string;
  lastName: string;
  lastUpdateDate: string;
  merchantId: number;
  position: string;
  status: NewsAuthorStatusEnum;
}

export interface NewsDescription {
  language: LanguageEnum;
  largeDescription: string;
  shortDescription: string;
  subtitle: string;
  title: string;
}

export interface NewsTag {
  creationDate: string;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  status: NewsTagStatusEnum;
  tagId: number;
}

export interface NewsCategory {
  categoryId: number;
  creationDate: string;
  descriptions: NewsCategoryDescription[];
  lastUpdateDate: string;
  merchantId: number;
  status: NewsCategoryStatusEnum;
}

export interface NewsCategoryDescription {
  creationDate: string;
  description: string;
  language: LanguageEnum;
  lastUpdateDate: string;
  name: string;
}
