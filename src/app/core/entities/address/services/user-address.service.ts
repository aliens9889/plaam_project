import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { UserSnackbars } from '../../user';
import { Address } from '../types/address';
import { UserAddressesHttpService } from './user-addresses-http.service';

@Injectable({
  providedIn: 'root',
})
export class UserAddressService {
  private userAddresses$ = new Subject<Address[]>();
  private userAddress$ = new Subject<Address>();
  private deletedUserAddressID$ = new Subject<number>();

  constructor(
    private allApp: AllAppService,
    private userAddressesHttp: UserAddressesHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public isAddressDefault(address: Address): boolean {
    if (address && address.default && address.default === true) {
      return true;
    }
    return false;
  }

  public sortAddressesDefault = (a: Address, b: Address) => {
    if (a.default && a.default === true) {
      return -1;
    } else if (b.default && b.default === true) {
      return 1;
    } else {
      return 0;
    }
  }

  // ==========================================================================================
  // Get User Addresses
  // ==========================================================================================

  public getUserAddresses$(): Observable<Address[]> {
    return this.userAddresses$.asObservable();
  }

  public getUserAddresses(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userAddressesHttp
        .getUserAddresses$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetUserAddresses, this.failureGetUserAddresses);
    }
  }

  private successGetUserAddresses = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      this.userAddresses$.next(data.body.result);
    } else {
      this.userAddresses$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetUserAddresses = (error: HttpErrorResponse): void => {
    this.userAddresses$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create User Address
  // ==========================================================================================

  public getUserAddress$(): Observable<Address> {
    return this.userAddress$.asObservable();
  }

  public createUserAddress(addressJSON: Address): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userAddressesHttp
        .createUserAddress$(this.oAuthService.getUserID, addressJSON)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successCreateUserAddress,
          this.failureCreateUserAddress
        );
    }
  }

  private successCreateUserAddress = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus201(data)) {
      const address: Address = data.body.result[0];
      this.userAddress$.next(address);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successCreateUserAddress.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successCreateUserAddress.closeBtn
        ),
        UserSnackbars.successCreateUserAddress.config
      );
    } else {
      this.userAddress$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureCreateUserAddress = (error: HttpErrorResponse): void => {
    this.userAddress$.next(null);
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        UserSnackbars.failureUpdateUserAddress.message
      ),
      this.allApp.translate.instant(
        UserSnackbars.failureUpdateUserAddress.closeBtn
      ),
      UserSnackbars.failureUpdateUserAddress.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update User Address
  // ==========================================================================================

  public updateUserAddress(
    addressID: number | string,
    addressJSON: Address
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userAddressesHttp
        .updateUserAddress$(this.oAuthService.getUserID, addressID, addressJSON)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateuserAddress,
          this.failureUpdateuserAddress
        );
    }
  }

  private successUpdateuserAddress = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const address: Address = data.body.result[0];
      this.userAddress$.next(address);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successUpdateUserAddress.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successUpdateUserAddress.closeBtn
        ),
        UserSnackbars.successUpdateUserAddress.config
      );
    } else {
      this.userAddress$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateuserAddress = (error: HttpErrorResponse): void => {
    this.userAddress$.next(null);
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        UserSnackbars.failureUpdateUserAddress.message
      ),
      this.allApp.translate.instant(
        UserSnackbars.failureUpdateUserAddress.closeBtn
      ),
      UserSnackbars.failureUpdateUserAddress.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Delete User Addresses
  // ==========================================================================================

  public getDeletedUserAddressID$(): Observable<number> {
    return this.deletedUserAddressID$.asObservable();
  }

  public deleteUserAddress(address: Address): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userAddressesHttp
        .deleteUserAddress$(this.oAuthService.getUserID, address.addressId)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successDeleteUserAddresses,
          this.failureDeleteUserAddresses
        );
    }
  }

  private successDeleteUserAddresses = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus204(data)) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successDeleteUserAddress.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successDeleteUserAddress.closeBtn
        ),
        UserSnackbars.successDeleteUserAddress.config
      );
      this.deletedUserAddressID$.next(parseInt(data.url.split('/').pop(), 10));
    } else {
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureDeleteUserAddresses = (error: HttpErrorResponse): void => {
    this.commonsHttp.errorsHttp.communication(error);
  }
}
