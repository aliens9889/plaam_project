export enum GroupTypeEnum {
  ENTITY = 'ENTITY',
  PERSON = 'PERSON',
}

export type GroupType = 'ENTITY' | 'PERSON';

export const GroupTypeArray = ['ENTITY', 'PERSON'];

export const GroupTypeInfo = [
  {
    entity: {
      name: 'Entidad',
    },
  },
  {
    person: {
      name: 'Persona',
    },
  },
];
