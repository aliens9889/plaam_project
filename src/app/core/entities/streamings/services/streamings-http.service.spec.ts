import { TestBed } from '@angular/core/testing';

import { StreamingsHttpService } from './streamings-http.service';

describe('StreamingsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StreamingsHttpService = TestBed.inject(StreamingsHttpService);
    expect(service).toBeTruthy();
  });
});
