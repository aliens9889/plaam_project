import { Component, Input, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { FieldTypeEnum, FormField } from '@qaroni-app/core/entities';
import { InputValidation } from '@qaroni-app/core/utils';
import * as moment from 'moment';

@Component({
  selector: 'qaroni-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
})
export class DynamicFormComponent implements OnInit {
  @Input() formFields: FormField[];

  public FieldTypeEnum = FieldTypeEnum;
  public InputValidation = InputValidation;

  public initLoaded = false;

  public dynamicForm: FormGroup;

  private submitting = false;

  constructor(private fb: FormBuilder) {
    this.createDynamicForm();
  }

  ngOnInit(): void {
    this.buildDynamicForm();
  }

  private createDynamicForm(): void {
    this.dynamicForm = this.fb.group({});
  }

  private buildDynamicForm(): void {
    if (this.formFields && this.formFields.length) {
      this.initLoaded = true;
      for (const formField of this.formFields) {
        const control = new FormControl('');
        if (formField.required && formField.field.type !== FieldTypeEnum.FILE) {
          control.setValidators(Validators.required);
        }
        if (formField.field.type === FieldTypeEnum.EMAIL) {
          control.setValidators(Validators.email);
        }
        this.dynamicForm.addControl(formField.field.name, control);
      }
    }
  }

  get validatedForm(): boolean {
    return this.dynamicForm.valid && !this.submitting;
  }

  public prepateFormToSend(): void {
    for (const formField of this.formFields) {
      if (
        formField &&
        formField.field &&
        formField.field.type &&
        formField.field.type === FieldTypeEnum.DATE
      ) {
        if (
          this.dynamicForm.get(formField.field.name).value &&
          this.dynamicForm.get(formField.field.name).value.isValid()
        ) {
          this.dynamicForm
            .get(formField.field.name)
            .setValue(
              this.dynamicForm
                .get(formField.field.name)
                .value.format('YYYY-MM-DD')
            );
        } else {
          this.dynamicForm.get(formField.field.name).patchValue('');
        }
      }
    }
  }

  public populateDynamicForm(formJson: any): void {
    for (const formField of this.formFields) {
      if (
        formField &&
        formField.field &&
        formField.field.type &&
        formField.field.type === FieldTypeEnum.DATE
      ) {
        this.dynamicForm
          .get(formField.field.name)
          .setValue(moment(formJson[formField.field.name]));
      }
    }
  }

  public populateWithGetData(formJson: any): void {
    for (const fieldInfo in formJson) {
      if (formJson.hasOwnProperty(fieldInfo)) {
        for (const formField of this.formFields) {
          if (formJson[fieldInfo]?.name === formField?.field?.name) {
            if (formField?.field?.type === FieldTypeEnum.DATE) {
              this.dynamicForm
                .get(formField?.field?.name)
                .setValue(moment(formJson[formField?.field?.name]));
            } else {
              this.dynamicForm
                .get(formField?.field?.name)
                .setValue(formJson[fieldInfo]?.value);
            }
            break;
          }
        }
      }
    }
  }

  public getWrapperClass(formField: FormField): string {
    if (formField && formField.field && formField.field.type) {
      if (
        formField.field.type === FieldTypeEnum.CHECKBOX ||
        formField.field.type === FieldTypeEnum.RADIO ||
        formField.field.type === FieldTypeEnum.TEXTAREA
      ) {
        return 'col-10 offset-1 col-sm-12 offset-sm-0';
      } else {
        return 'col-10 offset-1 col-sm-6 offset-sm-0';
      }
    }
    return null;
  }
}
