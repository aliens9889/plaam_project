import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppEnv } from '@qaroni-app/core/config';
import { LanguageEnum } from '@qaroni-app/core/types';
import { Observable, Subject } from 'rxjs';
import { AppStorageService } from '../app-storage/app-storage.service';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  private language$ = new Subject<LanguageEnum>();

  constructor(
    private translate: TranslateService,
    private appStorage: AppStorageService
  ) {
    if (this.checkLanguagesAllowed) {
      translate.addLangs(AppEnv.languagesAllowed);
      translate.setDefaultLang(AppEnv.languagesAllowed[0]);
    } else {
      translate.addLangs([LanguageEnum.default]);
      translate.setDefaultLang(LanguageEnum.default);
    }
    this.initializeLanguage();
  }

  private initializeLanguage(): void {
    if (this.appStorage.hasLanguage) {
      if (this.checkLanguagesAllowed) {
        if (AppEnv.languagesAllowed.includes(this.appStorage.getLanguage())) {
          this.setLanguage(this.appStorage.getLanguage());
        } else {
          this.setNavigatorLanguage();
        }
      } else {
        this.setLanguage(LanguageEnum.default);
      }
    } else {
      this.setNavigatorLanguage();
    }
  }

  private setNavigatorLanguage(): void {
    if (this.getNavigatorLanguage) {
      this.setLanguage(this.getNavigatorLanguage);
    } else {
      if (this.checkLanguagesAllowed) {
        this.setLanguage(AppEnv.languagesAllowed[0]);
      } else {
        this.setLanguage(LanguageEnum.default);
      }
    }
  }

  get getNavigatorLanguage(): LanguageEnum {
    if (this.checkLanguagesAllowed) {
      for (const languageAllowed of AppEnv.languagesAllowed) {
        if (navigator.languages.includes(languageAllowed) === true) {
          return languageAllowed;
        }
      }
    }
    return null;
  }

  public getLanguage$(): Observable<LanguageEnum> {
    return this.language$.asObservable();
  }

  public setLanguage(language: LanguageEnum): void {
    this.appStorage.setLanguage(language);
    this.translate.use(language.toLowerCase());
    this.language$.next(language);
  }

  get checkLanguagesAllowed(): boolean {
    if (
      Array.isArray(AppEnv.languagesAllowed) &&
      AppEnv.languagesAllowed.length &&
      AppEnv.languagesAllowed.length >= 1
    ) {
      return true;
    }
    return false;
  }

  // ==========================================================================================
  // Languages Buttons Validations
  // ==========================================================================================

  get showLanguageButton(): boolean {
    if (this.checkLanguagesAllowed && AppEnv.languagesAllowed.length >= 2) {
      return true;
    }
    return false;
  }

  get showLanguageOptionEn(): boolean {
    if (
      this.checkLanguagesAllowed &&
      this.showLanguageButton &&
      AppEnv.languagesAllowed.includes(LanguageEnum.en) === true &&
      this.appStorage.getLanguage() !== LanguageEnum.en
    ) {
      return true;
    }
    return false;
  }

  get showLanguageOptionEs(): boolean {
    if (
      this.checkLanguagesAllowed &&
      this.showLanguageButton &&
      AppEnv.languagesAllowed.includes(LanguageEnum.es) === true &&
      this.appStorage.getLanguage() !== LanguageEnum.es
    ) {
      return true;
    }
    return false;
  }

  get showLanguageOptionGl(): boolean {
    if (
      this.checkLanguagesAllowed &&
      this.showLanguageButton &&
      AppEnv.languagesAllowed.includes(LanguageEnum.gl) === true &&
      this.appStorage.getLanguage() !== LanguageEnum.gl
    ) {
      return true;
    }
    return false;
  }

  get showLanguageOptionCa(): boolean {
    if (
      this.checkLanguagesAllowed &&
      this.showLanguageButton &&
      AppEnv.languagesAllowed.includes(LanguageEnum.ca) === true &&
      this.appStorage.getLanguage() !== LanguageEnum.ca
    ) {
      return true;
    }
    return false;
  }
}
