import { OrderUpdateItemEnum } from './order-update-item.enum';

export interface OrderUpdateItem {
  action: OrderUpdateItemEnum;
  quantity: number;
  typeId?: number;
}
