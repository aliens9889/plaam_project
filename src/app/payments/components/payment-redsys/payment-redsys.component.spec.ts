import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentRedsysComponent } from './payment-redsys.component';

describe('PaymentRedsysComponent', () => {
  let component: PaymentRedsysComponent;
  let fixture: ComponentFixture<PaymentRedsysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentRedsysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentRedsysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
