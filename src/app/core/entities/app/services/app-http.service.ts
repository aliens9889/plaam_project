import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getSlides$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/slides`;

    const params: Params = Object.assign({}, queryParams);
    params.type = 'STORE';
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params });
  }

  public getNavbars$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/navbars`;

    const params: Params = Object.assign({}, queryParams);
    params.status = 'ACTIVE';
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params });
  }
}
