import { LanguageEnum } from '@qaroni-app/core/types';
import { GroupCategory } from './group-category';
import { GroupMonthTypeEnum } from './group-month-type.enum';
import { GroupStatusEnum } from './group-status.enum';
import { GroupTypeEnum } from './group-type.enum';

export interface Group {
  alertDays: number;
  category: GroupCategory;
  categoryId: number;
  creationDate: string;
  description: string;
  emailTemplateId: number;
  formId: number;
  groupId: number;
  hasApproval: boolean;
  hasPartner: boolean;
  hasRenewal: boolean;
  imageUrl: string;
  isPaid: boolean;
  isPartner: boolean;
  iva: number;
  language: LanguageEnum;
  lastUpdateDate: string;
  merchantId: number;
  months: number;
  monthType: GroupMonthTypeEnum;
  name: string;
  price: number;
  productId: number;
  serieId: number;
  status: GroupStatusEnum;
  templateUUID: string;
  type: GroupTypeEnum;
  variantId: number;
}
