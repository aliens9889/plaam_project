import { Component, OnInit } from '@angular/core';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-free-ticket-success',
  templateUrl: './free-ticket-success.component.html',
  styleUrls: ['./free-ticket-success.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class FreeTicketSuccessComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
