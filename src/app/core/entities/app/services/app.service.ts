import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { Navbar, Slide } from '@qaroni-app/core/entities';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AppHttpService } from './app-http.service';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private slides$ = new Subject<Slide[]>();
  private navbars$ = new Subject<Navbar[]>();

  constructor(
    private allApp: AllAppService,
    private appHttp: AppHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Slides
  // ==========================================================================================

  public getSlides$(): Observable<Slide[]> {
    return this.slides$.asObservable();
  }

  public getSlides(queryParams?: Params): void {
    this.enableLoading();
    this.appHttp
      .getSlides$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetSlides, this.failureGetSlides);
  }

  private successGetSlides = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const slides: Slide[] = data.body.result;
      this.slides$.next(slides);
    } else {
      this.slides$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetSlides = (error: HttpErrorResponse): void => {
    this.slides$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Navbars
  // ==========================================================================================

  public getNavbars$(): Observable<Navbar[]> {
    return this.navbars$.asObservable();
  }

  public getNavbars(): void {
    this.enableLoading();
    this.appHttp
      .getNavbars$()
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetNavbars, this.failureGetNavbars);
  }

  private successGetNavbars = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const navbars: Navbar[] = data.body.result;
      this.navbars$.next(navbars);
    } else {
      this.navbars$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetNavbars = (error: HttpErrorResponse): void => {
    this.navbars$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
