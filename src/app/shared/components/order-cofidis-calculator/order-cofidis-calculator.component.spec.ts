import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderCofidisCalculatorComponent } from './order-cofidis-calculator.component';

describe('OrderCofidisCalculatorComponent', () => {
  let component: OrderCofidisCalculatorComponent;
  let fixture: ComponentFixture<OrderCofidisCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderCofidisCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderCofidisCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
