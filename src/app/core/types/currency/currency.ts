export type Currency = 'EUR';

export const CurrencyArray = ['EUR'];

export const CurrencyInfo = [
  {
    eur: {
      name: 'Euros'
    }
  }
];
