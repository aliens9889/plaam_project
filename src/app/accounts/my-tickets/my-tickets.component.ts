import { Component, OnDestroy, OnInit } from '@angular/core';
import { Ticket, TicketsService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-tickets',
  templateUrl: './my-tickets.component.html',
  styleUrls: ['./my-tickets.component.scss'],
})
export class MyTicketsComponent implements OnInit, OnDestroy {
  public tickets: Ticket[] = [];

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private ticketsService: TicketsService
  ) {
    this.enableLoading();
    this.allApp.toolbar.showBackButton(['/my-account']);
  }

  ngOnInit(): void {
    this.subs.add(this.ticketsService.getTickets$().subscribe(this.getTickets));
    this.ticketsService.getTickets();

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getTickets = (tickets: Ticket[]): void => {
    this.tickets = tickets;
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.ticketsService.getTickets();
  }
}
