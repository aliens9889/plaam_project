import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageEventsEnum,
  AppLandingPageGroupsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '37',
};

export const appEnvironment = {
  title: 'Agaxede',
  basePath: 'https://my.agaxede.org/#',
  languagesAllowed: [LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  events: AppLandingPageEventsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  news: true,
  groups: AppLandingPageGroupsEnum.CATEGORIES,
  products: AppLandingPageProductsEnum.ONE,
  brands: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.ASSOCIATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/agaxede/logos/agaxede-logo.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/agaxede/logos/agaxede-logo.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/agaxede/logos/agaxede-logo.png',
};

export const urlsEnvironment = {
  website: 'https://my.agaxede.org/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@agaxede.org',
  phone: '+34 981 63 98 64',
  facebook: 'https://www.facebook.com/agaxede.xestoresdeportivos',
  twitter: 'https://twitter.com/AGAXEDE',
  linkedin: 'https://www.linkedin.com/company/agaxede',
  legal: 'https://my.agaxede.org/#/pages/873/aviso-legal/',
  privacy: 'https://my.agaxede.org/#/pages/615/política-de-privacidad/',
  cookies: 'https://my.agaxede.org/#/pages/616/política-de-cookies/',
};
