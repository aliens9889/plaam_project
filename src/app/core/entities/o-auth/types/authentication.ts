export interface SignUpJson {
  username: string;
  password: string;
  passwordConfirmation: string;
}

export interface LoginJson {
  username: string;
  password: string;
}

export interface OtpJson {
  otp: string;
}

export interface ForgotJson {
  username: string;
}

export interface ForgotChangePasswordJson {
  otp: string;
  password: string;
}
