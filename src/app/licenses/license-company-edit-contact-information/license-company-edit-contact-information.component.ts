import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  AssociationLicenseService,
  LicenseCompany,
  LicenseCompanyContact,
  UserDataUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-company-edit-contact-information',
  templateUrl: './license-company-edit-contact-information.component.html',
  styleUrls: ['./license-company-edit-contact-information.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseCompanyEditContactInformationComponent
  implements OnInit, OnDestroy {
  private licenseCompanyID: string;

  public initLoaded = false;

  private licenseCompany$: Observable<
    LicenseCompany
  > = this.licenseService.getLicenseCompany$().pipe(shareReplay(1));
  public licenseCompany: LicenseCompany;

  private subs: Subscription = new Subscription();

  public contactInfoForm: FormGroup;
  private contactInfoSkeleton = {
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    documentType: ['', [Validators.required]],
    document: ['', [Validators.required]],
    birthday: ['', [Validators.required]],
    gender: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
  };

  private submitting = false;
  private formSent = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private licenseService: AssociationLicenseService
  ) {
    this.licenseCompanyID = route.snapshot.paramMap.get('licenseCompanyID');
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.createContactInfoForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('licenseCompanyID')) {
      this.subs.add(this.licenseCompany$.subscribe(this.getLicenseCompany));
      this.licenseService.getLicenseCompany(this.licenseCompanyID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
    this.formSent = false;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private createContactInfoForm(): void {
    this.contactInfoForm = this.fb.group(this.contactInfoSkeleton);
  }

  public populateContactInfoForm(contact: LicenseCompanyContact): void {
    this.contactInfoForm.patchValue(contact);
    if (contact.birthday) {
      this.birthday.setValue(moment(contact.birthday));
    }
  }

  get InputValidation() {
    return InputValidation;
  }

  get UserDataUtils() {
    return UserDataUtils;
  }

  get firstName(): AbstractControl {
    return this.contactInfoForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.contactInfoForm.get('lastName');
  }

  get documentType(): AbstractControl {
    return this.contactInfoForm.get('documentType');
  }

  get document(): AbstractControl {
    return this.contactInfoForm.get('document');
  }

  get birthday(): AbstractControl {
    return this.contactInfoForm.get('birthday');
  }

  get gender(): AbstractControl {
    return this.contactInfoForm.get('gender');
  }

  get email(): AbstractControl {
    return this.contactInfoForm.get('email');
  }

  get phone(): AbstractControl {
    return this.contactInfoForm.get('phone');
  }

  get validatedForm(): boolean {
    return (
      this.contactInfoForm.dirty &&
      this.contactInfoForm.valid &&
      !this.submitting
    );
  }

  private getLicenseCompany = (licenseCompany: LicenseCompany): void => {
    if (licenseCompany) {
      if (this.formSent) {
        this.onBack();
      } else {
        this.licenseCompany = licenseCompany;
        this.populateContactInfoForm(this.licenseCompany?.contact);
      }
    }
    this.disableLoading();
  }

  private prepateFormToSend(): void {
    if (this.birthday.value && this.birthday.value.isValid()) {
      this.birthday.setValue(this.birthday.value.format('YYYY-MM-DD'));
    } else {
      this.birthday.patchValue('');
    }
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.prepateFormToSend();
      this.licenseService.updateLicenseCompanyContactInfo(
        this.licenseCompanyID,
        this.contactInfoForm.getRawValue()
      );
      this.formSent = true;
    }
  }

  get titlePage(): string {
    return this.licenseService.strings.getTitleLicenseCompanyEdit();
  }

  public onBack(): void {
    this.allApp.location.back();
  }
}
