import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssociationEnv } from '@qaroni-app/core/config';
import { AssociationService, GroupInscriptionCompany, GroupInscriptionFile, GroupTypeEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-complete-request-company',
  templateUrl: './complete-request-company.component.html',
  styleUrls: ['./complete-request-company.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class CompleteRequestCompanyComponent implements OnInit, OnDestroy {
  public inscription$: Observable<
    GroupInscriptionCompany
  > = this.associationService.getInscriptionCompany$().pipe(shareReplay(1));
  public inscription: GroupInscriptionCompany;

  private subs: Subscription = new Subscription();

  public submitting = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private associationService: AssociationService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (
      this.route.snapshot.paramMap.has('inscriptionID') &&
      AssociationEnv.allowsCompanies === true
    ) {
      this.subs.add(this.inscription$.subscribe(this.getInscription));
      this.associationService.getInscriptionCompany(
        this.route.snapshot.paramMap.get('inscriptionID')
      );
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private redirect(): void {
    this.allApp.router.navigate(['/associations', 'my-enrollment-requests']);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getInscription = (inscription: GroupInscriptionCompany): void => {
    this.submitting = false;
    if (inscription && inscription?.group?.type === GroupTypeEnum.ENTITY) {
    } else {
      this.redirect();
    }
  }

  public onFileChanged(theFile: File[], file: GroupInscriptionFile) {
    if (theFile && file) {
      let dataMultipart = new FormData();
      dataMultipart = this.allApp.BuildHttpData.buildMultipartWithImages(
        dataMultipart,
        file.name,
        theFile
      );

      this.submitting = true;
      if (this.route.snapshot.paramMap.has('inscriptionID')) {
        this.associationService.addFileToInscriptionCompany(
          this.route.snapshot.paramMap.get('inscriptionID'),
          dataMultipart
        );
      }
    }
  }
}
