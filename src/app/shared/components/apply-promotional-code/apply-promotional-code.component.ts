import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  OAuthService,
  Order,
  OrderCreatedService,
  PaymentService,
  PaymentSnackbars
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-apply-promotional-code',
  templateUrl: './apply-promotional-code.component.html',
  styleUrls: ['./apply-promotional-code.component.scss'],
})
export class ApplyPromotionalCodeComponent implements OnInit, OnDestroy {
  @Input() order: Order;

  public InputValidation = InputValidation;
  public InputConfig = InputConfig;

  public applyPromotionalCode = false;

  public promotionalCodeForm: FormGroup;
  private promotionalCodeSkeleton = {
    code: [
      '',
      Validators.compose([
        Validators.required,
        Validators.minLength(InputConfig.promotionalCode.minLength),
        Validators.maxLength(InputConfig.promotionalCode.maxLength),
        Validators.pattern(InputConfig.promotionalCode.pattern),
      ]),
    ],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private oAuthService: OAuthService,
    private fb: FormBuilder,
    private orderCreatedService: OrderCreatedService,
    private paymentService: PaymentService
  ) {}

  ngOnInit(): void {
    this.subs.add(
      this.paymentService
        .getPromotionalCode$()
        .subscribe(this.getApplyPromotionalCode)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  get code(): AbstractControl {
    return this.promotionalCodeForm.get('code');
  }

  get validatedForm(): boolean {
    return this.promotionalCodeForm.valid && !this.submitting;
  }

  public onApplyPromotionalCode(): void {
    if (this.oAuthService.hasOAuth) {
      this.createPromotionalCodeForm();
      this.applyPromotionalCode = true;
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(PaymentSnackbars.mustLogin.message),
        this.allApp.translate.instant(PaymentSnackbars.mustLogin.closeBtn),
        PaymentSnackbars.mustLogin.config
      );
    }
  }

  private createPromotionalCodeForm(): void {
    this.promotionalCodeForm = this.fb.group(this.promotionalCodeSkeleton);
  }

  public onSubmit(): void {
    if (this.oAuthService.hasOAuth) {
      if (this.order && this.order.orderId && this.validatedForm) {
        this.enableLoading();
        this.paymentService.applyPromotionalCode(
          this.order.orderId,
          this.promotionalCodeForm.getRawValue()
        );
      }
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(PaymentSnackbars.mustLogin.message),
        this.allApp.translate.instant(PaymentSnackbars.mustLogin.closeBtn),
        PaymentSnackbars.mustLogin.config
      );
    }
  }

  private getApplyPromotionalCode = (response: string): void => {
    if (response === 'success') {
      this.code.setValue('');
      this.applyPromotionalCode = false;
      this.orderCreatedService.getOrderCreated();
      this.submitting = false;
    } else {
      this.disableLoading();
    }
  }
}
