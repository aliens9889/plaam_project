import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PageHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getPage$(pageID: number | string): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/pages/${pageID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }
}
