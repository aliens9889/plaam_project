import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardLoadingContentComponent } from './card-loading-content.component';

describe('CardLoadingContentComponent', () => {
  let component: CardLoadingContentComponent;
  let fixture: ComponentFixture<CardLoadingContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardLoadingContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardLoadingContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
