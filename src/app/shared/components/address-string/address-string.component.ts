import { Component, Input, OnInit } from '@angular/core';
import { Address } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-address-string',
  templateUrl: './address-string.component.html',
  styleUrls: ['./address-string.component.scss'],
})
export class AddressStringComponent implements OnInit {
  @Input() address: Address;

  constructor() {}

  ngOnInit(): void {}
}
