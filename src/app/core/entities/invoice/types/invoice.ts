import { CountryEnum } from '@qaroni-app/core/types';
import { Item } from '../../order';

export interface Invoice {
  city: string;
  clientId: number;
  code: string;
  country: CountryEnum;
  creationDate: string;
  date: string;
  deliveryAmount: number;
  email: string;
  invoiceId: number;
  items: Item[];
  lastUpdateDate: string;
  line1: string;
  line2: string;
  merchantId: number;
  name: string;
  nif: string;
  number: number;
  orderId: number;
  paymentMethod: string;
  phone: string;
  postalCode: string;
  serieId: number;
  stateProvince: string;
  status: string;
  subtotal: number;
  tax: number;
  total: number;
  userId: number;
  year: number;
}
