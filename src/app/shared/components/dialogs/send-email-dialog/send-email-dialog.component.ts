import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { InputValidation } from '@qaroni-app/core/utils';

@Component({
  selector: 'qaroni-send-email-dialog',
  templateUrl: './send-email-dialog.component.html',
  styleUrls: ['./send-email-dialog.component.scss']
})
export class SendEmailDialogComponent implements OnInit {
  public InputValidation = InputValidation;

  public emailForm: FormGroup;
  private emailSkeleton = {
    email: ['', Validators.compose([Validators.required, Validators.email])]
  };

  constructor(
    public dialogRef: MatDialogRef<SendEmailDialogComponent>,
    private fb: FormBuilder
  ) {
    this.createEmailForm();
  }

  ngOnInit(): void {}

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  private createEmailForm(): void {
    this.emailForm = this.fb.group(this.emailSkeleton);
  }

  get email(): AbstractControl {
    return this.emailForm.get('email');
  }

  get validatedForm(): boolean {
    return this.emailForm.valid;
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.dialogRef.close(this.emailForm.getRawValue());
    }
  }
}
