import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleOneComponent } from './footer-style-one.component';

describe('FooterStyleOneComponent', () => {
  let component: FooterStyleOneComponent;
  let fixture: ComponentFixture<FooterStyleOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
