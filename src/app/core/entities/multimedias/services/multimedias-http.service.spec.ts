import { TestBed } from '@angular/core/testing';

import { MultimediasHttpService } from './multimedias-http.service';

describe('MultimediasHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MultimediasHttpService = TestBed.inject(MultimediasHttpService);
    expect(service).toBeTruthy();
  });
});
