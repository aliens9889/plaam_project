import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductsHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getProducts$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products`;

    const params: Params = Object.assign({}, queryParams);

    url = this.languageHttp.completeUrlWithLanguage(url);
    url = this.completeUrlWithClient(url);

    return this.http.get(url, { observe: 'response', params });
  }

  public getProduct$(
    productID: number | string
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/${productID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public getProductByOptions$(
    productID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/${productID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  // ==========================================================================================
  // Some satelite calls
  // ==========================================================================================
  public getProductBrands$(
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/brands`;

    url = this.completeUrlWithActive(url);
    url = this.completeUrlWithStatus(url);
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  public getProductBrand$(
    brandId: number | string
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/brands/${brandId}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public getProductCategories$(
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/categories`;

    url = this.completeUrlWithStatus(url);
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  public getProductCategory$(
    categoryId: number | string
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/categories/${categoryId}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public getProductCollections$(
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/collections`;

    url = this.completeUrlWithActive(url);
    url = this.completeUrlWithStatus(url);
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public getProductAttributes$(
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/products/attributes`;

    const params: Params = Object.assign({}, queryParams);
    params.status = 'ACTIVE';
    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params });
  }
  // ==========================================================================================
  // Private methods
  // ==========================================================================================
  public completeUrlWithLast(url: string, last: number): string {
    if (url && last) {
      if (!url.includes('?')) {
        url += `?last=${last}`;
      } else {
        url += `&last=${last}`;
      }
    }
    return url;
  }

  public completeUrlWithClient(url: string, client: string = 'WEB'): string {
    if (url) {
      if (!url.includes('?')) {
        url += `?client=${client}`;
      } else {
        url += `&client=${client}`;
      }
    }
    return url;
  }

  public completeUrlWithActive(url: string, active: string = 'WEB'): string {
    if (url) {
      if (!url.includes('?')) {
        url += `?active=${active}`;
      } else {
        url += `&active=${active}`;
      }
    }
    return url;
  }

  public completeUrlWithStatus(url: string, status: number = 1): string {
    if (url && status) {
      if (!url.includes('?')) {
        url += `?status=${status}`;
      } else {
        url += `&status=${status}`;
      }
    }
    return url;
  }

  public completeUrlWithParentId(
    url: string,
    parentId: number | string = null
  ): string {
    if (url) {
      if (parentId !== null) {
        if (!url.includes('?')) {
          url += `?parentId=${parentId}`;
        } else {
          url += `&parentId=${parentId}`;
        }
      }
    }
    return url;
  }

  public completeUrlWithQueryParams(
    url: string,
    key: string,
    value: string
  ): string {
    if (url && key && value) {
      if (!url.includes('?')) {
        url += `?${key}=${value}`;
      } else {
        url += `&${key}=${value}`;
      }
    }
    return url;
  }
}
