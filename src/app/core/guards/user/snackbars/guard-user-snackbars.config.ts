import { MatSnackBarConfig } from '@angular/material/snack-bar';
import { SnackbarConfig } from '@qaroni-app/core/config';

export const GuardUserSnackbars = {
  failUser: {
    message: 'To access, please login',
    closeBtn: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.danger,
      duration: SnackbarConfig.durations.danger
    } as MatSnackBarConfig
  },
  failNotUser: {
    message: 'You are already login',
    closeBtn: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.danger,
      duration: SnackbarConfig.durations.danger
    } as MatSnackBarConfig
  }
};
