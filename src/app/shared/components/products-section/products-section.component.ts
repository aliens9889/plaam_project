import { Component, OnInit } from '@angular/core';
import { LandingEnv, AppLandingPageProductsEnum } from '@qaroni-app/core/config';

@Component({
  selector: 'qaroni-products-section',
  templateUrl: './products-section.component.html',
  styleUrls: ['./products-section.component.scss'],
})
export class ProductsSectionComponent implements OnInit {
  public LandingEnv = LandingEnv;
  public AppLandingPageProductsEnum = AppLandingPageProductsEnum;

  constructor() {}

  ngOnInit(): void {}
}
