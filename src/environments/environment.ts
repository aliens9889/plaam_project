// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: null,
  merchantId: null,
};

export const appEnvironment = {
  title: null,
  basePath: null,
  languagesAllowed: null,
  styleTitle: null,
  footerStyle: null,
  showPrices: null,
  allowShoppingCart: null,
};

export const landingEnvironment = {
  showSlideButton: null,
  slides: null,
  brands: null,
  news: null,
  groups: null,
  products: null,
  events: null,
  blocks: null,
  boxes: null,
};

export const productsEnvironment = {
  featuredCount: null,
  productsPageStyle: null,
  cardImageAspectRatio: null,
  cardShowPriceFrom: null,
  showProductsPreviewSize: null,
  stockFewUnitsQuantity: null,
  showProductReference: null,
  detailsChangeTitleByCategory: null,
};

export const productsFiltersEnvironment = {
  categories: null,
  search: null,
  prices: null,
  noveltyOutlet: null,
  collections: null,
  brands: null,
  attributes: null,
  type: null,
};

export const myAccountEnvironment = {
  myInformation: null,
  myAddresses: null,
  myOrders: null,
  myWebinars: null,
  myStreamings: null,
  myTickets: null,
  mySubscriptions: null,
  myMultimedia: null,
  myRequests: null,
  myLicenses: null,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: null,
  toolbar: null,
  footer: null,
};

export const urlsEnvironment = {
  website: null,
  footerCredits: null,
  email: null,
  phone: null,
  facebook: null,
  twitter: null,
  instagram: null,
  linkedin: null,
  youtube: null,
  vimeo: null,
  whatsapp: null,
  flickr: null,
  legal: null,
  tsAndCs: null,
  privacy: null,
  cookies: null,
  purchase: null,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
