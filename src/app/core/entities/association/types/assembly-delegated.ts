export interface AssemblyDelegated {
  birthday: string;
  creationDate: string;
  document: string;
  documentType: string;
  email: string;
  firstName: string;
  gender: string;
  id: number;
  language: string;
  lastName: string;
  lastUpdateDate: string;
  memberId: number;
  phone: string;
}
