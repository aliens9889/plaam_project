import { Component, OnInit } from '@angular/core';
import { AppEnv, UrlsInfo } from '@qaroni-app/core/config';
import * as moment from 'moment';
import { urlsEnvironment } from 'src/environments/environment';

@Component({
  selector: 'qaroni-footer-style-four-bottom',
  templateUrl: './footer-style-four-bottom.component.html',
  styleUrls: ['./footer-style-four-bottom.component.scss'],
})
export class FooterStyleFourBottomComponent implements OnInit {
  public year = moment().format('Y');

  constructor() {}

  ngOnInit(): void {}

  get UrlsInfo() {
    return UrlsInfo;
  }

  get AppEnv() {
    return AppEnv;
  }

  get keysUrlsEnvironment() {
    return Object.keys(urlsEnvironment);
  }
}
