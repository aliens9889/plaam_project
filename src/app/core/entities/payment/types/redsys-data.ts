export interface RedsysData {
  Ds_MerchantParameters: string;
  Ds_Signature: string;
  Ds_SignatureVersion: string;
  postUrl: string;
}
