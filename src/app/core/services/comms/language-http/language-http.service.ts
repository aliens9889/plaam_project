import { Injectable } from '@angular/core';
import { LanguageEnum } from '@qaroni-app/core/types';
import { AppStorageService } from '../../app';

@Injectable({
  providedIn: 'root'
})
export class LanguageHttpService {
  constructor(private appStorage: AppStorageService) {}

  public completeUrlWithLanguage(url: string): string {
    if (
      url &&
      this.appStorage.hasLanguage &&
      Object.values(LanguageEnum).includes(this.appStorage.getLanguage())
    ) {
      if (!url.includes('?')) {
        url = url + '?language=' + this.appStorage.getLanguage();
      } else {
        url = url + '&language=' + this.appStorage.getLanguage();
      }
    }
    return url;
  }
}
