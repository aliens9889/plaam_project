import { Component, HostListener, Input, OnInit } from '@angular/core';
import { qaroniToggleFadeTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-scroll-top',
  templateUrl: './scroll-top.component.html',
  styleUrls: ['./scroll-top.component.scss'],
  animations: [qaroniToggleFadeTrigger],
})
export class ScrollTopComponent implements OnInit {
  @Input() offset = 1000;

  public windowScrolled: boolean;

  constructor() {}

  @HostListener('window:scroll') public onWindowScroll() {
    if (
      !this.windowScrolled &&
      (window.pageYOffset || document.documentElement.scrollTop) > this.offset
    ) {
      this.windowScrolled = true;
    } else if (
      this.windowScrolled &&
      (window.pageYOffset || document.documentElement.scrollTop) < this.offset
    ) {
      this.windowScrolled = false;
    }
  }

  public scrollToTop(): void {
    (function smoothScroll() {
      const currentScroll =
        window.pageYOffset || document.documentElement.scrollTop;
      if (currentScroll > 0) {
        window.requestAnimationFrame(smoothScroll);
        window.scrollTo(0, currentScroll - currentScroll / 4);
      }
    })();
  }

  ngOnInit(): void {}
}
