import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import {
  AssociationPartnerService,
  LicenseCompany,
  PartnerCompanyDataSource,
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-partner-companies-table',
  templateUrl: './partner-companies-table.component.html',
  styleUrls: ['./partner-companies-table.component.scss'],
})
export class PartnerCompaniesTableComponent implements OnInit, AfterViewInit {
  @Input() showPaginator = true;

  @Input() partnerID: number | string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatTable) table: MatTable<LicenseCompany>;
  public dataSource: PartnerCompanyDataSource;

  public displayedColumns: string[] = [
    'creationDate',
    'code',
    'name',
    'tradename',
    'cif',
    'email',
    'phone',
    'status',
  ];

  constructor(private partnerService: AssociationPartnerService) {}

  ngOnInit(): void {
    this.dataSource = new PartnerCompanyDataSource(
      this.partnerID,
      this.partnerService
    );
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
