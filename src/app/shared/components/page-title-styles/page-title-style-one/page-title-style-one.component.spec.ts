import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTitleStyleOneComponent } from './page-title-style-one.component';

describe('PageTitleStyleOneComponent', () => {
  let component: PageTitleStyleOneComponent;
  let fixture: ComponentFixture<PageTitleStyleOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTitleStyleOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTitleStyleOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
