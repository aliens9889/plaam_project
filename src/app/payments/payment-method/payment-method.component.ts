import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  GatewayEnum,
  GatewayService,
  Order,
  OrderPaymentService,
  OrderService,
  OrderSnackbars,
  OrderUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-payment-method',
  templateUrl: './payment-method.component.html',
  styleUrls: ['./payment-method.component.scss'],
})
export class PaymentMethodComponent implements OnInit, OnDestroy {
  private order$: Observable<Order> = this.orderService
    .getOrder$()
    .pipe(shareReplay(1));
  public order: Order = null;
  public OrderUtils = OrderUtils;
  public GatewayEnum = GatewayEnum;

  public initLoaded = false;

  public submitting = false;

  private subs: Subscription = new Subscription();

  public gateways: GatewayEnum[] = [];
  public gateways$: Observable<
    GatewayEnum[]
  > = this.gatewayService.getGateways$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderService: OrderService,
    private orderPaymentService: OrderPaymentService,
    private gatewayService: GatewayService
  ) {
    const orderID = route.snapshot.paramMap.get('orderID');
    this.allApp.toolbar.showBackButton(['/payments', orderID, 'billing']);
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      const orderID = this.route.snapshot.paramMap.get('orderID');

      this.subs.add(this.order$.subscribe(this.getOrder));
      this.orderService.getOrder(orderID);

      this.subs.add(this.gateways$.subscribe(this.getGateways));
      this.gatewayService.getPaymentGateways();

      this.subs.add(
        this.orderPaymentService
          .getRedirectUrl$()
          .subscribe(this.getRedirectUrl)
      );
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
  }

  private getOrder = (order: Order): void => {
    this.order = order;
    if (!OrderUtils.isCreated(this.order)) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(OrderSnackbars.mustBeCreated.message),
        this.allApp.translate.instant(OrderSnackbars.mustBeCreated.closeBtn),
        OrderSnackbars.mustBeCreated.config
      );
      this.allApp.router.navigate(['/orders']);
    }
    if (!OrderUtils.hasClient(this.order)) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(OrderSnackbars.mustHasClient.message),
        this.allApp.translate.instant(OrderSnackbars.mustHasClient.closeBtn),
        OrderSnackbars.mustHasClient.config
      );
      this.allApp.router.navigate(['/payments', order?.orderId, 'billing']);
    }
    this.disableLoading();
  }

  private getGateways = (gateways: GatewayEnum[]): void => {
    if (gateways) {
      this.gateways = gateways;
    }
  }

  private getRedirectUrl(redirectUrl: string): void {
    if (redirectUrl === null) {
      this.submitting = false;
    }
  }

  public onStartShopping(): void {
    this.allApp.router.navigate(['/']);
  }

  public hasPaymentMethod(
    gateways: GatewayEnum[],
    gateway: GatewayEnum
  ): boolean {
    return gateways.indexOf(gateway) !== -1 ? true : false;
  }

  public onLoading(): void {
    this.enableLoading();
  }

  get orderAmountAllowCofidis(): boolean {
    return this.orderPaymentService.orderAmountAllowCofidis(this.order);
  }
}
