import { imagesEnvironment } from 'src/environments/environment';
import { AppEnv } from '../app';

export const ImagesInfo = {
  logos: {
    logo: {
      source: imagesEnvironment.logo,
      alt: AppEnv.title,
    },
    toolbar: {
      source: imagesEnvironment.toolbar,
      alt: AppEnv.title,
    },
    footer: {
      source: imagesEnvironment.footer,
      alt: AppEnv.title,
    },
  },
  paymentMethods: {
    master: {
      source: './assets/images/payment-methods/payment-master-card-logo.svg',
      alt: 'Master Card',
    },
    visa: {
      source: './assets/images/payment-methods/payment-visa-logo.svg',
      alt: 'Visa',
    },
    amex: {
      source:
        './assets/images/payment-methods/payment-american-express-logo.svg',
      alt: 'American Express',
    },
    paypal: {
      source: './assets/images/payment-methods/payment-paypal-logo.png',
      alt: 'PayPal',
    },
  },
};
