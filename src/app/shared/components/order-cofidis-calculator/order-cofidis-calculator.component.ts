import { Component, Input, OnInit } from '@angular/core';
import { Order, OrderPaymentService } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-order-cofidis-calculator',
  templateUrl: './order-cofidis-calculator.component.html',
  styleUrls: ['./order-cofidis-calculator.component.scss'],
})
export class OrderCofidisCalculatorComponent implements OnInit {
  @Input() order: Order;

  public cofidisCalculator$: Observable<
    string
  > = this.orderPaymentService.getCofidisCalculator$().pipe(shareReplay(1));

  constructor(private orderPaymentService: OrderPaymentService) {}

  ngOnInit(): void {
    if (this.order && this.order.orderId && this.orderAmountAllowCofidis) {
      this.orderPaymentService.getCalculatorCofidisUrl(this.order.orderId);
    }
  }

  get orderAmountAllowCofidis(): boolean {
    return this.orderPaymentService.orderAmountAllowCofidis(this.order);
  }
}
