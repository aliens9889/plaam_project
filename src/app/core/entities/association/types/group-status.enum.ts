export enum GroupStatusEnum {
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
  INACTIVE = 'INACTIVE',
}

export type GroupStatusType = 'ACTIVE' | 'CREATED' | 'INACTIVE';

export const GroupStatusArray = ['ACTIVE', 'CREATED', 'INACTIVE'];

export const GroupStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    created: {
      name: 'Creado',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
