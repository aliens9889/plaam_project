import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyImageComponent } from './license-company-image.component';

describe('LicenseCompanyImageComponent', () => {
  let component: LicenseCompanyImageComponent;
  let fixture: ComponentFixture<LicenseCompanyImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
