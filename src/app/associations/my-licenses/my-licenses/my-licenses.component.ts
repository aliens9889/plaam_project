import { Component, OnDestroy, OnInit } from '@angular/core';
import { AssociationEnv } from '@qaroni-app/core/config';
import {
  AssociationLicenseService,
  LicenseCompany,
  LicenseMember
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-licenses',
  templateUrl: './my-licenses.component.html',
  styleUrls: ['./my-licenses.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class MyLicensesComponent implements OnInit, OnDestroy {
  public members$: Observable<
    LicenseMember[]
  > = this.licenseService.getLicensesMembers$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  public initLoaded = false;

  public AssociationEnv = AssociationEnv;

  public companies$: Observable<
    LicenseCompany[]
  > = this.licenseService.getLicensesCompanies$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private licenseService: AssociationLicenseService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    if (!AssociationEnv.allowsMembers && !AssociationEnv.allowsCompanies) {
      this.allApp.router.navigate(['/associations', 'groups']);
    }

    if (AssociationEnv.allowsMembers === true) {
      this.subs.add(this.members$.subscribe(this.getUserMembers));
      this.licenseService.getLicensesMembers();
    }

    if (AssociationEnv.allowsCompanies === true) {
      this.subs.add(this.companies$.subscribe(this.getUserCompanies));
      this.licenseService.getLicensesCompanies();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  get titleMembers(): string {
    return this.licenseService.strings.getTitleMyLicensesMembers();
  }

  get titleCompanies(): string {
    return this.licenseService.strings.getTitleMyLicensesCompanies();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getUserMembers = (members: LicenseMember[]): void => {
    this.disableLoading();
    this.initLoaded = true;
  }

  private getUserCompanies = (companies: LicenseCompany[]): void => {
    this.disableLoading();
    this.initLoaded = true;
  }
}
