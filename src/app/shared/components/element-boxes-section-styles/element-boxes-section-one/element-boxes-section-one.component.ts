import { Component, Input, OnInit } from '@angular/core';
import { ElementBox } from '@qaroni-app/core/entities';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-element-boxes-section-one',
  templateUrl: './element-boxes-section-one.component.html',
  styleUrls: ['./element-boxes-section-one.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ElementBoxesSectionOneComponent implements OnInit {
  @Input() boxes: ElementBox[];

  constructor() {}

  ngOnInit(): void {}
}
