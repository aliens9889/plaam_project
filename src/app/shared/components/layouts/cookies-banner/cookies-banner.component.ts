import { Component, OnInit } from '@angular/core';
import { AllAppService } from '@qaroni-app/core/services';
import { UrlsInfo } from '@qaroni-app/core/config';

@Component({
  selector: 'qaroni-cookies-banner',
  templateUrl: './cookies-banner.component.html',
  styleUrls: ['./cookies-banner.component.scss'],
})
export class CookiesBannerComponent implements OnInit {
  constructor(private allApp: AllAppService) {}

  ngOnInit(): void {}

  get hasCookies(): boolean {
    return this.allApp.cookies.hasCookies;
  }

  public acceptCookies(): void {
    this.allApp.cookies.setCookies(true);
  }

  get getCookiesUrl(): string {
    if (UrlsInfo.cookies) {
      return UrlsInfo.cookies;
    } else {
      return '#/';
    }
  }
}
