import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingCreateClientDialogComponent } from './billing-create-client-dialog.component';

describe('BillingCreateClientDialogComponent', () => {
  let component: BillingCreateClientDialogComponent;
  let fixture: ComponentFixture<BillingCreateClientDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingCreateClientDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingCreateClientDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
