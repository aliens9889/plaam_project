import { Pipe, PipeTransform } from '@angular/core';
import {
  LicenseCompanyStatusEnum,
  LicenseCompanyStatusInfo
} from '@qaroni-app/core/entities';

@Pipe({
  name: 'groupCompanyStatus',
})
export class GroupCompanyStatusPipe implements PipeTransform {
  transform(value: LicenseCompanyStatusEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of LicenseCompanyStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
