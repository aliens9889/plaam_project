import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentBankChargeComponent } from './payment-bank-charge.component';

describe('PaymentBankChargeComponent', () => {
  let component: PaymentBankChargeComponent;
  let fixture: ComponentFixture<PaymentBankChargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentBankChargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentBankChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
