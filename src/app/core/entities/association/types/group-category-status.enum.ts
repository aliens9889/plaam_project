export enum GroupCategoryStatusEnum {
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
  INACTIVE = 'INACTIVE',
}

export type GroupCategoryStatusType = 'ACTIVE' | 'CREATED' | 'INACTIVE';

export const GroupCategoryStatusArray = ['ACTIVE', 'CREATED', 'INACTIVE'];

export const GroupCategoryStatusInfo = [
  {
    active: {
      name: 'Activa',
    },
  },
  {
    created: {
      name: 'Creada',
    },
  },
  {
    inactive: {
      name: 'Inactiva',
    },
  },
];
