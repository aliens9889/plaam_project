import { TestBed } from '@angular/core/testing';

import { WebinarsHttpService } from './webinars-http.service';

describe('WebinarsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebinarsHttpService = TestBed.inject(WebinarsHttpService);
    expect(service).toBeTruthy();
  });
});
