import { Component, OnDestroy, OnInit } from '@angular/core';
import { Params } from '@angular/router';
import {
  AssociationService,
  Group,
  GroupStatusEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-partners-section-groups',
  templateUrl: './partners-section-groups.component.html',
  styleUrls: ['./partners-section-groups.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PartnersSectionGroupsComponent implements OnInit, OnDestroy {
  public groups$: Observable<
    Group[]
  > = this.associationService.getGroups$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private associationService: AssociationService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.groupsCalls();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  get title(): string {
    return this.associationService.strings.getTitleComponentString();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private groupsCalls(): void {
    this.enableLoading();
    const queryParams: Params = {
      last: 3,
      status: GroupStatusEnum.ACTIVE,
    };
    this.associationService.getGroups(queryParams);
  }

  private languageChanged = (): void => {
    this.groupsCalls();
  }
}
