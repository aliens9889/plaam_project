import { SnackbarConfig } from '@qaroni-app/core/config/snackbars';

export const FileSnackbars = {
  failureFile: {
    message:
      'No se puede subir el archivo. (Formatos: pdf, jpg, png. Tamaño máximo 4 Mb.)',
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.danger,
      duration: SnackbarConfig.durations.danger,
    },
  },
  failureImage: {
    message:
      'No se puede subir el archivo. (Formatos: jpg, png. Tamaño máximo 2 Mb.)',
    action: SnackbarConfig.strings.close,
    config: {
      panelClass: SnackbarConfig.classes.danger,
      duration: SnackbarConfig.durations.danger,
    },
  },
};
