import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageItemCardComponent } from './manage-item-card.component';

describe('ManageItemCardComponent', () => {
  let component: ManageItemCardComponent;
  let fixture: ComponentFixture<ManageItemCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageItemCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageItemCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
