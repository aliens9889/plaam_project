export enum OrderUpdateItemEnum {
  REPLACE = 'REPLACE',
  ADD = 'ADD'
}

export type OrderStatusType = 'REPLACE' | 'ADD';
