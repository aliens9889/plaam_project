export interface Page {
  creationDate: string;
  identifier: string;
  lastUpdateDate: string;
  merchantId: number;
  status: number;
  text: string;
  title: string;
  type: number;
}
