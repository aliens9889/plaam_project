import { Pipe, PipeTransform } from '@angular/core';
import { AssemblyTypeEnum, AssemblyTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'assemblyType',
})
export class AssemblyTypePipe implements PipeTransform {
  transform(value: AssemblyTypeEnum): string {
    if (value) {
      const key = value.trim().toUpperCase();
      for (const iterator of AssemblyTypeInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
