import { transition, trigger, useAnimation } from '@angular/animations';
import { qaroniEnterFadeIn } from './enter-fade-in';

export const qaroniEnterFadeInTrigger = trigger('enterFadeIn', [
  transition(':enter', [useAnimation(qaroniEnterFadeIn)]),
]);
