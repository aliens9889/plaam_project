import { Component, OnInit } from '@angular/core';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-free-ticket-failure',
  templateUrl: './free-ticket-failure.component.html',
  styleUrls: ['./free-ticket-failure.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class FreeTicketFailureComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
