import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { UserSnackbars } from '../snackbars/user-snackbar.config';
import { ChangePasswordJSON, UserData } from '../types/user-data';
import { UserDataHttpService } from './user-data-http.service';

@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  private userData$ = new Subject<UserData>();
  private updatedUserData$ = new Subject<UserData>();

  constructor(
    private allApp: AllAppService,
    private userDataHttp: UserDataHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get User Data
  // ==========================================================================================

  public getUserData$(): Observable<UserData> {
    return this.userData$.asObservable();
  }

  public getUserData(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userDataHttp
        .getUserData$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetUserData, this.failureGetUserData);
    }
  }

  private successGetUserData = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      this.userData$.next(data.body.result[0]);
    } else {
      this.userData$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetUserData = (error: HttpErrorResponse): void => {
    this.userData$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update User Data
  // ==========================================================================================

  public getUpdatedUserData$(): Observable<UserData> {
    return this.updatedUserData$.asObservable();
  }

  public updateUserData(userData: UserData): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userDataHttp
        .updateUserData$(this.oAuthService.getUserID, userData)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successUpdateUserData, this.failureUpdateUserData);
    }
  }

  private successUpdateUserData = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const userData = data.body.result[0] as UserData;
      if (userData && userData.language) {
        this.allApp.language.setLanguage(userData.language);
      }
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successUpdateUserData.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successUpdateUserData.closeBtn
        ),
        UserSnackbars.successUpdateUserData.config
      );
      this.updatedUserData$.next(userData);
    } else {
      this.updatedUserData$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateUserData = (error: HttpErrorResponse): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        UserSnackbars.failureUpdateUserData.message
      ),
      this.allApp.translate.instant(
        UserSnackbars.failureUpdateUserData.closeBtn
      ),
      UserSnackbars.failureUpdateUserData.config
    );
    this.updatedUserData$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // User Change Password
  // ==========================================================================================
  public changePassword(changePasswordJSON: ChangePasswordJSON): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.userDataHttp
        .changePassword$(this.oAuthService.getUserID, changePasswordJSON)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successChangePassword, this.failureChangePassword);
    }
  }

  private successChangePassword = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successChangePassword.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successChangePassword.closeBtn
        ),
        UserSnackbars.successChangePassword.config
      );
      const userData = data.body.result[0] as UserData;
      this.userData$.next(userData);
    } else {
      this.userData$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureChangePassword = (error: HttpErrorResponse): void => {
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        UserSnackbars.failureChangePassword.message
      ),
      this.allApp.translate.instant(
        UserSnackbars.failureChangePassword.closeBtn
      ),
      UserSnackbars.failureChangePassword.config
    );
    this.userData$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
