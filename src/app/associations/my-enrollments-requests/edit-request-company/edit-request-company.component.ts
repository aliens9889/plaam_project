import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssociationEnv } from '@qaroni-app/core/config';
import {
  AssociationService,
  GroupInscriptionCompany,
  GroupTypeEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-edit-request-company',
  templateUrl: './edit-request-company.component.html',
  styleUrls: ['./edit-request-company.component.scss'],
})
export class EditRequestCompanyComponent implements OnInit, OnDestroy {
  public inscription$: Observable<
    GroupInscriptionCompany
  > = this.associationService.getInscriptionCompany$().pipe(shareReplay(1));
  public inscription: GroupInscriptionCompany;

  private subs: Subscription = new Subscription();

  public submitting = false;

  private inscriptionID: string;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private associationService: AssociationService
  ) {
    this.inscriptionID = route.snapshot.paramMap.get('inscriptionID');
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (
      this.route.snapshot.paramMap.has('inscriptionID') &&
      AssociationEnv.allowsCompanies === true
    ) {
      this.subs.add(this.inscription$.subscribe(this.getInscription));
      this.associationService.getInscriptionCompany(this.inscriptionID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private redirect(): void {
    this.allApp.router.navigate(['/associations', 'my-enrollment-requests']);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getInscription = (inscription: GroupInscriptionCompany): void => {
    this.submitting = false;
    if (inscription?.group?.type === GroupTypeEnum.ENTITY) {
    } else {
      this.redirect();
    }
  }
}
