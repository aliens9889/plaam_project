import { TestBed } from '@angular/core/testing';

import { OAuthStorageService } from './o-auth-storage.service';

describe('OAuthStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OAuthStorageService = TestBed.inject(OAuthStorageService);
    expect(service).toBeTruthy();
  });
});
