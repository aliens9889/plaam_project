import { LanguageEnum } from '@qaroni-app/core/types';

export interface Multimedia {
  contents: MultimediaContent[];
  creationDate: string;
  descriptions: MultimediaDescription[];
  lastUpdateDate: string;
  license: string;
  merchantId: number;
  multimediaId: number;
}

export interface MultimediaContent {
  creationDate: string;
  lastUpdateDate: string;
  multimediaId: number;
  url: string;
}

export interface MultimediaDescription {
  creationDate: string;
  description: string;
  language: LanguageEnum;
  lastUpdateDate: string;
  name: string;
}
