import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostShowComponent } from './post-show/post-show.component';
import { PostsComponent } from './posts/posts.component';

const routes: Routes = [
  {
    path: '',
    component: PostsComponent,
  },
  {
    path: ':postID',
    component: PostShowComponent,
  },
  {
    path: ':postID/:postTitle',
    component: PostShowComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogsRoutingModule {}
