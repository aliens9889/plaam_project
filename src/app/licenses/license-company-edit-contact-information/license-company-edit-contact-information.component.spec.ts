import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyEditContactInformationComponent } from './license-company-edit-contact-information.component';

describe('LicenseCompanyEditContactInformationComponent', () => {
  let component: LicenseCompanyEditContactInformationComponent;
  let fixture: ComponentFixture<LicenseCompanyEditContactInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyEditContactInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyEditContactInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
