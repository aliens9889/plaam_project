import { TestBed } from '@angular/core/testing';

import { UserOrdersHttpService } from './user-orders-http.service';

describe('UserOrdersHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserOrdersHttpService = TestBed.inject(UserOrdersHttpService);
    expect(service).toBeTruthy();
  });
});
