export enum OrderStatusEnum {
  ALERT = 'ALERT',
  BANK_CHARGE = 'BANK_CHARGE',
  CANCELLED = 'CANCELLED',
  CHECKOUT = 'CHECKOUT',
  CREATED = 'CREATED',
  FINISHED = 'FINISHED',
  PAID = 'PAID',
  PENDING = 'PENDING',
  SENT = 'SENT',
}

export type OrderStatus =
  | 'ALERT'
  | 'BANK_CHARGE'
  | 'CANCELLED'
  | 'CHECKOUT'
  | 'CREATED'
  | 'FINISHED'
  | 'PAID'
  | 'PENDING'
  | 'SENT';

export const OrderStatusArray = [
  'ALERT',
  'BANK_CHARGE',
  'CANCELLED',
  'CHECKOUT',
  'CREATED',
  'FINISHED',
  'PAID',
  'PENDING',
  'SENT',
];

export const OrderStatusInfo = [
  {
    alert: {
      name: 'En alerta',
    },
  },
  {
    bank_charge: {
      name: 'Cargo bancario',
    },
  },
  {
    cancelled: {
      name: 'Cancelado',
    },
  },
  {
    checkout: {
      name: 'Checkout',
    },
  },
  {
    created: {
      name: 'Abierto',
    },
  },
  {
    finished: {
      name: 'Finalizado',
    },
  },
  {
    paid: {
      name: 'Pagado',
    },
  },
  {
    pending: {
      name: 'Pendiente de pago',
    },
  },
  {
    sent: {
      name: 'Enviado',
    },
  },
];
