import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { Item, Order, OrderUtils } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData } from '@qaroni-app/core/types';
import { ConfirmationDialogComponent } from '@qaroni-app/shared/components/dialogs';

@Component({
  selector: 'qaroni-shopping-cart-order',
  templateUrl: './shopping-cart-order.component.html',
  styleUrls: ['./shopping-cart-order.component.scss'],
})
export class ShoppingCartOrderComponent implements OnInit {
  public OrderUtils = OrderUtils;

  @Input() order: Order = null;

  @Output() emptyOrder: EventEmitter<Order> = new EventEmitter();

  @Output() deleteItem: EventEmitter<Item> = new EventEmitter();

  @Output() updateItem: EventEmitter<Item> = new EventEmitter();

  constructor(private allApp: AllAppService) {}

  ngOnInit(): void {}

  public onEmptyShoppingCart(): void {
    const confirmationData: ConfirmationDialogData = {
      title: this.allApp.translate.instant('empty-shopping-cart-dialog-title'),
      message: this.allApp.translate.instant(
        'empty-shopping-cart-dialog-text-1'
      ),
      cancelText: this.allApp.translate.instant('no-cancel'),
      confirmFaIcon: 'far fa-trash-alt',
      confirmText: this.allApp.translate.instant('yes-empty'),
    };

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '700px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;
    matDialogConfig.data = confirmationData;

    const dialog = this.allApp.dialog.open(
      ConfirmationDialogComponent,
      matDialogConfig
    );

    dialog.afterClosed().subscribe((confirmation) => {
      if (confirmation === true) {
        this.emptyOrder.emit(this.order);
      }
    });
  }

  public onDeleteItem(item: Item): void {
    this.deleteItem.emit(item);
  }

  public onUpdateItem(item: Item): void {
    this.updateItem.emit(item);
  }
}
