import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  ChangePasswordJSON,
  UserData,
  UserDataService
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { PasswordValidator } from '@qaroni-app/shared/validators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;
  public InputConfig = InputConfig;

  public changePasswordForm: FormGroup;
  private changePasswordSkeleton = {
    currentPassword: [
      '',
      [
        Validators.required,
        Validators.minLength(InputConfig.user.password.minLength),
      ],
    ],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(InputConfig.user.password.minLength),
      ],
    ],
    passwordConfirmation: [
      '',
      [
        Validators.required,
        Validators.minLength(InputConfig.user.password.minLength),
      ],
    ],
  };

  private submitting = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private userDataService: UserDataService
  ) {
    this.allApp.toolbar.showBackButton();
    this.createChangePasswordForm();
  }

  ngOnInit(): void {
    this.subs.add(
      this.userDataService.getUserData$().subscribe(this.getUserData)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createChangePasswordForm(): void {
    this.changePasswordForm = this.fb.group(this.changePasswordSkeleton, {
      validators: PasswordValidator.matchPassword,
    });
  }

  get currentPassword(): AbstractControl {
    return this.changePasswordForm.get('currentPassword');
  }

  get password(): AbstractControl {
    return this.changePasswordForm.get('password');
  }

  get passwordConfirmation(): AbstractControl {
    return this.changePasswordForm.get('passwordConfirmation');
  }

  get validatedForm(): boolean {
    return this.changePasswordForm.valid && !this.submitting;
  }

  private getUserData = (userData: UserData): void => {
    if (userData) {
      this.allApp.router.navigate(['/my-account', 'my-information']);
    }
    this.disableLoading();
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      const changePasswordJson: ChangePasswordJSON = {
        oldPassword: this.currentPassword.value,
        newPassword: this.password.value,
      };
      this.userDataService.changePassword(changePasswordJson);
    }
  }

  public onBack(): void {
    this.allApp.location.back();
  }
}
