import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardEmptyContentComponent } from './card-empty-content.component';

describe('CardEmptyContentComponent', () => {
  let component: CardEmptyContentComponent;
  let fixture: ComponentFixture<CardEmptyContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardEmptyContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardEmptyContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
