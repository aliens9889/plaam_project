import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderResumeItemComponent } from './order-resume-item.component';

describe('OrderResumeItemComponent', () => {
  let component: OrderResumeItemComponent;
  let fixture: ComponentFixture<OrderResumeItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderResumeItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderResumeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
