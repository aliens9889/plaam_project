import { Component, OnDestroy, OnInit } from '@angular/core';
import { Params } from '@angular/router';
import { AppLandingPageBlocksEnum, LandingEnv } from '@qaroni-app/core/config';
import {
  ElementBlock,
  ElementService,
  ElementStatusEnum,
  ElementTypeEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-element-block-section',
  templateUrl: './element-block-section.component.html',
  styleUrls: ['./element-block-section.component.scss'],
})
export class ElementBlockSectionComponent implements OnInit, OnDestroy {
  public blocks$: Observable<
    ElementBlock[]
  > = this.elementService.getBlocks$().pipe(shareReplay(1));

  private queryParams: Params = {
    type: ElementTypeEnum.BLOCK,
    status: ElementStatusEnum.ACTIVE,
  };

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private elementService: ElementService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    if (LandingEnv.blocks) {
      this.elementService.getBlocks(this.queryParams);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    if (LandingEnv.blocks) {
      this.elementService.getBlocks(this.queryParams);
    }
  }

  get LandingEnv() {
    return LandingEnv;
  }

  get AppLandingPageBlocksEnum() {
    return AppLandingPageBlocksEnum;
  }
}
