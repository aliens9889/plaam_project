import { TestBed } from '@angular/core/testing';

import { AllAppService } from './all-app.service';

describe('AllAppService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllAppService = TestBed.inject(AllAppService);
    expect(service).toBeTruthy();
  });
});
