import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterSearchComponent } from './products-filter-search.component';

describe('ProductsFilterSearchComponent', () => {
  let component: ProductsFilterSearchComponent;
  let fixture: ComponentFixture<ProductsFilterSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
