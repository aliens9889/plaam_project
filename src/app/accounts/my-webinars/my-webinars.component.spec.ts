import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyWebinarsComponent } from './my-webinars.component';

describe('MyWebinarsComponent', () => {
  let component: MyWebinarsComponent;
  let fixture: ComponentFixture<MyWebinarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyWebinarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyWebinarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
