import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  AssociationService,
  GroupInscriptionMember,
  GroupInscriptionMemberStatusEnum,
  GroupMonthTypeEnum
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-request-member-preview',
  templateUrl: './request-member-preview.component.html',
  styleUrls: ['./request-member-preview.component.scss'],
})
export class RequestMemberPreviewComponent implements OnInit {
  @Input() inscription: GroupInscriptionMember;

  @Output() payInscription: EventEmitter<
    GroupInscriptionMember
  > = new EventEmitter();

  constructor(private associationService: AssociationService) {}

  ngOnInit(): void {}

  get hasFiles(): boolean {
    if (this.inscription?.files?.length) {
      return true;
    }
    return false;
  }

  get isAdmitted(): boolean {
    if (this.inscription) {
      return (
        this.inscription.status === GroupInscriptionMemberStatusEnum.ADMITTED
      );
    }
    return false;
  }

  get isIncomplete(): boolean {
    if (this.inscription) {
      return (
        this.inscription.status === GroupInscriptionMemberStatusEnum.INCOMPLETE
      );
    }
    return false;
  }

  get hasProductVariantIDs(): boolean {
    if (this.inscription) {
      return this.associationService.hasProductVariantIDs(this.inscription);
    }
    return false;
  }

  public onPayInscription(): void {
    if (this.inscription) {
      this.payInscription.emit(this.inscription);
    }
  }

  get canManageFile(): boolean {
    if (
      this.inscription?.status ===
        GroupInscriptionMemberStatusEnum.INCOMPLETE ||
      this.inscription?.status === GroupInscriptionMemberStatusEnum.PENDING ||
      this.inscription?.status === GroupInscriptionMemberStatusEnum.REJECTED
    ) {
      return true;
    }
    return false;
  }

  get hasReason(): boolean {
    if (this.inscription?.reason) {
      return true;
    }
    return false;
  }

  public booleanToYesOrNo(status: boolean): string {
    return this.associationService.booleanToYesOrNo(status);
  }

  public timeUnitString(months: number): string {
    if (this.inscription?.group?.monthType === GroupMonthTypeEnum.MONTH) {
      return this.associationService.monthsString(months);
    } else if (
      this.inscription?.group?.monthType === GroupMonthTypeEnum.NATURAL_YEAR
    ) {
      return this.associationService.yearsString(months);
    }
  }

  get alertStatusClass(): string {
    if (this.inscription?.status) {
      switch (this.inscription.status) {
        case GroupInscriptionMemberStatusEnum.ACTIVE:
        case GroupInscriptionMemberStatusEnum.ADMITTED:
          return 'alert-success';
        case GroupInscriptionMemberStatusEnum.BANK_CHARGE:
        case GroupInscriptionMemberStatusEnum.INCOMPLETE:
        case GroupInscriptionMemberStatusEnum.PENDING:
        case GroupInscriptionMemberStatusEnum.REVISION:
          return 'alert-warning';
        case GroupInscriptionMemberStatusEnum.CANCELLED:
        case GroupInscriptionMemberStatusEnum.REJECTED:
          return 'alert-danger';

        default:
          return null;
      }
    }
    return null;
  }
}
