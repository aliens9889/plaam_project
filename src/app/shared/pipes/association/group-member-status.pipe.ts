import { Pipe, PipeTransform } from '@angular/core';
import { LicenseMemberStatusEnum, LicenseMemberStatusInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'groupMemberStatus',
})
export class GroupMemberStatusPipe implements PipeTransform {
  transform(value: LicenseMemberStatusEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of LicenseMemberStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
