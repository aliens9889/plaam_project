import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Order } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-shopping-cart-amounts',
  templateUrl: './shopping-cart-amounts.component.html',
  styleUrls: ['./shopping-cart-amounts.component.scss'],
})
export class ShoppingCartAmountsComponent implements OnInit {
  @Input() order: Order = null;

  @Output() goPayment: EventEmitter<Order> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  public onGoPayment(): void {
    this.goPayment.emit(this.order);
  }
}
