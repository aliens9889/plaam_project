import { Product } from './product';

export interface ProductCategory {
  categoryId: number;
  code: string;
  creationDate: string;
  description: string;
  image: string;
  importId: number;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  originalImage: string;
  parentId: number;
  position: number;
  showOnCover: number;
  status: number;
  urlText: string;
}

export interface SectionCategoryProducts {
  category: ProductCategory;
  products: Product[];
}
