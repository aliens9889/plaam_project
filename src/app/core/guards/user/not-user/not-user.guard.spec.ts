import { TestBed, async, inject } from '@angular/core/testing';

import { NotUserGuard } from './not-user.guard';

describe('NotUserGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotUserGuard]
    });
  });

  it('should ...', inject([NotUserGuard], (guard: NotUserGuard) => {
    expect(guard).toBeTruthy();
  }));
});
