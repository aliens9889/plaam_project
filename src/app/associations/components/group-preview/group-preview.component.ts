import { Component, Input, OnInit } from '@angular/core';
import {
  AssociationService,
  Group,
  GroupMonthTypeEnum
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-group-preview',
  templateUrl: './group-preview.component.html',
  styleUrls: ['./group-preview.component.scss'],
})
export class GroupPreviewComponent implements OnInit {
  @Input() group: Group;

  constructor(private associationService: AssociationService) {}

  ngOnInit(): void {}

  get hasImage(): boolean {
    if (this.group && this.group.imageUrl) {
      return true;
    }
    return false;
  }

  public booleanToYesOrNo(status: boolean): string {
    return this.associationService.booleanToYesOrNo(status);
  }

  public timeUnitString(months: number): string {
    if (this.group.monthType === GroupMonthTypeEnum.MONTH) {
      return this.associationService.monthsString(months);
    } else if (this.group.monthType === GroupMonthTypeEnum.NATURAL_YEAR) {
      return this.associationService.yearsString(months);
    }
  }
}
