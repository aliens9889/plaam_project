import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRequestsCompaniesComponent } from './my-requests-companies.component';

describe('MyRequestsCompaniesComponent', () => {
  let component: MyRequestsCompaniesComponent;
  let fixture: ComponentFixture<MyRequestsCompaniesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyRequestsCompaniesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRequestsCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
