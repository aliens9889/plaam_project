// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '94',
};

export const appEnvironment = {
  title:
    'Cámara Oficial de Comercio, Industria y Navegación de Santiago de Compostela',
  basePath: 'https://camaracompostela.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  brands: false,
  products: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.CHAMBER_OF_COMMERCE,
  allowsMembers: false,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://www.camaracompostela.com/assets/base/img/layout/logos/logo-3.png',
  toolbar:
    'https://www.camaracompostela.com/assets/base/img/layout/logos/logo-3.png',
  footer:
    'https://www.camaracompostela.com/assets/base/img/layout/logos/logo-blanco.png',
};

export const urlsEnvironment = {
  website: 'https://camaracompostela.staging.plaam.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'informacion@camaracompostela.com',
  phone: '+34 981 596 800',
  facebook:
    'https://www.facebook.com/pages/Camara-de-Comercio-de-Santiago-de-Compostela/243549438989859',
  twitter: 'https://twitter.com/ccompostela',
  instagram: 'https://www.instagram.com/ccompostela/',
  legal: 'https://camaracompostela.staging.plaam.com/#/pages/529/aviso-legal/',
  privacy:
    'https://camaracompostela.staging.plaam.com/#/pages/530/política-de-privacidad/',
  cookies:
    'https://camaracompostela.staging.plaam.com/#/pages/531/política-de-cookies/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
