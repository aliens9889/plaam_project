import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { CreateInvoice } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class InvoiceHttpService {
  constructor(private http: HttpClient) {}

  public getOrderInvoice$(
    userID: number | string,
    orderId: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderId}/invoices`;

    return this.http.get(url, { observe: 'response' });
  }

  public createOrderInvoice$(
    userID: number | string,
    orderID: number | string,
    createInvoiceJSON: CreateInvoice
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderID}/invoices`;

    return this.http.post(url, createInvoiceJSON, { observe: 'response' });
  }

  public getOrderInvoicePDFUrl(
    userID: number | string,
    orderId: number | string,
    invoiceId: number | string
  ): string {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderId}/invoices/${invoiceId}/pdf`;

    return url;
  }

  public getOrderTicketPDFUrl(
    userID: number | string,
    orderId: number | string
  ): string {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/orders/${orderId}/tickets/pdf`;

    return url;
  }
}
