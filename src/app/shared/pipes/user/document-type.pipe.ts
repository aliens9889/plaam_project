import { Pipe, PipeTransform } from '@angular/core';
import { DocumentTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'documentType'
})
export class DocumentTypePipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      value = value.trim().toLowerCase();
      for (const iterator of DocumentTypeInfo) {
        if (Object.keys(iterator)[0] === value) {
          return iterator[value].name;
        }
      }
    }
    return null;
  }
}
