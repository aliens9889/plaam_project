export interface CreateInvoice {
  addressId: string;
  email: string;
  name: string;
  nif: string;
  orderId: string;
  phone: string;
  userId: string;
}
