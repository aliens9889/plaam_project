import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductStockStringComponent } from './product-stock-string.component';

describe('ProductStockStringComponent', () => {
  let component: ProductStockStringComponent;
  let fixture: ComponentFixture<ProductStockStringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductStockStringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductStockStringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
