import { GenderEnum, LanguageEnum } from '@qaroni-app/core/types';
import { Address } from '../../address';
import { DocumentTypeEnum } from '../../user';
import { Group } from './group';
import { LicenseMemberStatusEnum } from './license-member-status.enum';

export interface LicenseMember {
  addresses: Address[];
  automaticRenewal: boolean;
  bankCharge: boolean;
  bic: string;
  clientId: number;
  code: string;
  creationDate: string;
  expirationDate: string;
  groupId: number;
  groupInfo: Group;
  iban: string;
  imageUrl: string;
  inscriptionId: number;
  lastUpdateDate: string;
  memberId: number;
  merchantId: number;
  partner: string;
  partnerId: number;
  renewalDate: string;
  status: LicenseMemberStatusEnum;
  unsubscribes: any[];
  userId: number;
  userInfo: LicenseMemberUserInfo;
  walletUrl: string;
}

export interface LicenseMemberUserInfo {
  birthday: string;
  creationDate: string;
  document: string;
  documentType: DocumentTypeEnum;
  email: string;
  firstName: string;
  gender: GenderEnum;
  id: number;
  language: LanguageEnum;
  lastName: string;
  lastUpdateDate: string;
  memberId: number;
  phone: string;
}
