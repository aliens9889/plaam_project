import { TestBed } from '@angular/core/testing';

import { UserAddressesHttpService } from './user-addresses-http.service';

describe('UserAddressesHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserAddressesHttpService = TestBed.inject(UserAddressesHttpService);
    expect(service).toBeTruthy();
  });
});
