import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageEventsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '57',
};

export const appEnvironment = {
  title: 'Zumbalacazan 4x4',
  basePath: 'https://www.zumbalacazan4x4.com/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.TWO,
  events: AppLandingPageEventsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.TWO,
  news: false,
  groups: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: true,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: '6Ld4MHoUAAAAAK7KwIqJsCuABSOdTCtJc_hgBSrK',
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc:
    'https://cdn.aplazame.com/aplazame.js?public-key=2f6783204ecd0136a459a4ac24bec132bb507e48&sandbox=false',
  publicKey: '2f6783204ecd0136a459a4ac24bec132bb507e48',
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/zumbalacazan4x4/logos/zumbalacazan.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/zumbalacazan4x4/logos/zumbalacazan.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/zumbalacazan4x4/logos/zumbalacazan.png',
};

export const urlsEnvironment = {
  website: 'https://www.zumbalacazan4x4.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'hola@zumbalacazan4x4.com',
  facebook: 'https://www.facebook.com/zumbalacazan4x4/',
  instagram: 'https://www.instagram.com/zumbalacazan4x4/',
  privacy:
    'https://www.zumbalacazan4x4.com/#/pages/862/política-de-privacidad/',
  cookies: 'https://www.zumbalacazan4x4.com/#/pages/902/política-de-cookies/',
  purchase:
    'https://www.zumbalacazan4x4.com/#/pages/863/condiciones-de-compra/',
};
