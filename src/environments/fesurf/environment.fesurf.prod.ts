import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageEventsEnum,
  AppLandingPageGroupsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '71',
};

export const appEnvironment = {
  title: 'Federación Española de Surf',
  basePath: 'https://my.fesurf.es/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  boxes: AppLandingPageBoxesEnum.ONE,
  events: AppLandingPageEventsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  groups: AppLandingPageGroupsEnum.CATEGORIES,
  brands: false,
  products: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.FEDERATION,
  allowsMembers: true,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: 'https://www.fesurf.es/images/logo_fesurfing_home.png',
  toolbar: 'https://www.fesurf.es/images/logo_fesurfing_home.png',
  footer: 'https://www.fesurf.es/images/logo_fesurfing_home.png',
};

export const urlsEnvironment = {
  website: 'https://my.fesurf.es/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'media@fesurf.es',
  phone: '+34 981 311 666',
  facebook: 'https://www.facebook.com/FESurfing/',
  twitter: 'https://twitter.com/fesurfing_',
  youtube: 'https://www.youtube.com/channel/UCkR7gFPi0h92AJEL59ylNEg',
  instagram: 'https://www.instagram.com/fesurfing_/',
  legal: 'https://my.fesurf.es/#/pages/880/aviso-legal/',
  privacy: 'https://my.fesurf.es/#/pages/881/política-de-privacidad/',
  cookies: 'https://my.fesurf.es/#/pages/889/política-de-cookies/',
};
