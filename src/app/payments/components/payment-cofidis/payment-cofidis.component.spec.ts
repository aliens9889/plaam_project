import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentCofidisComponent } from './payment-cofidis.component';

describe('PaymentCofidisComponent', () => {
  let component: PaymentCofidisComponent;
  let fixture: ComponentFixture<PaymentCofidisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentCofidisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentCofidisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
