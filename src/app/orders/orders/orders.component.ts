import { Component, OnDestroy, OnInit } from '@angular/core';
import { ImagesInfo } from '@qaroni-app/core/config';
import {
  Invoice,
  InvoiceService,
  Order,
  OrderService,
  OrderStatusEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit, OnDestroy {
  public ImagesInfo = ImagesInfo;

  public orders: Order[] = [];

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private orderService: OrderService,
    private invoiceService: InvoiceService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.orderService.getOrders$().subscribe(this.getOrders));
    this.orderService.getOrders();

    this.subs.add(
      this.invoiceService
        .getOrderInvoiceUrl$()
        .subscribe(this.getOrderInvoiceUrl)
    );

    this.subs.add(
      this.invoiceService.getOrderTicketUrl$().subscribe(this.getOrderTicketUrl)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getOrders = (orders: Order[]): void => {
    this.orders = orders.filter((order: Order) => {
      return order.status !== OrderStatusEnum.CREATED;
    });
    this.disableLoading();
  }

  public onStartShopping(): void {
    this.allApp.router.navigate(['/']);
  }

  public onOrderInvoice(invoice: Invoice): void {
    if (invoice?.userId && invoice?.orderId && invoice?.invoiceId) {
      const url = this.invoiceService.invoiceHttp.getOrderInvoicePDFUrl(
        invoice?.userId,
        invoice?.orderId,
        invoice?.invoiceId
      );
      this.getOrderInvoiceUrl(url);
    }
  }

  private getOrderInvoiceUrl = (invoiceUrl: string): void => {
    if (invoiceUrl) {
      window.location.href = invoiceUrl;
    }
  }

  public onOrderTicket(order: Order): void {
    this.invoiceService.getOrderTicket(order.orderId);
  }

  private getOrderTicketUrl = (ticketUrl: string): void => {
    if (ticketUrl) {
      window.location.href = ticketUrl;
    }
  }
}
