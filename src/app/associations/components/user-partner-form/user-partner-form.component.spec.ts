import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPartnerFormComponent } from './user-partner-form.component';

describe('UserPartnerFormComponent', () => {
  let component: UserPartnerFormComponent;
  let fixture: ComponentFixture<UserPartnerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPartnerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPartnerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
