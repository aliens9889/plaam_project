import { TestBed } from '@angular/core/testing';

import { AssociationSnackbarsService } from './association-snackbars.service';

describe('AssociationSnackbarsService', () => {
  let service: AssociationSnackbarsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssociationSnackbarsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
