import { Component, OnInit } from '@angular/core';
import { AppEnv, UrlsInfo } from '@qaroni-app/core/config';
import * as moment from 'moment';

@Component({
  selector: 'qaroni-footer-style-one-bottom',
  templateUrl: './footer-style-one-bottom.component.html',
  styleUrls: ['./footer-style-one-bottom.component.scss'],
})
export class FooterStyleOneBottomComponent implements OnInit {
  public year = moment().format('Y');

  constructor() {}

  ngOnInit(): void {}

  get UrlsInfo() {
    return UrlsInfo;
  }

  get AppEnv() {
    return AppEnv;
  }
}
