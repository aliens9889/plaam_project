import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementBoxesSectionComponent } from './element-boxes-section.component';

describe('ElementBoxesSectionComponent', () => {
  let component: ElementBoxesSectionComponent;
  let fixture: ComponentFixture<ElementBoxesSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementBoxesSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementBoxesSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
