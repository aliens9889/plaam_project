import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  UserData,
  UserDataService,
  UserDataUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-my-information-edit',
  templateUrl: './my-information-edit.component.html',
  styleUrls: ['./my-information-edit.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class MyInformationEditComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;
  public UserDataUtils = UserDataUtils;

  public userDataForm: FormGroup;
  private userDataSkeleton = {
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    documentType: [''],
    document: [''],
    birthday: [''],
    gender: ['', [Validators.required]],
    phone: ['', [Validators.required]],
    language: ['', [Validators.required]],
  };

  private submitting = false;

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private userDataService: UserDataService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.createUserDataForm();
  }

  ngOnInit(): void {
    this.subs.add(
      this.userDataService.getUserData$().subscribe(this.getUserData)
    );
    this.userDataService.getUserData();
    this.subs.add(
      this.userDataService.getUpdatedUserData$().subscribe(this.updateUserData)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private createUserDataForm(): void {
    this.userDataForm = this.fb.group(this.userDataSkeleton);
  }

  private populateUserDataForm(userData: UserData): void {
    this.userDataForm.patchValue(userData);
    if (userData.birthday) {
      this.birthday.setValue(moment(userData.birthday));
    }
  }

  get firstName(): AbstractControl {
    return this.userDataForm.get('firstName');
  }

  get lastName(): AbstractControl {
    return this.userDataForm.get('lastName');
  }

  get documentType(): AbstractControl {
    return this.userDataForm.get('documentType');
  }

  get document(): AbstractControl {
    return this.userDataForm.get('document');
  }

  get birthday(): AbstractControl {
    return this.userDataForm.get('birthday');
  }

  get gender(): AbstractControl {
    return this.userDataForm.get('gender');
  }

  get phone(): AbstractControl {
    return this.userDataForm.get('phone');
  }

  get language(): AbstractControl {
    return this.userDataForm.get('language');
  }

  get validatedForm(): boolean {
    return this.userDataForm.valid && !this.submitting;
  }

  private getUserData = (userData: UserData): void => {
    this.populateUserDataForm(userData);
    this.disableLoading();
    this.initLoaded = true;
  }

  private prepateFormToSend(): void {
    if (this.birthday.value && this.birthday.value.isValid()) {
      this.birthday.setValue(this.birthday.value.format('YYYY-MM-DD'));
    } else {
      this.birthday.patchValue('');
    }
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.prepateFormToSend();
      this.userDataService.updateUserData(this.userDataForm.getRawValue());
    }
  }

  private updateUserData = (userData: UserData): void => {
    this.populateUserDataForm(userData);
    this.disableLoading();
  }

  public onBack(): void {
    this.allApp.location.back();
  }
}
