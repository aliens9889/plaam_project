import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { CreateInvoice } from '../types/create-invoice';
import { Invoice } from '../types/invoice';
import { InvoiceHttpService } from './invoice-http.service';

@Injectable({
  providedIn: 'root',
})
export class InvoiceService {
  private invoiceUrl$ = new Subject<string>();

  private ticketUrl$ = new Subject<string>();

  constructor(
    private allApp: AllAppService,
    public invoiceHttp: InvoiceHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Get Order Invoice
  // ==========================================================================================

  public getOrderInvoiceUrl$(): Observable<string> {
    return this.invoiceUrl$.asObservable();
  }

  public getOrderInvoice(orderId: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.invoiceHttp
        .getOrderInvoice$(this.oAuthService.getUserID, orderId)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetOrderInvoice, this.failureGetOrderInvoice);
    }
  }

  private successGetOrderInvoice = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      if (data.body.result.length) {
        const invoice: Invoice = data.body.result[0];
        const url = this.invoiceHttp.getOrderInvoicePDFUrl(
          invoice.userId,
          invoice.orderId,
          invoice.invoiceId
        );
        // invoice['invoiceUrl'] = url;
        this.invoiceUrl$.next(url);
      } else {
        this.invoiceUrl$.next(null);
      }
    } else {
      this.invoiceUrl$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetOrderInvoice = (error: HttpErrorResponse): void => {
    this.invoiceUrl$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create Order Invoice
  // ==========================================================================================

  public createOrderInvoice(
    orderId: number | string,
    createInvoiceJSON: CreateInvoice
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.invoiceHttp
        .createOrderInvoice$(
          this.oAuthService.getUserID,
          orderId,
          createInvoiceJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successCreateOrderInvoice,
          this.failureCreateOrderInvoice
        );
    }
  }

  private successCreateOrderInvoice = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus201(data)) {
      const invoice: Invoice = data.body.result[0];
      const url = this.invoiceHttp.getOrderInvoicePDFUrl(
        invoice.userId,
        invoice.orderId,
        invoice.invoiceId
      );
      // invoice['invoiceUrl'] = url;
      this.invoiceUrl$.next(url);
    } else {
      this.invoiceUrl$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureCreateOrderInvoice = (error: HttpErrorResponse): void => {
    this.invoiceUrl$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Order Ticket
  // ==========================================================================================

  public getOrderTicketUrl$(): Observable<string> {
    return this.ticketUrl$.asObservable();
  }

  public getOrderTicket(orderId: number | string): void {
    if (this.oAuthService.hasOAuth) {
      const url = this.invoiceHttp.getOrderTicketPDFUrl(
        this.oAuthService.getUserID,
        orderId
      );
      this.invoiceUrl$.next(url);
    }
  }
}
