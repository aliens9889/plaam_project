import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Params } from '@angular/router';
import { Product } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-product-detail-options',
  templateUrl: './product-detail-options.component.html',
  styleUrls: ['./product-detail-options.component.scss'],
})
export class ProductDetailOptionsComponent implements OnInit {
  @Input() product: Product = null;

  @Output() selectOption: EventEmitter<Params> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  public onSelectOption(id: number, value: string): void {
    const params: Params = { 'options[]': [] };
    for (const option of this.product.variant.options) {
      if (id === option.id) {
        params['options[]'].unshift(option.id + '@' + value);
      } else {
        params['options[]'].push(option.id + '@' + option.value);
      }
    }
    this.selectOption.emit(params);
  }
}
