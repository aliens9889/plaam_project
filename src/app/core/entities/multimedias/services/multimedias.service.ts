import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Multimedia } from '../types/multimedia';
import { MultimediasHttpService } from './multimedias-http.service';

@Injectable({
  providedIn: 'root',
})
export class MultimediasService {
  private multimedias$ = new Subject<Multimedia[]>();

  constructor(
    private allApp: AllAppService,
    private multimediasHttp: MultimediasHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getMultimediaImageUrl(multimedia: Multimedia): string | null {
    return './assets/images/icons/multimedia.png';
    // if (
    //   multimedia.imagesURL &&
    //   multimedia.imagesURL.length &&
    //   multimedia.imagesURL[0]
    // ) {
    //   return multimedia.imagesURL[0];
    // } else {
    //   return './assets/images/icons/multimedia.png';
    // }
  }

  // ==========================================================================================
  // Get Multimedias
  // ==========================================================================================

  public getMultimedias$(): Observable<Multimedia[]> {
    return this.multimedias$.asObservable();
  }

  public getMultimedias(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.multimediasHttp
        .getMultimedias$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetMultimedias, this.failureGetMultimedias);
    }
  }

  private successGetMultimedias = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const multimedias: Multimedia[] = data.body.result;
      this.multimedias$.next(multimedias);
    } else {
      this.multimedias$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetMultimedias = (error: HttpErrorResponse): void => {
    this.multimedias$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
