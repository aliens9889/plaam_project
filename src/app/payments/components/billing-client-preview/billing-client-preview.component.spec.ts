import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingClientPreviewComponent } from './billing-client-preview.component';

describe('BillingClientPreviewComponent', () => {
  let component: BillingClientPreviewComponent;
  let fixture: ComponentFixture<BillingClientPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingClientPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingClientPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
