import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Post, PostsService } from '@qaroni-app/core/entities/blogs';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-blog-show',
  templateUrl: './post-show.component.html',
  styleUrls: ['./post-show.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PostShowComponent implements OnInit, OnDestroy {
  public post$: Observable<Post> = this.postsService
    .getPost$()
    .pipe(shareReplay(1));

  constructor(
    private postsService: PostsService,
    private allApp: AllAppService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('postID')) {
      this.postsService.getPost(this.route.snapshot.paramMap.get('postID'));
    } else {
      this.allApp.router.navigate(['/blogs']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public checkTagInfo(tagName: string): void {
    const queryParams: Params = { ...this.route.snapshot.queryParams };
    queryParams.tag = tagName.trim();
    this.allApp.router.navigate(['/blogs'], { queryParams });
  }
}
