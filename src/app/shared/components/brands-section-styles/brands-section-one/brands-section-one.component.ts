import { Component, Input, OnInit } from '@angular/core';
import { ProductBrand } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';

@Component({
  selector: 'qaroni-brands-section-one',
  templateUrl: './brands-section-one.component.html',
  styleUrls: ['./brands-section-one.component.scss'],
})
export class BrandsSectionOneComponent implements OnInit {
  @Input() brands$: Observable<ProductBrand[]>;

  constructor() {}

  ngOnInit(): void {}
}
