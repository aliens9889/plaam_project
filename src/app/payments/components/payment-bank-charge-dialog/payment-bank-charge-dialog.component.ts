import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  Client,
  GatewayEnum,
  Order,
  OrderPaymentService
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-payment-bank-charge-dialog',
  templateUrl: './payment-bank-charge-dialog.component.html',
  styleUrls: ['./payment-bank-charge-dialog.component.scss'],
})
export class PaymentBankChargeDialogComponent implements OnInit {
  public bankChargeForm: FormGroup;
  private bankChargeSkeleton = {
    paymentMethod: [
      { value: GatewayEnum.BANK_CHARGE, disabled: true },
      Validators.compose([Validators.required]),
    ],
    name: ['', Validators.compose([Validators.required])],
    iban: [
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern(/^[A-Z]{2}\d{18,26}$/),
      ]),
    ],
    bic: [''],
  };

  public submitting = false;

  private subs: Subscription = new Subscription();

  private order$: Observable<
    Order
  > = this.orderPaymentService.getBankChargeOrder$().pipe(shareReplay(1));

  constructor(
    public dialogRef: MatDialogRef<PaymentBankChargeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: [Order, Client],
    private allApp: AllAppService,
    private fb: FormBuilder,
    private orderPaymentService: OrderPaymentService
  ) {
    this.createBankChargeForm();
  }

  ngOnInit(): void {
    if (!this.data[0] && !this.data[1]) {
      this.onCancelClick();
    }
    this.subs.add(this.order$.subscribe(this.getOrder));
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
    this.dialogRef.disableClose = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.dialogRef.disableClose = false;
  }

  private createBankChargeForm(): void {
    this.bankChargeForm = this.fb.group(this.bankChargeSkeleton);
    this.populateBankChargeForm();
  }

  private populateBankChargeForm(): void {
    if (this.data[1]) {
      if (this.data[1]?.firstName || this.data[1]?.lastName) {
        this.name.patchValue(
          this.data[1]?.firstName + ' ' + this.data[1]?.lastName
        );
      }
      if (this.data[1]?.bankInfo?.length) {
        if (this.data[1]?.bankInfo[0]?.iban) {
          this.iban.patchValue(this.data[1]?.bankInfo[0]?.iban);
        }
        if (this.data[1]?.bankInfo[0]?.bic) {
          this.bic.patchValue(this.data[1]?.bankInfo[0]?.bic);
        }
      }
    }
  }

  get InputValidation() {
    return InputValidation;
  }

  get name(): AbstractControl {
    return this.bankChargeForm.get('name');
  }

  get iban(): AbstractControl {
    return this.bankChargeForm.get('iban');
  }

  get bic(): AbstractControl {
    return this.bankChargeForm.get('bic');
  }

  get validatedForm(): boolean {
    return this.bankChargeForm.valid && !this.submitting;
  }

  public onSubmit(): void {
    if (this.validatedForm && this.data[0]?.orderId) {
      this.enableLoading();
      this.orderPaymentService.paymentBankCharge(
        this.data[0]?.orderId,
        this.bankChargeForm.getRawValue()
      );
    }
  }

  private getOrder = (order: Order): void => {
    if (order) {
      this.dialogRef.close(order);
    } else {
      this.onCancelClick();
    }
    this.disableLoading();
  }
}
