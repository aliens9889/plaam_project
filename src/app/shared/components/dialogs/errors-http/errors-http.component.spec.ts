import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsHttpComponent } from './errors-http.component';

describe('ErrorsHttpComponent', () => {
  let component: ErrorsHttpComponent;
  let fixture: ComponentFixture<ErrorsHttpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorsHttpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsHttpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
