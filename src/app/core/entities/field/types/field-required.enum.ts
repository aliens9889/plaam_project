export enum FieldRequiredEnum {
  MANDATORY = 'MANDATORY',
  OPTIONAL = 'OPTIONAL'
}

export type FieldRequiredType = 'MANDATORY' | 'OPTIONAL';

export const FieldRequiredArray = ['MANDATORY', 'OPTIONAL'];

export const FieldRequiredInfo = [
  {
    mandatory: {
      name: 'Obligatorio'
    }
  },
  {
    optional: {
      name: 'Opcional'
    }
  }
];
