import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  ProductCategory,
  ProductService,
  SectionCategoryProducts
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-products-section-two',
  templateUrl: './products-section-two.component.html',
  styleUrls: ['./products-section-two.component.scss'],
})
export class ProductsSectionTwoComponent implements OnInit, OnDestroy {
  private subs: Subscription = new Subscription();

  public sections: SectionCategoryProducts[] = [];

  public categoriesLoaded = false;

  constructor(
    private allApp: AllAppService,
    private productService: ProductService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );

    this.subs.add(
      this.productService.getProductCategories$().subscribe(this.getCategories)
    );

    this.subs.add(
      this.productService
        .getProductsCategorySection$()
        .subscribe(this.getSection)
    );

    this.productsCalls();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private productsCalls(): void {
    this.productService.getProductCategories({ showOnCover: 1 });
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.productsCalls();
  }

  private getCategories = (categories: ProductCategory[]): void => {
    for (const category of categories) {
      this.productService.getProductsCategorySection({
        categories: category.categoryId,
        novelty: 1,
        last: 4,
      });
    }
    if (categories?.length === 0) {
      this.categoriesLoaded = true;
    }
  }

  private getSection = (section: SectionCategoryProducts) => {
    if (section) {
      this.sections.push(section);
    }
  }
}
