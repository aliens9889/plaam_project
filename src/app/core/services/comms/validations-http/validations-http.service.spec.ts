import { TestBed } from '@angular/core/testing';

import { ValidationsHttpService } from './validations-http.service';

describe('ValidationsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ValidationsHttpService = TestBed.inject(ValidationsHttpService);
    expect(service).toBeTruthy();
  });
});
