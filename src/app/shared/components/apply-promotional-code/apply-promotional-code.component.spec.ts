import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyPromotionalCodeComponent } from './apply-promotional-code.component';

describe('ApplyPromotionalCodeComponent', () => {
  let component: ApplyPromotionalCodeComponent;
  let fixture: ComponentFixture<ApplyPromotionalCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyPromotionalCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyPromotionalCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
