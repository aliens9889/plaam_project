import { Component, OnInit } from '@angular/core';
import { LandingEnv } from '@qaroni-app/core/config';
import { AllAppService } from '@qaroni-app/core/services';
import { landingEnvironment } from 'src/environments/environment';

@Component({
  selector: 'qaroni-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit {
  public LandingEnv = LandingEnv;

  constructor(private allApp: AllAppService) {
    this.enableLoading();
  }

  ngOnInit(): void {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get keysLandingEnvironment() {
    return Object.keys(landingEnvironment);
  }
}
