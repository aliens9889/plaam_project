import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleTwoBottomComponent } from './footer-style-two-bottom.component';

describe('FooterStyleTwoBottomComponent', () => {
  let component: FooterStyleTwoBottomComponent;
  let fixture: ComponentFixture<FooterStyleTwoBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleTwoBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleTwoBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
