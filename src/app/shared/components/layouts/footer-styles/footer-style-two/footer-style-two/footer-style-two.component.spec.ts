import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleTwoComponent } from './footer-style-two.component';

describe('FooterStyleTwoComponent', () => {
  let component: FooterStyleTwoComponent;
  let fixture: ComponentFixture<FooterStyleTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
