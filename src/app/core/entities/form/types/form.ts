export interface Form {
  creationDate: string;
  formId: number;
  lastUpdateDate: string;
  name: string;
}
