import { GoogleAnalyticsEnvironment } from 'src/environments/environment';

export const GoogleAnalyticsEnv = {
  TrackingID: GoogleAnalyticsEnvironment.hasOwnProperty('trackingID')
    ? GoogleAnalyticsEnvironment.trackingID
    : null,
};
