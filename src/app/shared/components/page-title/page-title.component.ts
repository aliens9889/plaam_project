import { Component, Input, OnInit } from '@angular/core';
import { AppEnv, AppTitleStyleEnum } from '@qaroni-app/core/config';

@Component({
  selector: 'qaroni-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.scss'],
})
export class PageTitleComponent implements OnInit {
  public AppEnv = AppEnv;
  public AppTitleStyleEnum = AppTitleStyleEnum;

  @Input() title: string = null;

  @Input() titleClass: string | string[] = null;

  constructor() {}

  ngOnInit(): void {}

  get getTitleClasses(): string {
    let classes = 'container-title';
    if (this.titleClass) {
      if (typeof this.titleClass === 'string') {
        classes += ' ' + this.titleClass;
      }
      if (
        typeof this.titleClass === 'object' &&
        Array.isArray(this.titleClass)
      ) {
        for (const theClass of this.titleClass) {
          classes += ' ' + theClass;
        }
      }
    }
    return classes;
  }
}
