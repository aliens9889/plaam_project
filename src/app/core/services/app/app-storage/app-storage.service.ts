import { Injectable } from '@angular/core';
import { AppEnv } from '@qaroni-app/core/config';
import { AppConfig, LanguageEnum } from '@qaroni-app/core/types';
import { Observable, Subject } from 'rxjs';
import { ServiceCommonService } from '../service-common/service-common.service';

@Injectable({
  providedIn: 'root',
})
export class AppStorageService {
  private appConfig: AppConfig;
  private appConfig$ = new Subject<AppConfig>();

  private lsKey = 'appConfig';

  constructor(private serviceCommon: ServiceCommonService) {}

  public get$(): Observable<AppConfig> {
    return this.appConfig$.asObservable();
  }

  public get(): AppConfig {
    if (this.serviceCommon.validateInLocalStorage(this.lsKey)) {
      return JSON.parse(localStorage.getItem(this.lsKey));
    } else {
      return this.appConfig;
    }
  }

  public set(appConfig: AppConfig) {
    if (this.serviceCommon.validateLocalStorage) {
      localStorage.setItem(this.lsKey, JSON.stringify(appConfig));
    } else {
      this.appConfig = appConfig;
    }
    this.appConfig$.next(appConfig);
  }

  public reset() {
    if (this.serviceCommon.validateInLocalStorage(this.lsKey)) {
      localStorage.removeItem(this.lsKey);
    } else {
      this.appConfig = undefined;
    }
    this.appConfig$.next(undefined);
  }

  // ==========================================================================================
  // Language config
  // ==========================================================================================

  get hasLanguage(): boolean {
    if (this.get() && this.get().language) {
      return true;
    }
    return false;
  }

  public getLanguage(): LanguageEnum {
    if (this.hasLanguage) {
      if (this.serviceCommon.validateInLocalStorage(this.lsKey)) {
        return JSON.parse(localStorage.getItem(this.lsKey)).language;
      } else {
        return this.appConfig.language;
      }
    } else {
      if (
        Array.isArray(AppEnv.languagesAllowed) &&
        AppEnv.languagesAllowed.length
      ) {
        return AppEnv.languagesAllowed[0];
      } else {
        return LanguageEnum.default;
      }
    }
  }

  public setLanguage(language: LanguageEnum) {
    if (this.serviceCommon.validateLocalStorage) {
      let appConfig: AppConfig = this.get();
      if (appConfig) {
        appConfig.language = language;
      } else {
        appConfig = { language };
      }
      localStorage.setItem(this.lsKey, JSON.stringify(appConfig));
      this.appConfig$.next(appConfig);
    } else {
      this.appConfig.language = language;
      this.appConfig$.next(this.appConfig);
    }
  }

  // ==========================================================================================
  // Cookies config
  // ==========================================================================================

  get hasCookies(): boolean {
    if (this.get() && this.get().cookies && this.get().cookies === true) {
      return true;
    }
    return false;
  }

  public getCookies(): boolean {
    if (this.hasCookies) {
      if (this.serviceCommon.validateInLocalStorage(this.lsKey)) {
        return JSON.parse(localStorage.getItem(this.lsKey)).cookies;
      } else {
        return this.appConfig.cookies;
      }
    } else {
      return false;
    }
  }

  public setCookies(cookies: boolean) {
    if (this.serviceCommon.validateLocalStorage) {
      let appConfig: AppConfig = this.get();
      if (appConfig) {
        appConfig.cookies = cookies;
      } else {
        appConfig = { cookies };
      }
      localStorage.setItem(this.lsKey, JSON.stringify(appConfig));
      this.appConfig$.next(appConfig);
    } else {
      this.appConfig.cookies = cookies;
      this.appConfig$.next(this.appConfig);
    }
  }
}
