import { Component, OnInit } from '@angular/core';
import { ImagesInfo, UrlsInfo } from '@qaroni-app/core/config';
import { urlsEnvironment } from 'src/environments/environment';

@Component({
  selector: 'qaroni-footer-style-two-first',
  templateUrl: './footer-style-two-first.component.html',
  styleUrls: ['./footer-style-two-first.component.scss'],
})
export class FooterStyleTwoFirstComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  get ImagesInfo() {
    return ImagesInfo;
  }

  get UrlsInfo() {
    return UrlsInfo;
  }

  get keysUrlsEnvironment() {
    return Object.keys(urlsEnvironment);
  }
}
