import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerCompanyInscriptionsTableComponent } from './partner-company-inscriptions-table.component';

describe('PartnerCompanyInscriptionsTableComponent', () => {
  let component: PartnerCompanyInscriptionsTableComponent;
  let fixture: ComponentFixture<PartnerCompanyInscriptionsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerCompanyInscriptionsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerCompanyInscriptionsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
