import { AssemblyStatusEnum } from './assembly-status.enum';
import { AssemblyTypeEnum } from './assembly-type.enum';

export interface Assembly {
  assemblyId: number;
  creationDate: string;
  description: string;
  electionDate: string;
  hasAttended: boolean;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  simpleTopics: AssemblyTopic[];
  status: AssemblyStatusEnum;
}

export interface AssemblyTopic {
  assemblyId: number;
  hasVoted: boolean;
  name: string;
  status: AssemblyStatusEnum;
  topicId: number;
  type: AssemblyTypeEnum;
}
