import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Links } from '../../app';
import { Post } from '../types/posts';
import { PostsHttpService } from './posts-http.service';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  private postsSubject = new Subject<Post[]>();
  private postSubject = new Subject<Post>();

  private linksSubject = new Subject<Links>();

  constructor(
    private allApp: AllAppService,
    private postsHttp: PostsHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getNextPage(links: Links): number {
    if (links && links.next) {
      const urlArr: string[] = links.next.split('?');
      if (urlArr.length === 2) {
        const page = JSON.parse(
          '{"' +
            decodeURI(urlArr[1].replace(/&/g, '","').replace(/=/g, '":"')) +
            '"}'
        );
        if (page.hasOwnProperty('page')) {
          return parseInt(page.page, 10);
        }
      }
    }
    return null;
  }

  // ==========================================================================================
  // Get Posts
  // ==========================================================================================

  public getPosts$(): Observable<Post[]> {
    return this.postsSubject.asObservable();
  }

  public getPosts(queryParams?: Params): void {
    this.enableLoading();
    this.postsHttp
      .getPosts$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetPosts, this.failureGetPosts);
  }

  private successGetPosts = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const posts: Post[] = data.body.result;
      const links: Links = data.body.links;
      this.postsSubject.next(posts);
      this.linksSubject.next(links);
    } else {
      this.postsSubject.next(null);
      this.linksSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPosts = (error: HttpErrorResponse): void => {
    this.postsSubject.next(null);
    this.linksSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Post
  // ==========================================================================================

  public getPost$(): Observable<Post> {
    return this.postSubject.asObservable();
  }

  public getPost(postID: number | string): void {
    this.enableLoading();
    this.postsHttp
      .getPost$(postID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetPost, this.failureGetPost);
  }

  private successGetPost = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const blogItem: Post = data.body.result[0];
      this.postSubject.next(blogItem);
    } else {
      this.postSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetPost = (error: HttpErrorResponse): void => {
    this.postSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Posts Links
  // ==========================================================================================

  public getLinks$(): Observable<Links> {
    return this.linksSubject.asObservable();
  }
}
