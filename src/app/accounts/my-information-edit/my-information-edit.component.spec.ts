import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyInformationEditComponent } from './my-information-edit.component';

describe('MyInformationEditComponent', () => {
  let component: MyInformationEditComponent;
  let fixture: ComponentFixture<MyInformationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInformationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyInformationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
