import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '188',
};

export const appEnvironment = {
  title: 'Kebun organik',
  basePath: 'https://kebun.es/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.TWO,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 24,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plataforma.plaam.com/subido/empresas/188_20200511233503_Screenshot20200511at23.34.49.png',
  toolbar:
    'https://plataforma.plaam.com/subido/empresas/188_20200511233503_Screenshot20200511at23.34.49.png',
  footer:
    'https://plataforma.plaam.com/subido/empresas/188_20200511233503_Screenshot20200511at23.34.49.png',
};

export const urlsEnvironment = {
  website: 'https://kebun.es/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@kebun.es',
  facebook: 'https://www.facebook.com/Kebun-Organik-106369594419872/',
  instagram: 'https://www.instagram.com/kebun.es/',
  privacy: 'https://kebun.es/#/pages/870/política-de-privacidad/',
  cookies: 'https://kebun.es/#/pages/869/política-de-cookies/',
  purchase: 'https://kebun.es/#/pages/871/condiciones-de-compra/',
};
