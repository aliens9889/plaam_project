export enum NewsStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export type NewsStatusType = NewsStatusEnum.ACTIVE | NewsStatusEnum.INACTIVE;

export const NewsStatusArray = [NewsStatusEnum.ACTIVE, NewsStatusEnum.INACTIVE];

export const NewsStatusInfo = [
  {
    inactive: {
      name: 'Inactivo',
    },
  },
  {
    active: {
      name: 'Activo',
    },
  },
];
