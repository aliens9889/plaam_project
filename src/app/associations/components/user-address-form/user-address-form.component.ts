import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { Address, UserAddressService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { CountryEnum, CountryUtils } from '@qaroni-app/core/types';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-user-address-form',
  templateUrl: './user-address-form.component.html',
  styleUrls: ['./user-address-form.component.scss'],
})
export class UserAddressFormComponent implements OnInit, OnDestroy {
  @Input() precharge = false;

  public InputValidation = InputValidation;
  public InputConfig = InputConfig;
  public CountryUtils = CountryUtils;

  public userAddressForm: FormGroup;
  private userAddressSkeleton = {
    line1: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    line2: [
      '',
      Validators.compose([
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    postalCode: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.postalCode.maxLength),
      ]),
    ],
    city: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.city.maxLength),
      ]),
    ],
    stateProvince: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.stateProvince.maxLength),
      ]),
    ],
    country: [
      { value: CountryEnum.DEFAULT, disabled: true },
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.country.maxLength),
      ]),
    ],
  };

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private fb: FormBuilder,
    private userAddressService: UserAddressService
  ) {
    this.enableLoading();
    this.createUserAddressForm();
  }

  ngOnInit(): void {
    if (this.precharge === true) {
      this.subs.add(
        this.userAddressService
          .getUserAddresses$()
          .subscribe(this.getUserAddresses)
      );
      this.userAddressService.getUserAddresses();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private createUserAddressForm(): void {
    this.userAddressForm = this.fb.group(this.userAddressSkeleton);
  }

  public populateUserAddressForm(address: Address): void {
    if (address) {
      this.userAddressForm.patchValue(address);
    }
  }

  get line1(): AbstractControl {
    return this.userAddressForm.get('line1');
  }

  get line2(): AbstractControl {
    return this.userAddressForm.get('line2');
  }

  get postalCode(): AbstractControl {
    return this.userAddressForm.get('postalCode');
  }

  get city(): AbstractControl {
    return this.userAddressForm.get('city');
  }

  get stateProvince(): AbstractControl {
    return this.userAddressForm.get('stateProvince');
  }

  get country(): AbstractControl {
    return this.userAddressForm.get('country');
  }

  get validatedForm(): boolean {
    return this.userAddressForm.valid;
  }

  private getUserAddresses = (addresses: Address[]): void => {
    if (addresses && addresses.length) {
      addresses = addresses.sort(this.userAddressService.sortAddressesDefault);
      this.populateUserAddressForm(addresses[0]);
    }
  }
}
