import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleThreeComponent } from './footer-style-three.component';

describe('FooterStyleThreeComponent', () => {
  let component: FooterStyleThreeComponent;
  let fixture: ComponentFixture<FooterStyleThreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleThreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
