import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  Address,
  AssociationLicenseService,
  LicenseMember
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { CountryEnum, CountryUtils } from '@qaroni-app/core/types';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-member-edit-address',
  templateUrl: './license-member-edit-address.component.html',
  styleUrls: ['./license-member-edit-address.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseMemberEditAddressComponent implements OnInit, OnDestroy {
  private licenseMemberID: string;

  public initLoaded = false;

  private licenseMember$: Observable<
    LicenseMember
  > = this.licenseService.getLicenseMember$().pipe(shareReplay(1));
  public licenseMember: LicenseMember;

  private subs: Subscription = new Subscription();

  public addressForm: FormGroup;
  private addressSkeleton = {
    line1: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    line2: [
      '',
      Validators.compose([
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    postalCode: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.postalCode.maxLength),
      ]),
    ],
    city: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.city.maxLength),
      ]),
    ],
    stateProvince: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.stateProvince.maxLength),
      ]),
    ],
    country: [
      { value: CountryEnum.DEFAULT, disabled: true },
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.country.maxLength),
      ]),
    ],
  };

  private submitting = false;
  private formSent = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private licenseService: AssociationLicenseService
  ) {
    this.licenseMemberID = route.snapshot.paramMap.get('licenseMemberID');
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.createAddressForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('licenseMemberID')) {
      this.subs.add(this.licenseMember$.subscribe(this.getLicenseMember));
      this.licenseService.getLicenseMember(this.licenseMemberID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
    this.formSent = false;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private createAddressForm(): void {
    this.addressForm = this.fb.group(this.addressSkeleton);
  }

  private populateAddressForm(address: Address): void {
    if (address) {
      this.addressForm.patchValue(address);
    }
  }

  get InputValidation() {
    return InputValidation;
  }

  get CountryUtils() {
    return CountryUtils;
  }

  get line1(): AbstractControl {
    return this.addressForm.get('line1');
  }

  get line2(): AbstractControl {
    return this.addressForm.get('line2');
  }

  get postalCode(): AbstractControl {
    return this.addressForm.get('postalCode');
  }

  get city(): AbstractControl {
    return this.addressForm.get('city');
  }

  get stateProvince(): AbstractControl {
    return this.addressForm.get('stateProvince');
  }

  get country(): AbstractControl {
    return this.addressForm.get('country');
  }

  get validatedForm(): boolean {
    return this.addressForm.dirty && this.addressForm.valid && !this.submitting;
  }

  private getLicenseMember = (licenseMember: LicenseMember): void => {
    if (licenseMember) {
      if (this.formSent) {
        this.onBack();
      } else {
        this.licenseMember = licenseMember;
        if (this.licenseMember?.addresses?.length) {
          this.populateAddressForm(this.licenseMember?.addresses[0]);
        }
      }
    }
    this.disableLoading();
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.licenseService.updateLicenseMemberAddress(
        this.licenseMemberID,
        this.addressForm.getRawValue()
      );
      this.formSent = true;
    }
  }

  get titlePage(): string {
    return this.licenseService.strings.getTitleLicenseMemberAddress();
  }

  get titleCard(): string {
    return this.licenseService.strings.getStepTitleMemberAddress();
  }

  public onBack(): void {
    this.allApp.location.back();
  }
}
