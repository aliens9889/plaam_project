import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsPreviewSizeComponent } from './products-preview-size.component';

describe('ProductsPreviewSizeComponent', () => {
  let component: ProductsPreviewSizeComponent;
  let fixture: ComponentFixture<ProductsPreviewSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsPreviewSizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsPreviewSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
