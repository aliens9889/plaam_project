import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { SendByEmailJson } from '@qaroni-app/core/types';
import { Observable } from 'rxjs';
import { FreeTicketJson } from '../types/ticket';

@Injectable({
  providedIn: 'root',
})
export class TicketsHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getTickets$(userID: number | string): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/users/${userID}/events/tickets`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public sendTicketCardByEmail$(
    userID: number | string,
    eventID: number | string,
    ticketUUID: string,
    emailJSON: SendByEmailJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/events/${eventID}/tickets/${ticketUUID}/cards/emails`;

    return this.http.post(url, emailJSON, { observe: 'response' });
  }

  public buildDownloadTicketCardPDF(
    userID: number | string,
    eventID: number | string,
    ticketUUID: string
  ): string {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/events/${eventID}/tickets/${ticketUUID}/cards/pdf`;

    return url;
  }

  public freeTicket$(
    eventID: number | string,
    freeTicketJSON: FreeTicketJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/events/${eventID}/tickets`;

    return this.http.post(url, freeTicketJSON, { observe: 'response' });
  }
}
