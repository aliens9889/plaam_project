import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleOneBottomComponent } from './footer-style-one-bottom.component';

describe('FooterStyleOneBottomComponent', () => {
  let component: FooterStyleOneBottomComponent;
  let fixture: ComponentFixture<FooterStyleOneBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleOneBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleOneBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
