import { Address } from '../../address';
import { UserData } from '../../user';
import { Company } from './company';
import { Group } from './group';
import { GroupInscriptionCompanyStatusEnum } from './group-inscription-company-status.enum';
import { GroupInscriptionFile } from './group-inscription-file';
import { GroupInscriptionTypeEnum } from './group-inscription-type.enum';

export interface GroupInscriptionCompany {
  address: Address;
  clientId: number;
  company: Company;
  companyAddress: Address;
  creationDate: string;
  files: GroupInscriptionFile[];
  formId: number;
  formJson: any;
  group: Group;
  groupId: number;
  inscriptionId: number;
  isPartner: boolean;
  lastUpdateDate: string;
  merchantId: number;
  modelId: number;
  partnerId: number;
  reason: string;
  status: GroupInscriptionCompanyStatusEnum;
  type: GroupInscriptionTypeEnum;
  user: UserData;
  userId: string;
}

export interface CreateInscriptionCompanyJson {
  addressJson: Address;
  companyAddressJson: Address;
  companyJson: Company;
  formId: number | string;
  formJson: any;
  partnerId: number | string;
  type: GroupInscriptionTypeEnum.INSCRIPTION;
  userJson: UserData;
}

export interface RenewalInscriptionCompanyJson {
  addressJson: Address;
  companyAddressJson: Address;
  companyJson: Company | object;
  modelId: number;
  partnerId: number;
  type: GroupInscriptionTypeEnum;
  userJson: UserData | object;
}
