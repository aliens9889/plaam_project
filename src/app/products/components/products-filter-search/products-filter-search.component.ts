import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'qaroni-products-filter-search',
  templateUrl: './products-filter-search.component.html',
  styleUrls: ['./products-filter-search.component.scss'],
})
export class ProductsFilterSearchComponent implements OnInit {
  public search = '';

  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(this.getQueryParams);
  }

  private getQueryParams = (queryParams: Params) => {
    if (queryParams.hasOwnProperty('search')) {
      this.search = queryParams.search;
    } else {
      this.search = '';
    }
  }

  get canFilterBySearch(): boolean {
    if (
      typeof this.search === 'string' &&
      this.search !== '' &&
      this.search !== null
    ) {
      return true;
    }
    return false;
  }

  public onFilterBySearch(): void {
    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );

    queryParamsEmit.search = this.search;
    this.queryParamsChange.emit(queryParamsEmit);
  }

  public onInputKeyUp(event): void {
    if (this.canFilterBySearch) {
      this.onFilterBySearch();
    }
  }
}
