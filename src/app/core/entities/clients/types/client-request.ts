export interface ClientRequest {
  birthday: string;
  city: string;
  country: string;
  document: string;
  firstName: string;
  gender: string;
  lastName: string;
  line1: string;
  line2: string;
  phone: string;
  postalCode: string;
  stateProvince: string;
}
