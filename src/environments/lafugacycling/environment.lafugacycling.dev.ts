// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '87',
};

export const appEnvironment = {
  title: 'La Fuga Cycling',
  basePath: 'https://lafugacycling.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.TWO,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  news: true,
  products: AppLandingPageProductsEnum.TWO,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 20,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '4:5',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: true,
  brands: true,
  attributes: true,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: 'pk_test_TYooMQauvdEDq54NiTphI7jx',
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: 'https://www.lafugacycling.com/images/logo.png',
  toolbar: 'https://www.lafugacycling.com/images/logo.png',
  footer: 'https://www.lafugacycling.com/images/logo.png',
};

export const urlsEnvironment = {
  website: 'https://lafugacycling.staging.plaam.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'info@lafugacycling.com',
  facebook: 'https://www.facebook.com/LaFugaCyclingStore',
  twitter: 'https://twitter.com/lafuga_cycling',
  instagram: 'https://www.instagram.com/lafuga_cycling/',
  legal: 'https://lafugacycling.staging.plaam.com/#/pages/301/aviso-legal/',
  tsAndCs:
    'https://lafugacycling.staging.plaam.com/#/pages/302/términos-y-condiciones/',
  privacy:
    'https://lafugacycling.staging.plaam.com/#/pages/297/política-de-privacidad/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
