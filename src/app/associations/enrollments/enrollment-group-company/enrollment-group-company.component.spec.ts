import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnrollmentGroupCompanyComponent } from './enrollment-group-company.component';

describe('EnrollmentGroupCompanyComponent', () => {
  let component: EnrollmentGroupCompanyComponent;
  let fixture: ComponentFixture<EnrollmentGroupCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnrollmentGroupCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnrollmentGroupCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
