import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Event, EventService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { SelectZoneDialogComponent } from '../components/dialogs/select-zone-dialog/select-zone-dialog.component';

@Component({
  selector: 'qaroni-event-show',
  templateUrl: './event-show.component.html',
  styleUrls: ['./event-show.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class EventShowComponent implements OnInit, OnDestroy {
  private hasEventID = this.route.snapshot.paramMap.has('eventID');
  private eventID = this.route.snapshot.paramMap.get('eventID');

  public event$: Observable<Event> = this.eventService
    .getEvent$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private eventService: EventService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.hasEventID) {
      this.subs.add(
        this.allApp.language.getLanguage$().subscribe(this.languageChanged)
      );
      this.eventService.getEvent(this.eventID);
    } else {
      this.allApp.router.navigate(['/events']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getEventImage(event: Event): string {
    return this.eventService.getEventImage(event);
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.eventService.getEvent(this.eventID);
  }

  public hasZones(event: Event): boolean {
    return this.eventService.hasZones(event);
  }

  public hasCapacity(event: Event): boolean {
    if (this.hasZones(event)) {
      for (const zone of event?.zones) {
        if (zone?.number < zone?.capacity) {
          return true;
        }
      }
    }
    return false;
  }

  public isPaid(event: Event): boolean {
    return this.eventService.isPaid(event);
  }

  public hasProducts(event: Event): boolean {
    return this.eventService.hasProducts(event);
  }

  public goToBuyTickets(): void {
    const el = document.getElementsByClassName('products-buy-tickets');
    if (el.length) {
      el[0].scrollIntoView({ block: 'center', behavior: 'smooth' });
    }
  }

  public freeRegister(event: Event): void {
    if (event?.zones?.length === 1) {
      this.allApp.router.navigate([
        '/events',
        event?.eventId,
        'zones',
        event?.zones[0]?.zoneId,
        'free',
      ]);
    } else if (event?.zones?.length > 1) {
      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = '700px';
      matDialogConfig.maxWidth = '90vw';
      matDialogConfig.panelClass = 'style-confirm-dialog';
      matDialogConfig.autoFocus = false;
      matDialogConfig.data = event;

      const dialog = this.allApp.dialog.open<
        SelectZoneDialogComponent,
        Event,
        number
      >(SelectZoneDialogComponent, matDialogConfig);

      this.subs.add(
        dialog.afterClosed().subscribe((zoneID: number) => {
          if (this.hasZones(event) && zoneID) {
            this.allApp.router.navigate([
              '/events',
              event?.eventId,
              'zones',
              zoneID,
              'free',
            ]);
          }
        })
      );
    }
  }
}
