import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';
import {
  CreateInscriptionCompanyJson,
  RenewalInscriptionCompanyJson
} from '../types/group-inscription-company';
import {
  InscriptionMemberJson,
  RenewalMemberJson
} from '../types/group-inscription-member';

@Injectable({
  providedIn: 'root',
})
export class AssociationHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  // ==========================================================================================
  // Categories and groups
  // ==========================================================================================

  public getCategories$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups/categories`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    const params: Params = Object.assign({}, queryParams);

    return this.http.get(url, { observe: 'response', params });
  }

  public getCategory$(
    categoryID: number | string
  ): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups/categories/${categoryID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  public getGroups$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    const params: Params = Object.assign({}, queryParams);

    return this.http.get(url, { observe: 'response', params });
  }

  public getGroup$(groupID: number | string): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups/${groupID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Inscriptions Member
  // ==========================================================================================

  public createGroupInscriptionMember$(
    userID: number | string,
    groupID: number | string,
    inscriptionJSON: InscriptionMemberJson | RenewalMemberJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/${groupID}/members/inscriptions`;

    return this.http.post(url, inscriptionJSON, { observe: 'response' });
  }

  public createGroupInscriptionMemberPartner$(
    userID: number | string,
    groupID: number | string,
    partnerID: number | string,
    inscriptionJSON: InscriptionMemberJson | RenewalMemberJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/${groupID}/partners/${partnerID}/members/inscriptions`;

    return this.http.post(url, inscriptionJSON, { observe: 'response' });
  }

  public getInscriptionsMembers$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/members/inscriptions`;

    return this.http.get(url, { observe: 'response' });
  }

  public getInscriptionMember$(
    userID: number | string,
    inscriptionID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/members/inscriptions/${inscriptionID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public updateInscriptionMember$(
    userID: number | string,
    inscriptionID: number | string,
    inscriptionJSON: InscriptionMemberJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/members/inscriptions/${inscriptionID}`;

    return this.http.patch(url, inscriptionJSON, { observe: 'response' });
  }

  public addFileToInscriptionMember$(
    userID: number | string,
    inscriptionID: number | string,
    dataMultipart: FormData
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/members/inscriptions/${inscriptionID}/files`;

    return this.http.post(url, dataMultipart, { observe: 'response' });
  }

  // ==========================================================================================
  // Inscription Companies
  // ==========================================================================================

  public createGroupInscriptionCompany$(
    userID: number | string,
    groupID: number | string,
    inscriptionJSON:
      | CreateInscriptionCompanyJson
      | RenewalInscriptionCompanyJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/${groupID}/companies/inscriptions`;

    return this.http.post(url, inscriptionJSON, { observe: 'response' });
  }

  public createGroupInscriptionCompanyPartner$(
    userID: number | string,
    groupID: number | string,
    partnerID: number | string,
    inscriptionJSON:
      | CreateInscriptionCompanyJson
      | RenewalInscriptionCompanyJson
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/${groupID}/partners/${partnerID}/companies/inscriptions`;

    return this.http.post(url, inscriptionJSON, { observe: 'response' });
  }

  public getInscriptionsCompanies$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/companies/inscriptions`;

    return this.http.get(url, { observe: 'response' });
  }

  public getInscriptionCompany$(
    userID: number | string,
    inscriptionID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/companies/inscriptions/${inscriptionID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public addFileToInscriptionCompany$(
    userID: number | string,
    inscriptionID: number | string,
    dataMultipart: FormData
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/companies/inscriptions/${inscriptionID}/files`;

    return this.http.post(url, dataMultipart, { observe: 'response' });
  }
}
