import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '126',
};

export const appEnvironment = {
  title: 'Almacen Erótico',
  basePath: 'https://almacenerotico.com/#',
  languagesAllowed: [LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.TWO,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.TWO,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 24,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: false,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: true,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: true,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plataforma.staging.plaam.com/subido/empresas/132_20200421104307_13120200303164835storelogo.png',
  toolbar:
    'https://plataforma.staging.plaam.com/subido/empresas/132_20200421104307_13120200303164835storelogo.png',
  footer:
    'https://plataforma.staging.plaam.com/subido/empresas/132_20200421104307_13120200303164835storelogo.png',
};

export const urlsEnvironment = {
  website: 'https://almacenerotico.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'info@almacenerotico.com',
  facebook: 'https://www.facebook.com/almacenerotico/',
  privacy: 'https://almacenerotico.com/#/pages/857/política-de-privacidad/',
  cookies: 'https://almacenerotico.com/#/pages/1005/política-de-cookies/',
  tsAndCs: 'https://almacenerotico.com/#/pages/858/términos-y-condiciones/',
  purchase: 'https://almacenerotico.com/#/pages/855/condiciones-de-compra/',
};
