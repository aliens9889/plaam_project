// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageEventsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '57',
};

export const appEnvironment = {
  title: 'Zumbalacazan 4x4',
  basePath: 'https://zumbalacazan4x4.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.TWO,
  events: AppLandingPageEventsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.TWO,
  news: false,
  groups: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: true,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: '6Ld4MHoUAAAAAK7KwIqJsCuABSOdTCtJc_hgBSrK',
};

export const StripeEnvironment = {
  publicKey: 'pk_test_M1rWCD76YZNSzCVJI9jFunfP',
};

export const AplazameEnvironment = {
  scriptSrc:
    'https://cdn.aplazame.com/aplazame.js?public-key=2f6783204ecd0136a459a4ac24bec132bb507e48&sandbox=true',
  publicKey: '2f6783204ecd0136a459a4ac24bec132bb507e48',
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/zumbalacazan4x4/logos/zumbalacazan.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/zumbalacazan4x4/logos/zumbalacazan.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/zumbalacazan4x4/logos/zumbalacazan.png',
};

export const urlsEnvironment = {
  website: 'https://zumbalacazan4x4.staging.plaam.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'hola@zumbalacazan4x4.com',
  facebook: 'https://www.facebook.com/zumbalacazan4x4/',
  instagram: 'https://www.instagram.com/zumbalacazan4x4/',
  privacy:
    'https://zumbalacazan4x4.staging.plaam.com/#/pages/853/política-de-privacidad/',
  cookies: 'https://zumbalacazan4x4.staging.plaam.com/#/',
  purchase:
    'https://zumbalacazan4x4.staging.plaam.com/#/pages/852/condiciones-de-compra/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
