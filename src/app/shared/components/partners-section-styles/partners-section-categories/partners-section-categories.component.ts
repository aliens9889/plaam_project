import { Component, OnDestroy, OnInit } from '@angular/core';
import { AssociationService, GroupCategory, GroupCategoryStatusEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-partners-section-categories',
  templateUrl: './partners-section-categories.component.html',
  styleUrls: ['./partners-section-categories.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PartnersSectionCategoriesComponent implements OnInit, OnDestroy {
  public categories$: Observable<
    GroupCategory[]
  > = this.associationService.getCategories$().pipe(shareReplay(1));
  public categories: GroupCategory[];

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private associationService: AssociationService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.associationService.getCategories({
      last: 3,
      status: GroupCategoryStatusEnum.ACTIVE,
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  get title(): string {
    return this.associationService.strings.getTitleComponentString();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.associationService.getCategories({
      last: 3,
      status: GroupCategoryStatusEnum.ACTIVE,
    });
  }
}
