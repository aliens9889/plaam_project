// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '80',
};

export const appEnvironment = {
  title: 'Ilustre Colegio Oficial de Ingenieros Industriales de Galicia',
  basePath: 'https://icoiig.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: false,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  boxes: AppLandingPageBoxesEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  blocks: AppLandingPageBlocksEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  brands: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '4:3',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.COLLEGIATE,
  allowsMembers: true,
  allowsCompanies: false,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: 'https://icoiig.es/images/logo.png',
  toolbar: 'https://icoiig.es/images/logo-white.png',
  footer: 'https://icoiig.es/images/logo-white.png',
};

export const urlsEnvironment = {
  website: 'https://icoiig.staging.plaam.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'colegio@icoiig.es',
  facebook: 'https://www.facebook.com/icoiig/',
  twitter: 'https://twitter.com/Icoiig',
  linkedin:
    'https://www.linkedin.com/company/colegio-oficial-de-ingenieros-industriales-de-galicia',
  whatsapp: 'https://api.whatsapp.com/send?phone=34615438200',
  flickr: 'https://www.flickr.com/photos/icoiigalicia/albums',
  legal: 'https://icoiig.staging.plaam.com/#/',
  privacy: 'https://icoiig.staging.plaam.com/#/',
  cookies: 'https://icoiig.staging.plaam.com/#/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
