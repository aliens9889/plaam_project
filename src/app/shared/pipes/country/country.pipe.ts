import { Pipe, PipeTransform } from '@angular/core';
import { CountryInfo } from '@qaroni-app/core/types';

@Pipe({
  name: 'country'
})
export class CountryPipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      value = value.trim().toLowerCase();
      for (const iterator of CountryInfo) {
        if (Object.keys(iterator)[0] === value) {
          return iterator[value].name;
        }
      }
    }
    return null;
  }
}
