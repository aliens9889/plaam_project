import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { LanguageHttpService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EventHttpService {
  constructor(
    private http: HttpClient,
    private languageHttp: LanguageHttpService
  ) {}

  public getEvents$(queryParams?: Params): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/events`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  public getEvent$(eventID: number | string): Observable<HttpResponse<any>> {
    let url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/events/${eventID}`;

    url = this.languageHttp.completeUrlWithLanguage(url);

    return this.http.get(url, { observe: 'response' });
  }
}
