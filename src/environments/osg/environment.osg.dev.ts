// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '91',
};

export const appEnvironment = {
  title: 'Orquesta Sinfónica de Galicia',
  basePath: 'https://osg.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  products: AppLandingPageProductsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  brands: false,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 20,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '16:9',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: true,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo: 'https://www.sinfonicadegalicia.com/img/logo-osg2.png',
  toolbar: 'https://www.sinfonicadegalicia.com/img/logo-osg2.png',
  footer: 'https://www.sinfonicadegalicia.com/img/logo-osg2.png',
};

export const urlsEnvironment = {
  website: 'https://osg.staging.plaam.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'info@sinfonicadegalicia.com',
  facebook: 'https://www.facebook.com/sinfonicadegalicia',
  twitter: 'https://twitter.com/osggalicia',
  youtube: 'https://www.youtube.com/user/SinfonicadeGalicia',
  instagram: 'https://instagram.com/osggalicia',
  legal: 'https://osg.staging.plaam.com/#/pages/668/aviso-legal/',
  cookies: 'https://osg.staging.plaam.com/#/pages/670/política-de-cookies/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
