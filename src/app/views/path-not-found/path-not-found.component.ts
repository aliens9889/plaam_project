import { Component, OnInit } from '@angular/core';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-path-not-found',
  templateUrl: './path-not-found.component.html',
  styleUrls: ['./path-not-found.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PathNotFoundComponent implements OnInit {
  constructor(private allApp: AllAppService) {}

  ngOnInit(): void {
    this.allApp.router.navigate(['/']);
  }
}
