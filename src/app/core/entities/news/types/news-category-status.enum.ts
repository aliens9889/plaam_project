export enum NewsCategoryStatusEnum {
  ACTIVE = 'ACTIVE',
  CREATED = 'CREATED',
  INACTIVE = 'INACTIVE',
}

export type NewsCategoryStatusType =
  | NewsCategoryStatusEnum.ACTIVE
  | NewsCategoryStatusEnum.CREATED
  | NewsCategoryStatusEnum.INACTIVE;

export const NewsCategoryStatusArray = [
  NewsCategoryStatusEnum.ACTIVE,
  NewsCategoryStatusEnum.CREATED,
  NewsCategoryStatusEnum.INACTIVE,
];

export const NewsCategoryStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    created: {
      name: 'Creado',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
