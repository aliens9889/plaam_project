import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AplazameEnv } from '@qaroni-app/core/config';
import { OrderPaymentService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

declare const aplazame: any;

@Component({
  selector: 'qaroni-payment-aplazame',
  templateUrl: './payment-aplazame.component.html',
  styleUrls: ['./payment-aplazame.component.scss'],
})
export class PaymentAplazameComponent implements OnInit, OnDestroy {
  @Output() loading: EventEmitter<null> = new EventEmitter();

  private subs: Subscription = new Subscription();

  public aplazameCheckoutId$: Observable<
    string
  > = this.orderPaymentService.getAplazameCheckoutId$().pipe(shareReplay(1));

  public submitting = false;

  private tagScriptId = 'aplazame-js-sdk';

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderPaymentService: OrderPaymentService
  ) {}

  ngOnInit(): void {
    if (
      this.hasPaymentAplazame &&
      this.route.snapshot.paramMap.has('orderID')
    ) {
      this.subs.add(
        this.aplazameCheckoutId$.subscribe(this.getPaymentAplazameCheckoutId)
      );
      this.addScript();
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    const script = document.getElementById(this.tagScriptId);
    if (script) {
      script.parentNode.removeChild(script);
    }
    this.subs.unsubscribe();
  }

  private getPaymentAplazameCheckoutId(checkoutId: string): void {
    if (checkoutId) {
      aplazame.checkout(checkoutId, {
        onStatusChange: (resultStatus) => {
          console.log('IMPORTANTE: EL CHECKOUT TODAVÍA SIGUE ABIERTO');

          switch (resultStatus) {
            case 'success':
              console.log('el checkout ha sido completado correctamente');
              break;
            case 'pending':
              console.log(
                'el checkout ha terminado pero está pendiente de ser validado'
              );
              break;
            case 'ko':
              console.log(
                'el proceso de pago ha sido rechazado por parte de Aplazame'
              );
              break;
          }
        },
        onClose: (resultStatus) => {
          switch (resultStatus) {
            case 'success':
              console.log('el checkout ha sido completado correctamente');
              break;
            case 'pending':
              console.log(
                'el checkout ha terminado pero está pendiente de ser validado'
              );
              break;
            case 'error':
              console.log('ha ocurrido un error al cargar el checkout');
              break;
            case 'dismiss':
              console.log(
                'el usuario ha cerrado el checkout sin haberlo completado'
              );
              break;
            case 'ko':
              console.log(
                'el proceso de pago ha sido rechazado por parte de Aplazame'
              );
              break;
          }
        },
      });
    }
  }

  public onPaymentAplazame(): void {
    this.submitting = true;
    this.orderPaymentService.getPaymentAplazameCheckoutId(
      this.route.snapshot.paramMap.get('orderID')
    );
    this.loading.emit();
  }

  private addScript(): void {
    if (!this.hasPaymentAplazame || document.getElementById(this.tagScriptId)) {
      return;
    }
    const script = document.createElement('script');
    script.id = this.tagScriptId;
    script.src = AplazameEnv.scriptSrc;
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  }

  get validatedPaymentButton(): boolean {
    return !this.submitting;
  }

  get hasPaymentAplazame(): boolean {
    if (AplazameEnv.publicKey) {
      return true;
    }
    return false;
  }
}
