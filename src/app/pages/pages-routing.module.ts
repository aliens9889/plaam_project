import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageShowComponent } from './page-show/page-show.component';

const routes: Routes = [
  { path: ':pageID', component: PageShowComponent },
  { path: ':pageID/:pageName', component: PageShowComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
