import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';

@Component({
  selector: 'qaroni-company-data-form',
  templateUrl: './company-data-form.component.html',
  styleUrls: ['./company-data-form.component.scss'],
})
export class CompanyDataFormComponent implements OnInit {
  public InputValidation = InputValidation;

  public companyDataForm: FormGroup;
  private companyDataSkeleton = {
    name: ['', [Validators.required]],
    tradename: ['', [Validators.required]],
    cif: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
    secondaryPhone: [''],
  };

  constructor(private allApp: AllAppService, private fb: FormBuilder) {
    this.enableLoading();
    this.createCompanyDataForm();
  }

  ngOnInit(): void {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private createCompanyDataForm(): void {
    this.companyDataForm = this.fb.group(this.companyDataSkeleton);
  }

  get name(): AbstractControl {
    return this.companyDataForm.get('name');
  }

  get tradename(): AbstractControl {
    return this.companyDataForm.get('tradename');
  }

  get cif(): AbstractControl {
    return this.companyDataForm.get('cif');
  }

  get email(): AbstractControl {
    return this.companyDataForm.get('email');
  }

  get phone(): AbstractControl {
    return this.companyDataForm.get('phone');
  }

  get secondaryPhone(): AbstractControl {
    return this.companyDataForm.get('secondaryPhone');
  }

  get validatedForm(): boolean {
    return this.companyDataForm.valid;
  }
}
