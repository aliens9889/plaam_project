import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldTypeEnum, FormField } from '@qaroni-app/core/entities';
import { InputValidation } from '@qaroni-app/core/utils';

@Component({
  selector: 'qaroni-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
})
export class FormFieldComponent implements OnInit {
  @Input() formFields: FormField[];

  @Output() formJSON: EventEmitter<any> = new EventEmitter();

  public FieldTypeEnum = FieldTypeEnum;
  public InputValidation = InputValidation;

  public initLoaded = false;

  public dynamicForm: FormGroup;

  private submitting = false;

  constructor(private fb: FormBuilder) {
    this.createDynamicForm();
  }

  ngOnInit(): void {
    this.buildDynamicForm();
  }

  private createDynamicForm(): void {
    this.dynamicForm = this.fb.group({});
  }

  private buildDynamicForm(): void {
    if (this.formFields && this.formFields.length) {
      this.initLoaded = true;
      for (const formField of this.formFields) {
        const control = new FormControl('');
        if (formField.required) {
          control.setValidators(Validators.required);
        }
        if (formField.field.type === FieldTypeEnum.EMAIL) {
          control.setValidators(Validators.email);
        }
        this.dynamicForm.addControl(formField.field.name, control);
      }
    }
  }

  get validatedForm(): boolean {
    return this.dynamicForm.valid && !this.submitting;
  }

  public onSubmit(): void {
    this.submitting = true;
    this.formJSON.emit(this.dynamicForm.getRawValue());
  }
}
