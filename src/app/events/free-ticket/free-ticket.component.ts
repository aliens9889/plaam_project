import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OAuthService, TicketsService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { InputValidation } from '@qaroni-app/core/utils';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-free-ticket',
  templateUrl: './free-ticket.component.html',
  styleUrls: ['./free-ticket.component.scss'],
})
export class FreeTicketComponent implements OnInit, OnDestroy {
  public InputValidation = InputValidation;

  private hasEventID: boolean = this.route.snapshot.paramMap.has('eventID');
  private eventID: string = this.route.snapshot.paramMap.get('eventID');

  private hasZoneID: boolean = this.route.snapshot.paramMap.has('zoneID');
  private zoneID: string = this.route.snapshot.paramMap.get('zoneID');

  public freeTicket$: Observable<
    string
  > = this.ticketService.getFreeTicket$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  public ticketsForm: FormGroup;
  private ticketsSkeleton = {
    zoneId: ['', [Validators.required]],
    quantity: [1, [Validators.required, Validators.min(1), Validators.max(5)]],
    userId: [''],
    attendees: this.fb.array([]),
  };

  private attendeeSkeleton = {
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: [''],
  };

  private submitting = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private ticketService: TicketsService,
    private fb: FormBuilder,
    private oAuthService: OAuthService
  ) {
    this.createTicketsForm();
  }

  ngOnInit(): void {
    if (this.hasEventID && this.hasZoneID) {
      this.subs.add(this.freeTicket$.subscribe(this.getFreeTicket));
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private getFreeTicket = (freeTicket: string): void => {
    if (freeTicket) {
      if (this.oAuthService.hasOAuth) {
        this.allApp.router.navigate(['/my-account', 'my-tickets']);
      } else {
        this.allApp.router.navigate(['/events', 'free', 'success']);
      }
    } else {
      this.allApp.router.navigate(['/events', 'free', 'failure', 'capacity']);
    }
  }

  private createTicketsForm(): void {
    this.ticketsForm = this.fb.group(this.ticketsSkeleton);
    if (this.quantity.value) {
      for (let index = 0; index < this.quantity.value; index++) {
        this.attendees.push(this.fb.group(this.attendeeSkeleton));
      }
    }
    this.populateTicketsForm();
  }

  private populateTicketsForm(): void {
    if (this.hasZoneID) {
      this.zoneId.patchValue(this.zoneID);
    }
    if (this.oAuthService.hasOAuth) {
      this.userId.setValidators(Validators.required);
      this.userId.setValue(this.oAuthService.getUserID);
    }
  }

  get zoneId(): AbstractControl {
    return this.ticketsForm.get('zoneId');
  }

  get quantity(): AbstractControl {
    return this.ticketsForm.get('quantity');
  }

  get userId(): AbstractControl {
    return this.ticketsForm.get('userId');
  }

  get attendees(): FormArray {
    return this.ticketsForm.get('attendees') as FormArray;
  }

  get validatedForm(): boolean {
    return this.ticketsForm.dirty && this.ticketsForm.valid && !this.submitting;
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.ticketService.freeTicket(this.eventID, this.ticketsForm.value);
    }
  }

  public onChangeQuantity(event): void {
    if (event?.value >= 1 && event?.value <= 5) {
      if (this.attendees.length < event.value) {
        while (this.attendees.length < event.value) {
          this.attendees.push(this.fb.group(this.attendeeSkeleton));
        }
      } else if (this.attendees.length > event.value) {
        while (this.attendees.length > event.value) {
          this.attendees.removeAt(this.attendees.length - 1);
        }
      }
    }
  }

  public onBack(): void {
    this.allApp.location.back();
  }
}
