import { Element } from './element';
import { ElementTypeEnum } from './element-type.enum';

export interface ElementBlock extends Element {
  type: ElementTypeEnum.BLOCK;
}
