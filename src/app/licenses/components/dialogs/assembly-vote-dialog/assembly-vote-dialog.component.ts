import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  AssemblyTopic,
  AssemblyVoteResultArray,
  AssemblyVoteResultEnum
} from '@qaroni-app/core/entities';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-assembly-vote-dialog',
  templateUrl: './assembly-vote-dialog.component.html',
  styleUrls: ['./assembly-vote-dialog.component.scss'],
})
export class AssemblyVoteDialogComponent implements OnInit {
  private voteSkeleton = {
    vote: ['', Validators.compose([Validators.required])],
  };
  public voteForm: FormGroup = this.fb.group(this.voteSkeleton);

  private subs: Subscription = new Subscription();

  constructor(
    public dialogRef: MatDialogRef<AssemblyVoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public topic: AssemblyTopic,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.subs.add(
      this.dialogRef.afterOpened().subscribe(() => {
        if (!this.topic) {
          this.onCancelClick();
        }
      })
    );
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  get vote(): AbstractControl {
    return this.voteForm.get('vote');
  }

  get validatedForm(): boolean {
    return this.voteForm.dirty && this.voteForm.valid;
  }

  get AssemblyVoteResultArray(): AssemblyVoteResultEnum[] {
    return AssemblyVoteResultArray;
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.dialogRef.close(this.vote.value);
    }
  }
}
