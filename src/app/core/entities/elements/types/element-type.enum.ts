export enum ElementTypeEnum {
  BLOCK = 'BLOCK',
  BOX = 'BOX',
}

export type ElementType = 'BLOCK' | 'BOX';

export const ElementTypeArray = ['BLOCK', 'BOX'];

export const ElementTypeInfo = [
  {
    block: {
      name: 'Bloque',
    },
  },
  {
    box: {
      name: 'Caja',
    },
  },
];
