export enum GroupInscriptionMemberStatusEnum {
  ACTIVE = 'ACTIVE',
  ADMITTED = 'ADMITTED',
  BANK_CHARGE = 'BANK_CHARGE',
  CANCELLED = 'CANCELLED',
  INCOMPLETE = 'INCOMPLETE',
  PENDING = 'PENDING',
  REJECTED = 'REJECTED',
  REVISION = 'REVISION',
}

export type GroupInscriptionMemberStatusType =
  | 'ACTIVE'
  | 'ADMITTED'
  | 'BANK_CHARGE'
  | 'CANCELLED'
  | 'INCOMPLETE'
  | 'PENDING'
  | 'REJECTED'
  | 'REVISION';

export const GroupInscriptionMemberStatusArray = [
  'ACTIVE',
  'ADMITTED',
  'BANK_CHARGE',
  'CANCELLED',
  'INCOMPLETE',
  'PENDING',
  'REJECTED',
  'REVISION',
];

export const GroupInscriptionMemberStatusInfo = [
  {
    active: {
      name: 'Activa',
    },
  },
  {
    admitted: {
      name: 'Admitida',
    },
  },
  {
    bank_charge: {
      name: 'Cargo bancario',
    },
  },
  {
    cancelled: {
      name: 'Cancelada',
    },
  },
  {
    incomplete: {
      name: 'Incompleta',
    },
  },
  {
    pending: {
      name: 'Pendiente',
    },
  },
  {
    rejected: {
      name: 'Rechazada',
    },
  },
  {
    revision: {
      name: 'En revisión',
    },
  },
];
