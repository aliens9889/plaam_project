import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OrderPaymentService, RedsysData } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-payment-redsys',
  templateUrl: './payment-redsys.component.html',
  styleUrls: ['./payment-redsys.component.scss'],
})
export class PaymentRedsysComponent implements OnInit, OnDestroy {
  @Output() loading: EventEmitter<null> = new EventEmitter();

  private subs: Subscription = new Subscription();

  public redsysData$: Observable<
    RedsysData
  > = this.orderPaymentService.getRedsysData$().pipe(shareReplay(1));

  public paymentForm: FormGroup;
  private paymentSkeleton = {
    Ds_MerchantParameters: ['', Validators.required],
    Ds_Signature: ['', Validators.required],
    Ds_SignatureVersion: ['', Validators.required],
  };

  public submitting = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderPaymentService: OrderPaymentService,
    private fb: FormBuilder
  ) {
    this.createPaymentForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(this.redsysData$.subscribe(this.getPaymentRedsysData));
      this.orderPaymentService.getPaymentRedsysData(
        this.route.snapshot.paramMap.get('orderID')
      );
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private createPaymentForm(): void {
    this.paymentForm = this.fb.group(this.paymentSkeleton);
  }

  get Ds_MerchantParameters(): AbstractControl {
    return this.paymentForm.get('Ds_MerchantParameters');
  }

  get Ds_Signature(): AbstractControl {
    return this.paymentForm.get('Ds_Signature');
  }

  get Ds_SignatureVersion(): AbstractControl {
    return this.paymentForm.get('Ds_SignatureVersion');
  }

  get validatedForm(): boolean {
    return this.paymentForm.valid && !this.submitting;
  }

  private getPaymentRedsysData = (redsysData: RedsysData): void => {
    this.Ds_MerchantParameters.setValue(redsysData.Ds_MerchantParameters);
    this.Ds_Signature.setValue(redsysData.Ds_Signature);
    this.Ds_SignatureVersion.setValue(redsysData.Ds_SignatureVersion);
  }

  public onPaymentRedsys(): void {
    this.submitting = true;
    this.loading.emit();
  }
}
