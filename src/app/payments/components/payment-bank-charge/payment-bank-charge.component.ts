import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import {
  Client,
  ClientService,
  Order,
  OrderStatusEnum,
  PaymentSnackbars
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { PaymentBankChargeDialogComponent } from '../payment-bank-charge-dialog/payment-bank-charge-dialog.component';

@Component({
  selector: 'qaroni-payment-bank-charge',
  templateUrl: './payment-bank-charge.component.html',
  styleUrls: ['./payment-bank-charge.component.scss'],
})
export class PaymentBankChargeComponent implements OnInit, OnDestroy {
  @Input() order: Order;

  @Output() loading: EventEmitter<null> = new EventEmitter();

  private client$: Observable<Client> = this.clientService
    .getClient$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  public disabledButton = true;

  constructor(
    private allApp: AllAppService,
    private clientService: ClientService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.client$.subscribe(this.getClient));
    if (this.order?.clientId) {
      this.clientService.getClient(this.order.clientId);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getClient = (client: Client): void => {
    if (client) {
      this.disabledButton = false;
    }
  }

  public onPaymentBankCharge(): void {
    this.subs.add(
      this.client$.subscribe((client: Client) => {
        if (client) {
          const matDialogConfig = new MatDialogConfig();
          matDialogConfig.width = '500px';
          matDialogConfig.maxWidth = '90vw';
          matDialogConfig.panelClass = 'style-confirm-dialog';
          matDialogConfig.autoFocus = false;
          matDialogConfig.data = [this.order, client];

          const dialog = this.allApp.dialog.open<
            PaymentBankChargeDialogComponent,
            [Order, Client],
            Order
          >(PaymentBankChargeDialogComponent, matDialogConfig);

          dialog.afterClosed().subscribe((order: Order) => {
            if (order) {
              if (order?.status === OrderStatusEnum.BANK_CHARGE) {
                this.allApp.snackbar.open(
                  this.allApp.translate.instant(
                    PaymentSnackbars.successBankCharge.message
                  ),
                  this.allApp.translate.instant(
                    PaymentSnackbars.successBankCharge.closeBtn
                  ),
                  PaymentSnackbars.successBankCharge.config
                );
                this.allApp.router.navigate([
                  '/payments',
                  'bank-charge',
                  'success',
                ]);
              } else {
                this.allApp.snackbar.open(
                  this.allApp.translate.instant(
                    PaymentSnackbars.failureBankCharge.message
                  ),
                  this.allApp.translate.instant(
                    PaymentSnackbars.failureBankCharge.closeBtn
                  ),
                  PaymentSnackbars.failureBankCharge.config
                );
              }
            }
          });
        }
      })
    );
  }
}
