import { Item } from './item';
import { Order } from './order';
import { OrderStatusEnum } from './order-status.enum';

export class OrderUtils {
  public static isCreated(order: Order): boolean {
    if (order) {
      return order.status === OrderStatusEnum.CREATED;
    }
    return false;
  }

  public static isCheckout(order: Order): boolean {
    if (order) {
      return order.status === OrderStatusEnum.CHECKOUT;
    }
    return false;
  }
  public static hasClient(order: Order): boolean {
    if (order && order.clientId) {
      return true;
    }
    return false;
  }

  public static totalItems(order: Order): number {
    let totalItemsOrder = 0;
    if (order && order.items && order.items.length) {
      order.items.forEach((item: Item) => {
        if (item && item.quantity) {
          totalItemsOrder += item.quantity;
        }
      });
    }
    return totalItemsOrder;
  }
}
