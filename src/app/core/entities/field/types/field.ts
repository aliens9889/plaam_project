import { FieldTypeEnum } from './field-type.enum';

export interface Field {
  creationDate: string;
  fieldId: number;
  formId: number;
  label: string;
  lastUpdateDate: string;
  name: string;
  options: FieldOption[];
  position: number;
  required: boolean;
  type: FieldTypeEnum;
}

export interface FieldOption {
  creationDate: string;
  fieldId: number;
  lastUpdateDate: string;
  name: string;
  optionId: number;
  value: string;
}

export interface CreateFieldJSON {
  name: string;
  type: FieldTypeEnum;
}

export interface AddFieldOptionJSON {
  name: string;
  value: string;
}
