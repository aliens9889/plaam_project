import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotUserGuard, UserGuard } from '@qaroni-app/core/guards';
import { ForgotChangePassComponent } from './forgot-change-pass/forgot-change-pass.component';
import { ForgotComponent } from './forgot/forgot.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { SignUpPosterComponent } from './sign-up-poster/sign-up-poster.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ValidateComponent } from './validate/validate.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [NotUserGuard] },
  { path: 'sign-up', component: SignUpComponent, canActivate: [NotUserGuard] },
  {
    path: 'sign-up/poster',
    component: SignUpPosterComponent,
    canActivate: [NotUserGuard],
  },
  { path: 'logout', component: LogoutComponent, canActivate: [UserGuard] },
  { path: 'validate/:userID/:otp', component: ValidateComponent },
  { path: 'forgot', component: ForgotComponent, canActivate: [NotUserGuard] },
  {
    path: 'forgot/:userID/:otp',
    component: ForgotChangePassComponent,
    canActivate: [NotUserGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
