import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AssociationPartnerHttpService {
  constructor(private http: HttpClient) {}

  // ==========================================================================================
  // Licence Company Partners
  // ==========================================================================================

  public getPartners$(queryParams?: Params): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups/companies/profiles`;

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  public getPartner$(
    partnerID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups/companies/profiles/${partnerID}`;

    return this.http.get(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Licence Company Partners Members
  // ==========================================================================================

  public getLicenseCompanyPartnerMembers$(
    userID: number | string,
    partnerID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/partners/${partnerID}/members`;

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  // ==========================================================================================
  // Licence Company Partners Members Inscription
  // ==========================================================================================

  public getLicenseCompanyPartnerMemberInscriptions$(
    userID: number | string,
    partnerID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/partners/${partnerID}/members/inscriptions`;

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  // ==========================================================================================
  // Licence Company Partners Companies
  // ==========================================================================================

  public getLicenseCompanyPartnerCompanies$(
    userID: number | string,
    partnerID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/partners/${partnerID}/companies`;

    return this.http.get(url, { observe: 'response', params: queryParams });
  }

  // ==========================================================================================
  // Licence Company Partners Companies Inscription
  // ==========================================================================================

  public getLicenseCompanyPartnerCompanyInscriptions$(
    userID: number | string,
    partnerID: number | string,
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/partners/${partnerID}/companies/inscriptions`;

    return this.http.get(url, { observe: 'response', params: queryParams });
  }
}
