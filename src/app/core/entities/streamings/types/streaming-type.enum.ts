export enum StreamingTypeEnum {
  CROWDCAST = 'CROWDCAST',
  FACEBOOK = 'FACEBOOK',
  PERISCOPE = 'PERISCOPE',
  RESTREAM = 'RESTREAM',
  SNAPCHAT = 'SNAPCHAT',
  YOUTUBE = 'YOUTUBE',
}

export type StreamingType =
  | 'CROWDCAST'
  | 'FACEBOOK'
  | 'PERISCOPE'
  | 'RESTREAM'
  | 'SNAPCHAT'
  | 'YOUTUBE';

export const StreamingTypeArray = [
  'CROWDCAST',
  'FACEBOOK',
  'PERISCOPE',
  'RESTREAM',
  'SNAPCHAT',
  'YOUTUBE',
];

export const StreamingTypeInfo = [
  {
    crowdcast: {
      name: 'Crowdcast',
    },
  },
  {
    facebook: {
      name: 'Facebook',
    },
  },
  {
    periscope: {
      name: 'Periscope',
    },
  },
  {
    restream: {
      name: 'Restream',
    },
  },
  {
    snapchat: {
      name: 'Snapchat',
    },
  },
  {
    youtube: {
      name: 'YouTube',
    },
  },
];
