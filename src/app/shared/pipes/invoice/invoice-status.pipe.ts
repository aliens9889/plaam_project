import { Pipe, PipeTransform } from '@angular/core';
import {
  InvoiceStatusEnum,
  InvoiceStatusInfo
} from '@qaroni-app/core/entities';

@Pipe({
  name: 'invoiceStatus',
})
export class InvoiceStatusPipe implements PipeTransform {
  transform(value: InvoiceStatusEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of InvoiceStatusInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
