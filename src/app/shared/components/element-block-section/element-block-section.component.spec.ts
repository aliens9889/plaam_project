import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementBlockSectionComponent } from './element-block-section.component';

describe('ElementBlockSectionComponent', () => {
  let component: ElementBlockSectionComponent;
  let fixture: ComponentFixture<ElementBlockSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementBlockSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementBlockSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
