import { LanguageEnum } from '@qaroni-app/core/types';

export interface Shipping {
  creationDate: string;
  description: string;
  language: LanguageEnum;
  lastUpdateDate: string;
  logoUrl: string;
  merchantId: number;
  name: string;
  shippingId: number;
  status: string;
  trackingUrl: string;
  type: string;
}

export interface DeliveriesJson {
  shippingId: number;
  addressId: number;
}
