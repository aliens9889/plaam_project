import { LanguageEnum } from '@qaroni-app/core/types';

export interface UserSubscription {
  creationDate: string;
  description: string;
  imagesURL: string[];
  language: LanguageEnum;
  lastUpdateDate: string;
  merchantId: number;
  name: string;
  subscriptionId: number;
  templateUUID: string;
  UUID: string;
  walletUrl: string;
}
