export interface Location {
  altitude: string;
  latitude: string;
  longitude: string;
  relevantText: string;
}
