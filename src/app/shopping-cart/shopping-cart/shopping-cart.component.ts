import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  Item,
  Order,
  OrderCreatedService,
  OrderUpdateItem,
  OrderUpdateItemEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  public order: Order = null;

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private orderCreatedService: OrderCreatedService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.orderCreatedService.getOrderCreated$().subscribe(this.getOrder)
    );
    this.orderCreatedService.getOrderCreated();
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getOrder = (order: Order): void => {
    this.order = order;
    this.disableLoading();

    if (this.order?.orderId && this.order?.deliveryAmount !== 0) {
      this.orderCreatedService.updateOrderDeliveries(this.order.orderId);
    }
  }

  public onEmptyOrder(order: Order): void {
    this.orderCreatedService.emptyOrderItems(order.orderId);
  }

  public onDeleteItem(item: Item): void {
    this.orderCreatedService.deleteOrderItem(this.order.orderId, item.itemId);
  }

  public onGoPayments(order: Order): void {
    this.enableLoading();
    this.allApp.router.navigate(['/payments', order.orderId, 'shippings']);
  }

  public onUpdateItem(item: Item): void {
    let variantID = item.productId;
    if (item.variantId) {
      variantID = item.variantId;
    }
    const dataJSON: OrderUpdateItem = {
      action: OrderUpdateItemEnum.REPLACE,
      quantity: item.quantity,
    };
    this.orderCreatedService.updateOrderItemFromProductVariant(
      item.productId,
      variantID,
      dataJSON
    );
  }

  public onStartShopping(): void {
    this.allApp.router.navigate(['/']);
  }
}
