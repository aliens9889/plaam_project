import { Injectable } from '@angular/core';
import { ServiceCommonService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { OAuth } from '../types/o-auth';

@Injectable({
  providedIn: 'root'
})
export class OAuthStorageService {
  private oAuth: OAuth;
  private oAuth$ = new Subject<OAuth>();

  private lsKey = 'oAuth';

  constructor(private serviceCommon: ServiceCommonService) {}

  public get(): OAuth {
    if (this.serviceCommon.validateInLocalStorage(this.lsKey)) {
      return JSON.parse(localStorage.getItem(this.lsKey));
    } else {
      return this.oAuth;
    }
  }

  public get$(): Observable<OAuth> {
    return this.oAuth$.asObservable();
  }

  public set(oAuth: OAuth) {
    if (this.serviceCommon.validateLocalStorage) {
      localStorage.setItem(this.lsKey, JSON.stringify(oAuth));
    } else {
      this.oAuth = oAuth;
    }
    this.oAuth$.next(oAuth);
  }

  public reset() {
    if (this.serviceCommon.validateInLocalStorage(this.lsKey)) {
      localStorage.removeItem(this.lsKey);
    } else {
      this.oAuth = undefined;
    }
    this.oAuth$.next(undefined);
  }

  get hasOAuth(): boolean {
    if (this.get() && this.get().access_token) {
      return true;
    }
    return false;
  }

  get getUserID(): number {
    return this.get().userId;
  }
}
