import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';
import { Address } from '../../address';
import {
  LicenseCompany,
  LicenseCompanyContact
} from '../types/license-company';
import { LicenseMemberUserInfo } from '../types/license-member';

@Injectable({
  providedIn: 'root',
})
export class AssociationLicenseHttpService {
  constructor(private http: HttpClient) {}

  public getGroupsCompanies$(
    queryParams?: Params
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/groups/companies`;

    const params: Params = Object.assign({}, queryParams);

    return this.http.get(url, { observe: 'response', params });
  }

  // ==========================================================================================
  // License Member
  // ==========================================================================================

  public getLicenseMember$(
    userID: number | string,
    memberID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public updateLicenseMemberUserInfo$(
    userID: number | string,
    memberID: number | string,
    userInfo: LicenseMemberUserInfo
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}`;

    return this.http.patch(url, userInfo, { observe: 'response' });
  }

  public updateLicenseMemberAddress$(
    userID: number | string,
    memberID: number | string,
    address: Address
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/addresses`;

    return this.http.patch(url, address, { observe: 'response' });
  }

  public uploadLicenseMemberImage$(
    userID: number | string,
    memberID: number | string,
    dataMultipart: FormData
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/images`;

    return this.http.post(url, dataMultipart, { observe: 'response' });
  }

  public deleteLicenseMemberImage$(
    userID: number | string,
    memberID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members/${memberID}/images`;

    return this.http.delete(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Licenses Members
  // ==========================================================================================

  public getLicensesMembers$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/members`;

    return this.http.get(url, { observe: 'response' });
  }

  // ==========================================================================================
  // License Company
  // ==========================================================================================

  public getLicenseCompany$(
    userID: number | string,
    companyID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public updateLicenseCompanyData$(
    userID: number | string,
    companyID: number | string,
    license: LicenseCompany
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}`;

    return this.http.patch(url, license, { observe: 'response' });
  }

  public updateLicenseCompanyAddress$(
    userID: number | string,
    companyID: number | string,
    address: Address
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}/addresses`;

    return this.http.patch(url, address, { observe: 'response' });
  }

  public updateLicenseCompanyContactInfo$(
    userID: number | string,
    companyID: number | string,
    contact: LicenseCompanyContact
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}/contacts`;

    return this.http.patch(url, contact, { observe: 'response' });
  }

  public updateLicenseCompanyContactAddress$(
    userID: number | string,
    companyID: number | string,
    address: Address
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}/contacts/addresses`;

    return this.http.patch(url, address, { observe: 'response' });
  }

  public uploadLicenseCompanyImage$(
    userID: number | string,
    companyID: number | string,
    dataMultipart: FormData
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}/images`;

    return this.http.post(url, dataMultipart, { observe: 'response' });
  }

  public deleteLicenseCompanyImage$(
    userID: number | string,
    companyID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies/${companyID}/images`;

    return this.http.delete(url, { observe: 'response' });
  }

  // ==========================================================================================
  // Licenses Companies
  // ==========================================================================================

  public getLicensesCompanies$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/groups/companies`;

    return this.http.get(url, { observe: 'response' });
  }
}
