import { apiPlaamEnvironment } from 'src/environments/environment';

export const ApiPlaam = {
  baseUrl: apiPlaamEnvironment.hasOwnProperty('baseUrl')
    ? apiPlaamEnvironment.baseUrl
    : null,
  merchantId: apiPlaamEnvironment.hasOwnProperty('merchantId')
    ? apiPlaamEnvironment.merchantId
    : null,
};
