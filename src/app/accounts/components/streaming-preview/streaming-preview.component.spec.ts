import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamingPreviewComponent } from './streaming-preview.component';

describe('StreamingPreviewComponent', () => {
  let component: StreamingPreviewComponent;
  let fixture: ComponentFixture<StreamingPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StreamingPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamingPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
