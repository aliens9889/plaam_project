import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qaroni-manage-item-card',
  templateUrl: './manage-item-card.component.html',
  styleUrls: ['./manage-item-card.component.scss'],
})
export class ManageItemCardComponent implements OnInit {
  @Input() link: string[];

  @Input() href: string;

  @Input() matIcon: string;
  @Input() faIcon: string;

  @Input() title: string;

  @Input() description: string;

  @Input() actionVerb: string;

  constructor() {}

  ngOnInit(): void {}
}
