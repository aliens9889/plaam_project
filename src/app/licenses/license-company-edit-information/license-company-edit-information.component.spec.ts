import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyEditInformationComponent } from './license-company-edit-information.component';

describe('LicenseCompanyEditInformationComponent', () => {
  let component: LicenseCompanyEditInformationComponent;
  let fixture: ComponentFixture<LicenseCompanyEditInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyEditInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyEditInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
