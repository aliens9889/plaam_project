import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Invoice, Order, OrderUtils } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-order-resume',
  templateUrl: './order-resume.component.html',
  styleUrls: ['./order-resume.component.scss'],
})
export class OrderResumeComponent implements OnInit {
  public OrderUtils = OrderUtils;

  @Input() order: Order = null;

  @Output() invoice: EventEmitter<Invoice> = new EventEmitter();

  @Output() ticket: EventEmitter<Order> = new EventEmitter();

  public triggerOrderItems = 2;

  public orderItemsPreview = true;

  constructor() {}

  ngOnInit(): void {}

  public orderItemsExpansionOpened(): void {
    this.orderItemsPreview = false;
  }

  public orderItemsExpansionClosed(): void {
    this.orderItemsPreview = true;
  }

  public onOrderInvoice(invoice: Invoice): void {
    this.invoice.emit(invoice);
  }

  public onOrderTicket(order: Order): void {
    this.ticket.emit(order);
  }
}
