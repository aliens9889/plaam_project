import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseMemberEditAddressComponent } from './license-member-edit-address.component';

describe('LicenseMemberEditAddressComponent', () => {
  let component: LicenseMemberEditAddressComponent;
  let fixture: ComponentFixture<LicenseMemberEditAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseMemberEditAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseMemberEditAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
