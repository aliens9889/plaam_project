import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Address } from '@qaroni-app/core/entities';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserAddressesHttpService {
  constructor(private http: HttpClient) {}

  public getUserAddresses$(
    userID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/addresses`;

    return this.http.get(url, { observe: 'response' });
  }

  public createUserAddress$(
    userID: number | string,
    addressJSON: Address
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/addresses`;

    return this.http.post(url, addressJSON, { observe: 'response' });
  }

  public getUserAddress$(
    userID: number | string,
    addressID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/addresses/${addressID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public updateUserAddress$(
    userID: number | string,
    addressID: number | string,
    addressJSON: Address
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/addresses/${addressID}`;

    return this.http.patch(url, addressJSON, { observe: 'response' });
  }

  public deleteUserAddress$(
    userID: number | string,
    addressID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/addresses/${addressID}`;

    return this.http.delete(url, { observe: 'response' });
  }
}
