import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from '@angular/router';
import { OAuthService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';
import { GuardUserSnackbars } from '../snackbars/guard-user-snackbars.config';

@Injectable({
  providedIn: 'root',
})
export class NotUserGuard implements CanActivate, CanLoad {
  private redirectTo = ['/'];

  constructor(
    private allApp: AllAppService,
    private oAuthService: OAuthService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.oAuthService.hasOAuth) {
      // console.log('NotUserGuard -> canActivate -> return true');
      return true;
    }

    if (
      next.url.toString() === 'login' &&
      next.queryParamMap.has('redirectTo')
    ) {
      this.allApp.router.navigate([next.queryParamMap.get('redirectTo')]);
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(GuardUserSnackbars.failNotUser.message),
        this.allApp.translate.instant(GuardUserSnackbars.failNotUser.closeBtn),
        GuardUserSnackbars.failNotUser.config
      );
      // console.log('NotUserGuard -> canActivate -> return false');
      this.allApp.router.navigate(this.redirectTo);
    }

    return false;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('route', route);
    console.log('segments', segments);
    if (!this.oAuthService.hasOAuth) {
      // console.log('NotUserGuard -> canLoad -> return true');
      return true;
    }
    this.allApp.snackbar.open(
      this.allApp.translate.instant(GuardUserSnackbars.failNotUser.message),
      this.allApp.translate.instant(GuardUserSnackbars.failNotUser.closeBtn),
      GuardUserSnackbars.failNotUser.config
    );
    // console.log('NotUserGuard -> canLoad -> return false');
    this.allApp.router.navigate(this.redirectTo);
    return false;
  }
}
