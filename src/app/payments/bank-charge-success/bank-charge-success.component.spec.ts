import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankChargeSuccessComponent } from './bank-charge-success.component';

describe('BankChargeSuccessComponent', () => {
  let component: BankChargeSuccessComponent;
  let fixture: ComponentFixture<BankChargeSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankChargeSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankChargeSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
