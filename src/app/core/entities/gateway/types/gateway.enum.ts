export enum GatewayEnum {
  APLAZAME = 'APLAZAME',
  BANK_CHARGE = 'BANK_CHARGE',
  CECABANK = 'CECABANK',
  COFIDIS = 'COFIDIS',
  PAYCOMET = 'PAYCOMET',
  PAYPAL = 'PAYPAL',
  REDSYS = 'REDSYS',
  STRIPE = 'STRIPE',
}

export type GatewayType =
  | 'APLAZAME'
  | 'BANK_CHARGE'
  | 'CECABANK'
  | 'COFIDIS'
  | 'PAYCOMET'
  | 'PAYPAL'
  | 'REDSYS'
  | 'STRIPE';

export const GatewayArray = [
  'APLAZAME',
  'BANK_CHARGE',
  'CECABANK',
  'COFIDIS',
  'PAYCOMET',
  'PAYPAL',
  'REDSYS',
  'STRIPE',
];

export const GatewayInfo = [
  {
    aplazame: {
      name: 'Aplazame',
    },
  },
  {
    bank_charge: {
      name: 'Recibo bancario',
    },
  },
  {
    cecabank: {
      name: 'Cecabank',
    },
  },
  {
    cofidis: {
      name: 'Cofidis',
    },
  },
  {
    paycomet: {
      name: 'Paycomet',
    },
  },
  {
    paypal: {
      name: 'PayPal',
    },
  },
  {
    redsys: {
      name: 'Redsys',
    },
  },
  {
    stripe: {
      name: 'Stripe',
    },
  },
];
