import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@qaroni-app/shared';
import { ApplicationsRoutingModule } from './applications-routing.module';
import { AppsLoginComponent } from './apps-login/apps-login.component';

@NgModule({
  declarations: [AppsLoginComponent],
  imports: [CommonModule, ApplicationsRoutingModule, SharedModule],
})
export class ApplicationsModule {}
