import { Injectable } from '@angular/core';
import { ToolbarConfiguration } from '@qaroni-app/core/types';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToolbarService {
  private backButton$ = new Subject<boolean>();

  private stateBackButton = false;

  private config: ToolbarConfiguration = {
    backUrl: null
  };

  constructor() {}

  public getConfig(): ToolbarConfiguration {
    return this.config;
  }

  public setConfig(config: ToolbarConfiguration): void {
    this.config = config;
  }

  public resetConfig(): void {
    this.config.backUrl = null;
  }

  public hasConfigBackUrl(): boolean {
    if (
      this.config.backUrl !== null &&
      Array.isArray(this.config.backUrl) &&
      this.config.backUrl.length
    ) {
      return true;
    }
    return false;
  }

  public getConfigBackUrl(): string[] {
    return this.config.backUrl;
  }

  public setConfigBackUrl(backUrl: string[]): void {
    this.config.backUrl = backUrl;
  }

  public resetConfigBackUrl(): void {
    this.config.backUrl = null;
  }

  public getBackButton$(): Observable<boolean> {
    return this.backButton$.asObservable();
  }

  public showBackButton(backUrl: string[] = null): void {
    if (backUrl) {
      this.setConfigBackUrl(backUrl);
    }
    this.stateBackButton = true;
    this.backButton$.next(this.stateBackButton);
  }

  public hideBackButton(): void {
    this.resetConfigBackUrl();
    this.stateBackButton = false;
    this.backButton$.next(this.stateBackButton);
  }

  public toggleBackButton(): void {
    this.stateBackButton = !this.stateBackButton;
    this.backButton$.next(this.stateBackButton);
  }
}
