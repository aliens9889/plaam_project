import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterTypeComponent } from './products-filter-type.component';

describe('ProductsFilterTypeComponent', () => {
  let component: ProductsFilterTypeComponent;
  let fixture: ComponentFixture<ProductsFilterTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
