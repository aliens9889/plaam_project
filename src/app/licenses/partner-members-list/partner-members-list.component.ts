import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AssociationPartnerService,
  AssociationSnackbarsService,
  AssociationStringsService,
  LicenseMember
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-partner-members-list',
  templateUrl: './partner-members-list.component.html',
  styleUrls: ['./partner-members-list.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PartnerMembersListComponent implements OnInit {
  private hasLicenseCompanyID: boolean = this.route.snapshot.paramMap.has(
    'licenseCompanyID'
  );
  public licenseCompanyID: string = this.route.snapshot.paramMap.get(
    'licenseCompanyID'
  );

  public members$: Observable<
    LicenseMember[]
  > = this.partnerService.getPartnerMembers$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private partnerService: AssociationPartnerService,
    private partnerStrings: AssociationStringsService,
    private partnerSnackbars: AssociationSnackbarsService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (!this.hasLicenseCompanyID) {
      this.redirect();
    }
    if (!this.partnerService.allowPartnerMember) {
      this.partnerSnackbars.failureAllowedPartnerType();
      this.allApp.router.navigate([
        '/licenses',
        'companies',
        this.licenseCompanyID,
        'details',
      ]);
    }
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  get title(): string {
    return this.partnerStrings.getTextAffiliatedMembers();
  }
}
