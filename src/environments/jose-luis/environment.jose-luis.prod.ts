import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '63',
};

export const appEnvironment = {
  title: 'Jose Luis Joyerías',
  basePath: 'https://joseluisjoyerias.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.ONE,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 12,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: true,
  brands: true,
  attributes: true,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plataforma.staging.plaam.com/subido/empresas/63_20200303195046_g155.png',
  toolbar:
    'https://plataforma.staging.plaam.com/subido/empresas/63_20200303195046_g155.png',
  footer:
    'https://plataforma.staging.plaam.com/subido/empresas/63_20200303195046_g155.png',
};

export const urlsEnvironment = {
  website: 'https://joseluisjoyerias.staging.plaam.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'acliente@joseluisjoyerias.net',
  facebook: 'https://www.facebook.com/JoseLuisJoyerias',
  twitter: 'https://twitter.com/JLuisJoyerias',
  linkedin: 'https://www.linkedin.com/company/24777631/',
  legal: 'https://joseluisjoyerias.staging.plaam.com/#/',
  privacy: 'https://joseluisjoyerias.staging.plaam.com/#/',
  cookies: 'https://joseluisjoyerias.staging.plaam.com/#/',
  purchase: 'https://joseluisjoyerias.staging.plaam.com/#/',
};
