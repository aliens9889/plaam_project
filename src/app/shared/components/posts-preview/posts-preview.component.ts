import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post, PostsService } from '@qaroni-app/core/entities/blogs';

@Component({
  selector: 'qaroni-posts-preview',
  templateUrl: './posts-preview.component.html',
  styleUrls: ['./posts-preview.component.scss'],
})
export class PostsPreviewComponent implements OnInit {
  @Output() queryParamsTag: EventEmitter<Params> = new EventEmitter();
  @Input() post: Post;

  constructor(
    private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {}

  get getUrlTitle(): string {
    if (this.post?.descriptions?.length && this.post?.descriptions[0]?.title) {
      const urlTitle = this.post.descriptions[0].title
        .replace(/  +/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase();
      return urlTitle;
    }
    return '';
  }

  public checkTagInfo(tagName: string): void {
    this.onSearchTag(tagName);
  }

  private onSearchTag(tagName: string): void {
    const queryParamsEmit: Params = { ...this.route.snapshot.queryParams };
    queryParamsEmit.tag = tagName.trim();
    this.queryParamsTag.emit(queryParamsEmit);
  }

  public checkRoute(): boolean {
    if (this.router.url !== '/') {
      return true;
    }
    return false;
  }
}
