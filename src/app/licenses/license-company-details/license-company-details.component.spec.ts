import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseCompanyDetailsComponent } from './license-company-details.component';

describe('LicenseCompanyDetailsComponent', () => {
  let component: LicenseCompanyDetailsComponent;
  let fixture: ComponentFixture<LicenseCompanyDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseCompanyDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseCompanyDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
