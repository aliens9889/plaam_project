export enum LicenseMemberStatusEnum {
  ACTIVE = 'ACTIVE',
  CANCELLED = 'CANCELLED',
  EXPIRED = 'EXPIRED',
  LOCKED = 'LOCKED',
}

export type LicenseMemberStatusType =
  | 'ACTIVE'
  | 'CANCELLED'
  | 'EXPIRED'
  | 'LOCKED';

export const LicenseMemberStatusArray = [
  'ACTIVE',
  'CANCELLED',
  'EXPIRED',
  'LOCKED',
];

export const LicenseMemberStatusInfo = [
  {
    active: {
      name: 'Active',
    },
  },
  {
    cancelled: {
      name: 'Cancelled',
    },
  },
  {
    expired: {
      name: 'Expired',
    },
  },
  {
    locked: {
      name: 'Locked',
    },
  },
];
