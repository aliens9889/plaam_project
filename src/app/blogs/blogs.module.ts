import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@qaroni-app/shared';
import { PostShowComponent } from './post-show/post-show.component';
import { BlogsRoutingModule } from './blogs-routing.module';
import { PostsComponent } from './posts/posts.component';

@NgModule({
  declarations: [PostsComponent, PostShowComponent],
  imports: [SharedModule, BlogsRoutingModule, TranslateModule],
})
export class BlogsModule {}
