import { TestBed } from '@angular/core/testing';

import { OrderPaymentService } from './order-payment.service';

describe('OrderPaymentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderPaymentService = TestBed.inject(OrderPaymentService);
    expect(service).toBeTruthy();
  });
});
