import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsSectionOneComponent } from './products-section-one.component';

describe('ProductsSectionOneComponent', () => {
  let component: ProductsSectionOneComponent;
  let fixture: ComponentFixture<ProductsSectionOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsSectionOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsSectionOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
