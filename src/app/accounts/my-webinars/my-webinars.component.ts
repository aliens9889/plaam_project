import { Component, OnDestroy, OnInit } from '@angular/core';
import { Webinar, WebinarsService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-my-webinars',
  templateUrl: './my-webinars.component.html',
  styleUrls: ['./my-webinars.component.scss'],
})
export class MyWebinarsComponent implements OnInit, OnDestroy {
  public webinars: Webinar[] = [];

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private webinarsService: WebinarsService
  ) {
    this.enableLoading();
    this.allApp.toolbar.showBackButton();
  }

  ngOnInit(): void {
    this.subs.add(
      this.webinarsService.getWebinars$().subscribe(this.getWebinars)
    );
    this.webinarsService.getWebinars();

    this.subs.add(
      this.allApp.language
        .getLanguage$()
        .pipe(shareReplay(1))
        .subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getWebinars = (webinars: Webinar[]): void => {
    this.webinars = webinars;
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.webinarsService.getWebinars();
  }
}
