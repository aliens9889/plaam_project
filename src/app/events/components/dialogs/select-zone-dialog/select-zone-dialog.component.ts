import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Event, EventService, Zone } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-select-zone-dialog',
  templateUrl: './select-zone-dialog.component.html',
  styleUrls: ['./select-zone-dialog.component.scss'],
})
export class SelectZoneDialogComponent implements OnInit {
  public zone: string;

  constructor(
    public dialogRef: MatDialogRef<SelectZoneDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public event: Event,
    private eventService: EventService
  ) {}

  ngOnInit(): void {
    if (!this.hasZones) {
      this.dialogRef.close(false);
    }
  }

  onCancelClick(): void {
    this.dialogRef.close(false);
  }

  get validatedForm(): boolean {
    if (this.hasZones && this.zone) {
      return true;
    }
    return false;
  }

  get hasZones(): boolean {
    return this.eventService.hasZones(this.event);
  }

  public requestRegister(): void {
    if (this.validatedForm) {
      this.dialogRef.close(this.zone);
    }
  }

  public hasCapacity(zone: Zone): boolean {
    return zone?.number < zone?.capacity;
  }
}
