import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'qaroni-page-title-style-one',
  templateUrl: './page-title-style-one.component.html',
  styleUrls: ['./page-title-style-one.component.scss'],
})
export class PageTitleStyleOneComponent implements OnInit {
  @Input() title: string;

  @Input() titleClass: string;

  constructor() {}

  ngOnInit(): void {}
}
