import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerMembersListComponent } from './partner-members-list.component';

describe('PartnerMembersListComponent', () => {
  let component: PartnerMembersListComponent;
  let fixture: ComponentFixture<PartnerMembersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerMembersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerMembersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
