import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterNoveltyOutletComponent } from './products-filter-novelty-outlet.component';

describe('ProductsFilterNoveltyOutletComponent', () => {
  let component: ProductsFilterNoveltyOutletComponent;
  let fixture: ComponentFixture<ProductsFilterNoveltyOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterNoveltyOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterNoveltyOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
