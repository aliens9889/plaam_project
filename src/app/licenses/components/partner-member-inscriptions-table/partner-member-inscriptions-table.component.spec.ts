import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerMemberInscriptionsTableComponent } from './partner-member-inscriptions-table.component';

describe('PartnerMemberInscriptionsTableComponent', () => {
  let component: PartnerMemberInscriptionsTableComponent;
  let fixture: ComponentFixture<PartnerMemberInscriptionsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerMemberInscriptionsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerMemberInscriptionsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
