export interface Variant {
  barcode: number;
  color: string;
  dimensions: VariantDimensions;
  factoryCode: number;
  imagesURL: string[];
  offer: number;
  options: VariantOptions[];
  price: number;
  stock: number;
  stockControl: boolean;
  variantId: number;
}

export interface VariantDimensions {
  depth: number;
  height: number;
  weight: number;
  width: number;
}

export interface VariantOptions {
  id: number;
  value: string;
}
