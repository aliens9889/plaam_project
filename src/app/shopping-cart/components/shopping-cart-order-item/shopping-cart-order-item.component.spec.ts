import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingCartOrderItemComponent } from './shopping-cart-order-item.component';

describe('ShoppingCartOrderItemComponent', () => {
  let component: ShoppingCartOrderItemComponent;
  let fixture: ComponentFixture<ShoppingCartOrderItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingCartOrderItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingCartOrderItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
