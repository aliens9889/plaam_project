import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  UserData,
  UserDataService,
  UserDataUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-my-information',
  templateUrl: './my-information.component.html',
  styleUrls: ['./my-information.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class MyInformationComponent implements OnInit, OnDestroy {
  public userData: UserData = null;

  public UserDataUtils = UserDataUtils;

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private userDataService: UserDataService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.userDataService.getUserData$().subscribe(this.getUserData)
    );
    this.userDataService.getUserData();
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private getUserData = (userData: UserData): void => {
    this.userData = userData;
    this.initLoaded = true;
  }
}
