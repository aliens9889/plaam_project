import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from '@qaroni-app/core/guards';
import { AssembliesComponent } from './assemblies/assemblies.component';
import { AssemblyDetailsComponent } from './assembly-details/assembly-details.component';
import { LicenseCompanyDetailsComponent } from './license-company-details/license-company-details.component';
import { LicenseCompanyEditAddressComponent } from './license-company-edit-address/license-company-edit-address.component';
import { LicenseCompanyEditContactAddressComponent } from './license-company-edit-contact-address/license-company-edit-contact-address.component';
import { LicenseCompanyEditContactInformationComponent } from './license-company-edit-contact-information/license-company-edit-contact-information.component';
import { LicenseCompanyEditInformationComponent } from './license-company-edit-information/license-company-edit-information.component';
import { LicenseMemberDetailsComponent } from './license-member-details/license-member-details.component';
import { LicenseMemberEditAddressComponent } from './license-member-edit-address/license-member-edit-address.component';
import { LicenseMemberEditInformationComponent } from './license-member-edit-information/license-member-edit-information.component';
import { PartnerCompaniesListComponent } from './partner-companies-list/partner-companies-list.component';
import { PartnerCompanyInscriptionsListComponent } from './partner-company-inscriptions-list/partner-company-inscriptions-list.component';
import { PartnerMemberInscriptionsListComponent } from './partner-member-inscriptions-list/partner-member-inscriptions-list.component';
import { PartnerMembersListComponent } from './partner-members-list/partner-members-list.component';

const routes: Routes = [
  {
    path: 'members/:licenseMemberID/details',
    component: LicenseMemberDetailsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'members/:licenseMemberID/edit/information',
    component: LicenseMemberEditInformationComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'members/:licenseMemberID/edit/address',
    component: LicenseMemberEditAddressComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'members/:licenseMemberID/assemblies',
    component: AssembliesComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'members/:licenseMemberID/assemblies/:assemblyID/details',
    component: AssemblyDetailsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/details',
    component: LicenseCompanyDetailsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/edit/information',
    component: LicenseCompanyEditInformationComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/edit/address',
    component: LicenseCompanyEditAddressComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/edit/information/contacts',
    component: LicenseCompanyEditContactInformationComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/edit/address/contacts',
    component: LicenseCompanyEditContactAddressComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/partners/members',
    component: PartnerMembersListComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/partners/members/inscriptions',
    component: PartnerMemberInscriptionsListComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/partners/companies',
    component: PartnerCompaniesListComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'companies/:licenseCompanyID/partners/companies/inscriptions',
    component: PartnerCompanyInscriptionsListComponent,
    canActivate: [UserGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LicensesRoutingModule {}
