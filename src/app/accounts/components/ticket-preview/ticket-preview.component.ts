import { Component, Input, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { Ticket, TicketsService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { SendByEmailJson } from '@qaroni-app/core/types';
import { SendEmailDialogComponent } from '@qaroni-app/shared/components/dialogs';

@Component({
  selector: 'qaroni-ticket-preview',
  templateUrl: './ticket-preview.component.html',
  styleUrls: ['./ticket-preview.component.scss'],
})
export class TicketPreviewComponent implements OnInit {
  @Input() ticket: Ticket = null;

  constructor(
    private allApp: AllAppService,
    public ticketsService: TicketsService
  ) {}

  ngOnInit(): void {}

  public onSendByEmail(): void {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '700px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;

    const dialog = this.allApp.dialog.open(
      SendEmailDialogComponent,
      matDialogConfig
    );

    dialog.afterClosed().subscribe((emailJSON: SendByEmailJson) => {
      if (emailJSON && emailJSON.email) {
        this.ticketsService.sendTicketCardByEmail(
          this.ticket.eventId,
          this.ticket?.ticketUUID,
          emailJSON
        );
      }
    });
  }

  get hasUUID(): boolean {
    if (this.ticket?.ticketUUID) {
      return true;
    }
    return false;
  }

  get hasWalletURL(): boolean {
    if (this.ticket?.walletUrl) {
      return true;
    }
    return false;
  }

  get downloadPDF(): string {
    if (this.hasUUID) {
      return this.ticketsService.getDownloadTicketPDF(
        this.ticket.eventId,
        this.ticket?.ticketUUID
      );
    }
    return '';
  }
}
