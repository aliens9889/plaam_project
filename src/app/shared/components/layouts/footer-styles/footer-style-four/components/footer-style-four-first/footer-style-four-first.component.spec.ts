import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleFourFirstComponent } from './footer-style-four-first.component';

describe('FooterStyleFourFirstComponent', () => {
  let component: FooterStyleFourFirstComponent;
  let fixture: ComponentFixture<FooterStyleFourFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleFourFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleFourFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
