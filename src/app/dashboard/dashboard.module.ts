import { NgModule } from '@angular/core';
import { SharedModule } from '@qaroni-app/shared';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [DashboardComponent],
  imports: [SharedModule, DashboardRoutingModule],
  exports: []
})
export class DashboardModule {}
