import { Address } from '../../address';
import { UserData } from '../../user';
import { Group } from './group';
import { GroupInscriptionFile } from './group-inscription-file';
import { GroupInscriptionMemberStatusEnum } from './group-inscription-member-status.enum';
import { GroupInscriptionTypeEnum } from './group-inscription-type.enum';
import { LicenseMemberUserInfo } from './license-member';

export interface GroupInscriptionMember {
  address: Address;
  creationDate: string;
  files: GroupInscriptionFile[];
  formId: number;
  formJson: any;
  group: Group;
  groupId: number;
  inscriptionId: number;
  lastUpdateDate: string;
  merchantId: number;
  partnerId: number;
  reason: string;
  type: GroupInscriptionTypeEnum;
  status: GroupInscriptionMemberStatusEnum;
  user: UserData;
  userId: number;
  modelId: number;
  clientId: number;
}

export interface InscriptionMemberJson {
  addressJson: Address;
  clubId: number;
  formId: number | string;
  formJson: any;
  partnerId: number;
  type: GroupInscriptionTypeEnum.INSCRIPTION;
  userJson: UserData;
}

export interface RenewalMemberJson {
  addressJson: Address;
  modelId: number;
  partnerId: number;
  type: GroupInscriptionTypeEnum;
  userJson: LicenseMemberUserInfo;
}
