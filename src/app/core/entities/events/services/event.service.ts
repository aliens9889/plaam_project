import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Links } from '../../app';
import { Event } from '../types/event';
import { EventHttpService } from './event-http.service';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  private eventsSubject = new Subject<Event[]>();
  private eventSubject = new Subject<Event>();

  private linksSubject = new Subject<Links>();

  constructor(
    private allApp: AllAppService,
    private eventHttp: EventHttpService,
    private commonsHttp: CommonsHttpService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public getEventImage(event: Event): string {
    if (event.imagesURL?.length && event.imagesURL[0]) {
      return event.imagesURL[0];
    } else {
      return null;
    }
  }

  public isPaid(event: Event): boolean {
    if (event?.isPaid) {
      return true;
    }
    return false;
  }

  public hasZones(event: Event): boolean {
    if (event?.zones?.length >= 1) {
      return true;
    }
    return false;
  }

  public hasProducts(event: Event): boolean {
    if (event?.products?.length >= 1) {
      return true;
    }
    return false;
  }

  public getNextPage(links: Links): number {
    if (links && links.next) {
      const urlArr: string[] = links.next.split('?');
      if (urlArr.length === 2) {
        const page = JSON.parse(
          '{"' +
            decodeURI(urlArr[1].replace(/&/g, '","').replace(/=/g, '":"')) +
            '"}'
        );
        if (page.hasOwnProperty('page')) {
          return parseInt(page.page, 10);
        }
      }
    }

    return null;
  }

  // ==========================================================================================
  // Get Events
  // ==========================================================================================

  public getEvents$(): Observable<Event[]> {
    return this.eventsSubject.asObservable();
  }

  public getEvents(queryParams?: Params): void {
    this.enableLoading();
    this.eventHttp
      .getEvents$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetEvents, this.failureGetEvents);
  }

  private successGetEvents = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const events: Event[] = data.body.result;
      const links: Links = data.body.links;
      this.eventsSubject.next(events);
      this.linksSubject.next(links);
    } else {
      this.eventsSubject.next(null);
      this.linksSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetEvents = (error: HttpErrorResponse): void => {
    this.eventsSubject.next(null);
    this.linksSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Event
  // ==========================================================================================

  public getEvent$(): Observable<Event> {
    return this.eventSubject.asObservable();
  }

  public getEvent(eventID: number | string): void {
    this.enableLoading();
    this.eventHttp
      .getEvent$(eventID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetEvent, this.failureGetEvent);
  }

  private successGetEvent = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const event: Event = data.body.result[0];
      this.eventSubject.next(event);
    } else {
      this.eventSubject.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetEvent = (error: HttpErrorResponse): void => {
    this.eventSubject.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get News Links
  // ==========================================================================================

  public getLinks$(): Observable<Links> {
    return this.linksSubject.asObservable();
  }
}
