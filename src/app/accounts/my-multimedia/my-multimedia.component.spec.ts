import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyMultimediaComponent } from './my-multimedia.component';

describe('MyMultimediaComponent', () => {
  let component: MyMultimediaComponent;
  let fixture: ComponentFixture<MyMultimediaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyMultimediaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyMultimediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
