import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgxGalleryImage, NgxGalleryOptions } from '@kolkov/ngx-gallery';
import { ProductsEnv } from '@qaroni-app/core/config';
import { Product, ProductService } from '@qaroni-app/core/entities';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-product-detail-gallery',
  templateUrl: './product-detail-gallery.component.html',
  styleUrls: ['./product-detail-gallery.component.scss'],
})
export class ProductDetailGalleryComponent implements OnInit, OnDestroy {
  @Input() product$: Observable<Product>;

  public options: NgxGalleryOptions[];
  public images: NgxGalleryImage[];

  private subs: Subscription = new Subscription();

  constructor(public productService: ProductService) {}

  ngOnInit(): void {
    this.subs.add(this.product$.subscribe(this.getProduct));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private getProduct = (product: Product): void => {
    this.images = this.productService.getNgxGalleryImages(product);
    if (this.hasFewImages) {
      this.options = [
        {
          width: '100%',
          height: '500px',
          previewCloseOnClick: true,
          thumbnails: false,
          thumbnailsArrows: false,
          imageArrows: false,
          previewArrows: false,
        },
        // max-width 1199
        {
          breakpoint: 1199,
          height: '500px',
        },
        // max-width 991
        {
          breakpoint: 991,
          height: '500px',
        },
        // max-width 767
        {
          breakpoint: 767,
          height: '500px',
        },
        // max-width 575
        {
          breakpoint: 575,
          height: '450px',
        },
        // max-width 479
        {
          breakpoint: 479,
          height: '400px',
          preview: false,
        },
      ];
    } else {
      this.options = [
        {
          width: '100%',
          height: '600px',
          previewCloseOnClick: true,
          thumbnailsColumns: 4,
          imagePercent: 85,
          thumbnailsPercent: 15,
        },
        // max-width 1199
        {
          breakpoint: 1199,
          height: '600px',
        },
        // max-width 991
        {
          breakpoint: 991,
          height: '600px',
        },
        // max-width 767
        {
          breakpoint: 767,
          height: '600px',
          imagePercent: 80,
          thumbnailsPercent: 20,
        },
        // max-width 575
        {
          breakpoint: 575,
          height: '500px',
        },
        // max-width 479
        {
          breakpoint: 479,
          height: '500px',
          preview: false,
          thumbnailsColumns: 2,
        },
      ];
    }
    this.improvedByAspectRatio();
  }

  get hasFewImages(): boolean {
    if (this.images.length <= 1) {
      return true;
    }
    return false;
  }

  private improvedByAspectRatio(): void {
    if (ProductsEnv.cardImageAspectRatio === '9:16') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '955px';
        }
        if (this.options[1]) {
          this.options[1].height = '795px';
        }
        if (this.options[2]) {
          this.options[2].height = '580px';
        }
        if (this.options[3]) {
          this.options[3].height = '870px';
        }
        if (this.options[4]) {
          this.options[4].height = '845px';
        }
        if (this.options[5]) {
          this.options[5].height = '710px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '1120px';
        }
        if (this.options[1]) {
          this.options[1].height = '935px';
        }
        if (this.options[2]) {
          this.options[2].height = '680px';
        }
        if (this.options[3]) {
          this.options[3].height = '1090px';
        }
        if (this.options[4]) {
          this.options[4].height = '1055px';
        }
        if (this.options[5]) {
          this.options[5].height = '890px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '2:3') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '805px';
        }
        if (this.options[1]) {
          this.options[1].height = '670px';
        }
        if (this.options[2]) {
          this.options[2].height = '490px';
        }
        if (this.options[3]) {
          this.options[3].height = '735px';
        }
        if (this.options[4]) {
          this.options[4].height = '710px';
        }
        if (this.options[5]) {
          this.options[5].height = '600px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '945px';
        }
        if (this.options[1]) {
          this.options[1].height = '790px';
        }
        if (this.options[2]) {
          this.options[2].height = '575px';
        }
        if (this.options[3]) {
          this.options[3].height = '920px';
        }
        if (this.options[4]) {
          this.options[4].height = '890px';
        }
        if (this.options[5]) {
          this.options[5].height = '750px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '3:4') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '715px';
        }
        if (this.options[1]) {
          this.options[1].height = '595px';
        }
        if (this.options[2]) {
          this.options[2].height = '435px';
        }
        if (this.options[3]) {
          this.options[3].height = '650px';
        }
        if (this.options[4]) {
          this.options[4].height = '630px';
        }
        if (this.options[5]) {
          this.options[5].height = '530px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '840px';
        }
        if (this.options[1]) {
          this.options[1].height = '700px';
        }
        if (this.options[2]) {
          this.options[2].height = '510px';
        }
        if (this.options[3]) {
          this.options[3].height = '810px';
        }
        if (this.options[4]) {
          this.options[4].height = '790px';
        }
        if (this.options[5]) {
          this.options[5].height = '660px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '4:5') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '670px';
        }
        if (this.options[1]) {
          this.options[1].height = '560px';
        }
        if (this.options[2]) {
          this.options[2].height = '410px';
        }
        if (this.options[3]) {
          this.options[3].height = '610px';
        }
        if (this.options[4]) {
          this.options[4].height = '590px';
        }
        if (this.options[5]) {
          this.options[5].height = '500px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '790px';
        }
        if (this.options[1]) {
          this.options[1].height = '660px';
        }
        if (this.options[2]) {
          this.options[2].height = '485px';
        }
        if (this.options[3]) {
          this.options[3].height = '760px';
        }
        if (this.options[4]) {
          this.options[4].height = '740px';
        }
        if (this.options[5]) {
          this.options[5].height = '625px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '1:1') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '540px';
        }
        if (this.options[1]) {
          this.options[1].height = '450px';
        }
        if (this.options[2]) {
          this.options[2].height = '330px';
        }
        if (this.options[3]) {
          this.options[3].height = '490px';
        }
        if (this.options[4]) {
          this.options[4].height = '450px';
        }
        if (this.options[5]) {
          this.options[5].height = '420px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '630px';
        }
        if (this.options[1]) {
          this.options[1].height = '525px';
        }
        if (this.options[2]) {
          this.options[2].height = '385px';
        }
        if (this.options[3]) {
          this.options[3].height = '615px';
        }
        if (this.options[4]) {
          this.options[4].height = '550px';
        }
        if (this.options[5]) {
          this.options[5].height = '420px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '5:4') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '430px';
        }
        if (this.options[1]) {
          this.options[1].height = '360px';
        }
        if (this.options[2]) {
          this.options[2].height = '265px';
        }
        if (this.options[3]) {
          this.options[3].height = '395px';
        }
        if (this.options[4]) {
          this.options[4].height = '385px';
        }
        if (this.options[5]) {
          this.options[5].height = '330px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '505px';
        }
        if (this.options[1]) {
          this.options[1].height = '425px';
        }
        if (this.options[2]) {
          this.options[2].height = '315px';
        }
        if (this.options[3]) {
          this.options[3].height = '500px';
        }
        if (this.options[4]) {
          this.options[4].height = '490px';
        }
        if (this.options[5]) {
          this.options[5].height = '415px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '4:3') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '405px';
        }
        if (this.options[1]) {
          this.options[1].height = '340px';
        }
        if (this.options[2]) {
          this.options[2].height = '250px';
        }
        if (this.options[3]) {
          this.options[3].height = '370px';
        }
        if (this.options[4]) {
          this.options[4].height = '360px';
        }
        if (this.options[5]) {
          this.options[5].height = '300px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '480px';
        }
        if (this.options[1]) {
          this.options[1].height = '400px';
        }
        if (this.options[2]) {
          this.options[2].height = '300px';
        }
        if (this.options[3]) {
          this.options[3].height = '470px';
        }
        if (this.options[4]) {
          this.options[4].height = '450px';
        }
        if (this.options[5]) {
          this.options[5].height = '375px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '3:2') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '360px';
        }
        if (this.options[1]) {
          this.options[1].height = '300px';
        }
        if (this.options[2]) {
          this.options[2].height = '220px';
        }
        if (this.options[3]) {
          this.options[3].height = '330px';
        }
        if (this.options[4]) {
          this.options[4].height = '320px';
        }
        if (this.options[5]) {
          this.options[5].height = '280px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '430px';
        }
        if (this.options[1]) {
          this.options[1].height = '360px';
        }
        if (this.options[2]) {
          this.options[2].height = '270px';
        }
        if (this.options[3]) {
          this.options[3].height = '420px';
        }
        if (this.options[4]) {
          this.options[4].height = '410px';
        }
        if (this.options[5]) {
          this.options[5].height = '370px';
        }
      }
    } else if (ProductsEnv.cardImageAspectRatio === '16:9') {
      if (this.hasFewImages) {
        if (this.options[0]) {
          this.options[0].height = '320px';
        }
        if (this.options[1]) {
          this.options[1].height = '275px';
        }
        if (this.options[2]) {
          this.options[2].height = '200px';
        }
        if (this.options[3]) {
          this.options[3].height = '300px';
        }
        if (this.options[4]) {
          this.options[4].height = '280px';
        }
        if (this.options[5]) {
          this.options[5].height = '250px';
        }
      } else {
        if (this.options[0]) {
          this.options[0].height = '390px';
        }
        if (this.options[1]) {
          this.options[1].height = '350px';
        }
        if (this.options[2]) {
          this.options[2].height = '250px';
        }
        if (this.options[3]) {
          this.options[3].height = '390px';
        }
        if (this.options[4]) {
          this.options[4].height = '370px';
        }
        if (this.options[5]) {
          this.options[5].height = '325px';
        }
      }
    }
  }
}
