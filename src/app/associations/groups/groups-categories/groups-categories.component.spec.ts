import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsCategoriesComponent } from './groups-categories.component';

describe('GroupsCategoriesComponent', () => {
  let component: GroupsCategoriesComponent;
  let fixture: ComponentFixture<GroupsCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupsCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
