import { Pipe, PipeTransform } from '@angular/core';
import { OrderTypeEnum, OrderTypeInfo } from '@qaroni-app/core/entities';

@Pipe({
  name: 'orderType'
})
export class OrderTypePipe implements PipeTransform {
  transform(value: OrderTypeEnum): string {
    if (value) {
      const key = value.trim().toLowerCase();
      for (const iterator of OrderTypeInfo) {
        if (Object.keys(iterator)[0] === key) {
          return iterator[key].name;
        }
      }
    }
    return null;
  }
}
