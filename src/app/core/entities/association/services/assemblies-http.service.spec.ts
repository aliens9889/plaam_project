import { TestBed } from '@angular/core/testing';
import { AssembliesHttpService } from './assemblies-http.service';

describe('AssembliesHttpService', () => {
  let service: AssembliesHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssembliesHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
