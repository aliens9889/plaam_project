import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ShippingsHttpService {
  constructor(private http: HttpClient) {}

  public getShippings$(): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/merchants/${ApiPlaam.merchantId}/shippings`;

    return this.http.get(url, { observe: 'response' });
  }
}
