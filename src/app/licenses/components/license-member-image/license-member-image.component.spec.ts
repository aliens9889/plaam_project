import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseMemberImageComponent } from './license-member-image.component';

describe('LicenseMemberImageComponent', () => {
  let component: LicenseMemberImageComponent;
  let fixture: ComponentFixture<LicenseMemberImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseMemberImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseMemberImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
