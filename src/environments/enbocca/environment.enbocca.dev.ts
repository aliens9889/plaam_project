// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: false,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://staging.api.plaam.com/v1',
  merchantId: '195',
};

export const appEnvironment = {
  title: 'Enbocca',
  basePath: 'https://enbocca.staging.plaam.com/#',
  languagesAllowed: [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: AppTitleStyleEnum.ONE,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: false,
  allowShoppingCart: false,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  brands: AppLandingPageBrandsEnum.ONE,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  products: AppLandingPageProductsEnum.ONE,
  news: false,
  groups: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '5:4',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: false,
  noveltyOutlet: true,
  collections: false,
  brands: true,
  attributes: true,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: false,
  myLicenses: false,
};

export const associationsEnvironment = {
  associationType: null,
  allowsMembers: null,
  allowsCompanies: null,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/enbocca/logo.png',
  toolbar:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/enbocca/logo.png',
  footer:
    'https://plaam.s3.eu-central-1.amazonaws.com/despliegues/enbocca/logo.png',
};

export const urlsEnvironment = {
  website: 'https://enbocca.staging.plaam.com/#/',
  footerCredits: 'https://www.plaam.com/',
  email: 'info@enbocca.com',
  facebook: 'https://www.facebook.com/',
  twitter: 'http://twitter.com/',
  instagram: 'https://www.instagram.com/',
  privacy:
    'https://enbocca.staging.plaam.com/#/pages/861/política-de-privacidad/',
  tsAndCs:
    'https://enbocca.staging.plaam.com/#/pages/860/términos-y-condiciones/',
  purchase:
    'https://enbocca.staging.plaam.com/#/pages/862/condiciones-de-compra/',
};
