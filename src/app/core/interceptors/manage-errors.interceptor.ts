import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ErrorsHttpComponent } from '@qaroni-app/shared/components/dialogs';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ManageErrorsInterceptor implements HttpInterceptor {
  private disabledRoutes = [
    '/checkouts/',
    '/codes',
    '/deliveries',
    '/emails',
    '/orders/products/',
    '/passwords',
    '/validates',
    '/users',
  ];

  private disabledRegExpRoutes: RegExp[] = [
    // '/passwords',
    /\/users\/registers/i,
    /\/users\/logins/i,
    /\/users\/forgots/i,
    /\/users\/\d+\/validates/i,
    /\/users\/\d+\/events\/\d+\/tickets/i,
  ];

  constructor(private dialog: MatDialog) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let errorInfo: HttpErrorResponse = null;

    return next.handle(req).pipe(
      tap(
        () => {},
        (error) => {
          if (error instanceof HttpErrorResponse) {
            errorInfo = error;
          }
        }
      ),
      finalize(() => {
        if (errorInfo !== null) {
          console.error('INTERCEPTOR', errorInfo);

          if (
            !this.isTokenInvalidError(errorInfo.status) &&
            !this.urlBelongsToDisabledRoutes(req.url) &&
            !this.urlBelongsToDisabledRegExpRoutes(req.url)
          ) {
            const matDialogConfig = new MatDialogConfig();
            matDialogConfig.data = errorInfo;
            matDialogConfig.width = '700px';
            matDialogConfig.maxWidth = '90vw';
            matDialogConfig.panelClass = 'style-confirm-dialog';
            matDialogConfig.autoFocus = false;
            matDialogConfig.disableClose = true;

            this.dialog.open(ErrorsHttpComponent, matDialogConfig);
          }
        }
      })
    );
  }

  private isTokenInvalidError(status: number): boolean {
    if (status === 401 || status === 403) {
      return true;
    } else {
      return false;
    }
  }

  private urlBelongsToDisabledRoutes(url: string): boolean {
    let belong = false;

    for (const substringRoute of this.disabledRoutes) {
      if (url.includes(substringRoute)) {
        belong = true;
        break;
      }
    }

    return belong;
  }

  private urlBelongsToDisabledRegExpRoutes(url: string): boolean {
    let belong = false;

    for (const regExpRoute of this.disabledRegExpRoutes) {
      if (regExpRoute.test(url)) {
        belong = true;
        break;
      }
    }

    return belong;
  }
}
