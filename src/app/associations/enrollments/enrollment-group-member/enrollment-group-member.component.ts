import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DynamicFormComponent } from '@qaroni-app/associations/components/dynamic-form/dynamic-form.component';
import { UserAddressFormComponent } from '@qaroni-app/associations/components/user-address-form/user-address-form.component';
import { UserDataFormComponent } from '@qaroni-app/associations/components/user-data-form/user-data-form.component';
import { UserPartnerFormComponent } from '@qaroni-app/associations/components/user-partner-form/user-partner-form.component';
import { AssociationEnv } from '@qaroni-app/core/config';
import {
  AssociationLicenseService,
  AssociationService,
  AssociationSnackbarsService,
  FormField,
  FormService,
  Group,
  GroupInscriptionMember,
  GroupInscriptionTypeEnum,
  GroupTypeEnum,
  InscriptionMemberJson,
  LicenseCompany
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-enrollment-group-member',
  templateUrl: './enrollment-group-member.component.html',
  styleUrls: ['./enrollment-group-member.component.scss'],
})
export class EnrollmentGroupMemberComponent implements OnInit, OnDestroy {
  private formFields$: Observable<
    FormField[]
  > = this.formService.getFormFields$().pipe(shareReplay(1));
  public formFields: FormField[];

  private group$: Observable<Group> = this.associationService
    .getGroup$()
    .pipe(shareReplay(1));
  public group: Group;

  public inscription$: Observable<
    GroupInscriptionMember
  > = this.associationService.getInscriptionMember$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  public initLoaded = false;

  private submitting = false;

  @ViewChild(UserDataFormComponent)
  userDataComponent: UserDataFormComponent;

  @ViewChild(UserAddressFormComponent)
  userAddressComponent: UserAddressFormComponent;

  @ViewChild(UserPartnerFormComponent)
  userPartnerComponent: UserPartnerFormComponent;

  @ViewChild(DynamicFormComponent)
  dynamicFormComponent: DynamicFormComponent;

  private hasGroupID: boolean = this.route.snapshot.paramMap.has('groupID');
  private groupID: string = this.route.snapshot.paramMap.get('groupID');

  private hasPartnerID: boolean = this.route.snapshot.paramMap.has('partnerID');
  private partnerID: string = this.route.snapshot.paramMap.get('partnerID');

  private licenseCompany$: Observable<
    LicenseCompany
  > = this.licenseService.getLicenseCompany$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private associationService: AssociationService,
    private formService: FormService,
    private licenseService: AssociationLicenseService,
    private associationSnackbars: AssociationSnackbarsService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.hasGroupID && AssociationEnv.allowsMembers === true) {
      this.subs.add(this.group$.subscribe(this.getGroup));
      this.associationService.getGroup(this.groupID);
      this.subs.add(this.inscription$.subscribe(this.getInscription));
      if (this.hasPartnerID) {
        this.subs.add(this.licenseCompany$.subscribe(this.getLicenseCompany));
        this.licenseService.getLicenseCompany(this.partnerID);
      }
    } else {
      this.associationSnackbars.failureAllowMember();
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private redirect(): void {
    if (this.hasPartnerID) {
      this.allApp.router.navigate([
        '/associations',
        'groups',
        'partners',
        this.partnerID,
      ]);
    } else {
      this.allApp.router.navigate(['/associations', 'groups']);
    }
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
  }

  private getGroup = (group: Group): void => {
    if (group?.type === GroupTypeEnum.PERSON) {
      if (this.hasPartnerID && group?.hasPartner !== true) {
        this.associationSnackbars.failureHasPartner();
        this.redirect();
      }
      this.group = group;
      if (this.group?.formId) {
        this.subs.add(this.formFields$.subscribe(this.getFormFields));
        this.formService.getFormFields(this.group?.formId);
      } else {
        this.disableLoading();
      }
    } else {
      this.associationSnackbars.failureEnrollmentType();
      this.redirect();
    }
  }

  private getFormFields = (formFields: FormField[]): void => {
    if (formFields?.length) {
      this.formFields = formFields;
      this.disableLoading();
    }
  }

  get hasPartnerForm(): boolean {
    if (!this.hasPartnerID && this.group?.hasPartner === true) {
      return true;
    }
    return false;
  }

  get hasDynamicForm(): boolean {
    if (this.formFields?.length) {
      return true;
    }
    return false;
  }

  public saveInscription(): void {
    if (this.validatedForm && this.hasGroupID) {
      this.enableLoading();
      this.userDataComponent.prepateFormToSend();
      if (this.hasDynamicForm) {
        this.dynamicFormComponent.prepateFormToSend();
      }

      const createInscription: InscriptionMemberJson = {
        addressJson: this.userAddressComponent.userAddressForm.getRawValue(),
        clubId: null,
        formId: this.group?.formId,
        formJson: this.hasDynamicForm
          ? this.dynamicFormComponent.dynamicForm.getRawValue()
          : null,
        partnerId: this.hasPartnerID
          ? this.partnerID
          : this.hasPartnerForm
          ? this.userPartnerComponent.partnerId.value
          : null,
        type: GroupInscriptionTypeEnum.INSCRIPTION,
        userJson: this.userDataComponent.userDataForm.getRawValue(),
      };

      this.callSaveInscription(createInscription);

      this.userDataComponent.populateUserDataForm(
        this.userDataComponent.userDataForm.getRawValue()
      );
      if (this.hasDynamicForm) {
        this.dynamicFormComponent.populateDynamicForm(
          this.dynamicFormComponent.dynamicForm.getRawValue()
        );
      }
    }
  }

  private callSaveInscription(createInscription: InscriptionMemberJson): void {
    if (this.hasPartnerID) {
      this.subs.add(
        this.licenseCompany$.subscribe((licenseCompany: LicenseCompany) => {
          if (licenseCompany?.isPartner === true) {
            this.associationService.createGroupInscriptionMemberPartner(
              this.groupID,
              this.partnerID,
              createInscription
            );
          } else {
            this.associationSnackbars.failureHasPartner();
            this.redirect();
          }
        })
      );
    } else {
      this.associationService.createGroupInscriptionMember(
        this.groupID,
        createInscription
      );
    }
  }

  private getInscription = (inscription: GroupInscriptionMember): void => {
    if (inscription) {
      this.associationSnackbars.successEnrollmentRequest();
      if (this.hasPartnerID) {
        this.allApp.router.navigate([
          '/licenses',
          'companies',
          this.partnerID,
          'details',
        ]);
      } else {
        this.allApp.router.navigate([
          '/associations',
          'my-enrollment-requests',
        ]);
      }
    }
    this.disableLoading();
  }

  get validatedFormUserData(): boolean {
    if (this.userDataComponent?.userDataForm) {
      return this.userDataComponent.userDataForm.valid;
    }
    return false;
  }

  get validatedFormUserAddress(): boolean {
    if (this.userAddressComponent?.userAddressForm) {
      return this.userAddressComponent.userAddressForm.valid;
    }
    return false;
  }

  get validatedDynamicForm(): boolean {
    if (this.dynamicFormComponent?.dynamicForm) {
      return this.dynamicFormComponent.dynamicForm.valid;
    }
    return false;
  }

  get validatedForm(): boolean {
    if (this.hasDynamicForm) {
      return (
        !this.submitting &&
        this.validatedFormUserData &&
        this.validatedFormUserAddress &&
        this.validatedDynamicForm
      );
    } else {
      return (
        !this.submitting &&
        this.validatedFormUserData &&
        this.validatedFormUserAddress
      );
    }
  }

  get titlePersonalInfo(): string {
    return this.associationService.strings.getStepTitlePersonalInfo();
  }

  get titleAddress(): string {
    return this.associationService.strings.getStepTitleMemberAddress();
  }

  private getLicenseCompany = (licenseCompany: LicenseCompany): void => {
    if (licenseCompany?.isPartner !== true) {
      this.associationSnackbars.failureHasPartner();
      this.redirect();
    }
    this.disableLoading();
  }
}
