export enum PostStatusEnum {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export type PostStatusType = PostStatusEnum.ACTIVE | PostStatusEnum.INACTIVE;

export const PostStatusArray = [PostStatusEnum.ACTIVE, PostStatusEnum.INACTIVE];

export const PostStatusInfo = [
  {
    active: {
      name: 'Activo',
    },
  },
  {
    inactive: {
      name: 'Inactivo',
    },
  },
];
