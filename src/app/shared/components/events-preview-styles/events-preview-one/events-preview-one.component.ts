import { Component, Input, OnInit } from '@angular/core';
import { Event, EventService } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-events-preview-one',
  templateUrl: './events-preview-one.component.html',
  styleUrls: ['./events-preview-one.component.scss'],
})
export class EventsPreviewOneComponent implements OnInit {
  @Input() event: Event;

  constructor(private eventService: EventService) {}

  ngOnInit(): void {}

  get getEventImage(): string {
    return this.eventService.getEventImage(this.event);
  }

  get getUrlTitle(): string {
    if (this.event && this.event?.title) {
      const urlTitle = this.event.title
        .replace(/  +/g, ' ')
        .split(' ')
        .join('-')
        .toLowerCase();
      return urlTitle;
    }
    return '';
  }
}
