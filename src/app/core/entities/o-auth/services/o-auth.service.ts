import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UserSnackbars } from '../../user/snackbars/user-snackbar.config';
import { UserAccount } from '../../user/types/user-account';
import {
  ForgotChangePasswordJson,
  ForgotJson,
  LoginJson,
  OtpJson,
  SignUpJson
} from '../types/authentication';
import { OAuth } from '../types/o-auth';
import { OAuthHttpService } from './o-auth-http.service';
import { OAuthStorageService } from './o-auth-storage.service';

@Injectable({
  providedIn: 'root',
})
export class OAuthService {
  private oAuth$ = new Subject<OAuth>();
  private userAccount$ = new Subject<UserAccount>();

  constructor(
    private allApp: AllAppService,
    private oAuthHttp: OAuthHttpService,
    private commonsHttp: CommonsHttpService,
    public oAuthStorage: OAuthStorageService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get hasOAuth(): boolean {
    return this.oAuthStorage.hasOAuth;
  }

  get getUserID(): number {
    return this.oAuthStorage.getUserID;
  }

  // ==========================================================================================
  // Register OAuth User
  // ==========================================================================================

  public getUserAccount$(): Observable<UserAccount> {
    return this.userAccount$.asObservable();
  }

  public registerUser(signUpJSON: SignUpJson): void {
    this.enableLoading();
    this.oAuthHttp
      .registerUser$(signUpJSON)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successRegisterUser, this.failureRegisterUser);
  }

  private successRegisterUser = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus201(data)) {
      const userAccount: UserAccount = data.body.result[0];
      this.userAccount$.next(userAccount);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successRegisterUser.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successRegisterUser.closeBtn
        ),
        UserSnackbars.successRegisterUser.config
      );
    } else {
      this.userAccount$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureRegisterUser = (error: HttpErrorResponse): void => {
    this.userAccount$.next(null);
    if (
      error.status === 400 &&
      error.error &&
      error.error.errors &&
      error.error.errors.length &&
      error.error.errors[0] &&
      error.error.errors[0].code &&
      error.error.errors[0].code === 'E0006' &&
      error.error.errors[0].title &&
      error.error.errors[0].title === 'The record already exists.'
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.failureRegisterUserExists.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.failureRegisterUserExists.closeBtn
        ),
        UserSnackbars.failureRegisterUserExists.config
      );
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.failureRegisterUser.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.failureRegisterUser.closeBtn
        ),
        UserSnackbars.failureRegisterUser.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Validate User
  // ==========================================================================================

  public validateUser(userID: number | string, otpJSON: OtpJson): void {
    this.enableLoading();
    this.oAuthHttp
      .validate$(userID, otpJSON)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successValidate, this.failureValidate);
  }

  private successValidate = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const oAuth: OAuth = data.body.result[0];
      this.oAuthStorage.set(oAuth);
      this.oAuth$.next(oAuth);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(UserSnackbars.successValidate.message),
        this.allApp.translate.instant(UserSnackbars.successValidate.closeBtn),
        UserSnackbars.successValidate.config
      );
    } else {
      this.oAuth$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureValidate = (error: HttpErrorResponse): void => {
    this.oAuth$.next(null);
    this.allApp.snackbar.open(
      this.allApp.translate.instant(UserSnackbars.failureValidate.message),
      this.allApp.translate.instant(UserSnackbars.failureValidate.closeBtn),
      UserSnackbars.failureValidate.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get OAuth
  // ==========================================================================================

  public getOAuth$(): Observable<OAuth> {
    return this.oAuth$.asObservable();
  }

  public getOAuth(loginJSON: LoginJson): void {
    this.enableLoading();
    this.oAuthHttp
      .getOAuth$(loginJSON)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetOAuth, this.failureGetOAuth);
  }

  private successGetOAuth = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const oAuth: OAuth = data.body.result[0];
      this.oAuthStorage.set(oAuth);
      this.oAuth$.next(oAuth);
    } else {
      this.oAuth$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetOAuth = (error: HttpErrorResponse): void => {
    this.oAuth$.next(null);
    if (
      error.status === 400 &&
      error.error &&
      error.error.errors &&
      error.error.errors.length &&
      error.error.errors[0] &&
      error.error.errors[0].code &&
      error.error.errors[0].code === 'E0004' &&
      error.error.errors[0].detail &&
      error.error.errors[0].detail ===
        'Cuenta no validada. Revisa tu email para validarla'
    ) {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.accountNotValidated.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.accountNotValidated.closeBtn
        ),
        UserSnackbars.accountNotValidated.config
      );
      this.allApp.router.navigate(['/auth', 'sign-up', 'poster']);
    } else {
      this.allApp.snackbar.open(
        this.allApp.translate.instant(UserSnackbars.failureGetOAuth.message),
        this.allApp.translate.instant(UserSnackbars.failureGetOAuth.closeBtn),
        UserSnackbars.failureGetOAuth.config
      );
    }
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Forgot Password
  // ==========================================================================================

  public forgotPassword(forgotJSON: ForgotJson): void {
    this.enableLoading();
    this.oAuthHttp
      .forgotPassword$(forgotJSON)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successForgot, this.failureForgot);
  }

  private successForgot = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const userAccount: UserAccount = data.body.result[0];
      this.userAccount$.next(userAccount);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(UserSnackbars.successForgot.message),
        this.allApp.translate.instant(UserSnackbars.successForgot.closeBtn),
        UserSnackbars.successForgot.config
      );
    } else {
      this.userAccount$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureForgot = (error: HttpErrorResponse): void => {
    this.userAccount$.next(null);
    this.allApp.snackbar.open(
      this.allApp.translate.instant(UserSnackbars.successForgot.message),
      this.allApp.translate.instant(UserSnackbars.successForgot.closeBtn),
      UserSnackbars.successForgot.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Forgot Change Password
  // ==========================================================================================

  public forgotChangePassword(
    userID: number | string,
    forgotChangePasswordJSON: ForgotChangePasswordJson
  ): void {
    this.enableLoading();
    this.oAuthHttp
      .forgotChangePassword$(userID, forgotChangePasswordJSON)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successChangePassword, this.failureChangePassword);
  }

  private successChangePassword = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const userAccount: UserAccount = data.body.result[0];
      this.userAccount$.next(userAccount);
      this.allApp.snackbar.open(
        this.allApp.translate.instant(
          UserSnackbars.successForgotChangePassword.message
        ),
        this.allApp.translate.instant(
          UserSnackbars.successForgotChangePassword.closeBtn
        ),
        UserSnackbars.successForgotChangePassword.config
      );
    } else {
      this.userAccount$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureChangePassword = (error: HttpErrorResponse): void => {
    this.userAccount$.next(null);
    this.allApp.snackbar.open(
      this.allApp.translate.instant(
        UserSnackbars.failureForgotChangePassword.message
      ),
      this.allApp.translate.instant(
        UserSnackbars.failureForgotChangePassword.closeBtn
      ),
      UserSnackbars.failureForgotChangePassword.config
    );
    this.commonsHttp.errorsHttp.communication(error);
  }
}
