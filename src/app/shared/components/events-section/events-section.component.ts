import { Component, OnDestroy, OnInit } from '@angular/core';
import { Params } from '@angular/router';
import { AppLandingPageEventsEnum, LandingEnv } from '@qaroni-app/core/config';
import { Event, EventService, EventStateEnum } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-events-section',
  templateUrl: './events-section.component.html',
  styleUrls: ['./events-section.component.scss'],
})
export class EventsSectionComponent implements OnInit, OnDestroy {
  public events$: Observable<Event[]> = this.eventService
    .getEvents$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  private queryParams: Params = {
    last: 3,
    status: EventStateEnum.ACTIVE,
  };

  constructor(
    private allApp: AllAppService,
    private eventService: EventService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    this.eventService.getEvents(this.queryParams);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    this.eventService.getEvents(this.queryParams);
  }

  get LandingEnv() {
    return LandingEnv;
  }

  get AppLandingPageEventsEnum() {
    return AppLandingPageEventsEnum;
  }
}
