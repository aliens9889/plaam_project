import { Component, Input, OnInit } from '@angular/core';
import { Streaming, StreamingsService, StreamingTypeEnum } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-streaming-preview',
  templateUrl: './streaming-preview.component.html',
  styleUrls: ['./streaming-preview.component.scss']
})
export class StreamingPreviewComponent implements OnInit {
  @Input() streaming: Streaming = null;

  public StreamingTypeEnum = StreamingTypeEnum;

  constructor(public streamingsService: StreamingsService) {}

  ngOnInit(): void {}
}
