import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  AssociationService,
  GroupInscriptionCompany,
  GroupInscriptionCompanyStatusEnum,
  GroupMonthTypeEnum
} from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-request-company-preview',
  templateUrl: './request-company-preview.component.html',
  styleUrls: ['./request-company-preview.component.scss'],
})
export class RequestCompanyPreviewComponent implements OnInit {
  @Input() inscription: GroupInscriptionCompany;

  @Output() payInscription: EventEmitter<
    GroupInscriptionCompany
  > = new EventEmitter();

  constructor(private associationService: AssociationService) {}

  ngOnInit(): void {}

  get hasFiles(): boolean {
    if (this.inscription?.files?.length) {
      return true;
    }
    return false;
  }

  get isAdmitted(): boolean {
    if (this.inscription) {
      return (
        this.inscription.status === GroupInscriptionCompanyStatusEnum.ADMITTED
      );
    }
    return false;
  }

  get hasProductVariantIDs(): boolean {
    if (this.inscription) {
      return this.associationService.hasInscriptionCompanyProductVariantIDs(
        this.inscription
      );
    }
    return false;
  }

  public onPayInscription(): void {
    if (this.inscription) {
      this.payInscription.emit(this.inscription);
    }
  }

  get canManageFile(): boolean {
    if (
      this.inscription?.status ===
        GroupInscriptionCompanyStatusEnum.INCOMPLETE ||
      this.inscription?.status === GroupInscriptionCompanyStatusEnum.PENDING ||
      this.inscription?.status === GroupInscriptionCompanyStatusEnum.REJECTED
    ) {
      return true;
    }
    return false;
  }

  get hasReason(): boolean {
    if (this.inscription?.reason) {
      return true;
    }
    return false;
  }

  public booleanToYesOrNo(status: boolean): string {
    return this.associationService.booleanToYesOrNo(status);
  }

  public timeUnitString(months: number): string {
    if (this.inscription?.group?.monthType === GroupMonthTypeEnum.MONTH) {
      return this.associationService.monthsString(months);
    } else if (
      this.inscription?.group?.monthType === GroupMonthTypeEnum.NATURAL_YEAR
    ) {
      return this.associationService.yearsString(months);
    }
  }

  get alertStatusClass(): string {
    if (this.inscription?.status) {
      switch (this.inscription.status) {
        case GroupInscriptionCompanyStatusEnum.ACTIVE:
        case GroupInscriptionCompanyStatusEnum.ADMITTED:
          return 'alert-success';
        case GroupInscriptionCompanyStatusEnum.BANK_CHARGE:
        case GroupInscriptionCompanyStatusEnum.INCOMPLETE:
        case GroupInscriptionCompanyStatusEnum.PENDING:
        case GroupInscriptionCompanyStatusEnum.REVISION:
          return 'alert-warning';
        case GroupInscriptionCompanyStatusEnum.CANCELLED:
        case GroupInscriptionCompanyStatusEnum.REJECTED:
          return 'alert-danger';

        default:
          return null;
      }
    }
    return null;
  }
}
