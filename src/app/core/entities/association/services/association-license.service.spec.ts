import { TestBed } from '@angular/core/testing';

import { AssociationLicenseService } from './association-license.service';

describe('AssociationLicenseService', () => {
  let service: AssociationLicenseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AssociationLicenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
