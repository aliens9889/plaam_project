export enum GroupMonthTypeEnum {
  MONTH = 'MONTH',
  NATURAL_YEAR = 'NATURAL_YEAR',
}

export type GroupMonthType = 'MONTH' | 'NATURAL_YEAR';

export const GroupMonthTypeArray = ['MONTH', 'NATURAL_YEAR'];

export const GroupMonthTypeInfo = [
  {
    month: {
      name: 'Month',
    },
  },
  {
    natural_year: {
      name: 'Natural year',
    },
  },
];
