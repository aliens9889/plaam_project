import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductsFiltersEnv } from '@qaroni-app/core/config';
import { AllAppService } from '@qaroni-app/core/services';
import { productsFiltersEnvironment } from 'src/environments/environment';

@Component({
  selector: 'qaroni-products-filters',
  templateUrl: './products-filters.component.html',
  styleUrls: ['./products-filters.component.scss'],
})
export class ProductsFiltersComponent implements OnInit {
  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  constructor(private allApp: AllAppService, private route: ActivatedRoute) {}

  ngOnInit(): void {}

  public all(): void {
    this.allApp.router.navigate(['/products']);
  }

  get showResetFilters(): boolean {
    if (Object.keys(this.route.snapshot.queryParams).length) {
      return true;
    }
    return false;
  }

  public onGetQueryParams(queryParamsEmit: Params): void {
    this.queryParamsChange.emit(queryParamsEmit);
  }

  get keysProductsFiltersEnvironment() {
    return Object.keys(productsFiltersEnvironment);
  }

  get hasBrandsFilter(): boolean {
    return ProductsFiltersEnv.brands;
  }

  get hasCategoriesFilter(): boolean {
    return ProductsFiltersEnv.categories;
  }

  get hasSearchFilter(): boolean {
    return ProductsFiltersEnv.search;
  }

  get hasCollectionsFilter(): boolean {
    return ProductsFiltersEnv.collections;
  }

  get hasNoveltyOutletFilter(): boolean {
    return ProductsFiltersEnv.noveltyOutlet;
  }

  get hasPricesFilter(): boolean {
    return ProductsFiltersEnv.prices;
  }

  get hasAttributesFilters(): boolean {
    return ProductsFiltersEnv.attributes;
  }

  get hasTypeFilter(): boolean {
    return ProductsFiltersEnv.type;
  }
}
