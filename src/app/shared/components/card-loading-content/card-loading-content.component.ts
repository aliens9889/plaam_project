import { Component, OnInit } from '@angular/core';
import { ImagesInfo } from '@qaroni-app/core/config';

@Component({
  selector: 'qaroni-card-loading-content',
  templateUrl: './card-loading-content.component.html',
  styleUrls: ['./card-loading-content.component.scss']
})
export class CardLoadingContentComponent implements OnInit {
  public ImagesInfo = ImagesInfo;

  constructor() {}

  ngOnInit(): void {}
}
