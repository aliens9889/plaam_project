export enum FieldTypeEnum {
  CHECKBOX = 'CHECKBOX',
  DATE = 'DATE',
  EMAIL = 'EMAIL',
  FILE = 'FILE',
  NUMBER = 'NUMBER',
  PASSWORD = 'PASSWORD',
  RADIO = 'RADIO',
  SELECT = 'SELECT',
  TEXT = 'TEXT',
  TEXTAREA = 'TEXT_AREA',
}

export type FieldTypeType =
  | 'CHECKBOX'
  | 'DATE'
  | 'EMAIL'
  | 'FILE'
  | 'NUMBER'
  | 'PASSWORD'
  | 'RADIO'
  | 'SELECT'
  | 'TEXT_AREA'
  | 'TEXT';

export const FieldTypeArray = [
  'CHECKBOX',
  'DATE',
  'EMAIL',
  'FILE',
  'NUMBER',
  'PASSWORD',
  'RADIO',
  'SELECT',
  'TEXT_AREA',
  'TEXT',
];

export const FieldTypeInfo = [
  {
    checkbox: {
      name: 'Check',
    },
  },
  {
    date: {
      name: 'Fecha',
    },
  },
  {
    email: {
      name: 'Correo electrónico',
    },
  },
  {
    file: {
      name: 'Archivo adjunto',
    },
  },
  {
    number: {
      name: 'Número',
    },
  },
  {
    password: {
      name: 'Contraseña',
    },
  },
  {
    radio: {
      name: 'Radio',
    },
  },
  {
    select: {
      name: 'Desplegable con opciones',
    },
  },
  {
    text: {
      name: 'Texto',
    },
  },
  {
    text_area: {
      name: 'Texto largo',
    },
  },
];
