import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  Invoice,
  InvoiceService,
  Order,
  OrderService,
  OrderUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qaroni-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
})
export class OrderDetailsComponent implements OnInit, OnDestroy {
  public OrderUtils = OrderUtils;
  public order: Order = null;

  public initLoaded = false;

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderService: OrderService,
    private invoiceService: InvoiceService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(this.orderService.getOrder$().subscribe(this.getOrder));
      this.orderService.getOrder(this.route.snapshot.paramMap.get('orderID'));

      this.subs.add(
        this.invoiceService
          .getOrderTicketUrl$()
          .subscribe(this.getOrderTicketUrl)
      );
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private getOrder = (order: Order): void => {
    this.order = order;
    this.disableLoading();
  }

  public onOrderInvoice(invoice: Invoice): void {
    if (invoice?.userId && invoice?.orderId && invoice?.invoiceId) {
      const url = this.invoiceService.invoiceHttp.getOrderInvoicePDFUrl(
        invoice?.userId,
        invoice?.orderId,
        invoice?.invoiceId
      );
      this.getOrderInvoiceUrl(url);
    }
  }

  private getOrderInvoiceUrl = (invoiceUrl: string): void => {
    if (invoiceUrl) {
      window.location.href = invoiceUrl;
    }
  }

  public onOrderTicket(order: Order): void {
    if (this.order && this.order.orderId) {
      this.invoiceService.getOrderTicket(this.order.orderId);
    }
  }

  private getOrderTicketUrl = (ticketUrl: string): void => {
    if (ticketUrl) {
      window.location.href = ticketUrl;
    }
  }
}
