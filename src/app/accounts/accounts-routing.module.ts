import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from '@qaroni-app/core/guards';
import { AccountPanelComponent } from './account-panel/account-panel.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MyAddressesComponent } from './my-addresses/my-addresses.component';
import { MyInformationEditComponent } from './my-information-edit/my-information-edit.component';
import { MyInformationComponent } from './my-information/my-information.component';
import { MyMultimediaComponent } from './my-multimedia/my-multimedia.component';
import { MyStreamingsComponent } from './my-streamings/my-streamings.component';
import { MySubscriptionsComponent } from './my-subscriptions/my-subscriptions.component';
import { MyTicketsComponent } from './my-tickets/my-tickets.component';
import { MyWebinarsComponent } from './my-webinars/my-webinars.component';

const routes: Routes = [
  { path: '', component: AccountPanelComponent, canActivate: [UserGuard] },
  {
    path: 'my-information',
    component: MyInformationComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-information/edit',
    component: MyInformationEditComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-addresses',
    component: MyAddressesComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-webinars',
    component: MyWebinarsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-tickets',
    component: MyTicketsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-subscriptions',
    component: MySubscriptionsComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-multimedia',
    component: MyMultimediaComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'my-streamings',
    component: MyStreamingsComponent,
    canActivate: [UserGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountsRoutingModule {}
