import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsFilterAttributesComponent } from './products-filter-attributes.component';

describe('ProductsFilterAttributesComponent', () => {
  let component: ProductsFilterAttributesComponent;
  let fixture: ComponentFixture<ProductsFilterAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFilterAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsFilterAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
