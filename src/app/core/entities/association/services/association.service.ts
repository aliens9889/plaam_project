import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Group } from '../types/group';
import { GroupCategory } from '../types/group-category';
import {
  CreateInscriptionCompanyJson,
  GroupInscriptionCompany,
  RenewalInscriptionCompanyJson
} from '../types/group-inscription-company';
import {
  GroupInscriptionMember,
  InscriptionMemberJson,
  RenewalMemberJson
} from '../types/group-inscription-member';
import { AssociationHttpService } from './association-http.service';
import { AssociationStringsService } from './association-strings.service';

@Injectable({
  providedIn: 'root',
})
export class AssociationService {
  private categories$ = new Subject<GroupCategory[]>();
  private category$ = new Subject<GroupCategory>();
  private groups$ = new Subject<Group[]>();
  private group$ = new Subject<Group>();
  private inscriptionMember$ = new Subject<GroupInscriptionMember>();
  private inscriptionsMembers$ = new Subject<GroupInscriptionMember[]>();
  private inscriptionCompany$ = new Subject<GroupInscriptionCompany>();
  private inscriptionsCompanies$ = new Subject<GroupInscriptionCompany[]>();

  constructor(
    private allApp: AllAppService,
    private associationsHttp: AssociationHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService,
    public strings: AssociationStringsService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  public booleanToYesOrNo(status: boolean): string {
    if (status) {
      return 'Yes';
    } else {
      return 'No';
    }
  }

  public monthsString(months: number): string {
    if (months || months === 0) {
      if (months === 1) {
        return 'Month';
      } else {
        return 'Months';
      }
    } else {
      return null;
    }
  }

  public yearsString(years: number): string {
    if (years || years === 0) {
      if (years === 1) {
        return 'Year';
      } else {
        return 'Years';
      }
    } else {
      return null;
    }
  }

  public hasProductVariantIDs(inscription: GroupInscriptionMember): boolean {
    if (
      inscription &&
      inscription.group &&
      inscription.group.productId &&
      inscription.group.variantId
    ) {
      return true;
    }
    return false;
  }

  public hasInscriptionCompanyProductVariantIDs(
    inscription: GroupInscriptionCompany
  ): boolean {
    if (
      inscription &&
      inscription.group &&
      inscription.group.productId &&
      inscription.group.variantId
    ) {
      return true;
    }
    return false;
  }

  // ==========================================================================================
  // Get Groups Categories
  // ==========================================================================================
  public getCategories$(): Observable<GroupCategory[]> {
    return this.categories$.asObservable();
  }

  public getCategories(queryParams?: Params): void {
    this.enableLoading();
    this.associationsHttp
      .getCategories$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetCategories, this.failureGetCategories);
  }

  private successGetCategories = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const categories: GroupCategory[] = data.body.result;
      this.categories$.next(categories);
    } else {
      this.categories$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetCategories = (error: HttpErrorResponse): void => {
    this.categories$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Groups Category
  // ==========================================================================================
  public getCategory$(): Observable<GroupCategory> {
    return this.category$.asObservable();
  }

  public getCategory(categoryID: number | string): void {
    this.enableLoading();
    this.associationsHttp
      .getCategory$(categoryID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetCategory, this.failureGetCategory);
  }

  private successGetCategory = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const category: GroupCategory = data.body.result[0];
      this.category$.next(category);
    } else {
      this.category$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetCategory = (error: HttpErrorResponse): void => {
    this.category$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Association Groups
  // ==========================================================================================
  public getGroups$(): Observable<Group[]> {
    return this.groups$.asObservable();
  }

  public getGroups(queryParams?: Params): void {
    this.enableLoading();
    this.associationsHttp
      .getGroups$(queryParams)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetGroups, this.failureGetGroups);
  }

  private successGetGroups = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const groups: Group[] = data.body.result;
      this.groups$.next(groups);
    } else {
      this.groups$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetGroups = (error: HttpErrorResponse): void => {
    this.groups$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Association Group
  // ==========================================================================================
  public getGroup$(): Observable<Group> {
    return this.group$.asObservable();
  }

  public getGroup(groupID: number | string): void {
    this.enableLoading();
    this.associationsHttp
      .getGroup$(groupID)
      .pipe(finalize(() => this.disableLoading()))
      .subscribe(this.successGetGroup, this.failureGetGroup);
  }

  private successGetGroup = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const group: Group = data.body.result[0];
      this.group$.next(group);
    } else {
      this.group$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetGroup = (error: HttpErrorResponse): void => {
    this.group$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create Group Inscription Member
  // ==========================================================================================
  public getInscriptionMember$(): Observable<GroupInscriptionMember> {
    return this.inscriptionMember$.asObservable();
  }

  public createGroupInscriptionMember(
    groupID: number | string,
    inscriptionJSON: InscriptionMemberJson | RenewalMemberJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .createGroupInscriptionMember$(
          this.oAuthService.getUserID,
          groupID,
          inscriptionJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successCreateGroupInscriptionMember,
          this.failureCreateGroupInscriptionMember
        );
    }
  }

  private successCreateGroupInscriptionMember = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus201(data)) {
      const inscription: GroupInscriptionMember = data.body.result[0];
      this.inscriptionMember$.next(inscription);
    } else {
      this.inscriptionMember$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureCreateGroupInscriptionMember = (
    error: HttpErrorResponse
  ): void => {
    this.inscriptionMember$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create Group Inscription Member Partner
  // ==========================================================================================
  public createGroupInscriptionMemberPartner(
    groupID: number | string,
    partnerID: number | string,
    inscriptionJSON: InscriptionMemberJson | RenewalMemberJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .createGroupInscriptionMemberPartner$(
          this.oAuthService.getUserID,
          groupID,
          partnerID,
          inscriptionJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successCreateGroupInscriptionMember,
          this.failureCreateGroupInscriptionMember
        );
    }
  }

  // ==========================================================================================
  // Get Inscriptions Members
  // ==========================================================================================
  public getInscriptionsMembers$(): Observable<GroupInscriptionMember[]> {
    return this.inscriptionsMembers$.asObservable();
  }

  public getInscriptionsMembers(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .getInscriptionsMembers$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetInscriptionsMembers,
          this.failureGetInscriptionsMembers
        );
    }
  }

  private successGetInscriptionsMembers = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscriptions: GroupInscriptionMember[] = data.body.result;
      this.inscriptionsMembers$.next(inscriptions);
    } else {
      this.inscriptionsMembers$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetInscriptionsMembers = (error: HttpErrorResponse): void => {
    this.inscriptionsMembers$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Inscription Member
  // ==========================================================================================
  public getInscriptionMember(inscriptionID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .getInscriptionMember$(this.oAuthService.getUserID, inscriptionID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetInscriptionMember,
          this.failureGetInscriptionMember
        );
    }
  }

  private successGetInscriptionMember = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscription: GroupInscriptionMember = data.body.result[0];
      this.inscriptionMember$.next(inscription);
    } else {
      this.inscriptionMember$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetInscriptionMember = (error: HttpErrorResponse): void => {
    this.inscriptionMember$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Update Inscription Member
  // ==========================================================================================
  public updateInscriptionMember(
    inscriptionID: number | string,
    inscriptionJSON: InscriptionMemberJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .updateInscriptionMember$(
          this.oAuthService.getUserID,
          inscriptionID,
          inscriptionJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successUpdateInscriptionMember,
          this.failureUpdateInscriptionMember
        );
    }
  }

  private successUpdateInscriptionMember = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscription: GroupInscriptionMember = data.body.result[0];
      this.inscriptionMember$.next(inscription);
    } else {
      this.inscriptionMember$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureUpdateInscriptionMember = (error: HttpErrorResponse): void => {
    this.inscriptionMember$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Add File to Inscription Member
  // ==========================================================================================
  public addFileToInscriptionMember(
    inscriptionID: number | string,
    dataMultipart: FormData
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .addFileToInscriptionMember$(
          this.oAuthService.getUserID,
          inscriptionID,
          dataMultipart
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successAddFileToInscriptionMember,
          this.failureAddFileToInscriptionMember
        );
    }
  }

  private successAddFileToInscriptionMember = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscription: GroupInscriptionMember = data.body.result[0];
      this.inscriptionMember$.next(inscription);
    } else {
      this.inscriptionMember$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureAddFileToInscriptionMember = (
    error: HttpErrorResponse
  ): void => {
    this.inscriptionMember$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create Group Inscription Company
  // ==========================================================================================
  public getInscriptionCompany$(): Observable<GroupInscriptionCompany> {
    return this.inscriptionCompany$.asObservable();
  }

  public createGroupInscriptionCompany(
    groupID: number | string,
    inscriptionJSON:
      | CreateInscriptionCompanyJson
      | RenewalInscriptionCompanyJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .createGroupInscriptionCompany$(
          this.oAuthService.getUserID,
          groupID,
          inscriptionJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successCreateGroupInscriptionCompany,
          this.failureCreateGroupInscriptionCompany
        );
    }
  }

  private successCreateGroupInscriptionCompany = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus201(data)) {
      const inscription: GroupInscriptionCompany = data.body.result[0];
      this.inscriptionCompany$.next(inscription);
    } else {
      this.inscriptionCompany$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureCreateGroupInscriptionCompany = (
    error: HttpErrorResponse
  ): void => {
    this.inscriptionCompany$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Create Group Inscription Company Partner
  // ==========================================================================================
  public createGroupInscriptionCompanyPartner(
    groupID: number | string,
    partnerID: number | string,
    inscriptionJSON:
      | CreateInscriptionCompanyJson
      | RenewalInscriptionCompanyJson
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .createGroupInscriptionCompanyPartner$(
          this.oAuthService.getUserID,
          groupID,
          partnerID,
          inscriptionJSON
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successCreateGroupInscriptionCompany,
          this.failureCreateGroupInscriptionCompany
        );
    }
  }

  // ==========================================================================================
  // Get Inscriptions Companies
  // ==========================================================================================
  public getInscriptionsCompanies$(): Observable<GroupInscriptionCompany[]> {
    return this.inscriptionsCompanies$.asObservable();
  }

  public getInscriptionsCompanies(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .getInscriptionsCompanies$(this.oAuthService.getUserID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetInscriptionsCompanies,
          this.failureGetInscriptionsCompanies
        );
    }
  }

  private successGetInscriptionsCompanies = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscriptions: GroupInscriptionCompany[] = data.body.result;
      this.inscriptionsCompanies$.next(inscriptions);
    } else {
      this.inscriptionsCompanies$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetInscriptionsCompanies = (
    error: HttpErrorResponse
  ): void => {
    this.inscriptionsCompanies$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Get Inscription Company
  // ==========================================================================================
  public getInscriptionCompany(inscriptionID: number | string): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .getInscriptionCompany$(this.oAuthService.getUserID, inscriptionID)
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successGetInscriptionCompany,
          this.failureGetInscriptionCompany
        );
    }
  }

  private successGetInscriptionCompany = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscription: GroupInscriptionCompany = data.body.result[0];
      this.inscriptionCompany$.next(inscription);
    } else {
      this.inscriptionCompany$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetInscriptionCompany = (error: HttpErrorResponse): void => {
    this.inscriptionCompany$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }

  // ==========================================================================================
  // Add File to Inscription Company
  // ==========================================================================================
  public addFileToInscriptionCompany(
    inscriptionID: number | string,
    dataMultipart: FormData
  ): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.associationsHttp
        .addFileToInscriptionCompany$(
          this.oAuthService.getUserID,
          inscriptionID,
          dataMultipart
        )
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(
          this.successAddFileToInscriptionCompany,
          this.failureAddFileToInscriptionCompany
        );
    }
  }

  private successAddFileToInscriptionCompany = (
    data: HttpResponse<any>
  ): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const inscription: GroupInscriptionCompany = data.body.result[0];
      this.inscriptionCompany$.next(inscription);
    } else {
      this.inscriptionCompany$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureAddFileToInscriptionCompany = (
    error: HttpErrorResponse
  ): void => {
    this.inscriptionCompany$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
