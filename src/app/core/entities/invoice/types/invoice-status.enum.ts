export enum InvoiceStatusEnum {
  CANCELLED = 'CANCELLED',
  CREATED = 'CREATED',
  PAID = 'PAID',
}

export type InvoiceStatusType = 'CANCELLED' | 'CREATED' | 'PAID';

export const InvoiceStatusArray = ['CANCELLED', 'CREATED', 'PAID'];

export const InvoiceStatusInfo = [
  {
    cancelled: {
      name: 'Cancelada',
    },
  },
  {
    created: {
      name: 'Abierta',
    },
  },
  {
    paid: {
      name: 'Pagada',
    },
  },
];
