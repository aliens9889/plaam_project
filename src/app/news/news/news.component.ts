import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {
  Links,
  News,
  NewsService,
  NewsStatusEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class NewsComponent implements OnInit, OnDestroy {
  public news: News[] = [];

  private news$: Observable<News[]> = this.newsService
    .getNews$()
    .pipe(shareReplay(1));

  private newsLinks$: Observable<Links> = this.newsService
    .getLinks$()
    .pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  private nextPage = 1;
  public searchNews = '';

  constructor(
    private allApp: AllAppService,
    private newsService: NewsService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(this.news$.subscribe(this.getNews));
    this.subs.add(this.newsLinks$.subscribe(this.getLinks));
    this.subs.add(this.route.queryParams.subscribe(this.getQueryParams));
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  get showMoreNews(): boolean {
    if (!this.nextPage || this.nextPage === 1) {
      return false;
    }
    return true;
  }

  public onShowMoreNews(): void {
    const allParams: Params = {
      page: this.nextPage,
      status: NewsStatusEnum.ACTIVE,
    };
    this.callNews(allParams);
  }

  private getLinks = (links: Links): void => {
    this.nextPage = this.newsService.getNextPage(links);
  }

  private getQueryParams = (queryParams: Params) => {
    this.nextPage = 1;
    this.news = [];
    const allParams: Params = {
      page: this.nextPage,
      status: NewsStatusEnum.ACTIVE,
    };
    this.callNews(allParams);
  }

  private callNews(queryParams: Params): void {
    this.enableLoading();
    this.newsService.getNews(queryParams);
  }

  private getNews = (news: News[]): void => {
    this.news.push(...news);
    this.disableLoading();
  }

  private languageChanged = (): void => {
    this.nextPage = 1;
    this.news = [];
    const allParams: Params = {
      page: this.nextPage,
      status: NewsStatusEnum.ACTIVE,
    };
    this.callNews(allParams);
  }

  public onQueryParamsChange(queryParams: Params): void {
    this.allApp.router.navigate(['/news'], { queryParams });
  }
}
