import { LanguageEnum } from '@qaroni-app/core/types';
import {
  appEnvironment,
  associationsEnvironment,
  landingEnvironment,
  myAccountEnvironment,
  productsEnvironment,
  productsFiltersEnvironment,
} from 'src/environments/environment';
import {
  AppFooterStyleEnum,
  AppLandingPageBrandsEnum,
  AppLandingPageProductsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum,
} from './app.env.enum';

export const AppEnv = {
  title: appEnvironment.hasOwnProperty('title') ? appEnvironment.title : null,
  basePath: appEnvironment.hasOwnProperty('basePath')
    ? appEnvironment.basePath
    : null,
  languagesAllowed: appEnvironment.hasOwnProperty('languagesAllowed')
    ? appEnvironment.languagesAllowed
    : [LanguageEnum.es, LanguageEnum.en, LanguageEnum.gl],
  styleTitle: appEnvironment.hasOwnProperty('styleTitle')
    ? appEnvironment.styleTitle
    : AppTitleStyleEnum.ONE,
  footerStyle: appEnvironment.hasOwnProperty('footerStyle')
    ? appEnvironment.footerStyle
    : AppFooterStyleEnum.ONE,
  showPrices: appEnvironment.hasOwnProperty('showPrices')
    ? appEnvironment.showPrices
    : true,
  allowShoppingCart: appEnvironment.hasOwnProperty('allowShoppingCart')
    ? appEnvironment.allowShoppingCart
    : true,
};

export const LandingEnv = {
  showSlideButton: landingEnvironment.hasOwnProperty('showSlideButton')
    ? landingEnvironment.showSlideButton
    : true,
  slides: landingEnvironment.hasOwnProperty('slides')
    ? landingEnvironment.slides
    : true,
  products: landingEnvironment.hasOwnProperty('products')
    ? landingEnvironment.products
    : AppLandingPageProductsEnum.ONE,
  brands: landingEnvironment.hasOwnProperty('brands')
    ? landingEnvironment.brands
    : AppLandingPageBrandsEnum.ONE,
  news: landingEnvironment.hasOwnProperty('news')
    ? landingEnvironment.news
    : true,
  groups: landingEnvironment.hasOwnProperty('groups')
    ? landingEnvironment.groups
    : false,
  events: landingEnvironment.hasOwnProperty('events')
    ? landingEnvironment.events
    : false,
  blocks: landingEnvironment.hasOwnProperty('blocks')
    ? landingEnvironment.blocks
    : false,
  boxes: landingEnvironment.hasOwnProperty('boxes')
    ? landingEnvironment.boxes
    : false,
};

export const ProductsEnv = {
  featuredCount: productsEnvironment.hasOwnProperty('featuredCount')
    ? productsEnvironment.featuredCount
    : null,
  productsPageStyle: productsEnvironment.hasOwnProperty('productsPageStyle')
    ? productsEnvironment.productsPageStyle
    : AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: productsEnvironment.hasOwnProperty(
    'cardImageAspectRatio'
  )
    ? productsEnvironment.cardImageAspectRatio
    : '1:1',
  cardShowPriceFrom: productsEnvironment.hasOwnProperty('cardShowPriceFrom')
    ? productsEnvironment.cardShowPriceFrom
    : true,
  showProductsPreviewSize: productsEnvironment.hasOwnProperty(
    'showProductsPreviewSize'
  )
    ? productsEnvironment.showProductsPreviewSize
    : true,
  stockFewUnitsQuantity: productsEnvironment.hasOwnProperty(
    'stockFewUnitsQuantity'
  )
    ? productsEnvironment.stockFewUnitsQuantity
    : 3,
  showProductReference: productsEnvironment.hasOwnProperty(
    'showProductReference'
  )
    ? productsEnvironment.showProductReference
    : true,
  detailsChangeTitleByCategory: productsEnvironment.hasOwnProperty(
    'detailsChangeTitleByCategory'
  )
    ? productsEnvironment.detailsChangeTitleByCategory
    : false,
};

export const ProductsFiltersEnv = {
  brands: productsFiltersEnvironment.hasOwnProperty('brands')
    ? productsFiltersEnvironment.brands
    : false,
  categories: productsFiltersEnvironment.hasOwnProperty('categories')
    ? productsFiltersEnvironment.categories
    : false,
  search: productsFiltersEnvironment.hasOwnProperty('search')
    ? productsFiltersEnvironment.search
    : false,
  collections: productsFiltersEnvironment.hasOwnProperty('collections')
    ? productsFiltersEnvironment.collections
    : false,
  noveltyOutlet: productsFiltersEnvironment.hasOwnProperty('noveltyOutlet')
    ? productsFiltersEnvironment.noveltyOutlet
    : false,
  prices: productsFiltersEnvironment.hasOwnProperty('prices')
    ? productsFiltersEnvironment.prices
    : false,
  attributes: productsFiltersEnvironment.hasOwnProperty('attributes')
    ? productsFiltersEnvironment.attributes
    : false,
  type: productsFiltersEnvironment.hasOwnProperty('type')
    ? productsFiltersEnvironment.type
    : false,
};

export const MyAccountEnv = {
  myInformation: myAccountEnvironment.hasOwnProperty('myInformation')
    ? myAccountEnvironment.myInformation
    : true,
  myAddresses: myAccountEnvironment.hasOwnProperty('myAddresses')
    ? myAccountEnvironment.myAddresses
    : true,
  myOrders: myAccountEnvironment.hasOwnProperty('myOrders')
    ? myAccountEnvironment.myOrders
    : true,
  myWebinars: myAccountEnvironment.hasOwnProperty('myWebinars')
    ? myAccountEnvironment.myWebinars
    : true,
  myStreamings: myAccountEnvironment.hasOwnProperty('myStreamings')
    ? myAccountEnvironment.myStreamings
    : true,
  myTickets: myAccountEnvironment.hasOwnProperty('myTickets')
    ? myAccountEnvironment.myTickets
    : true,
  mySubscriptions: myAccountEnvironment.hasOwnProperty('mySubscriptions')
    ? myAccountEnvironment.mySubscriptions
    : true,
  myMultimedia: myAccountEnvironment.hasOwnProperty('myMultimedia')
    ? myAccountEnvironment.myMultimedia
    : true,
  myRequests: myAccountEnvironment.hasOwnProperty('myRequests')
    ? myAccountEnvironment.myRequests
    : false,
  myLicenses: myAccountEnvironment.hasOwnProperty('myLicenses')
    ? myAccountEnvironment.myLicenses
    : false,
};

export const AssociationEnv = {
  associationType: associationsEnvironment.hasOwnProperty('associationType')
    ? associationsEnvironment.associationType
    : false,
  allowsMembers: associationsEnvironment.hasOwnProperty('allowsMembers')
    ? associationsEnvironment.allowsMembers
    : false,
  allowsCompanies: associationsEnvironment.hasOwnProperty('allowsCompanies')
    ? associationsEnvironment.allowsCompanies
    : false,
};
