import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AssociationPartnerService,
  CompanyProfile
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-partner-details',
  templateUrl: './partner-details.component.html',
  styleUrls: ['./partner-details.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class PartnerDetailsComponent implements OnInit, OnDestroy {
  public partner$: Observable<
    CompanyProfile
  > = this.partnerService.getPartner$().pipe(shareReplay(1));

  private hasPartnerID: boolean = this.route.snapshot.paramMap.has('partnerID');
  private partnerID: string = this.route.snapshot.paramMap.get('partnerID');

  constructor(
    private allApp: AllAppService,
    private partnerService: AssociationPartnerService,
    private route: ActivatedRoute
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.hasPartnerID) {
      this.partnerService.getPartner(this.partnerID);
    } else {
      this.allApp.router.navigate(['/associations', 'partners']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }
}
