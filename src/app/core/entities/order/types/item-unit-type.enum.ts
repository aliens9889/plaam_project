export enum ItemUnitTypeEnum {
  METRIC = 'METRIC',
  UNIT = 'UNIT',
}

export type ItemUnitType = 'METRIC' | 'UNIT';

export const ItemUnitTypeArray = ['METRIC', 'UNIT'];

export const ItemUnitTypeInfo = [
  {
    metric: {
      name: 'Por medida',
    },
  },
  {
    unit: {
      name: 'Por unidad',
    },
  },
];
