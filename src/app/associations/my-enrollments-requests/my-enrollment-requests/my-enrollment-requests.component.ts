import { Component, OnInit } from '@angular/core';
import { AssociationEnv } from '@qaroni-app/core/config';
import { AllAppService } from '@qaroni-app/core/services';

@Component({
  selector: 'qaroni-my-enrollment-requests',
  templateUrl: './my-enrollment-requests.component.html',
  styleUrls: ['./my-enrollment-requests.component.scss'],
})
export class MyEnrollmentRequestsComponent implements OnInit {
  public AssociationEnv = AssociationEnv;

  constructor(private allApp: AllAppService) {
    this.enableLoading();
  }

  ngOnInit(): void {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }
}
