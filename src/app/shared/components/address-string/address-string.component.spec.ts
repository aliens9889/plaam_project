import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressStringComponent } from './address-string.component';

describe('AddressStringComponent', () => {
  let component: AddressStringComponent;
  let fixture: ComponentFixture<AddressStringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressStringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressStringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
