import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterStyleFourBottomComponent } from './footer-style-four-bottom.component';

describe('FooterStyleFourBottomComponent', () => {
  let component: FooterStyleFourBottomComponent;
  let fixture: ComponentFixture<FooterStyleFourBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterStyleFourBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterStyleFourBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
