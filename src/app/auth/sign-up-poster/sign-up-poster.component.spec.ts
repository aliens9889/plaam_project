import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpPosterComponent } from './sign-up-poster.component';

describe('SignUpPosterComponent', () => {
  let component: SignUpPosterComponent;
  let fixture: ComponentFixture<SignUpPosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpPosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpPosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
