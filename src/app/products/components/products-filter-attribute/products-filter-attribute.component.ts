import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatSelectionListChange } from '@angular/material/list';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductAttribute } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-products-filter-attribute',
  templateUrl: './products-filter-attribute.component.html',
  styleUrls: ['./products-filter-attribute.component.scss'],
})
export class ProductsFilterAttributeComponent implements OnInit {
  @Input() attribute: ProductAttribute;

  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  public showFewOptions = true;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {}

  public isOptionSelected(id: number): boolean {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('attributes')) {
      const attributesArr: string[] = queryParams.attributes.split(',');
      if (attributesArr.indexOf(`${this.attribute.attributeId}`) !== -1) {
        if (queryParams.hasOwnProperty(`${this.attribute.attributeId}`)) {
          const valuesArr: string[] = queryParams[
            this.attribute.attributeId
          ].split(',');
          if (valuesArr.indexOf(`${id}`) !== -1) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public onSelectionChange(list: MatSelectionListChange): void {
    const selectedValues: number[] = [];
    list.source.selectedOptions.selected.forEach((option) => {
      selectedValues.push(option.value);
    });
    const values = selectedValues.join(',');

    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );

    if (queryParamsEmit.hasOwnProperty('attributes')) {
      const attributesArr: string[] = queryParamsEmit.attributes.split(',');
      const index = attributesArr.indexOf(`${this.attribute.attributeId}`);

      if (values) {
        if (index === -1) {
          attributesArr.push(`${this.attribute.attributeId}`);
          queryParamsEmit.attributes = attributesArr.sort().join(',');
        }
      } else {
        if (index !== -1) {
          attributesArr.splice(index, 1);
        }
        if (attributesArr.length && attributesArr[0]) {
          queryParamsEmit.attributes = attributesArr.sort().join(',');
        } else {
          delete queryParamsEmit.attributes;
        }
      }
    }

    if (values) {
      queryParamsEmit[this.attribute.attributeId] = values;
      if (!queryParamsEmit.hasOwnProperty('attributes')) {
        queryParamsEmit.attributes = this.attribute.attributeId;
      }
    } else {
      delete queryParamsEmit[this.attribute.attributeId];
    }

    this.queryParamsChange.emit(queryParamsEmit);
  }

  public onShowAll(): void {
    this.showFewOptions = !this.showFewOptions;
  }
}
