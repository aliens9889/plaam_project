import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  Address,
  AssociationLicenseService,
  LicenseCompany
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { CountryEnum, CountryUtils } from '@qaroni-app/core/types';
import { InputConfig, InputValidation } from '@qaroni-app/core/utils';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-company-edit-contact-address',
  templateUrl: './license-company-edit-contact-address.component.html',
  styleUrls: ['./license-company-edit-contact-address.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseCompanyEditContactAddressComponent
  implements OnInit, OnDestroy {
  private licenseCompanyID: string;

  public initLoaded = false;

  private licenseCompany$: Observable<
    LicenseCompany
  > = this.licenseService.getLicenseCompany$().pipe(shareReplay(1));
  public licenseCompany: LicenseCompany;

  private subs: Subscription = new Subscription();

  public addressForm: FormGroup;
  private addressSkeleton = {
    line1: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    line2: [
      '',
      Validators.compose([
        Validators.maxLength(InputConfig.address.line.maxLength),
      ]),
    ],
    postalCode: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.postalCode.maxLength),
      ]),
    ],
    city: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.city.maxLength),
      ]),
    ],
    stateProvince: [
      '',
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.stateProvince.maxLength),
      ]),
    ],
    country: [
      { value: CountryEnum.DEFAULT, disabled: true },
      Validators.compose([
        Validators.required,
        Validators.maxLength(InputConfig.address.country.maxLength),
      ]),
    ],
  };

  private submitting = false;
  private formSent = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private licenseService: AssociationLicenseService
  ) {
    this.licenseCompanyID = route.snapshot.paramMap.get('licenseCompanyID');
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
    this.createAddressForm();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('licenseCompanyID')) {
      this.subs.add(this.licenseCompany$.subscribe(this.getLicenseCompany));
      this.licenseService.getLicenseCompany(this.licenseCompanyID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
    this.formSent = false;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private createAddressForm(): void {
    this.addressForm = this.fb.group(this.addressSkeleton);
  }

  private populateAddressForm(address: Address): void {
    if (address) {
      this.addressForm.patchValue(address);
    }
  }

  get InputValidation() {
    return InputValidation;
  }

  get CountryUtils() {
    return CountryUtils;
  }

  get line1(): AbstractControl {
    return this.addressForm.get('line1');
  }

  get line2(): AbstractControl {
    return this.addressForm.get('line2');
  }

  get postalCode(): AbstractControl {
    return this.addressForm.get('postalCode');
  }

  get city(): AbstractControl {
    return this.addressForm.get('city');
  }

  get stateProvince(): AbstractControl {
    return this.addressForm.get('stateProvince');
  }

  get country(): AbstractControl {
    return this.addressForm.get('country');
  }

  get validatedForm(): boolean {
    return this.addressForm.dirty && this.addressForm.valid && !this.submitting;
  }

  private getLicenseCompany = (licenseCompany: LicenseCompany): void => {
    if (licenseCompany) {
      if (this.formSent) {
        this.onBack();
      } else {
        this.licenseCompany = licenseCompany;
        if (this.licenseCompany?.contact?.addresses?.length) {
          this.populateAddressForm(this.licenseCompany?.contact?.addresses[0]);
        }
      }
    }
    this.disableLoading();
  }

  public onSubmit(): void {
    if (this.validatedForm) {
      this.enableLoading();
      this.licenseService.updateLicenseCompanyContactAddress(
        this.licenseCompanyID,
        this.addressForm.getRawValue()
      );
      this.formSent = true;
    }
  }

  get titlePage(): string {
    return this.licenseService.strings.getTitleLicenseCompanyEdit();
  }

  public onBack(): void {
    this.allApp.location.back();
  }
}
