import { Field } from '../../field';
import { Form } from './form';

export interface FormField {
  creationDate: string;
  field: Field;
  fieldId: number;
  form: Form;
  formId: number;
  hint: string;
  lastUpdateDate: string;
  line: number;
  merchantId: number;
  position: number;
  required: boolean;
}
