import { Component, OnDestroy, OnInit } from '@angular/core';
import { Params } from '@angular/router';
import { AppLandingPageBoxesEnum, LandingEnv } from '@qaroni-app/core/config';
import {
  ElementBox,
  ElementService,
  ElementStatusEnum,
  ElementTypeEnum
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-element-boxes-section',
  templateUrl: './element-boxes-section.component.html',
  styleUrls: ['./element-boxes-section.component.scss'],
})
export class ElementBoxesSectionComponent implements OnInit, OnDestroy {
  public boxes$: Observable<
    ElementBox[]
  > = this.elementService.getBoxes$().pipe(shareReplay(1));

  private queryParams: Params = {
    type: ElementTypeEnum.BOX,
    status: ElementStatusEnum.ACTIVE,
  };

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private elementService: ElementService
  ) {
    this.enableLoading();
  }

  ngOnInit(): void {
    this.subs.add(
      this.allApp.language.getLanguage$().subscribe(this.languageChanged)
    );
    if (LandingEnv.boxes) {
      this.elementService.getBoxes(this.queryParams);
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  private languageChanged = (): void => {
    this.enableLoading();
    if (LandingEnv.boxes) {
      this.elementService.getBoxes(this.queryParams);
    }
  }

  get LandingEnv() {
    return LandingEnv;
  }

  get AppLandingPageBoxesEnum() {
    return AppLandingPageBoxesEnum;
  }
}
