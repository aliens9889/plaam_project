export enum ClientTypeEnum {
  LEGAL = 'LEGAL',
  PHYSICAL = 'PHYSICAL',
}

export type ClientType = 'LEGAL' | 'PHYSICAL';

export const ClientTypeArray = ['LEGAL', 'PHYSICAL'];

export const ClientTypeInfo = [
  {
    legal: {
      name: 'Legal',
    },
  },
  {
    physical: {
      name: 'Physical',
    },
  },
];
