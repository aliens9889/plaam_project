import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductTypeArray, ProductTypeEnum } from '@qaroni-app/core/entities';

@Component({
  selector: 'qaroni-products-filter-type',
  templateUrl: './products-filter-type.component.html',
  styleUrls: ['./products-filter-type.component.scss'],
})
export class ProductsFilterTypeComponent implements OnInit {
  public ProductTypeArray = ProductTypeArray;

  @Output() queryParamsChange: EventEmitter<Params> = new EventEmitter();

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {}

  public isChecked(type: ProductTypeEnum): boolean {
    const queryParams: Params = this.route.snapshot.queryParams;
    if (queryParams.hasOwnProperty('type')) {
      if (queryParams.type === type) {
        return true;
      }
    }
    return false;
  }

  public onTypeChange(event: MatRadioChange): void {
    const queryParamsEmit: Params = Object.assign(
      {},
      this.route.snapshot.queryParams
    );
    if (event.value) {
      queryParamsEmit.type = event.value;
    }
    this.queryParamsChange.emit(queryParamsEmit);
  }
}
