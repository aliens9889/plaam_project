import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsStyleOneComponent } from './products-style-one.component';

describe('ProductsStyleOneComponent', () => {
  let component: ProductsStyleOneComponent;
  let fixture: ComponentFixture<ProductsStyleOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsStyleOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsStyleOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
