import { TestBed } from '@angular/core/testing';

import { ErrorsHttpService } from './errors-http.service';

describe('ErrorsHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErrorsHttpService = TestBed.inject(ErrorsHttpService);
    expect(service).toBeTruthy();
  });
});
