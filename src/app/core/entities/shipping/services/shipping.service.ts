import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AllAppService, CommonsHttpService } from '@qaroni-app/core/services';
import { Observable, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { OAuthService } from '../../o-auth';
import { Shipping } from '../types/shipping';
import { ShippingsHttpService } from './shippings-http.service';

@Injectable({
  providedIn: 'root',
})
export class ShippingService {
  private shippings$ = new Subject<Shipping[]>();

  constructor(
    private allApp: AllAppService,
    private shippingsHttp: ShippingsHttpService,
    private commonsHttp: CommonsHttpService,
    private oAuthService: OAuthService
  ) {}

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
  }

  // ==========================================================================================
  // Merchant Shippings
  // ==========================================================================================
  public getShippings$(): Observable<Shipping[]> {
    return this.shippings$.asObservable();
  }

  public getShippings(): void {
    if (this.oAuthService.hasOAuth) {
      this.enableLoading();
      this.shippingsHttp
        .getShippings$()
        .pipe(finalize(() => this.disableLoading()))
        .subscribe(this.successGetShippings, this.failureGetShippings);
    }
  }

  private successGetShippings = (data: HttpResponse<any>): void => {
    if (this.commonsHttp.validationsHttp.verifyStatus200(data)) {
      const shippings: Shipping[] = data.body.result;
      this.shippings$.next(shippings);
    } else {
      this.shippings$.next(null);
      this.commonsHttp.errorsHttp.apiInvalidResponse(data);
    }
  }

  private failureGetShippings = (error: HttpErrorResponse): void => {
    this.shippings$.next(null);
    this.commonsHttp.errorsHttp.communication(error);
  }
}
