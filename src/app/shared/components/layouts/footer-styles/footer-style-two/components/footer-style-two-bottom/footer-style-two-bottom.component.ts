import { Component, OnInit } from '@angular/core';
import { AppEnv, UrlsInfo } from '@qaroni-app/core/config';
import * as moment from 'moment';

@Component({
  selector: 'qaroni-footer-style-two-bottom',
  templateUrl: './footer-style-two-bottom.component.html',
  styleUrls: ['./footer-style-two-bottom.component.scss'],
})
export class FooterStyleTwoBottomComponent implements OnInit {
  public year = moment().format('Y');

  constructor() {}

  ngOnInit(): void {}

  get UrlsInfo() {
    return UrlsInfo;
  }

  get AppEnv() {
    return AppEnv;
  }
}
