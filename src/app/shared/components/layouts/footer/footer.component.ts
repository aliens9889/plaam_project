import { Component, OnInit } from '@angular/core';
import { AppEnv, AppFooterStyleEnum } from '@qaroni-app/core/config';

@Component({
  selector: 'qaroni-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  get AppEnv() {
    return AppEnv;
  }

  get AppFooterStyleEnum() {
    return AppFooterStyleEnum;
  }
}
