import { GenderEnum, LanguageEnum } from '@qaroni-app/core/types';
import { DocumentTypeEnum } from './document-type.enum';

export interface UserData {
  birthday: string;
  creationDate: string;
  document: string;
  documentType: DocumentTypeEnum;
  email: string;
  firstName: string;
  gender: GenderEnum;
  language: LanguageEnum;
  lastName: string;
  lastUpdateDate: string;
  merchantId: number;
  phone: string;
  userId: number;
}

export interface ChangePasswordJSON {
  newPassword: string;
  oldPassword: string;
}
