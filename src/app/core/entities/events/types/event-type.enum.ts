export enum EventTypeEnum {
  ONLINE = 'ONLINE',
  BRICK = 'BRICK',
}

export type EventType = 'ONLINE' | 'BRICK';

export const EventTypeArray = ['ONLINE', 'BRICK'];

export const EventTypeInfo = [
  {
    online: {
      name: 'Online',
    },
  },
  {
    brick: {
      name: 'Presencial',
    },
  },
];
