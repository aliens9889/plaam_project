import { TestBed } from '@angular/core/testing';

import { PageHttpService } from './page-http.service';

describe('PageHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PageHttpService = TestBed.inject(PageHttpService);
    expect(service).toBeTruthy();
  });
});
