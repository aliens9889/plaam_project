import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from '@angular/router';
import { OAuthService } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { Observable } from 'rxjs';
import { GuardUserSnackbars } from '../snackbars/guard-user-snackbars.config';

@Injectable({
  providedIn: 'root',
})
export class UserGuard implements CanActivate, CanLoad {
  private redirectTo = ['auth/login'];

  constructor(
    private allApp: AllAppService,
    private oAuthService: OAuthService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.oAuthService.hasOAuth) {
      // console.log('UserGuard -> canActivate -> return true');
      return true;
    }
    this.allApp.snackbar.open(
      this.allApp.translate.instant(GuardUserSnackbars.failUser.message),
      this.allApp.translate.instant(GuardUserSnackbars.failUser.closeBtn),
      GuardUserSnackbars.failUser.config
    );
    // console.log('UserGuard -> canActivate -> return false');
    this.allApp.router.navigate(this.redirectTo);
    return false;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.oAuthService.hasOAuth) {
      // console.log('UserGuard -> canLoad -> return true');
      return true;
    }
    this.allApp.snackbar.open(
      this.allApp.translate.instant(GuardUserSnackbars.failUser.message),
      this.allApp.translate.instant(GuardUserSnackbars.failUser.closeBtn),
      GuardUserSnackbars.failUser.config
    );
    // console.log('UserGuard -> canLoad -> return false');
    this.allApp.router.navigate(this.redirectTo);
    return false;
  }
}
