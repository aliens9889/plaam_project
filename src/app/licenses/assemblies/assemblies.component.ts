import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssembliesService, Assembly } from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-assemblies',
  templateUrl: './assemblies.component.html',
  styleUrls: ['./assemblies.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class AssembliesComponent implements OnInit, OnDestroy {
  private hasLicenseMemberID: boolean = this.route.snapshot.paramMap.has(
    'licenseMemberID'
  );

  private licenseMemberID: string = this.route.snapshot.paramMap.get(
    'licenseMemberID'
  );

  public initLoaded = false;

  public assemblies$: Observable<
    Assembly[]
  > = this.assembliesService.getAssemblies$().pipe(shareReplay(1));

  private subs: Subscription = new Subscription();

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private assembliesService: AssembliesService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.hasLicenseMemberID) {
      this.subs.add(this.assemblies$.subscribe(this.getAssemblies));
      this.assembliesService.getAssemblies(this.licenseMemberID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private getAssemblies = (assemblies: Assembly[]): void => {
    if (assemblies) {
      if (assemblies.length === 1) {
        this.onAssemblyClick(assemblies[0].assemblyId);
      }
    }
    this.disableLoading();
  }

  public onAssemblyClick(assemblyID: number): void {
    if (this.hasLicenseMemberID && assemblyID) {
      this.allApp.router.navigate([
        '/licenses',
        'members',
        this.licenseMemberID,
        'assemblies',
        assemblyID,
        'details',
      ]);
    }
  }
}
