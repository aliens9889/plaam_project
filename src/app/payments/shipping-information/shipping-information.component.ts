import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute } from '@angular/router';
import {
  Address,
  DeliveriesJson,
  Order,
  OrderService,
  OrderSnackbars,
  OrderUtils,
  ProductTypeEnum,
  Shipping,
  ShippingService,
  UserAddressService
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { AddressFormDialogComponent } from '@qaroni-app/shared/components/dialogs';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-shipping-information',
  templateUrl: './shipping-information.component.html',
  styleUrls: ['./shipping-information.component.scss'],
})
export class ShippingInformationComponent implements OnInit, OnDestroy {
  public initLoaded = false;

  public submitting = false;

  private subs: Subscription = new Subscription();

  public addresses: Address[] = [];
  private addresses$: Observable<
    Address[]
  > = this.userAddressService.getUserAddresses$().pipe(shareReplay(1));

  public shippings: Shipping[] = [];
  private shippings$: Observable<
    Shipping[]
  > = this.shippingService.getShippings$().pipe(shareReplay(1));

  public order: Order;
  private order$: Observable<Order> = this.orderService
    .getOrder$()
    .pipe(shareReplay(1));

  public selectedAddress: number = null;
  public selectedShipping: number = null;

  public hasAddressDefault = false;

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private userAddressService: UserAddressService,
    private shippingService: ShippingService,
    private orderService: OrderService
  ) {
    this.allApp.toolbar.showBackButton(['/shopping-cart']);
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(this.addresses$.subscribe(this.getUserAddresses));
      this.userAddressService.getUserAddresses();
      this.subs.add(this.shippings$.subscribe(this.getShippings));
      this.shippingService.getShippings();
      this.subs.add(this.order$.subscribe(this.getOrder));
      this.updateDeliveries();
    } else {
      this.allApp.router.navigate(['/orders']);
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.submitting = false;
    this.initLoaded = true;
  }

  private getUserAddresses = (addresses: Address[]): void => {
    if (addresses && addresses.length) {
      this.addresses = addresses.sort(
        this.userAddressService.sortAddressesDefault
      );
      for (const address of this.addresses) {
        if (this.userAddressService.isAddressDefault(address)) {
          this.hasAddressDefault = true;
          this.selectedAddress = address.addressId;
          break;
        }
      }
    }
  }

  private getShippings = (shippings: Shipping[]): void => {
    if (shippings) {
      this.shippings = shippings;
    }
  }

  private getOrder = (order: Order): void => {
    if (order) {
      if (!OrderUtils.isCreated(order)) {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(OrderSnackbars.mustBeCreated.message),
          this.allApp.translate.instant(OrderSnackbars.mustBeCreated.closeBtn),
          OrderSnackbars.mustBeCreated.config
        );
        this.allApp.router.navigate(['/orders']);
      }
      if (order && order.items && order.items.length) {
        let hasMerchandise = false;
        for (const item of order.items) {
          if (item.type === ProductTypeEnum.MERCHANDISE) {
            hasMerchandise = true;
            break;
          }
        }
        if (hasMerchandise) {
          this.order = order;
        } else {
          this.onBillingOrder();
        }
      }
    } else {
      this.selectedAddress = null;
      this.selectedShipping = null;
    }
    this.disableLoading();
  }

  public onStartShopping(): void {
    this.allApp.router.navigate(['/']);
  }

  get disabledCreateAddressButton(): boolean {
    return this.submitting;
  }

  public createAddress(): void {
    if (!this.disabledCreateAddressButton) {
      this.actionAddress();
    }
  }

  public editAddress(address: Address): void {
    if (!this.disabledCreateAddressButton && address) {
      this.actionAddress(address);
    }
  }

  public actionAddress(address?: Address): void {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.width = '500px';
    matDialogConfig.maxWidth = '90vw';
    matDialogConfig.panelClass = 'style-confirm-dialog';
    matDialogConfig.autoFocus = false;

    if (address) {
      matDialogConfig.data = address;
    }

    const dialog = this.allApp.dialog.open<
      AddressFormDialogComponent,
      Address,
      Address
    >(AddressFormDialogComponent, matDialogConfig);

    dialog.afterClosed().subscribe((addressResponse: Address) => {
      if (addressResponse) {
        this.userAddressService.getUserAddresses();
      }
    });
  }

  public onChangeAddress(event: MatRadioChange): void {
    this.selectedAddress = event.value;
    if (this.selectedShipping) {
      if (!this.selectedAddress) {
        this.selectedShipping = null;
      }
      this.updateDeliveries();
    }
  }

  public onChangeShipping(event: MatRadioChange): void {
    this.selectedShipping = event.value;
    if (this.selectedAddress && this.selectedShipping) {
      this.updateDeliveries();
    }
  }

  get disabledAddress(): boolean {
    return this.submitting;
  }

  get disabledShipping(): boolean {
    if (this.selectedAddress) {
      return false || this.submitting;
    } else {
      return true || this.submitting;
    }
  }

  private updateDeliveries(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.enableLoading();
      const deliveries: DeliveriesJson = {
        addressId: this.selectedAddress,
        shippingId: this.selectedShipping,
      };
      this.orderService.updateOrderDeliveries(
        this.route.snapshot.paramMap.get('orderID'),
        deliveries
      );
    }
  }

  get validatedBillingButton(): boolean {
    if (this.selectedAddress) {
      if (this.selectedShipping) {
        return true || this.submitting;
      } else {
        return false || this.submitting;
      }
    } else {
      return true || this.submitting;
    }
  }

  public onBillingOrder(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.updateDeliveries();
      this.allApp.router.navigate([
        '/payments',
        this.route.snapshot.paramMap.get('orderID'),
        'billing',
      ]);
    }
  }

  public isAddressDefault(address: Address): boolean {
    return this.userAddressService.isAddressDefault(address);
  }
}
