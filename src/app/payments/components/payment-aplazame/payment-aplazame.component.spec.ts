import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentAplazameComponent } from './payment-aplazame.component';

describe('PaymentAplazameComponent', () => {
  let component: PaymentAplazameComponent;
  let fixture: ComponentFixture<PaymentAplazameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentAplazameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentAplazameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
