import { Component, OnInit } from '@angular/core';
import { MyAccountEnv } from '@qaroni-app/core/config';
import { AssociationLicenseService } from '@qaroni-app/core/entities';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';

@Component({
  selector: 'qaroni-account-panel',
  templateUrl: './account-panel.component.html',
  styleUrls: ['./account-panel.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class AccountPanelComponent implements OnInit {
  constructor(private licenseService: AssociationLicenseService) {}

  ngOnInit(): void {}

  get hasMyInformation(): boolean {
    return MyAccountEnv.myInformation;
  }

  get hasMyAddresses(): boolean {
    return MyAccountEnv.myAddresses;
  }

  get hasMyOrders(): boolean {
    return MyAccountEnv.myOrders;
  }

  get hasMyWebinars(): boolean {
    return MyAccountEnv.myWebinars;
  }

  get hasMyStreamings(): boolean {
    return MyAccountEnv.myStreamings;
  }

  get hasMyTickets(): boolean {
    return MyAccountEnv.myTickets;
  }

  get hasMySubscriptions(): boolean {
    return MyAccountEnv.mySubscriptions;
  }

  get hasMyMultimedia(): boolean {
    return MyAccountEnv.myMultimedia;
  }

  get hasMyRequests(): boolean {
    return MyAccountEnv.myRequests;
  }

  get hasMyLicenses(): boolean {
    return MyAccountEnv.myLicenses;
  }

  get titleMyLicenses(): string {
    return this.licenseService.strings.getTitleAccountCardMyLicenses();
  }

  get textMyLicenses(): string {
    return this.licenseService.strings.getTextAccountCardMyLicenses();
  }
}
