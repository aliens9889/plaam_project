import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute } from '@angular/router';
import {
  Client,
  ClientService,
  ClientTypeEnum,
  Order,
  OrderService,
  OrderSnackbars,
  OrderUtils
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData } from '@qaroni-app/core/types';
import { ConfirmationDialogComponent } from '@qaroni-app/shared/components/dialogs';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { BillingCreateClientDialogComponent } from '../components/billing-create-client-dialog/billing-create-client-dialog.component';

@Component({
  selector: 'qaroni-billing-information',
  templateUrl: './billing-information.component.html',
  styleUrls: ['./billing-information.component.scss'],
})
export class BillingInformationComponent implements OnInit, OnDestroy {
  private orderID: string;

  public initLoaded = false;

  public submitting = false;

  private subs: Subscription = new Subscription();

  public order: Order;
  private order$: Observable<Order> = this.orderService
    .getOrder$()
    .pipe(shareReplay(1));

  public clients: Client[] = [];
  private clients$: Observable<
    Client[]
  > = this.clientService.getClients$().pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private orderService: OrderService,
    private clientService: ClientService
  ) {
    this.orderID = route.snapshot.paramMap.get('orderID');
    this.allApp.toolbar.showBackButton([
      '/payments',
      this.orderID,
      'shippings',
    ]);
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('orderID')) {
      this.subs.add(this.order$.subscribe(this.getOrder));
      this.orderService.getOrder(this.orderID);

      this.subs.add(this.clients$.subscribe(this.getClients));
      this.clientService.getClients(this.orderID);
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
    this.submitting = true;
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
    this.submitting = false;
  }

  public onStartShopping(): void {
    this.redirect();
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private getOrder = (order: Order): void => {
    if (order) {
      if (!OrderUtils.isCreated(order)) {
        this.allApp.snackbar.open(
          this.allApp.translate.instant(OrderSnackbars.mustBeCreated.message),
          this.allApp.translate.instant(OrderSnackbars.mustBeCreated.closeBtn),
          OrderSnackbars.mustBeCreated.config
        );
        this.redirect();
      }
      this.order = order;
    }
    this.disableLoading();
  }

  private getClients = (clients: Client[]): void => {
    if (clients) {
      this.clients = clients;
    }
    this.disableLoading();
  }

  get disabledCreateClientButton(): boolean {
    return this.submitting;
  }

  public actionClient(client?: Client): void {
    if (!this.disabledCreateClientButton) {
      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = '750px';
      matDialogConfig.maxWidth = '90vw';
      matDialogConfig.panelClass = 'style-confirm-dialog';
      matDialogConfig.autoFocus = false;

      if (client) {
        matDialogConfig.data = client;
      }

      const dialog = this.allApp.dialog.open<
        BillingCreateClientDialogComponent,
        Client,
        Client[]
      >(BillingCreateClientDialogComponent, matDialogConfig);

      dialog.afterClosed().subscribe((clients: Client[]) => {
        if (clients) {
        }
      });
    }
  }

  public deleteClient(client: Client): void {
    if (!this.submitting) {
      const confirmationData: ConfirmationDialogData = {
        title:
          client?.firstName + ' ' + client?.lastName + ' - ' + client?.document,
        message: this.allApp.translate.instant('Delete billing information'),
        confirmFaIcon: 'far fa-trash-alt',
        confirmText: this.allApp.translate.instant('yes-delete'),
      };

      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = '700px';
      matDialogConfig.maxWidth = '90vw';
      matDialogConfig.panelClass = 'style-confirm-dialog';
      matDialogConfig.autoFocus = false;
      matDialogConfig.data = confirmationData;

      const dialog = this.allApp.dialog.open(
        ConfirmationDialogComponent,
        matDialogConfig
      );

      dialog.afterClosed().subscribe((confirmation) => {
        if (confirmation === true) {
          if (client?.clientId) {
            this.clientService.deleteClient(client?.clientId);
          }
        }
      });
    }
  }

  public onChangeClient(event: MatRadioChange): void {
    if (this.orderID) {
      this.enableLoading();
      this.orderService.associateOrderToClient(this.orderID, event.value);
    }
  }

  get disabledClients(): boolean {
    return this.submitting;
  }

  public isClientSelected(client: Client): boolean {
    if (this.order && client && this.order?.clientId === client?.clientId) {
      return true;
    }
    return false;
  }

  get validatedPaymentMethodsButton(): boolean {
    if (!this.submitting && this.order?.clientId) {
      return true;
    }
    return false;
  }

  public onPaymentMethodsOrder(): void {
    if (this.orderID) {
      this.allApp.router.navigate(['/payments', this.orderID, 'methods']);
    }
  }

  public isLegal(client: Client): boolean {
    if (client?.type === ClientTypeEnum.LEGAL) {
      return true;
    }
    return false;
  }

  public isPhysical(client: Client): boolean {
    if (client?.type === ClientTypeEnum.PHYSICAL) {
      return true;
    }
    return false;
  }
}
