import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiPlaam } from '@qaroni-app/core/config';
import { Observable } from 'rxjs';
import { ClientRequest } from '../types/client-request';

@Injectable({
  providedIn: 'root',
})
export class ClientHttpService {
  constructor(private http: HttpClient) {}

  public getClients$(userID: number | string): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/clients`;

    return this.http.get(url, { observe: 'response' });
  }

  public createClient$(
    userID: number | string,
    clientJSON: ClientRequest
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/clients`;

    return this.http.post(url, clientJSON, { observe: 'response' });
  }

  public editClient$(
    userID: number | string,
    clientID: number | string,
    clientJSON: ClientRequest
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/clients/${clientID}`;

    return this.http.patch(url, clientJSON, { observe: 'response' });
  }

  public deleteClient$(
    userID: number | string,
    clientID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/clients/${clientID}`;

    return this.http.delete(url, { observe: 'response' });
  }

  public getClient$(
    userID: number | string,
    clientID: number | string
  ): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/users/${userID}/clients/${clientID}`;

    return this.http.get(url, { observe: 'response' });
  }

  public getProvinces$(): Observable<HttpResponse<any>> {
    const url = `${ApiPlaam.baseUrl}/temps/provinces`;

    return this.http.get(url, { observe: 'response' });
  }
}
