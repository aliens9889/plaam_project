export interface Address {
  addressId: number;
  alias: string;
  city: string;
  country: string;
  creationDate: string;
  default: boolean;
  lastUpdateDate: string;
  line1: string;
  line2: string;
  postalCode: string;
  stateProvince: string;
  userId: number;
}
