import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventShowComponent } from './event-show/event-show.component';
import { EventsComponent } from './events/events.component';
import { FreeTicketFailureComponent } from './free-ticket-failure/free-ticket-failure.component';
import { FreeTicketSuccessComponent } from './free-ticket-success/free-ticket-success.component';
import { FreeTicketComponent } from './free-ticket/free-ticket.component';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent,
  },
  {
    path: 'free/success',
    component: FreeTicketSuccessComponent,
  },
  {
    path: 'free/failure/capacity',
    component: FreeTicketFailureComponent,
  },
  {
    path: ':eventID',
    component: EventShowComponent,
  },
  {
    path: ':eventID/:eventTitle',
    component: EventShowComponent,
  },
  {
    path: ':eventID/zones/:zoneID/free',
    component: FreeTicketComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EventsRoutingModule {}
