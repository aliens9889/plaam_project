import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import {
  AssociationLicenseService,
  AssociationService,
  GroupInscriptionMember,
  GroupInscriptionTypeEnum,
  LicenseMember,
  LicenseMemberStatusEnum,
  RenewalMemberJson,
} from '@qaroni-app/core/entities';
import { AllAppService } from '@qaroni-app/core/services';
import { ConfirmationDialogData } from '@qaroni-app/core/types';
import { qaroniEnterFadeInTrigger } from '@qaroni-app/shared/animations';
import { ConfirmationDialogComponent } from '@qaroni-app/shared/components/dialogs';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'qaroni-license-member-details',
  templateUrl: './license-member-details.component.html',
  styleUrls: ['./license-member-details.component.scss'],
  animations: [qaroniEnterFadeInTrigger],
})
export class LicenseMemberDetailsComponent implements OnInit, OnDestroy {
  private licenseMemberID: string = this.route.snapshot.paramMap.get(
    'licenseMemberID'
  );

  public initLoaded = false;

  private licenseMember$: Observable<LicenseMember> = this.licenseService
    .getLicenseMember$()
    .pipe(shareReplay(1));
  public licenseMember: LicenseMember;

  private subs: Subscription = new Subscription();

  public GroupInscriptionTypeEnum = GroupInscriptionTypeEnum;

  public inscription$: Observable<GroupInscriptionMember> = this.associationService
    .getInscriptionMember$()
    .pipe(shareReplay(1));

  constructor(
    private allApp: AllAppService,
    private route: ActivatedRoute,
    private licenseService: AssociationLicenseService,
    private associationService: AssociationService
  ) {
    this.allApp.toolbar.showBackButton();
    this.enableLoading();
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('licenseMemberID')) {
      this.subs.add(this.licenseMember$.subscribe(this.getLicenseMember));
      this.licenseService.getLicenseMember(this.licenseMemberID);
      this.subs.add(this.inscription$.subscribe(this.getInscription));
    } else {
      this.redirect();
    }
  }

  ngOnDestroy(): void {
    this.allApp.toolbar.hideBackButton();
    this.subs.unsubscribe();
  }

  private enableLoading(): void {
    this.allApp.progressBar.show();
  }

  private disableLoading(): void {
    this.allApp.progressBar.hide();
    this.initLoaded = true;
  }

  private redirect(): void {
    this.allApp.router.navigate(['/']);
  }

  private getLicenseMember = (licenseMember: LicenseMember): void => {
    if (licenseMember) {
      this.licenseMember = licenseMember;
    }
    this.disableLoading();
  }

  public onEditInformation(): void {
    this.allApp.router.navigate([
      '/licenses',
      'members',
      this.licenseMemberID,
      'edit',
      'information',
    ]);
  }

  public onEditAddress(): void {
    this.allApp.router.navigate([
      '/licenses',
      'members',
      this.licenseMemberID,
      'edit',
      'address',
    ]);
  }

  public onDownloadWallet(): void {
    if (this.licenseMember.walletUrl) {
      window.location.href = this.licenseMember.walletUrl;
    }
  }

  get titleLicenseDetails(): string {
    return this.licenseService.strings.getTitleLicenseMemberDetails();
  }

  get groupHasRenewal(): boolean {
    if (this.licenseMember?.groupInfo?.hasRenewal === true) {
      return true;
    }
    return false;
  }

  get groupHasAlertDays(): boolean {
    if (
      this.licenseMember?.groupInfo?.alertDays &&
      this.licenseMember?.groupInfo?.alertDays > 0
    ) {
      return true;
    }
    return false;
  }

  get showRenew(): boolean {
    if (
      this.groupHasRenewal &&
      this.groupHasAlertDays &&
      this.licenseMember?.status === LicenseMemberStatusEnum.ACTIVE &&
      this.licenseMember?.expirationDate &&
      moment(this.licenseMember?.expirationDate).isValid() &&
      moment().isAfter(
        moment(this.licenseMember?.expirationDate)
          .subtract(this.licenseMember?.groupInfo?.alertDays, 'days')
          .startOf('day')
      ) &&
      moment().isBefore(moment(this.licenseMember?.expirationDate).endOf('day'))
    ) {
      return true;
    }
    return false;
  }

  get showReactivateDates(): boolean {
    if (
      this.groupHasRenewal &&
      this.licenseMember?.expirationDate &&
      moment(this.licenseMember?.expirationDate).isValid() &&
      moment().isAfter(moment(this.licenseMember?.expirationDate))
    ) {
      return true;
    }
    return false;
  }

  get showReactivateStatus(): boolean {
    if (
      this.groupHasRenewal &&
      (this.licenseMember?.status === LicenseMemberStatusEnum.CANCELLED ||
        this.licenseMember?.status === LicenseMemberStatusEnum.EXPIRED)
    ) {
      return true;
    }
    return false;
  }

  public renewReactivateLicenseMember(type: GroupInscriptionTypeEnum): void {
    if (
      this.licenseMember &&
      type &&
      (type === GroupInscriptionTypeEnum.RENEWAL ||
        type === GroupInscriptionTypeEnum.REACTIVATION) &&
      ((type === GroupInscriptionTypeEnum.RENEWAL && this.showRenew) ||
        (type === GroupInscriptionTypeEnum.REACTIVATION &&
          (this.showReactivateDates || this.showReactivateStatus)))
    ) {
      const confirmationData: ConfirmationDialogData = {
        title: this.allApp.translate.instant(`Renew license`),
        message: this.allApp.translate.instant(
          `Create license renewal request`
        ),
        confirmFaIcon: 'fas fa-check',
        confirmText: this.allApp.translate.instant(`Yes, create request`),
      };

      if (type === GroupInscriptionTypeEnum.REACTIVATION) {
        confirmationData.title = this.allApp.translate.instant(
          `Reactivate license`
        );
        confirmationData.message = this.allApp.translate.instant(
          `Create license reactivate request`
        );
      }

      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.width = '700px';
      matDialogConfig.maxWidth = '90vw';
      matDialogConfig.panelClass = 'style-confirm-dialog';
      matDialogConfig.autoFocus = false;
      matDialogConfig.data = confirmationData;

      const dialog = this.allApp.dialog.open<
        ConfirmationDialogComponent,
        ConfirmationDialogData,
        boolean
      >(ConfirmationDialogComponent, matDialogConfig);

      dialog.afterClosed().subscribe((confirmation) => {
        if (confirmation === true) {
          this.enableLoading();
          const createInscription: RenewalMemberJson = {
            addressJson: this.licenseMember?.addresses?.length
              ? this.licenseMember?.addresses[0]
              : null,
            userJson: this.licenseMember?.userInfo,
            modelId: this.licenseMember?.memberId,
            partnerId: this.licenseMember?.partnerId
              ? this.licenseMember.partnerId
              : null,
            type,
          };

          this.associationService.createGroupInscriptionMember(
            this.licenseMember?.groupId,
            createInscription
          );
        }
      });
    }
  }

  private getInscription = (inscription: GroupInscriptionMember): void => {
    if (inscription) {
      this.allApp.router.navigate(['/associations', 'my-enrollment-requests']);
    }
    this.disableLoading();
  }

  public onAssemblies(): void {
    this.allApp.router.navigate([
      '/licenses',
      'members',
      this.licenseMemberID,
      'assemblies',
    ]);
  }
}
