import {
  AppAssociationTypeEnum,
  AppFooterStyleEnum,
  AppLandingPageBlocksEnum,
  AppLandingPageBoxesEnum,
  AppLandingPageGroupsEnum,
  AppProductsPageStyleEnum,
  AppTitleStyleEnum
} from '@qaroni-app/core/config/app/app.env.enum';
import { LanguageEnum } from '@qaroni-app/core/types';

export const environment = {
  production: true,
};

export const apiPlaamEnvironment = {
  baseUrl: 'https://rest.api.plaam.com/v1',
  merchantId: '94',
};

export const appEnvironment = {
  title:
    'Cámara Oficial de Comercio, Industria y Navegación de Santiago de Compostela',
  basePath: 'https://my.camaracompostela.com/#',
  languagesAllowed: [LanguageEnum.gl, LanguageEnum.es],
  styleTitle: AppTitleStyleEnum.MATERIAL,
  footerStyle: AppFooterStyleEnum.TWO,
  showPrices: true,
  allowShoppingCart: true,
};

export const landingEnvironment = {
  showSlideButton: true,
  slides: true,
  news: true,
  blocks: AppLandingPageBlocksEnum.ONE,
  boxes: AppLandingPageBoxesEnum.ONE,
  groups: AppLandingPageGroupsEnum.GROUPS,
  brands: false,
  products: false,
  events: false,
};

export const productsEnvironment = {
  featuredCount: 8,
  productsPageStyle: AppProductsPageStyleEnum.TWO,
  cardImageAspectRatio: '1:1',
  cardShowPriceFrom: true,
  showProductsPreviewSize: true,
  stockFewUnitsQuantity: 3,
  showProductReference: true,
  detailsChangeTitleByCategory: false,
};

export const productsFiltersEnvironment = {
  categories: true,
  search: true,
  prices: true,
  noveltyOutlet: true,
  collections: false,
  brands: false,
  attributes: false,
  type: false,
};

export const myAccountEnvironment = {
  myInformation: true,
  myAddresses: true,
  myOrders: true,
  myWebinars: true,
  myStreamings: true,
  myTickets: true,
  mySubscriptions: true,
  myMultimedia: true,
  myRequests: true,
  myLicenses: true,
};

export const associationsEnvironment = {
  associationType: AppAssociationTypeEnum.CHAMBER_OF_COMMERCE,
  allowsMembers: false,
  allowsCompanies: true,
};

export const GoogleAnalyticsEnvironment = {
  trackingID: null,
};

export const GoogleReCaptchaEnvironment = {
  sitekey: null,
};

export const StripeEnvironment = {
  publicKey: null,
};

export const AplazameEnvironment = {
  scriptSrc: null,
  publicKey: null,
};

export const imagesEnvironment = {
  logo:
    'https://www.camaracompostela.com/assets/base/img/layout/logos/logo-3.png',
  toolbar:
    'https://www.camaracompostela.com/assets/base/img/layout/logos/logo-3.png',
  footer:
    'https://www.camaracompostela.com/assets/base/img/layout/logos/logo-blanco.png',
};

export const urlsEnvironment = {
  website: 'https://my.camaracompostela.com/#/',
  footerCredits: 'https://qaroni.com/',
  email: 'informacion@camaracompostela.com',
  phone: '+34 981 596 800',
  facebook:
    'https://www.facebook.com/pages/Camara-de-Comercio-de-Santiago-de-Compostela/243549438989859',
  twitter: 'https://twitter.com/ccompostela',
  instagram: 'https://www.instagram.com/ccompostela/',
  legal: 'https://my.camaracompostela.com/#/pages/529/aviso-legal/',
  privacy:
    'https://my.camaracompostela.com/#/pages/530/política-de-privacidad/',
  cookies: 'https://my.camaracompostela.com/#/pages/531/política-de-cookies/',
};
